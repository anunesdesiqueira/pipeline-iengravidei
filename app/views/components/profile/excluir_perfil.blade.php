<h2>excluir perfil</h2>
{{ Form::open(array('id' => 'excluir-perfil')) }}
    <ul class="excluir-perfil">
        <li>Todas as informações do ser perfil serão apagadas.</li>
        <li>Você tem certeza que deseja excluir esse perfil?</li>
        <li>
            <label><input type="radio" name="opt" value="0" checked="checked" /> Quero manter meu perfil</label>
            <label><input type="radio" name="opt" value="1" /> Quero Excluir</label>
        </li>
        <li><input class="bt_perfil_excluir" type="submit" value="confirmar" disabled /></li>
    </ul>
{{ Form::close() }}