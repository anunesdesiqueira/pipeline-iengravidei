@extends('layouts.scaffold')

@section('main')
<div class="container">

<div class="row-fluid">

<div class="span3 sidebar denuncie">

        <h4>Tweets</h4>
        <label>Periodo dos Tweets:</label>

        <div class="data">
            <input type="text" /> à <input type="text" />
        </div>

        <p>Refinar por:</p>

        <h4>Status</h4>

        <select>
            <option>Selecione</option>
            <option>Ativa</option>
            <option>Desativada</option>
        </select>

        <button type="submit" class="btn">Pesquisar</button>

</div>

<div class="span9 conteudo denuncie">
    <h4>Resultado da Pesquisa</h4>

    <p>Status: Ativa</p>
    @if ($tweets->count())
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Autor</th>
                <th>Tweet</th>
                <th>Ações</th>

            </tr>
        </thead>
        <tbody>
        @foreach ($tweets as $tweet)
        <tr>
            <td>{{{ $tweet->author }}}</td>
            <td>{{{ $tweet->body }}}</td>
            <td>
                {{ link_to_route('admin.tweets.edit', 'Editar', array($tweet->id), array('class' => 'btn btn-info')) }}
                {{ Form::open(array('method' => 'DELETE', 'route' => array('admin.tweets.destroy', $tweet->id))) }}
                {{ Form::submit('Excluir', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @else
        Nenhum registro encontrado!!
    @endif
    <div class="pagination pagination-centered">
        <ul>
            <li><a href="#">Anterior</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">Próximo</a></li>
        </ul>
    </div>

</div>

</div>
</div>




@stop

