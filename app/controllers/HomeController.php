<?php
/**
 * Class HomeController
 */
class HomeController extends BaseController {

    public function __construct()
    {
		parent::__construct();	
    }

	public function index()
	{
        if(Auth::client()->check()) {
            return Redirect::route('user.my_profile');
        }

        return View::make('home.index');
	}

}