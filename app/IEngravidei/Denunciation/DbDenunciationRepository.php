<?php namespace IEngravidei\Denunciation;

use IEngravidei\Repositories\DenunciationRepositoryInterface,
    IEngravidei\Entities\Denunciation;

class DbDenunciationRepository implements DenunciationRepositoryInterface  {

    public function __construct()
    {
        $this->denunciation = new Denunciation;
    }

    public function find($id)
    {
        return 'find';
    }

    public function all()
    {
        return 'all';
    }

    public function create($input)
    {

        $validator = $this->denunciation->validate($input);

        if( $validator->passes())
        {
            $this->denunciation->author = $input['author'];
            $this->denunciation->type = $input['type'];
            $this->denunciation->email = $input['email'];
            $this->denunciation->s3_object = $input['s3_object'];
            $this->denunciation->comments = $input['comments'];
            $this->denunciation->posted = $input['posted'];

            return $this->denunciation->save();
        }

        return $validator;

    }

    public function destroy($id)
    {
        return 'destroy';
    }

}
