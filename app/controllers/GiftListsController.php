<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\PollRepositoryInterface as Poll;
use IEngravidei\Repositories\GiftRepositoryInterface as Gift;

class GiftListsController extends BaseController
{
	public function __construct(Profile $profile, Poll $poll, Gift $gift)
	{
        parent::__construct();
        $this->profile = $profile;
        $this->poll = $poll;
        $this->gift = $gift;
	}

    public function index()
    {
        $lists = $this->gift->getProfileList(array('id' => $this->user->id));

        if(Request::ajax()) {
            $page =  View::make('components.lista_presentes.index', compact('lists'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('lista_presentes.index', compact('lists'));
        }
    }

    public function createList()
    {
        if(Session::has('iengravidei.pcart')) {
            $cart = Session::get('iengravidei.pcart');

            if(count($cart) > 0) {
                $cartGifts = $this->gift->listProducts($cart);
                $cartGifts = $this->createPathImagesProducts($cartGifts);
                $cartGifts->total = $this->addDecimal($cartGifts);

                View::share(compact('cartGifts'));
            }
        }
        $page = View::make('lista_presentes.create')->render();
        return Response::json(array('page' => $page));
    }

    public function createListEvent()
    {
        if(Session::has('iengravidei.pcart')) {
            $cart = Session::get('iengravidei.pcart');

            if(count($cart) > 0) {
                $cartGifts = $this->gift->listProducts($cart);
                $cartGifts = $this->createPathImagesProducts($cartGifts);
                $cartGifts->total = $this->addDecimal($cartGifts);
                View::share(compact('cartGifts'));
            }
        }
        return View::make('lista_presentes.create_event_list')->render();
    }

    public function createListProducts()
    {
        $faixa = Input::get('faixa', null);
        $gifts = $this->gift->listProducts(null, null, $faixa, 9);
        $gifts = $this->createPathImagesProducts($gifts);
        $gifts->faixa = array('faixa' => $faixa);

        $page = View::make('components.lista_presentes.produtos', compact('gifts'))->render();
        return Response::json(array('page' => $page));
    }

    public function createCart()
    {
        if(Request::isMethod('post')) {
            $data = Input::all();

            $validation = Validator::make($data, array(
                'prod' => 'required'
            ), array(
                'prod.required' => 'Precisa enviar pelo menos um produto.',
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                if(!Session::has('iengravidei.pcart'))
                    Session::put('iengravidei.pcart', '');

                Session::push('iengravidei.pcart', $data['prod']);
            }
        }

        $cart = Session::get('iengravidei.pcart');

        $cartGifts = $this->gift->listProducts($cart);
        $cartGifts = $this->createPathImagesProducts($cartGifts);
        $cartGifts->total = $this->addDecimal($cartGifts);

        $page = View::make('components.lista_presentes.carrinho', compact('cartGifts'))->render();
        return Response::json(array('page' => $page));
    }

    public function confirm()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'terms' => 'required'
            ), array(
                'terms.required' => 'Aceitar os termos é obrigatório para criar sua lista de presentes.'
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $cart = Session::get('iengravidei.pcart');

                if(count($cart) > 0) {
                    $page = View::make('components.lista_presentes.confirm')->render();
                    return Response::json(array('page' => $page));
                } else {
                    return Response::json(array('error' => 1));
                }
            }
        }
    }

    public function confirmItens()
    {
        $cart = Session::get('iengravidei.pcart');

        if(count($cart) > 0) {
            $cartGifts = $this->gift->listProducts($cart, null, null, 9);
            $cartGifts = $this->createPathImagesProducts($cartGifts);
            $cartGifts->cartTotal = $this->addDecimal($cartGifts);

            $page = View::make('components.lista_presentes.confirm_produtos', compact('cartGifts'))->render();
            return Response::json(array('page' => $page));
        } else {
            return Response::json(array('error' => 1));
        }
    }

    public function saveList()
    {
        $data = Input::all();

        if($data)
        {
            $validation = Validator::make($data, array(
                'name' => 'required'
            ), array(
                'name.required' => 'A lista precisa de um nome!'
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $data['id'] = $this->user->id;
                $data['products'] = Session::get('iengravidei.pcart');

                if(count($data['products']) > 0) {
                    $createList = $this->gift->createList($data);

                    if($createList) {
                        Session::forget('iengravidei.pcart');
                        return Response::json(array('status' => 'true', 'list' => $createList->hash));
                    } else {
                        return Response::json(array('error' => 1));
                    }
                } else {
                    return Response::json(array('error' => 1));
                }
            }
        }
    }

    public function removeItemCart()
    {
        $data = Input::all();

        $validation = Validator::make($data, array(
            'prod' => 'required',
            'action' => 'required'
        ), array(
            'prod.required' => 'Precisa enviar pelo menos um produto.',
            'action.required' => 'Action não pode estar vazia.'
        ));

        if ($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $cart = Session::get('iengravidei.pcart');

            foreach($cart as $k => $v) {
                if($v == $data['prod']) {
                    unset($cart[$k]);
                }
            }

            Session::set('iengravidei.pcart', $cart);

            if(count($cart) > 0) {
                $cartGifts = $this->gift->listProducts($cart);
                $cartGifts = $this->createPathImagesProducts($cartGifts);
                $cartGifts->total = $this->addDecimal($cartGifts);
                View::share(compact('cartGifts'));
            }

            switch($data['action']) {
                case 'cart';
                $page = View::make('components.lista_presentes.carrinho')->render();
                break;

                case 'confirm';
                $page = View::make('components.lista_presentes.confirm_produtos')->render();
                break;
            }

            return Response::json(array('page' => $page));
        }
    }

    public function editList()
    {
        $data = Input::all();

        if($data)
        {
            $validation = Validator::make($data, array(
                'listascriadas' => 'required'
            ), array(
                'listascriadas.required' => 'Escolha uma das listas já criadas para editar.',
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $list = $this->gift->findById($data['listascriadas']);
                $listGifts = $this->gift->show(array('id' => $this->user->id, 'hash' => $data['listascriadas']));

                if(Session::has('iengravidei.editpcart'))
                    Session::put('iengravidei.editpcart', '');

                foreach($listGifts as $gift) {
                    Session::push('iengravidei.editpcart', $gift->id);
                }

                $page = View::make('lista_presentes.edit', compact('list'))->render();
                return Response::json(array('page' => $page));
            }
        }
    }

    public function editListEvent()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'listascriadas' => 'required'
            ), array(
                'listascriadas.required' => 'Escolha uma das listas já criadas para editar.',
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $list = $this->gift->findById($data['listascriadas']);
                $listGifts = $this->gift->show(array('id' => $this->user->id, 'hash' => $data['listascriadas']));

                if(Session::has('iengravidei.editpcart'))
                    Session::put('iengravidei.editpcart', '');

                foreach($listGifts as $gift) {
                    Session::push('iengravidei.editpcart', $gift->id);
                }
                return View::make('lista_presentes.edit_event_list', compact('list'))->render();
            }
        }
    }

    public function saveEditList()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'hash' => 'required'
            ), array(
                'hash.required' => 'Esse campo não pode estar vazio.'
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $list = $this->gift->findById($data['hash']);
                $data['id_giftslist'] = $list->id;
                $data['products'] = Session::get('iengravidei.editpcart');

                if(count($data['products']) > 0) {
                    if($this->gift->updateList($data)) {
                        return Response::json(array('status' => 'true'));
                    } else {
                        return Response::json(array('error' => 1));
                    }
                } else {
                    return Response::json(array('error' => 2));
                }
            }
        }
    }

    public function editListEscolhidos()
    {
        $cart = Session::get('iengravidei.editpcart');

        if(Request::isMethod('post')) {
            $data = Input::all();

            $validation = Validator::make($data, array(
                'prod' => 'required',
            ), array(
                'prod.required' => 'Precisa enviar pelo menos um produto.'
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                foreach($cart as $k => $v) {
                    if($v == $data['prod']) {
                        unset($cart[$k]);
                    }
                }
                Session::set('iengravidei.editpcart', $cart);
            }
        }

        if(count($cart) > 0) {
            $gifts = $this->gift->listProducts($cart, null, null, 9);
            $gifts = $this->createPathImagesProducts($gifts);
            $gifts->cartTotal = $this->addDecimal($gifts);
            View::share(compact('gifts'));
        }

        $page = View::make('components.lista_presentes.itens_escolhidos')->render();
        return Response::json(array('page' => $page));
    }

    public function editListNaoEscolhidos()
    {
        if(Request::isMethod('post')) {
            $data = Input::all();

            $validation = Validator::make($data, array(
                'prod' => 'required',
            ), array(
                'prod.required' => 'Precisa enviar pelo menos um produto.'
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                Session::push('iengravidei.editpcart', $data['prod']);
            }
        }

        $cart = Session::get('iengravidei.editpcart');
        $gifts = $this->gift->listProducts(null, $cart, null, 9);
        $gifts = $this->createPathImagesProducts($gifts);
        $gifts->cartTotal = $this->addDecimal($gifts);

        $page = View::make('components.lista_presentes.itens_nao_escolhidos', compact('gifts'))->render();
        return Response::json(array('page' => $page));
    }

    /**
     * lista de presentes actions publics
     * @param $publicId
     */
    public function publicList($publicId)
    {
        $public = $this->profile->find($publicId);

        if(Request::isMethod('get')) {

            if(Session::has('iengravidei.buypcart')) {
                Session::put('iengravidei.buypcart', '');
            }

            $profile = $this->createPathImagesProfile(array($public));
            $profile = $profile[0];
            $profile->month = $this->calcGestation($public->pregnancy_weeks);
            $profile->countdown = (40 - $public->pregnancy_weeks);
            $layout = $profile->layout;

            $convite = Input::get('param_data');

            if(isset($this->user->id)) {
                if($this->user->id === $profile->id) {
                    $permissions['profile_user'] = true;
                }

                if($this->profile->getFriend($this->user->id, $profile->id)) {
                    $permissions['amiga'] = true;
                }

                if(!isset($permissions['profile_user']) && !isset($permissions['amiga'])) {
                    $permissions['solicitacao'] = $this->profile->getRequireFriend($this->user->id, $profile->id);
                }
            }

            if(!is_null($convite)) {
                if($this->visitantes->getVisitante($profile->id, $convite)) {
                    $permissions['convidado'] = true;
                }
            }

            if(!isset($permissions['solicitacao']) && !isset($permissions['profile_user']) && !isset($permissions['amiga']) && !isset($permissions['convidado'])) {
                $permissions['visitante'] = true;
            }

            $poll = $this->poll->getPoll($profile->id);
            if(!empty($poll->question)) {
                if($this->poll->findLogAnswer($poll->question->id, getenv("REMOTE_ADDR"), isset($this->user->id)? $this->user->id : '')) {
                    $permissions['voted'] = true;
                }
            }

            View::share(compact('profile','layout', 'poll', 'permissions'));
        }

        $faixa = Input::get('faixa');

        $data_param = Input::get('data_param');
        $selectlist = Input::get('selectlist');

        $lists = $this->gift->getProfileList(array('id' => $public->id));

        if(!is_null($selectlist) && !empty($selectlist)) {
            $selected = $selectlist;
        } else {
            if(!is_null($data_param))
                $selected = $data_param;
            else
                $selected = key($lists);
        }

        $getList = $this->gift->findByIdAndIdprofile($selected, $public->id);

        if(is_null($getList))
            return Response::view('errors.404', array(), 404);

        $gifts = $this->gift->selectlistProductsId($getList->id, $faixa);
        $gifts = $this->createPathImagesProducts($gifts);
        $gifts->faixa = array('data_param' => $selected, 'faixa' => $faixa);

        if(Request::ajax()) {
            $page =  View::make('components.lista_presentes.produtos', compact('gifts'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('lista_presentes.public', compact('lists', 'selected', 'gifts','publicId'));
        }
    }

    public function publicCreateCart()
    {
        if(Request::isMethod('post')) {
            $data = Input::all();

            $validation = Validator::make($data, array(
                'prod' => 'required'
            ), array(
                'prod.required' => 'Precisa enviar pelo menos um produto.',
            ));

            if ($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                Session::push('iengravidei.buypcart', $data['prod']);
            }
        }

        $cart = Session::get('iengravidei.buypcart');
        $cartGifts = $this->gift->listProducts($cart);
        $cartGifts = $this->createPathImagesProducts($cartGifts);
        $cartGifts->total = $this->addDecimal($cartGifts);

        $page = View::make('components.lista_presentes.carrinho', compact('cartGifts'))->render();
        return Response::json(array('page' => $page));
    }

    public function publicRemoveItenCart()
    {
        $data = Input::all();

        $validation = Validator::make($data, array(
            'prod' => 'required',
        ), array(
            'prod.required' => 'Precisa enviar pelo menos um produto.',
        ));

        if ($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $cart = Session::get('iengravidei.buypcart');

            foreach($cart as $k => $v) {
                if($v == $data['prod']) {
                    unset($cart[$k]);
                }
            }

            Session::set('iengravidei.buypcart', $cart);

            if(count($cart) > 0) {
                $cartGifts = $this->gift->listProducts($cart);
                $cartGifts = $this->createPathImagesProducts($cartGifts);
                $cartGifts->total = $this->addDecimal($cartGifts);
                View::share(compact('cartGifts'));
            }

            $page = View::make('components.lista_presentes.carrinho')->render();
            return Response::json(array('page' => $page));
        }
    }
}