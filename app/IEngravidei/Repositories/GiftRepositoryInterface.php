<?php

namespace IEngravidei\Repositories;

interface GiftRepositoryInterface
{
    public function findById($hash);
    public function findByIdAndIdprofile($hash, $id_profile);
    public function getProfileList($data);
    public function show($data);
    public function listProducts($products = null, $notProducts = null, $filter = null, $paginate = null);
    public function selectlistProductsId($id, $filter = null, $paginate = null);
    public function createList($data);
    public function updateList($data);
}