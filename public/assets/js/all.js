$(function() {
    // Init
    getUrlAnchor();
    loadSelectMask();
    startCarousel();

    // Tooltip
    $('.tooltip').tooltipster({
        theme : 'tooltipster-iegranvidei'
    });
});

$(window).load(function () {
    // Centraliza as imagens dentro das molduras do album de fotos
    $('.album-fotos ul.lista-album-fotos li .thumb a img, .album-fotos ul.lista-album li .thumb a img').each(function() {
        var imgWidth = $(this).width();
        $(this).css({
            'left' : '50%',
            width: imgWidth,
            'margin-left': -imgWidth / 2
        });
    });
});

function loadSelectMask() {
    $('select').each(function(){
        var valorCombo = $(this).find('option:selected').html();
        $(this).prev('.mascara').find('.optName').text(valorCombo);
    });
}

// Esqueci a senha
$(document.body).on('click', '.login a.esqueci-senha', function(e) {
    e.preventDefault();
    $('.login input[name="password"]').parents('li').hide();
    $('.login input[name="remember"]').parents('label').hide();
    $(this).parents('form').attr('action', baseUri + '/recuperar-senha');
});

// Faz o mouseover do menu
$(document.body).on('mouseenter mouseleave', 'header li', function() {
    $(this).toggleClass('show');
});

// Ativa a rolagem do veja mais
$(document.body).on('click', '.veja-mais', function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $("section.content").offset().top - 8
    }, 1000);
});

// Ativa a rolagem do cadastre-se
$(document.body).on('click', 'a.clique-cadastre-se', function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $("header nav").offset().top - 20
    }, 500);
});

// Select mask
$(document.body).on('change', 'select', function(e) {
    e.preventDefault();
    $(this).prev('.mascara').find('.optName').text($(this).find('option:selected').html());
});

// Função para resetar o campo input[type='file']
window.reset = function(e) {
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}

// Ajax call method
function CallMethod(url, type, params, beforeSendCallBack, completeCallback, successCallback) {
    $.ajax({
        url: url,
        type: type,
        data: params,
        dataType    : 'json',
        cache       : false,
        beforeSend: function()
        {
            if (typeof beforeSendCallBack === 'function')
            {
                beforeSendCallBack();
            }
        },
        complete: function()
        {
            if (typeof completeCallback === 'function')
            {
                completeCallback();
            }
        },
        success: function (data)
        {
            if (typeof successCallback === 'function')
            {
                successCallback(data);
            }
        },
        error: function (xhr, textStatus, errorThrown)
        {
            console.log(xhr);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

//Get Url Anchor
function getUrlAnchor() {
    var urlPathArray = window.location.pathname.split('/');
    var name;

    if("" !== urlPathArray[1]) {
        if("meu-perfil" === urlPathArray[1] || publicId === urlPathArray[1]) {
            if(urlPathArray[2] !== undefined)
                name = urlPathArray[2];
            else
                return false;
        } else {
            name = urlPathArray[1];
        }

        var selector = 'h2[name="'+ name +'"]';

        if ($(selector).length ) {
            scrollToAnchor(selector);
        }
    }
}

//Hash location
function scrollToAnchor(selector) {
    var aTag = $(selector);
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

// Lightbox de Galeria de Imagens (Álbum)
$('ul.lista-album-fotos, ul.lista-babyline-fotos').magnificPopup({
    delegate: '.thumb a',
    type: 'image',
    tLoading: 'Carregando a imagem #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
        tError: '<a href="%url%">A imagem #%curr%</a> não pode ser carregada.'
    }
});

// Start carousel babyline
function startCarousel() {
    $('#carouselBabyLine').tinycarousel({
        display: 1,
        duration: 500,
        infinite: false
    });
}

// Populate Cities
$(document.body).on('change', '#states', function(e) {
    e.preventDefault();
    populateCities($(this).val());
});

function populateCities(value) {
    CallMethod(baseUri + '/ajax/cidades/' + value,'GET','',loddingShowCities,loddingHideCities,successCities);
}

function loddingShowCities() {
    var selectStates = $('#states');
    var selectCities = $('#cities');

    selectStates.prop('disabled', true);
    selectCities.prop('disabled', true);

    selectCities.find('option').remove().end().append('<option value="">Selecione</option>').val('');
    selectCities.prev('.mascara').find('.optName').text(selectCities.find('option:selected').html());
}

function loddingHideCities() {
    var selectStates = $('#states');
    var selectCities = $('#cities');

    selectStates.prop('disabled', false);
    selectCities.prop('disabled', false);
}

function successCities(data) {
    if(data) {
        var select = $('#cities');
        $.each(data, function(id, name) {
            select.append($('<option>').text(name).attr('value', id));
        });
        select.prev('.mascara').find('.optName').text(select.find('option:selected').html());
    }
}

// Add methods validate
$.validator.addMethod("brazilianDate", function(value, element) {
    return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
}, "Entre com a data no formato dd/mm/yyyy.");

$.validator.addMethod("notEqualToPublicId", function(value, element){

    var result = false;

    $.ajax({
        type:"GET",
        async: false,
        url: '/ajax/verificar-usuario/' + value,
        success: function(data) {
            result = data.available;
        }
    });

    return result;

}, "*Endereço não disponivel");

// Faz o "Leia mais" dos murais
function readMore() {
    $('.tabList .description p, .mural-nao-logado .description p').expander({
        slicePoint:       210,  // default is 100
        expandPrefix:     '... ', // default is '... '
        expandText:       'Leia mais', // default is 'read more'
        collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
        userCollapseText: 'Fechar'  // default is 'read less'
    });
}
