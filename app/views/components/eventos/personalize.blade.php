<p>personalize e envie seu convite</p>
<ul class="convite-templates">
@foreach ($templates as $k => $template)
    <li @if(count($templates) -1 == $k) class="last" @endif >
        <label>
            <div class="thumb"><img src="{{asset('assets/email/'.$template->preview)}}" /></div>
            <div class="radioHolder">
                <div class="mascara"></div>
                {{ Form::radio('template_id', $template->hash, 'false', array('data-href' => $template->preview)) }}
            </div>
            <span>Escolher este template</span>
        </label>
    </li>
@endforeach
</ul>
<div class="visuTplError">
    <label for="template_id" class="error"></label>
</div>
<a href="#" class="visualizar-convite"><span class="ico-visualizar">&nbsp;</span>Visualizar</a>