<?php
class SpecialMessagesTableSeeder extends Seeder
{
    public function run()
    {
        $specialMessages = array(

            array(
                'from_id'    => 1,
                'to_id'      => 6,
                'body'       => 'Exige muito de ti e espera pouco dos outros. Assim, evitarás muitos aborrecimentos.',
                'deleted'    => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ),

            array(
                'from_id'    => 2,
                'to_id'      => 6,
                'body'       => 'A paixão aumenta em função dos obstáculos que se lhe opõe.',
                'deleted'    => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ),

            array(
                'from_id'    => 3,
                'to_id'      => 6,
                'body'       => 'A matemática do tempo é simples. Você tem menos do que pensa e precisa mais do que acha.',
                'deleted'    => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ),

            array(
                'from_id'    => 4,
                'to_id'      => 6,
                'body'       => 'As grandes idéias surgem da observação dos pequenos detalhes.',
                'deleted'    => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ),

            array(
                'from_id'    => 5,
                'to_id'      => 6,
                'body'       => 'Construí amigos, enfrentei derrotas, venci obstáculos, bati na porta da vida e disse-lhe: Não tenho medo de vivê-la.',
                'deleted'    => 0,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ),


        );



        DB::table('special_messages')->insert($specialMessages);
    }
}