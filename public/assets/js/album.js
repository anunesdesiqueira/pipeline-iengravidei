var uploader = new plupload.Uploader({
    runtimes: 'html5,flash,silverlight,html4',
    browse_button: 'pickfiles',
    url: baseUri + '/meus-albuns/upload-photo',
    container: upAlbum,
    max_file_size: '2mb',
    max_file_count: totalAlbumPhotos,
    filters: {mime_types: [
        {title: "Image files", extensions: "jpg,gif,png"}
    ]},
    flash_swf_url: baseUri + '/assets/js/plupload.flash.swf',
    silverlight_xap_url: baseUri + '/assets/js/plupload.silverlight.xap',
    preinit: callBack
});

uploader.bind('PostInit', function() {
    document.getElementById('filelist').innerHTML = '';
});

uploader.bind('FilesAdded', function(up, files) {
    var count = up.files.length;
    var error = false;
    var msgAddFoto = '';

    plupload.each(files, function(file) {
        if(count > uploader.settings.max_file_count) {
            error = true;
            if(uploader.settings.max_file_count > 0) {
                msgAddFoto = "*Você só pode adicionar "+ uploader.settings.max_file_count +" fotos neste álbum";
            } else {
                msgAddFoto = "*Você já atingiu o limite de fotos neste álbum";
            }

            setTimeout(function(){ up.removeFile(file); }, 50);
        } else {
            document.getElementById('filelist').innerHTML += '<div id="' + file.id + '" class="lista">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
        }
    });

    if(error) {
        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                    '<ul class="modal-errors">' +
                    '<li>'+ msgAddFoto +'</li>' +
                    '</ul>' +
                    '</div>',
                type: 'inline'
            }]
        });
    }
});

uploader.bind('UploadProgress', function (up, file) {
    document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = "<span>" + file.percent + "%</span>";
});

uploader.bind('UploadComplete', function (up, files) {
    up.splice();
});

uploader.bind('Error', function(up, err) {
    filelist.append("<div class='error'>Atenção: Você deve subir imagens com a extensão JPG</div>");
    setTimeout(function(){
        divError.fadeOut();
    }, 3000);
});

function callBack(Uploader) {
    Uploader.bind('FileUploaded', function(up, file) {
        document.getElementById(file.id).getElementsByTagName('span').innerHTML = file.percent + "100%";

        if((Uploader.total.uploaded + 1) == Uploader.files.length) {
            var images = new Array();
            var i = 0;

            $.each(Uploader.files, function(index, f) {
                images[i] = f.name;
                i++
            });

            CallMethod(uriAlbum,'POST',{ 'name' : $('input.nomeAlbum').val(), 'slug' : $('#name-album').attr('rel'), 'images' : images, 'limit' : limitAlbum },loadingCreateAlbum,loadingHideAlbum,successAlbum);
        }
    });
}

$('.album-fotos a.criar').on('click', function(e) {
    e.preventDefault();

    removeImagesLista();
    $('.cntUpFotos').slideDown();
});

$('.cntUpFotos a.close').on('click', function(e){
    e.preventDefault();

    removeImagesLista();
    $('.cntUpFotos').slideUp('slow');
});

$('#uploadfiles').on('click', function(e) {
    e.preventDefault();

    if('' === $('input.nomeAlbum').val()) {
        $('input.nomeAlbum').addClass('error');
        $('label span.error').show();
    } else if(0 == $('#filelist').children().length) {
        $('div.semFotos').show();
        setTimeout(function(){$('div.semFotos').fadeOut();}, 3000);
    } else {
        uploader.start();
    }
});

$('input.nomeAlbum').keypress(function() {
    $('input.nomeAlbum').removeClass('error');
    $('label span.error').hide();
});

$(document.body).confirmOn({
    questionText: 'Quer mesmo apagar esta foto?',
    textYes: 'Sim',
    textNo: 'Não'
}, 'click', '.lista-album-fotos .delete-photo', function(e, confirmed) {
    if(confirmed) {
        var photo = $(e.toElement).attr('rel');
        var slug = $('.album-fotos .delete-album').attr('rel');
        CallMethod(baseUri + '/meus-albuns/deletar-photo/' + slug,'POST',{ photo_image : photo },'','',successAlbum);
        $(e.toElement).parent().remove();
    }
});

$('.album-fotos .delete-album').confirmOn({
    questionText: 'Quer mesmo apagar este álbum?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', function(e, confirmed) {
    if(confirmed) {
        var slug = $(e.toElement).attr('rel');
        CallMethod(baseUri + '/meus-albuns/ajax/del-album','POST',{ 'slug' : slug },'','',successDelAlbum);
    }
});

$('.album-fotos .alterar').on('click', function(e) {
    var name = $('#name-album');

    if(name.val() != name.prop("defaultValue")) {
        CallMethod(baseUri + '/meus-albuns/name-album/'+name.attr('rel'),'POST',{ 'name' : name.val() },'','',successAlterNameAlbum);
    }
});

function removeImagesLista() {
    var count = uploader.files.length;
    var itens = $.map(uploader.files, function (item) { return item.id; });

    for(i=0; i < count; i++) {
        uploader.removeFile(uploader.getFile(itens[i]));
    }
    $('#filelist').find('.lista').remove();
    $('input.nomeAlbum').val('');
}

function loadingCreateAlbum() {
    $('#uploadfiles').css({color : '#ded5ab'}).prop({ disabled : true });
    $('#pickfiles').css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadingHideAlbum() {
    $('#uploadfiles').css({color : '#867a42'}).prop({ disabled : true });
    $('#pickfiles').css({color : '#867a42'}).prop({ disabled : true });

    setTimeout(function(){
        $('.cntUpFotos').slideUp('slow');
        removeImagesLista();
    }, 300);
}

function successAlbum(data) {
    if(data.page != undefined) {
        var container = $('.lista-album');
        container.empty().html(data.page);
    }

    if(data.pageTodos != undefined) {
        var container = $('.album-fotos .bt-confira-todos');
        container.empty().html(data.pageTodos);
    }

    if(data.pageCriar != undefined) {
        var container = $('.album-criar');
        container.empty().html(data.pageCriar);
    }

    if(data.totalAlbumPhotos != undefined) {
        uploader.settings.max_file_count = data.totalAlbumPhotos;
    }
}

function successDelAlbum(data) {
    if(data.status) {
        window.location.href = baseUri + '/meu-perfil/album-de-fotos';
    }
}

function successAlterNameAlbum(data) {
    if(data.status) {
        $('.msg').css('display', 'block');
    }
    setMsgTimeout();
}

function setMsgTimeout() {
    var msg = $('.msg');

    msg.fadeOut( 3000, function() {
        msg.css('display', 'none');
    });
}