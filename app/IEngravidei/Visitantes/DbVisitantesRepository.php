<?php

namespace IEngravidei\Visitantes;

use IEngravidei\Repositories\VisitantesRepositoryInterface;
use IEngravidei\Entities\Visitantes as Visitantes;

class DbVisitantesRepository implements VisitantesRepositoryInterface
{
    public function add($data)
    {
        return Visitantes::create(array(
            'id_profile' => $data['id'],
            'nome' => $data['visitantes_nome'],
            'sobrenome' => $data['visitantes_sobrenome'],
            'email' => $data['visitantes_email'],
            'hash' => sha1(microtime(). uniqid(rand(), true) . $data['visitantes_email']),
            'read' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function count($id_profile)
    {
        return Visitantes::where('id_profile', '=', $id_profile)
            ->where('read', '=', 0)->whereNull('deleted_at')->count();
    }

    public function read($id_profile)
    {
        return Visitantes::where('id_profile', '=', $id_profile)->where('read', '=', 0)
            ->update(array('read' => 1, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function getVisitante($id_profile, $hash)
    {
        return Visitantes::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->whereNull('deleted_at')->first();
    }

    public function solicitacoes($id_profile, $paginate = 6)
    {
        return Visitantes::where('id_profile', '=', $id_profile)->whereNull('deleted_at')
            ->orderBy('created_at', 'DESC')->paginate($paginate);
    }

    public function deletar($id_profile, $hash)
    {
        return Visitantes::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->update(array('updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => date('Y-m-d H:i:s')));
    }

    public function update($id_profile, $hash)
    {
        return Visitantes::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->update(array('updated_at' => date('Y-m-d H:i:s')));
    }
}