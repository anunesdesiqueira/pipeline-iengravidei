@extends('layouts.master')

@section('content')
    <div class="holder">
        @include('components.cabecalho.public')
        <h2 name="lista-de-presentes" class="lista-presentes-title" title="Lista de Presentes"><span>Lista de Presentes</span></h2>
        <div class="lista-presentes">
            <div class="col-left">
                @include('components.lista_presentes.filtros_public')
                <div class="container-presentes-cart">
                    @include('components.lista_presentes.carrinho')
                </div>
            </div>
            <div class="col-center">
                <h3>Escolha seu Presente</h3>
                <div class="container-presentes">
                    @include('components.lista_presentes.produtos')
                </div>
                {{ Form::open(array('route' => array('user.public.giftsCartPayment', $publicId), 'id' => 'form-efetuar-pagamento')) }}
                    <label>Envie uma mensagem:{{ Form::textarea('mensagem', '', array('class' => 'textarea-mensagem')) }}</label>
                    {{ Form::submit('Efetuar Pagamento', array('class' => 'bt-salvar')) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/public.js') }}
@stop