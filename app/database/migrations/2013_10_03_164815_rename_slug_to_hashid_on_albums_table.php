<?php

use Illuminate\Database\Migrations\Migration;

class RenameSlugToHashidOnAlbumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('albums', function($table)
        {
            $table->renameColumn('slug', 'hash_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('albums', function($table)
        {
            $table->renameColumn('hash', 'slug');
        });
	}

}