<?php namespace Bg7\Eventme;

use Illuminate\Support\ServiceProvider;

class EventmeServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('bg7/eventme');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
        $this->app['eventme']  = $this->app->share(function($app)
        {
           return new Eventme;
        });

        $this->app->booting(function()
        {
           $loader = \Illuminate\Foundation\AliasLoader::getInstance();
           $loader->alias('Eventme', 'Bg7\Eventme\Facades\Eventme');
        });

        $this->app->bind(
          'Bg7\Eventme\Repository\EventRepository',
          'Bg7\Eventme\Repository\EloquentEventRepository'

        );
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('eventme');
	}

}