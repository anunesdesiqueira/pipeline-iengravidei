@if($user->plan_roles['album_limited'] > $countAlbums)
    <a href="#" class="criar">criar álbum</a>
    <div class="clearfix"></div>
    <div id="album-container" class="cntUpFotos">
        <h3>Criar novo álbum</h3>
        <a class="close">X</a>
        <label>
            Nome do Álbum: <input type="text" class="nomeAlbum" />
            <span class="error">Não esqueça o nome do álbum :)</span>
        </label>
        <div class="semFotos">Você deve adicionar pelo menos uma foto para criar um novo álbum.</div>
        <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
        <div class="clearfix"></div>
        <div class="teste"></div>
        <a id="pickfiles" href="javascript:;">Selecione as fotos</a>
        <a id="uploadfiles" href="javascript:;">Criar o álbum</a>
    </div>
    <div class="clearfix"></div>
@else
    <h4>você já criou seus {{$user->plan_roles['album_limited']}} álbuns</h4>
    <div class="clearfix"></div>
@endif