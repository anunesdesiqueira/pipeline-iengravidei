<!DOCTYPE html>
<html>
<head>
    <title>iEngravidei | Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->

    <link href="<?php echo asset('admin/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo asset('admin/css/default.css'); ?>" rel="stylesheet" media="screen">
</head>
<body>

<!-- HEADER -->
<header>
    <div class="container">
        <h1><span>iEngravidei</span></h1>
        <a href="<?php echo URL::route('admin.logout'); ?>" class="btn btn-small logout">Logout</a>
        <div class="login-info">Você está logado como: <strong>ADMINISTRADOR</strong></div>
    </div>
</header>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="dropdown active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Planos <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Pesquisar</a></li>
                            <li><a href="#">Criar Plano</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dicas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Pesquisar</a></li>
                            <li><a href="#">Criar Dica</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuários <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Pesquisar</a></li>
                            <li><a href="#">Criar Usuário</a></li>
                        </ul>
                    </li>

                    <li class="active"><a href="#">Banners</a></li>
                    <li class=""><a href="#">Denúncias</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produtos <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Pesquisar</a></li>
                            <li><a href="#">Incluir Produto</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">CMS <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Pesquisar</a></li>
                            <li><a href="#">Cadastrar Conteúdo</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tweets(Exemplo) <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>{{ link_to_route('admin.tweets.index', 'Pesquisar') }}</li>
                            <li>{{ link_to_route('admin.tweets.create', 'Incluir Tweet') }}</li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- HEADER -->

<!-- Container -->
@yield('main')

<!-- Container -->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo asset('admin/js_old/bootstrap.min.js_old'); ?>"></script>

</body>
</html>