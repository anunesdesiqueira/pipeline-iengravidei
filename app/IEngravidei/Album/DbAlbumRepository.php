<?php
namespace IEngravidei\Album;

use IEngravidei\Entities\Album;
use IEngravidei\Entities\AlbumPhoto;
use IEngravidei\Repositories\AlbumRepositoryInterface;

class DbAlbumRepository implements AlbumRepositoryInterface
{
    public function getAlbum($slug)
    {
        return Album::where('slug', '=', $slug)->first();
    }

    public function countAlbuns($id_profile)
    {
        return Album::where('id_profile', '=', $id_profile)->count();
    }

    public function getAlbums($id_profile, $limit = 0)
    {
        $albums = Album::leftJoin('album_photo', 'album_photo.id_album', '=', 'album.id');
        $albums->where('album.id_profile', '=', $id_profile);
        $albums->orderBy('album.created_at', 'DESC');

        if($limit) {
            $albums->limit($limit);
        }

        $albums->groupBy('album.id');

        return $albums->get();
    }

    public function getPhotos($slug)
    {
        return AlbumPhoto::join('album', 'album.id', '=', 'album_photo.id_album')
            ->where('album.slug', '=', $slug)->orderBy('album_photo.created_at', 'DESC')
            ->select('album.id_profile', 'album_photo.id', 'album_photo.picture','album.slug')->get();
    }

    public function createAlbum($input)
    {
        return Album::create(array(
            'id_profile' => $input['id'],
            'name' => $input['name'],
            'slug' => sha1(microtime(). uniqid(rand(), true) . $input['name']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function createAlbumPhoto($picture, $id_album)
    {
        return AlbumPhoto::create(array(
            'id_album' => $id_album,
            'picture' => $picture,
            'created_at' => date('Y-m-d H:i:s')
        ));
    }

    public function deleteAlbum($input)
    {
        $getAlbum = Album::where('slug', '=', $input['slug'])->where('id_profile', '=', $input['id_profile'])->first();

        if($getAlbum) {
            if(AlbumPhoto::where('id_album', '=', $getAlbum->id)->forceDelete()) {
                if(Album::where('id', '=', $getAlbum->id)->forceDelete()) {
                    return true;
                }
            } else if(Album::where('id', '=', $getAlbum->id)->forceDelete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function deletePhoto($input)
    {
        $getAlbum =  Album::where('slug', '=', $input['slug'])
            ->where('id_profile', '=', $input['id_profile'])->first();

        return AlbumPhoto::where('id', '=', $input['photo_image'])
            ->where('id_album', '=', $getAlbum->id)->forceDelete();
    }

    public function updateName($input)
    {
        return Album::where('slug', '=', $input['slug'])->where('id_profile', '=', $input['id_profile'])
            ->update(array('name' => $input['name'], 'created_at' => date('Y-m-d H:i:s')));
    }

}