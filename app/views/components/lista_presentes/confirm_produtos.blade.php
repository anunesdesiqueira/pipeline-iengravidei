@if(isset($cartGifts))
    <ul class="itens-medium">
        @foreach($cartGifts as $gift)
        <li>
            <div class="thumb"><img src="{{ $gift->picture['medium'] }}" alt="{{ $gift->name }}"/></div>
            <p>{{ $gift->description }}</p>
            <span>R$ {{ str_replace('.', ',', $gift->price) }}</span>
            {{-- Form::button('remover',  array('rel' => $gift->id, 'class' => 'bt-itens-medium')) --}}
        </li>
        @endforeach
    </ul>
    <span class="total"><b>Total:</b> R$ {{ str_replace('.', ',', $cartGifts->cartTotal) }}</span>
    {{ $cartGifts->links() }}
@else
    <p class="list-presentes-msg">A lista não pode ficar vazia!</p>
@endif