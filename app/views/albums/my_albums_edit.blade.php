@extends('layouts.master')

@section('content')
<div class="holder album-fotos">
    @include('components.cabecalho.header_active')
    @include('components.album_photos_edit')
</div>
@stop

@section('customjs')
    <script type="text/javascript">
        @if(isset($upAlbum))
            var upAlbum = document.getElementById('{{ $upAlbum }}');
        @else
        var upAlbum = 0;
        @endif

        var totalAlbumPhotos = {{{ isset($countAlbumFotosAdd) && isset($user->plan_roles['album_photo_limited']) ? $user->plan_roles['album_photo_limited'] - $countAlbumFotosAdd : isset($user->plan_roles['album_photo_limited']) ? $user->plan_roles['album_photo_limited'] : 0 }}};
        var limitAlbum = "{{{ isset($limitAlbum) ? $limitAlbum : '' }}}";
        var totalAlbum = {{{ isset($user->plan_roles['album_limited']) ? $user->plan_roles['album_limited'] : 0}}};
        var uriAlbum = "{{{ isset($uriAlbum) ? route($uriAlbum) : '' }}}";
    </script>
    {{ HTML::script('assets/js/album.js') }}

    @if(isset($countAlbums) && $countAlbums < $user->plan_roles['album_limited'] || isset($countAlbumFotosAdd))
        <script type="text/javascript">uploader.init();</script>
    @endif
@stop