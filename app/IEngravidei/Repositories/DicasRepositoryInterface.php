<?php

namespace IEngravidei\Repositories;

interface DicasRepositoryInterface
{
    public function getAll();
    public function find($str);
}