@if(count($history) > 0)
    <div class="historico">
        <h2>Histórico de Enquetes</h2>
        <ul>
            @foreach ($history as $poll)
            <li>
                <ul>
                    <li><span>Pergunta:</span> {{ $poll->question }}</li>
                    <li><span>Mais votada:</span> {{ $poll->answer }}</li>
                    <li><span>Data de início:</span> {{ $poll->created_at }}</li>
                    <li><span>Data finalização:</span> {{ $poll->updated_at }}</li>
                </ul>
            </li>
            @endforeach
        </ul>
    </div>
@endif