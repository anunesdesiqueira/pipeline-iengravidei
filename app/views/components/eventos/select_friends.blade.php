<div class="col1">
    <p>Escolha seus amigos</p>
    <span class="info">Escolha na lista as amigas do iengravidei que deseja convidar:</span>
    <h4>lista de amigos / Visitantes</h4>
    <div class="clearfix"></div>
    {{ Form::select('friends[]', $friends, array(''), array('id' => 'list-friends', 'multiple')) }}
    <span class="info mail-margin">Adicione outras pessoas:</span>
    <label>E-mails externos: {{ Form::text('emails', Input::old('emails'), array('id' => 'emails')) }}</label>
    <a href="#" class="addMail">&nbsp;</a>
    <div class="clearfix"></div>
    <div class="emailsInvalidos">
        <h5>Os e-mails abaixo não são válidos, por favor confira e adicione novamente:</h5>
        <p></p>
    </div>
</div>
<div class="col2">
    <p>Lista de convidados selecionados</p>
    <span class="info">Confira a lista de convidados</span>
</div>
<div class="clearfix"></div>

<div class="visuTplError"><label for="list-friends" class="error"></label></div>

<button type="submit" class="enviar-convite">
    <span class="ico-enviar">&nbsp;</span>Enviar convite
</button>