<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<!-- Só aparece na HOME -->
<section class="home">
	<div class="holder">		
		<h1><span>iengravidei</span></h1>
		<div class="box-cadastro">
			<ul>
				<li class="facebook">Crie um novo login ou <a href="#">Facebook</a></li>
				<li><label>Seu Nome: <input type="text" style="width:370px;" /></label></li>
				<li><label>Nome do Bebê<span>*</span>: <input type="text" style="width:330px;" /></label></li>
				<li><label>E-mail: <input type="text" style="width:399px;" /></label></li>
				<li>
					<label>Senha: <input type="text" style="width:114px;" /></label>
					<label style="padding-left:8px;">Confirmar Senha: <input type="text" style="width:114px;" /></label>
				</li>
				<li><p>* CAMPO OBRIGATÓRIO</p></li>
				<li><input type="submit" value="CADASTRAR" /></li>
			</ul>
		</div>
		<a href="#" class="veja-mais">Veja mais</a>
	</div>
</section>
<!-- End: Só aparece na HOME -->

<section class="content">
	<div class="holder home">
		<h2>Bem-vinda à 1 Rede Social para Gestantes do mundo!</h2>
		<p class="intro">Somos uma comunidade apaixonada pela maternidade. Pelo indescritível e sublime estado em que ficamos quando<br/>estamos esperando nosso filho ou nossa filha! Compartilhe, emocione-se, converse, aprenda, ensine e viva<br/>intensamente esse momento! Estamos juntos com você para a contagem regressiva.</p>
		<div class="divisor top"></div>
		<div class="col1">			
			<h3>Em destaque<span>O que oferecemos</span></h3>			
			<div class="box">
				<div class="thumb"></div>
				<div class="description">
					<h4>Lista de Presentes</h4>
					<p>Crie sua lista de presentes e receba tudo sem sair do site! Facilite a sua vida e de seus amigos!</p>
				</div>
			</div>			
			<div class="box presentes">
				<div class="thumb"></div>
				<div class="description">
					<h4>Baby Line</h4>
					<p>Uma linha para postar uma foto por semana e ver o desenvolvimento da barriga. Muito legal!</p>
				</div>
			</div>			
			<h3 class="lista">Outros serviços<span>Lista completa</span></h3>
			<ul class="lista-completa">
				<li class="ico1"></li>
				<li class="ico2"></li>
				<li class="ico3"></li>
				<li class="ico4"></li>
				<li class="ico5"></li>
				<li class="ico6"></li>
				<li class="ico7"></li>
				<li class="ico8"></li>
				<li class="ico9"></li>
				<li class="ico10"></li>
			</ul>
		</div>		
		<div class="col2">
			<img id="zoom" src="img/zoom-small.jpg" data-zoom-image="img/zoom-big.jpg" />
			<p>Veja como ficará sua página</p>
		</div>			
		<div class="divisor botton"></div>
		<div class="cadastre-tambem">
			<h5>Cadastre-se você também.</h5>
			<p>Na 1ª Rede Social para Gestantes do mundo!</p>
			<a href="#">CLIQUE AQUI</a>
		</div>
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>