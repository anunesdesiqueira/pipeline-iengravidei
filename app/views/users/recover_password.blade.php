<html>
	<head></head>
	<body>
		@if (Session::has('error'))
		    {{ trans(Session::get('reason')) }}
		@elseif (Session::has('success'))
		    Uma mensagem foi enviada para o seu e-mail.
		@endif
		<h1>Recuperar senha</h1>
		<form name="password" action="{{ URL::to('recuperar-senha') }}" method="post">
			Email: <input name="email" type="text"><br/>
			<input type="submit" value="Enviar">
		</form>
	</body>
</html>