<?php namespace IEngravidei\Repositories;

interface AlbumRepositoryInterface
{
    public function getAlbums($id_profile, $limit = 0);
    public function countAlbuns($id_profile);
    public function getPhotos($slug);
    public function createAlbum($input);
    public function createAlbumPhoto($picture, $id_album);
    public function deletePhoto($input);
    public function deleteAlbum($input);
    public function updateName($input);
}