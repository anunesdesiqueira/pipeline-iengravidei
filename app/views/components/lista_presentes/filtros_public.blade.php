<h3>faixa de preço</h3>
{{ Form::open( array('id' => 'form-lista-presentes-filter-public') ) }}
    <span class="selecione">Selecione uma lista de presente:</span>
    <div class="comboHolder">
        <div class="mascara"><div class="optName"></div></div>
        {{ Form::select('selectlist', $lists, $selected) }}
    </div>
    <span class="refine">Refine sua busca pelo valor:</span>
    <ul class="filtros">
        <li><label>{{ Form::checkbox('faixa[]', '9.99,50.99') }} R$ 10,00 a R$ 50,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '49.99,100.99') }} R$ 50,00 a R$ 100,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '99.99,250.99') }} R$ 100,00 a R$ 250,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '249.99,499.99') }} R$ 250,00 a R$ 500,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '499.99') }} Acima de R$ 500,00</label></li>
    </ul>
    {{ Form::submit('Listar', array('class' => 'bt-salvar bt-lista-presentes-filter')) }}
{{ Form::close() }}
<div class="observacao">
    <p><strong>Obs.:</strong> selecione quantas faixas de preço quiser.</p>
</div>