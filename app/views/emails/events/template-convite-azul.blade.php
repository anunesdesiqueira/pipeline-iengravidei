<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>IEngravidei - Convite</title>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table id="Table_01" width="600" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <img src="{{ asset('assets/img/email/convites/images/convite-azul_01.jpg') }}" width="600" height="185" style="display:block">
                    </td>
                </tr>
                <tr>
                    <td width="204" height="221">
                        <img src="{{ asset('assets/img/email/convites/images/convite-azul_02.jpg') }}" width="204" height="221" style="display:block">
                    </td>
                    <td valign="top"><table width="396" border="0" align="left" cellpadding="2" cellspacing="2">
                            <tr valign="top">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight: bold; line-height: 22px; color:#6d6e71">Você foi convidado(a) pela {{ $data['mom_name'] }} para um evento especial:</p>
                                </td>
                            </tr>
                            <tr>
                                <td><h1 style="font-family:Verdana, Geneva, sans-serif; font-size:20px; font-weight:bold; line-height:24px; color:#178db3">{{ $data['name'] }}</h1></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top"><table width="396" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <img src="{{ asset('assets/img/email/convites/images/convite-azul_04.jpg') }}" width="600" height="84" style="display:block" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <h2 style="font-family:Verdana, Geneva, sans-serif; font-size:20px; font-weight: bold; text-indent:10px; color:#6d6e71">{{ $data['date'] }}</h2>
                                    <p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; text-indent:10px; color:#6d6e71"><b style="font-weight: bold;">Endereço:</b> {{ $data['address'] }}</p>
                                    <p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; text-indent:10px; color:#6d6e71"><b style="font-weight: bold;">Horário:</b> {{ $data['time'] }}</p>
                                    @if (!empty($data['comments']))
                                        <p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; margin-left:10px; color:#6d6e71"><b style="font-weight: bold;">Demais informações:</b> {{ $data['comments'] }}</p>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <img src="{{ asset('assets/img/email/convites/images/convite-azul_05.jpg') }}" width="600" height="131" style="display:block">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><table width="600" border="0" cellspacing="2" cellpadding="2">
                            <tr>
                                <td align="center">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="{{ route('user.public.gifts', array('publicId' => $data['public_id'])) }}?data_param={{ $data['list_hash'] }}">
                                        <img src="{{ asset('assets/img/email/convites/images/convite-azul-bt-lista-presentes.jpg') }}" width="235" height="48" style="display:block"/>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; font-weight: bold; color:#6d6e71">Ou  se preferir acesse o perfil da mamãe abaixo.</p>
                                    <p><a href="{{ route('user.public', array('publicId' => $data['public_id'])) }}" style="font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight: bold; text-decoration:none; color:#178db3">http://www.iengravidei.com.br/{{ $data['public_id'] }}</a></p>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <img src="{{ asset('assets/img/email/convites/images/convite-azul_06.jpg') }}" width="600" height="195" style="display:block" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" align="center" style="background-color:#178db3;">
                        <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; font-weight: bold; line-height: 2px; color:#ffffff">Para mais informações acesse:
                            <a style="color:#ffffff; text-decoration: none;" href="http://www.iengravidei.com.br">http://www.iengravidei.com.br</a>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>