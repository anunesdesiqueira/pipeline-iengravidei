<?php
class InstitutionalController extends BaseController {

    public function sobre_nos()
    {
        return View::make('institutional.sobre_nos');
    }

    public function termos_de_uso()
    {
        return View::make('institutional.termos_de_uso');
    }

    public function planos()
    {
        return View::make('institutional.planos');
    }
}