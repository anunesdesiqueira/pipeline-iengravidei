<?php namespace IEngravidei\Libraries;

/**
 * Class Upload
 * @package IEngravidei\Libraries
 */
class Upload {

    /** 
     * @param $file
     * @param array $validMimes
     * @param string $destinationPath
     * @return array|bool
     */
    public function toApp($file, $validMimes = array('image/jpeg', 'image/png'), $destinationPath = 'uploads/', $maxSize )
    {
        if( !$file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile)
        {
            return false;
        }

        if( !in_array($file->getMimeType(), $validMimes))
        {
            return false;
        }

        if( $maxSize < $file->getSize())
        {
            return false;
        }

        $extension = $file->getClientOriginalExtension();
        $name = $this->randomName($extension);
        $upload = $file->move($destinationPath, $name);

        if( !$upload instanceof \Symfony\Component\HttpFoundation\File\File)
        {
            return false;
        }

        return array(
            'fullPath'  => public_path() . '/'. $destinationPath. '/' . $name,
            'name'      => $name
        );


    }
    //public-read
    /**
     * @param $bucket
     * @param $name
     * @param $fullPath
     * @param string $acl
     * @return bool
     */
    public function toS3($bucket, $name, $fullPath, $acl = 'private')
    {
        $s3 = AWS::get('s3');

        try
        {   //$s3->upload('iengravidei.denuncias', $name, fopen($fullPath, 'r'), 'public-read');
            $s3->upload($bucket, $name, fopen($fullPath, 'r'), $acl);

        }
        catch(S3Exception $e)
        {
            error_log("Erro ao subir arquivo da denuncia no S3");
            $s3 = false;
        }

        return $s3 instanceof \Aws\S3\S3Client;

    }

    private function randomName($extension)
    {
        return sha1(microtime(). uniqid(rand(), true)). '.' .$extension;
    }

}