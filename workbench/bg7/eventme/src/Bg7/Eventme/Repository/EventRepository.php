<?php namespace Bg7\Eventme\Repository;
/**
 * Created by JetBrains PhpStorm.
 * User: threehundred
 * Date: 9/2/13
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */


interface EventRepository {

    public function all();
    public function find($id);
    public function create($input);

}