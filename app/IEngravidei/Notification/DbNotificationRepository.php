<?php namespace IEngravidei\Notification;

use IEngravidei\Repositories\NotificationRepositoryInterface;
use IEngravidei\Entities\Notification;

class DbNotificationRepository implements NotificationRepositoryInterface {

    public function __construct()
    {
        $this->notification = new Notification;
    }

    public function push($profileId, $message)
    {
       
       $input = array(
           'profile_id'     =>  $profileId,
           'message'        =>  $message
       );

       $validate = $this->notification->validate($input);

       if( $validate->fails())
       {
           return false;
       }

       $this->notification->profile_id = $profileId;
       $this->notification->message = $message;
       $this->notification->read = 0;

       return $this->notification->save();

    }

    public function pull($profileId)
    {
        $notifications = $this->notification->whereRaw('profile_id = ? AND YEAR(created_at) = ?', array($profileId, date('Y')))->orderBy('created_at', 'desc')->get();
        $months = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');

        $cleanNotifications = array();
        foreach($notifications as $notification)
        {
            $month = $months[$notification->created_at->month - 1];
            
            $cleanNotifications[$month][] = array(
                'day_time'  =>  $notification->created_at->day . '-' . $notification->created_at->hour . ':' . $notification->created_at->minute,
                'message'   =>  $notification->message

            );

        }

        return $cleanNotifications;
    }

    public function numberOfNotifications($profileId)
    {

        return $this->notification->whereRaw('profile_id = ? and read = 0', array($profileId))->count();

    }
}