@extends('layouts.master')

@section('content')

	<div class="holder">

		<div class="primeiro-acesso-cadastro">
			<h2 name="complete-seu-perfil">Complete agora seu perfil e mostre para todos os amigos!</h2>

            @if($errors->any())
                <ul>
                    {{ implode('', $errors->all('<li>:message</li>'))}}
                </ul>
            @endif

            {{ Form::open(array('route' => 'register.complete', 'files' => true, 'id' => 'complete-profile')) }}
				<h3>Escolha uma foto para seu perfil</h3>
				<div class="uploadCad">
					<div class="mascara"><div class="fileName"></div></div>
					{{ Form::file('profile_picture') }}
				</div>
				<h3>Complete suas informações</h3>
			
				<ul class="cadastro">
					<li>
						<label class="pai">Nome do Papai: {{ Form::text('father_name', Input::old('father_name')) }}</label>
					</li>
                    <li>
                        <label class="bebe">Nome do Bebê:&nbsp; {{ Form::text('baby_name', Input::old('baby_name')) }}</label>
                    </li>
					<li>
						<label class="nasc">Sua Data de Nascimento:* {{ Form::text('birth', Input::old('birth'), array('placeholder' => '')) }}</label>
                        <label for="birth" class="error"></label>
                    </li>
					<li>
                        <label for="nameGroup" class="error error-select"></label>
                        <label>
                            <span>País:*</span>
                            <div class="comboHolder">
                                <div class="mascara"><div class="optName"></div></div>
                                {{ Form::select('country_id',  array('' => 'Selecione') + $countries) }}
                            </div>
                        </label>
                        <label>
                            <span>Estado:*</span>
                            <div class="comboHolder">
                                <div class="mascara"><div class="optName"></div></div>
                                {{ Form::select('state_id', array('' => 'Selecione') + $states, '', array('id' => 'states')) }}
                            </div>
                        </label>
                        <label>
                            <span>Cidade:*</span>
                            <div class="comboHolder">
                                <div class="mascara"><div class="optName"></div></div>
                                {{ Form::select('city_id', array('' => 'Selecione'), '', array('readonly' => 'readonly', 'id' => 'cities')) }}
                            </div>
                        </label>
                    </li>
					<li>
                        <label>
                            <span>Quantas semanas de gravidez?*</span>
                            <div class="comboHolder">
                                <div class="mascara"><div class="optName"></div></div>
                                {{ Form::select('pregnancy_weeks', array('' => 'Selecione') + $pregnancy_weeks) }}
                            </div>
                        </label>
                        <label for="pregnancy_weeks" class="error error-select"></label>
					</li>
					<li>
						<label class="url">
							Crie um endereço exclusivo para seu perfil:* www.iengravidei.com.br/
							{{ Form::text('public_id',Input::old('public_id'),  array('class' => 'check-username')) }}
							<span class="exemplos">
								ex:<br/>
								www.iengravidei.com.br/anapaula22<br/>
								www.iengravidei.com.br/anapaulasilva
							</span>
                            <div class="visuTplError"><label for="public_id" class="error"></label></div>
						</label>					
					</li>
					<li><span class="obs">* Campo obrigatório</span></li>
					<li class="botao"><button class="btn">Salvar alterações</button></li>
				</ul>
			{{ Form::close() }}
        </div>
	</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/register.js') }}
@stop