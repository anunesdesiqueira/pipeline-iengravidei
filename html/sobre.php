<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<!-- Só aparece na HOME -->
<section class="home">
	<div class="holder">		
		<h1><span>iengravidei</span></h1>
		<div class="box-cadastro">
			<ul>
				<li class="facebook">Crie um novo login ou <a href="#">Facebook</a></li>
				<li><label>Seu Nome: <input type="text" style="width:370px;" /></label></li>
				<li><label>Nome do Bebê<span>*</span>: <input type="text" style="width:330px;" /></label></li>
				<li><label>E-mail: <input type="text" style="width:399px;" /></label></li>
				<li>
					<label>Senha: <input type="text" style="width:114px;" /></label>
					<label style="padding-left:8px;">Confirmar Senha: <input type="text" style="width:114px;" /></label>
				</li>
				<li><p>* CAMPO OBRIGATÓRIO</p></li>
				<li><input type="submit" value="CADASTRAR" /></li>
			</ul>
		</div>
		<a href="#" class="veja-mais">Veja mais</a>
	</div>
</section>
<!-- End: Só aparece na HOME -->

<section class="content">
	<div class="holder">
		
		<div class="sobre-nos">
			
			<h2>Sobre Nós</h2>
			
			<div class="varal"></div>			
			<div class="foto-a"></div>
			<div class="foto-b"></div>
			
			<div class="texto-a">
				<h3>Na 1ª Rede Social para Gestantes do mundo!</h3>
				<p>Somos uma comunidade apaixonada pela maternidade. Pelo indescritível e sublime estado em que ficamos quando estamos esperando nosso filho ou nossa filha!</p>
			</div>
			
			<div class="texto-b">
				<h3>Estamos juntos com você para a contagem regressiva.</h3>
				<p>E é para viver esta ao máximo emoção - postar fotos, fazer novas amigas, compartilhar com seus amigos diretamente no Facebook – que abrimos este espaço para você!</p>
			</div>
			
			<div class="texto-c">
				<h3>E mais:</h3>
				<ul>
					<li>Compartilhe, emocione-se, converse, aprenda, ensine e viva intensamente esse momento!</li>
					<li>E tudo vira recordação para quando seu bebê nascer.</li>
					<li>Estamos juntos com você para a contagem regressiva.</li>
				</ul>
			</div>
			
			<div class="lista-servicos">
				<h3 class="lista"><span>Lista completa</span></h3>
				<ul class="lista-completa">
					<li class="ico1"></li>
					<li class="ico2"></li>
					<li class="ico3"></li>
					<li class="ico4"></li>
					<li class="ico5"></li>
					<li class="ico6"></li>
					<li class="ico7"></li>
					<li class="ico8"></li>
					<li class="ico9"></li>
					<li class="ico10"></li>
				</ul>
			</div>
			
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>