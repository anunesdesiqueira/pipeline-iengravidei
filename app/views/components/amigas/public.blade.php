<div class="adicione-amiga">
    <div class="holderAdd">
        <div class="verMais">Quer ver mais?</div>
        <div class="floatLeft">
            <h6>Adicione como amiga <span>&nbsp;</span></h6>
            <p>Solicite abaixo liberação para acessar esse perfil completo.</p>
            @if(isset($permissions['solicitacao']) && !$permissions['solicitacao'])
                <a href="{{ $publicId }}" class="solicitar-amizade">Clique aqui</a>
            @endif
        </div>
    </div>
    @if(isset($permissions['solicitacao']) && $permissions['solicitacao'])
        <div class="botShadow">
            <p>a solicitação foi enviada para a usuária, aguarde a liberação de acesso.</p>
        </div>
    @endif
</div>