<p>Que tipo de evento você deseja criar?</p>
<ul class="lista-tipo-evento">
    <li class="cha">
        <label>
            <div class="thumb"></div>
            {{ Form::radio('type', 'cha-de-bebe') }} Chá de Bebê
        </label>
    </li>
    <li class="nascimento">
        <label>
            <div class="thumb"></div>
            {{ Form::radio('type', 'nascimento') }} Nascimento
        </label>
    </li>
    <li class="fralda">
        <label>
            <div class="thumb"></div>
            {{ Form::radio('type', 'cha-de-fralda') }} Chá de Fralda
    </li>
    <li class="outros">
        <label>
            <div class="thumb"></div>
            {{ Form::radio('type', 'outros') }} Outros
        </label>
    </li>
</ul>
<div class="visuTplError">
    <label for="type" class="error"></label>
</div>