<h3>faixa de preço</h3>
{{ Form::open( array('id' => 'form-lista-presentes-filter') ) }}
    <span>Filtre pela faixa de preço:</span>
    <ul class="filtros">
        <li><label>{{ Form::checkbox('faixa[]', '9.99,50.99') }} R$ 10,00 a R$ 50,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '49.99,100.99') }} R$ 50,00 a R$ 100,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '99.99,250.99') }} R$ 100,00 a R$ 250,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '249.99,499.99') }} R$ 250,00 a R$ 500,00</label></li>
        <li><label>{{ Form::checkbox('faixa[]', '499.99') }} Acima de R$ 500,00</label></li>
    </ul>
    {{ Form::submit('Filtrar', array('class' => 'bt-salvar bt-lista-presentes-filter')) }}
{{ Form::close() }}