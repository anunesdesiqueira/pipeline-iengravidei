<?php

use Illuminate\Database\Migrations\Migration;

class AlterPhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('photos', function($table)
        {
            $table->boolean('actived')->default(0);
            $table->boolean('deleted')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('photos', function($table)
        {
            $table->dropColumn('actived');
            $table->dropColumn('deleted');
        });
	}

}