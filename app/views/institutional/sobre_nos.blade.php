@extends('layouts.master')

@if(!Auth::client()->check())
    @section('cadastro')
        @include('components.register')
    @stop
@endif

@section('content')
<div class="holder">
    @if(Auth::client()->check())
        @include('components.cabecalho.header_active')
    @endif
    <div class="sobre-nos">
        <h2 name="sobre-nos">Sobre Nós</h2>
        <div class="varal"></div>
        <div class="foto-a"></div>
        <div class="foto-b"></div>
        <div class="texto-a">
            <h3>Na 1ª Rede Social para Gestantes do mundo!</h3>
            <p>Somos uma comunidade apaixonada pela maternidade. Pelo indescritível e sublime estado em que ficamos quando estamos esperando nosso filho ou nossa filha!</p>
        </div>
        <div class="texto-b">
            <h3>Estamos juntos com você para a contagem regressiva.</h3>
            <p>E é para viver esta ao máximo emoção - postar fotos, fazer novas amigas, compartilhar com seus amigos diretamente no Facebook – que abrimos este espaço para você!</p>
        </div>
        <div class="texto-c">
            <h3>E mais:</h3>
            <ul>
                <li>Compartilhe, emocione-se, converse, aprenda, ensine e viva intensamente esse momento!</li>
                <li>E tudo vira recordação para quando seu bebê nascer.</li>
                <li>Estamos juntos com você para a contagem regressiva.</li>
            </ul>
        </div>
        <div class="lista-servicos">
            <h3 class="lista"><span>Lista completa</span></h3>
            <ul class="lista-completa">
                <li class="ico1"><a class="tooltip" href="{{ route('services') }}" title="Meu Mural">Meu Mural</a></li>
                <li class="ico2"><a class="tooltip" href="{{ route('services') }}" title="Lista de Presentes">Lista de Presentes</a></li>
                <li class="ico3"><a class="tooltip" href="{{ route('services') }}" title="Clube de Descontos">Clube de Descontos</a></li>
                <li class="ico4"><a class="tooltip" href="{{ route('services') }}" title="Baby Line">Baby Line</a></li>
                <li class="ico5"><a class="tooltip" href="{{ route('services') }}" title="Dicas Profissionais">Dicas Profissionais</a></li>
                <li class="ico6"><a class="tooltip" href="{{ route('services') }}" title="Enquete">Enquete</a></li>
                <li class="ico7"><a class="tooltip" href="{{ route('services') }}" title="Recados Especiais">Recados Especiais</a></li>
                <li class="ico8"><a class="tooltip" href="{{ route('services') }}" title="Personalização de Convites">Personalização de Convites</a></li>
                <li class="ico9"><a class="tooltip" href="{{ route('services') }}" title="Álbum de Fotos">Álbum de Fotos</a></li>
                <li class="ico10"><a class="tooltip" href="{{ route('services') }}" title="Criador de Eventos">Criador de Eventos</a></li>
            </ul>
        </div>
    </div>
</div>
@stop