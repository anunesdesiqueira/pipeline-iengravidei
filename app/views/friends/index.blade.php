@extends('layouts.master')

@section('content')
<div class="holder">
    @include('components.cabecalho.header_active')
    <div class="amigas">
        <div class="lista-solitacoes"></div>
        <div class="minhas-amigas">
            <h2 name="amigas">Lista de amigas</h2>
            <div class="lista"></div>
        </div>
    </div>
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/enquete-logged.js') }}
    {{ HTML::script('assets/js/friends.js') }}
@stop
