<?php

namespace IEngravidei\Entities;

class ProfileGift extends \Eloquent
{
    protected $table = 'profiles_giftslist';
    protected $guarded = array();
    public $timestamps = false;
}