@foreach($albums as $album)
    <li>
        <a href="{{ $album->slug }}" class="editar">Editar álbum</a>
        <div class="thumb">
            @if (!empty($album->picture['main']) && !is_null($album->picture['main']) )
                <a href="{{ $album->slug }}"><img src="{{ $album->picture['main'] }}" /></a>
            @else
                <a href="{{ $album->slug }}"><img src="{{ asset('assets/img/album-exemplo.jpg') }}" /></a>
            @endif
        </div>
        <a href="{{ $album->slug }}" class="nome">{{ $album->name }}</a>
    </li>
@endforeach

@for ($i = count($albums); $i < 3; $i++)
    <li class="example">
        <div class="thumb">
            <img src="{{ asset('assets/img/album-exemplo.jpg') }}" />
        </div>
        <span class="nome">NOME ÁLBUM</span>
    </li>
@endfor