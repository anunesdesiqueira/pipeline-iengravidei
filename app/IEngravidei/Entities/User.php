<?php namespace IEngravidei\Entities;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Eloquent implements UserInterface, RemindableInterface {

    protected $table = 'profiles';
	protected $hidden = array('password');

	public function getAuthIdentifier()
	{
	    return $this->getKey();
	}

	public function getAuthPassword()
	{
	    return $this->password;
	}

    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function validate($input)
    {
        $rules = array(
            'name' => 'required|max:200',
            'email' => 'required|email|max:200|unique:profiles',
            'password'  => 'required|between:5,16|confirmed',
        );

        $messages = array(
            'name.required'  => 'Digite Seu Nome',
            'email.required'    => 'Digite o E-mail',
            'email.email'    => 'E-mail inválido',
            'email.max'     => 'E-mail deve ter no máximo 200 caracteres',
            'email.unique'  => 'E-mail já cadastrado',
            'password.required' => 'Digite sua senha',
            'password.between' => 'Senha deve possuir no minimo 5 e no máximo 16 caracteres',
            'password.confirmed'    => 'Senhas não conferem'
        );

        return \Validator::make($input, $rules, $messages);
    }

    public function friends()
    {
        return $this->belongsToMany('IEngravidei\Entities\User', 'profile_profile', 'who_asked', 'who_accepted');
    }

    /*public function messages()
    {
        return $this->hasMany('IEngravidei\Entities\Message', 'to_id');
    }*/

    public function facebook_profile()
    {
        return $this->belongsTo('IEngravidei\Entities\FacebookProfile', 'facebook_uid');
    }
}