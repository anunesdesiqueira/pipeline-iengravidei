<?php

use IEngravidei\Repositories\BabylineRepositoryInterface as Babyline;

class BabyLineController extends BaseController {

	public function __construct(Babyline $babyline)
	{
        parent::__construct();
        $this->babyline = $babyline;
	}

    public function add()
    {
        $data = Input::all();
        $data['id'] = $this->user->id;

        $validation = \Validator::make($data, array(
            'picture' => 'required|image|max:200|mimes:jpg,jpeg',
            'babyline_weeks' => 'required|integer'
        ), array(
            'picture.required' => 'A imagem é obrigatória',
            'picture.image' => 'O arquivo enviado não é uma imagem',
            'picture.max' => 'O arquivo deve ser menor que :max kbs',
            'picture.mimes' => 'A imagem de perfil deve ser do tipo JPG',
            'babyline_weeks.required' => 'A semana é obrigatória',
            'babyline_weeks.numeric' => 'Apenas números'
        ));

        if($validation->fails())
        {
            return $validation->messages()->toJson();
        }
        else if($this->babyline->findWeek($data['id'], $data['babyline_weeks']))
        {
            return Response::json(array('error' => 'Já existe uma imagem para '.$data['babyline_weeks'].'ª Semana.'));
        }
        else
        {
            $data['picture'] = $this->uploadBabylinePicture($data['picture'], $data['id']);
            $this->babyline->createImage($data);

            $babyline = $this->babyline->getList($data['id']);
            $babyline = $this->createPathImagesBabyline($babyline);

            return Response::json(array('status' => 'true', 'list' => $babyline->toArray()));
        }
    }

    public function deletar()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'babyline_image' => 'required|integer'
        ), array(
            'babyline_image.required' => 'A imagem é obrigatória',
            'babyline_image.numeric' => 'Apenas números'
        ));

        if($validation->fails())
        {
            return $validation->messages()->toJson();
        }
        else
        {
            $data['id'] = $this->user->id;
            $delete = $this->babyline->deleteImage($data);

            if ($delete)
                return Response::json(array('status' => 'true'));
            else
                return Response::json(array('status' => 'false'));
        }
    }
}