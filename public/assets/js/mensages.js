$(window).load(function () {
    scrollDown();
});

$(document.body).on('submit', 'form#form-mensages-send', function(e) {
    e.preventDefault();
    var params = $(this).serialize();
    CallMethod(baseUri + '/meu-perfil/mensagens/enviar','POST',params,loadingFormShowMensagens,loadindFormHideMensagens,successMensagens);
});

$(document.body).confirmOn({
    questionText: 'Deseja mesmo excluir?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.mensagem section .description a.excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/mensagen/excluir','POST',{ public : $('form#form-mensages-send').find(':input[name="public"]').val(), send : $(e.toElement).attr('data-type'), msg : $(e.toElement).attr('rel') },'','',successMensagens);
    }
});

$(document.body).confirmOn({
    questionText: 'Deseja mesmo excluir?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.mensagem .lista-mensagens a.excluir-todas', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/mensagen/excluir/all','POST',{ public : $('form#form-mensages-send').find(':input[name="public"]').val() },'','',successMensagens);
    }
});

$(document.body).on('submit', 'form#form-mensages-search', function(e) {
    e.preventDefault();

    var params = $(this).serialize();
    CallMethod(baseUri + '/meu-perfil/search/mensagens/amigas','GET',params,'','',successSearch);
});

function loadingFormShowMensagens() {
    var submit = document.getElementsByClassName('bt_enviar_mensagem');
    $(submit).val('Enviando...');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHideMensagens() {
    var submit = document.getElementsByClassName('bt_enviar_mensagem');
    $(submit).val('Enviar');
    $(submit).css({color : '#867a42'}).prop({ disabled : false });
}

function successMensagens(data) {
    if('true' === data.status) {
        $( 'form#form-mensages-send' ).each(function(){
            this.reset();
        });

        CallMethod(baseUri + '/meu-perfil/search/mensagens/amigas','GET',{pesquisar : $('form#form-mensages-search').find(':input[name="pesquisar"]').val() },'','',successSearch);

        var container = $('.mensagem .resultado-mensagens');
        container.empty().html(data.page);
        scrollDown();
    }
}

function successSearch(data) {
    if(data.page !== undefined) {
        var container = $('.mensagem .resultado-busca');
        container.empty().html(data.page);
    }
}

function scrollDown() {
    $('.mensagem .resultado-mensagens').animate({
        scrollTop: $('.mensagem .resultado-mensagens ul').height()
    }, 10);
}