@if(isset($albums) && count($albums) > 0)
    <div class="album-fotos perfil">
        <h2 title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>
        <div class="clearfix"></div>
        <ul class="lista-album">
            @foreach ($albums as $album)
            <li>
                <div class="thumb"><a href="{{ $album->slug }}"><img src="{{ $album->picture['main'] }}" /></a></div>
                <a href="{{ $album->slug }}" class="nome">{{ $album->name }}</a>
            </li>
            @endforeach
        </ul>
        @if($countAlbums > 3)
            <a href="{{ route('show_my_album', array('publicId' => $publicId)) }}" class="verAlbuns">confira todos os albuns</a>
        @endif
    </div>
@endif