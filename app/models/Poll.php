<?php

class Poll extends Eloquent {

    protected $table = 'polls';
    protected $guarded = array('id', 'id');

    public static function existe($nome, $user_id)
    {
        $return = Poll::whereRaw('
            profile_id = ?
            and
            name = ?',
            array($user_id, $nome)
        )->get();

        return $return;
    }

    public static function criar($nome, $user_id)
    {
        $poll = Poll::create(array('profile_id' => $user_id, 'name' => $nome));

        return $poll->id;
    }

    public static function pollsActive($user_id)
    {
        $return = Poll::whereRaw('
            profile_id = ?
            and
            is_active = 1',
            array($user_id)
        )->get();

        return $return;
    }

    public static function closedPoll($user_id, $poll_id)
    {
        $return = Poll::whereRaw('profile_id = ? and id = ?', array($user_id, $poll_id))->update(array('is_active' => 2));

        return $return;
    }

    public static function returnClosedPoll($user_id)
    {
        $return = DB::table('polls as a')
            ->join('polls_questions as b', 'b.poll_id', '=', 'a.id')
            ->join('questions as c', 'c.id', '=', 'b.question_id')
            ->join('questions_alternatives as d', 'd.questions_id', '=', 'c.id')
            ->join('alternatives as e', 'e.id', '=', 'd.alternative_id')
            ->where('a.profile_id', '=', $user_id)
            ->where('a.is_active', '=', 2)
            ->groupBy('d.questions_id')
            ->get(array(DB::raw('count(e.id) as total'), 'a.created_at', 'a.updated_at', 'a.name'));

        return $return;
    }
}