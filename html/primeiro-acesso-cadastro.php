<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<section class="content">
	<div class="holder">
	
		<div class="primeiro-acesso-cadastro">
			
			<h2>Complete agora seu perfil e mostre para todos os amigos!</h2>
			<h3>Escolha uma foto para seu perfil</h3>
			<div class="uploadCad">
				<div class="mascara"><div class="fileName"></div></div>
				<input type="file" />
			</div>
			<h3>Complete suas informações</h3>
			<ul class="cadastro">
				<li>
					<label class="pai">Nome do Papai:* <input type="text" /></label>
				</li>
				<li>
					<label class="bebe">Nome do Bebê:*&nbsp; <input type="text" /></label>
				</li>
				<li>
					<label class="nasc">Sua Data de Nascimento: <input type="text" /></label>
				</li>
				<li>
					<label>País: <input type="text" /></label>
					<label class="mgnLeft">Estado: <input type="text" /></label>
					<label class="mgnLeft">Cidade: <input type="text" /></label>
				</li>
				<li>
					<label>Quantas semanas de gravidez? <input type="text" /></label>
				</li>
				<li>
					<label class="url">
						Crie um endereço exclusivo para seu perfil: www.iengravidei.com.br/ 
						<input type="text" />
						<span>
							ex:<br/>
							www.iengravidei.com.br/anapaula22<br/>
							www.iengravidei.com.br/anapaulasilva
						</span>
					</label>					
				</li>
				<li><span class="obs">* Campo não obrigatório</span></li>
				<li class="botao"><button class="btn">Salvar alterações</button></li>
			</ul>
			
			<div class="servicos">
				<h4>Agora uma das áreas mais importante do seu perfil:</h4>
				<a href="#" class="btn">Crie sua lista de presentes</a>
			</div>
			
			<div class="servicos">
				<h4>Aproveite todos os recursos do iengravidei:</h4>
				<ul class="servList">
					<li>
						<p>Voce já tem fotos de sua gestação?</p>
						<a href="#" class="btn">Crie sua baby line</a>
					</li>
					<li>
						<p>Vamos criar um álbum para ilustrar sua página?</p>
						<a href="#" class="btn">Criar álbum de fotos</a>
					</li>
					<li>
						<p>O que acha de fazer uma enquete? <span>Ex. Que nome coloco no meu bebê? Que maternidade escolher?</span></p>
						<a href="#" class="btn">Crie sua baby line</a>
					</li>					
				</ul>
			</div>
			
			<div class="servicos">
				<div class="botoes">
					<button class="btn">Publique agora</button>
				</div>
			</div>
		
			<div class="servicos">
				<h5>Perfil cadastrado com sucesso</h5>
				<p>Guarde seu endereço e envie para seus amigos: <strong>www.iengravidei.com.br/anapaula1982</strong></p>
				<h6>Que tal compartilhar seu perfil no Facebook?</h6>
				<a href="#" class="btnFacebook"><img src="img/bt-facebook.gif" border="0" /></a>
				<p>Avise também por e-mail</p>
				<ul class="listEmail">
					<li>
						<label>
							<div class="thumb hotmail"></div>
							<input type="checkbox" />
						</label>
					</li>
					<li>
						<label>
							<div class="thumb outlook"></div>
							<input type="checkbox" />
						</label>
					</li>
					<li>
						<label>
							<div class="thumb gmail"></div>
							<input type="checkbox" />
						</label>
					</li>
					<li>
						<label>
							<div class="thumb yahoo"></div>
							<input type="checkbox" />
						</label>
					</li>
					<li>
						<label>
							<div class="thumb outros"></div>
							<input type="checkbox" />
						</label>
					</li>
				</ul>
			</div>
			
			<div class="servicos">
				<div class="botoes">
					<button class="btn">Acesse já</button>
				</div>
			</div>
			
		</div>	
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>