<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\MessageRepositoryInterface as Message;

class MessagesController extends BaseController
{
    public function __construct(Profile $profile, Message $message)
    {
        parent::__construct();
        $this->profile = $profile;
        $this->message = $message;
    }

    public function show($publicId = null)
    {
        $friends = array();
        $history = array();
        $i = 0;

        $this->message->read($this->user->id);

        $getFriends = $this->profile->getAllFriends($this->user->id);

        if(count($getFriends) > 0) {
            foreach ($getFriends as $k => $friend) {
                if($friend['id'] !== $this->user->id) {
                    $msg = $this->message->history($friend['id'], $this->user->id);

                    $friend = $this->createPathImagesProfile(array($friend));
                    $friend = $this->createUrl($friend, 'messages');

                    $friends[$i]['id'] = $friend[0]['id'];
                    $friends[$i]['public_id'] = $friend[0]['public_id'];
                    $friends[$i]['profile_picture'] = $friend[0]->profile_picture;
                    $friends[$i]['name'] = $friend[0]->name;
                    $friends[$i]['slug'] = $friend[0]->slug;
                    $friends[$i]['msg'] = (count($msg) > 0) ? $this->truncate($msg[0]->body, 50) : null;

                    $i++;
                }
            }

            if(is_null($publicId)) {
                $publicId = $friends[0]['public_id'];
            }

            $profileFrom = $this->profile->find($publicId);

            $history = $this->message->history($profileFrom->id, $this->user->id);
            $history = $this->createPathImagesProfile($history);
            $history = $this->createUrl($history, 'messages');
            $history = array_reverse($history);
            $history = $this->senders($history, $this->user->id);

            View::share(compact('profileFrom', 'publicId'));
        }

        return View::make('messages.index', compact('friends', 'history'));
    }

    public function postSend()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'public' => 'required',
            'body' => 'required'
        ), array(
            'public.required' => 'Usuário não encontrado.',
            'body.required' => 'Conteúdo não pode estar vazio.'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $profileTo = $this->profile->find($data['public']);

            $input = array(
                'parent_id'   => 0,
                'from_id'     => $this->user->id,
                'to_id'       => $profileTo->id,
                'body'        => $data['body'],
                'type'        => 'private',
            );

            $result = $this->message->send($input);

            if($result) {
                if(isset($data['acao']) && 'lista' === $data['acao']) {
                    $history = $this->message->history($profileTo->id, $this->user->id);
                    $history = $this->createPathImagesProfile($history);
                    $history = $this->createUrl($history, 'messages');
                    $history = array_reverse($history);
                    $history = $this->senders($history, $this->user->id);

                    $page = View::make('components.mensagens.results', compact('history'))->render();
                    $res = array('status' => 'true', 'page' => $page);
                } else {
                    $res = array('status' => 'true');
                }
            } else {
                $res = array('status' => 'false');
            }
        }

        return Response::json($res);
    }

    public function delMsg()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'public' => 'required',
            'send' => 'required',
            'msg' => 'required',
        ), array(
            'public.required' => 'Usuário não encontrado.',
            'send.required' => 'Tipo não encontrado.',
            'msg.required' => 'Mensagem é obrigatória.',
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {

            if('from' === $data['send']) {
                $destroy = $this->message->destroyFrom($data['msg'], $this->user->id);
            }else{
                $destroy = $this->message->destroyTo($data['msg'], $this->user->id);
            }

            if($destroy) {
                $profileTo = $this->profile->find($data['public']);
                $history = $this->message->history($profileTo->id, $this->user->id);
                $history = $this->createPathImagesProfile($history);
                $history = $this->createUrl($history, 'messages');
                $history = array_reverse($history);
                $history = $this->senders($history, $this->user->id);

                $page = View::make('components.mensagens.results', compact('history'))->render();
                $res = array('status' => 'true', 'page' => $page);
            } else {
                $res = array('status' => 'false');
            }
        }

        return Response::json($res);
    }
    public function delMsgAll()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'public' => 'required',
        ), array(
            'public.required' => 'Usuário não encontrado.',
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $profileTo = $this->profile->find($data['public']);
            $msgs = $this->message->history($profileTo->id, $this->user->id);

            foreach($msgs as $v) {
                if($v->from_id === $this->user->id) {
                    $this->message->destroyFrom($v->idMsg, $this->user->id);
                } else {
                    $this->message->destroyTo($v->idMsg, $this->user->id);
                }
            }

            $page = View::make('components.mensagens.results')->render();
            return Response::json(array('status' => 'true', 'page' => $page));
        }
    }

    public function search()
    {
        $data = Input::all();

        $friends = array();
        $i = 0;

        $getFriends = $this->profile->getAllSearchFriends($this->user->id, $data['pesquisar']);

        if(count($getFriends) > 0) {
            foreach ($getFriends as $friend) {
                if ($friend['id'] != $this->user->id) {
                    $msg = $this->message->history($friend['id'], $this->user->id);

                    $friend = $this->createPathImagesProfile(array($friend));
                    $friend = $this->createUrl($friend, 'messages');

                    $friends[$i]['id'] = $friend[0]['id'];
                    $friends[$i]['profile_picture'] = $friend[0]->profile_picture;
                    $friends[$i]['name'] = $friend[0]->name;
                    $friends[$i]['slug'] = $friend[0]->slug;
                    $friends[$i]['msg'] = (count($msg) > 0) ? $this->truncate($msg[0]->body, 50) : null;

                    $i++;
                }
            }

            $page = View::make('components.mensagens.friends', compact('friends'))->render();
            return Response::json(array('page' => $page));
        } else {
            return Response::json(array('status' => false));
        }
    }

    private function senders($history, $id)
    {
        foreach($history as $k => $v) {
            if($v->from_id === $id) {
                $history[$k]->send = 'from';
            } else {
                $history[$k]->send = 'to';
            }
        }

        return $history;
    }
}