<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		/*$this->call('ProfilesTableSeeder');
        $this->call('ProfilesFriendsTableSeeder');*/


		//$this->call('NotificationsTableSeeder');
		//$this->call('OutrasAmizadesTableSeeder');
		//$this->call('MessagesTableSeeder');
		//$this->call('GiftlistsTableSeeder');
        $this->call('SpecialMessagesTableSeeder');
	}

}