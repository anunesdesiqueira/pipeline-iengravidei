<?php

use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('events', function($table)
        {
            $table->bigIncrements('id');
            $table->integer('profile_id');
            $table->enum('type', array('cha-de-bebe', 'nascimento', 'cha-de-fraldas', 'outros'));
            $table->integer('giftlist_id');
            $table->string('name', 200);
            $table->dateTime('when_date');
            $table->string('mom_name', 200);
            $table->string('dad_name', 200);
            $table->string('address');
            $table->text('comments');
            $table->integer('template_id');
            $table->boolean('finished')->default(0);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('events');
	}

}