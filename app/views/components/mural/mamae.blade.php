@if(count($momPublic) > 0)
    @foreach($momPublic as $mom)
        <li>
            <div class="thumb">
                <img src="{{ $mom->profile_picture['small'] }}" />
            </div>
            <div class="description">
                <h4>{{ $mom->name }} <span>{{ date('d/m à\\s h:i a', strtotime($mom->created_at)) }}</span></h4>
                {{ $mom->message }}
            </div>
        </li>
    @endforeach
@endif