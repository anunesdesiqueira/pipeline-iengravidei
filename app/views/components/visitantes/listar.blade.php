<h2 name="visitantes">Visitantes</h2>
<h3>Confira abaixo as solicitações de acesso</h3>
<p>Ao liberar o acesso um e-mail será enviado com a url de acesso ao seu perfil para solicitante</p>
@if(count($solicitacoes) > 0)
<ul class="visitantes-lista">
    @foreach($solicitacoes as $solicitacao)
    <li>
        <span class="nome">{{ $solicitacao->nome }}</span>
        <span class="email">{{ $solicitacao->email }}</span>
        <a class="excluir" href="#" data-url="{{ $solicitacao->hash }}" title="Excluir">excluir</a>
        <a class="liberar-acesso" href="#" data-url="{{ $solicitacao->hash }}" title="Liberar Acesso">liberar acesso</a>
    </li>
    @endforeach
</ul>
@endif
{{ $solicitacoes->links() }}