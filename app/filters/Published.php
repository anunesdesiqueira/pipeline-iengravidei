<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;

class Published
{
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    public function filter()
    {
        $user = Auth::client()->get();

        $published = $this->profile->getPublished($user->id);
        View::share(compact('published'));
    }
}