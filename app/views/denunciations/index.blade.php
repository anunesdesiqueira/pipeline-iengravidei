@extends('layouts.master')

@if(!Auth::check())
    @section('cadastro')
        @include('components.register')
    @stop
@endif

@section('content')
<div class="holder">

    @if(Auth::check())
        @include('components.cabecalho.header_active')
    @endif

    <div class="denuncia">

        <div id="msg" style="color: red; margin: 0 0 20px 20px;" >
            @if($errors->any())
                <ul>
                    {{ implode('', $errors->all('<li>:message</li>'))}}
                </ul>
            @endif
        </div>

        {{  Form::open(array(
            'files'     => 'true',
            'id'        => 'frm-denuncie'
            ));
        }}

        <h2 name="denuncie">Faça sua Denúncia</h2>

        <div class="box">
            <h3>Gostaria de se identificar ao fazer essa denúncia?</h3>
            <ul>
                <li><label><input type="radio" name="anonymous" value="N" /> Não. Quero fazer essa denúncia anonimamente.</label></li>
                <li><label><input type="radio" name="anonymous" value="Y" /> Sim. Por favor, pegue meu login já cadastrado.</label></li>
            </ul>

            <h3>Que tipo de denúncia você quer fazer?</h3>
            <ul>
                <li><label><input type="radio" name="type" value="PPI" />Pedofilia e pornografia infantil</label></li>
                <li><label><input type="radio" name="type" value="ES" />Exploração sexual</label></li>
                <li><label><input type="radio" name="type" value="AIC" />Apologia e incitação ao crime</label></li>
                <li><label><input type="radio" name="type" value="NZM" />Neonazismo</label></li>
                <li><label><input type="radio" name="type" value="AIPCA" />Apologia e incitação a práticas cruéis contra animais</label></li>
                <li><label><input type="radio" name="type" value="CDICH" />Calúnia, difamação, injúria e crimes contra a honra</label></li>
                <li><label><input type="radio" name="type" value="DA" />Direitos autorais</label></li>
                <li><label><input type="radio" name="type" value="FI" />Falsa identidade</label></li>
                <li><label><input type="radio" name="type" value="PRN" />Pornografia</label></li>
                <li><label><input type="radio" name="type" value="VS" />Vírus e Spam</label></li>
                <li><label><input type="radio" name="type" value="RXISR" />Racismo, xenofobia e intolerância sexual ou religiosa</label></li>
                <li><label><input type="radio" name="type" value="OTR" />Outros</label></li>
            </ul>
        </div>

        <div class="box">
            <h3>E-mail para contato:</h3>
            <input type="text" name="email" value="" />
            <h3>Que tipo de conteúdo foi postado?</h3>
            <ul>
                <li><label><input type="radio" name="posted" value="AUD" />Áudio</label></li>
                <li><label><input type="radio" name="posted" value="IMG" />Imagens</label></li>
                <li><label><input type="radio" name="posted" value="USE" />Url/Sites Externos</label></li>
                <li><label><input type="radio" name="posted" value="CTC" />Comentários/Textos/Conversa</label></li>
                <li><label><input type="radio" name="posted" value="VID" />Vídeos</label></li>
                <li><label><input type="radio" name="posted" value="PFR" />Perfil</label></li>
                <li><label><input type="radio" name="posted" value="OTR" checked />Outros</label></li>
            </ul>
        </div>

        <div class="clearfix"></div>

        <div class="box">

            <h3>Envie uma imagem do conteúdo abusivo (campo de upload limitado a 300Kb)</h3>
            <div class="upload">
                <?php echo Form::file('image') ?>
                <div class="btn-fake">Upload de foto</div>
            </div>
            <div class="fileName"></div>

            <div class="clearfix"></div>

            <h3 class="mgTop40px">Caso necessário deixe aqui outros comentários</h3>
            <textarea name="comments" ></textarea>

            <div class="clearfix"></div>

            <input type="submit" value="Enviar Denúncia" />
        </div>
        <?php Form::close(); ?>
    </div>

</div>
@stop

@section('customjs')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
<script>
    (function($){

       $('#frm-denuncie').validate({
            rules : {
                'anonymous' : 'required',
                'type'      : 'required',
                'email'     :  {
                    'required'  : true,
                    'email'     : true
                },
                'posted'    : 'required'

            },
            messages: {

                'anonymous' : {
                    'required' : 'Selecione se deseja ou não se identificar ao fazer essa denúncia'
                },
                'type' : {
                    'required' : 'Selecione que tipo de denúncia você quer fazer'
                },
                email   : {
                    'required' : 'Digite um e-mail para contato',
                    'email'    : 'E-mail para contato inválido'
                },
                'posted'       : 'Selecione que tipo de conteúdo foi postado'

            }

        });

    })(jQuery);
</script>
@stop