@if(count($showAllMoms) > 0)
    @foreach($showAllMoms as $mom)
        <li>
            <div class="thumb">
                <a href="{{ route('user.public', array('publicId' => $mom->public_id)) }}" title="{{ $mom->name }}">
                    <img src="{{ $mom->profile_picture['small'] }}" />
                </a>
            </div>
            <div class="description">
                <h4><a href="{{ route('user.public', array('publicId' => $mom->public_id)) }}" title="{{ $mom->name }}">{{ $mom->name }}</a> <span>{{ date('d/m à\\s h:i a', strtotime($mom->created_at)) }}</span></h4>
                {{ $mom->message }}
                @if($mom->options)
                    <div class="control">
                        <div class="fb-share-button" data-href="{{ route('show_mural.status', array('publicId' => $user->public_id, 'slug' => $mom->hash)) }}" data-layout="link"></div>
                        |
                        <a rel="{{ $mom->hash }}" class="bt-mural-excluir" href="#">Excluir</a>
                    </div>
                @endif
            </div>
        </li>
    @endforeach
@endif