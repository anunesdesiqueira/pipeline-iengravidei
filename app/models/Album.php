<?php

class Album extends Eloquent {

    protected $table = 'albums';
    protected $guarded = array('id', 'id');

    public static function returnExistsAlbums($user_id, $album_name)
    {
        $return = Album::whereRaw('
                profile_id = ?
                and title = ?
                and is_active = 1',
            array($user_id, $album_name)
        )->get();

        return $return;
    }

    public static function createAlbum($profile_id, $title)
    {
        $album = Album::create(array('profile_id' => $profile_id, 'title' => $title));

        return $album->id;
    }

    public static function returnAlbums($profile_id)
    {
        $return = DB::table('albums as a')
            ->join('albums_medias as b', 'b.album_id', '=', 'a.id')
            ->join('medias as c', 'c.id', '=', 'b.medias_id')
            ->where('a.profile_id', '=', $profile_id)
            ->where('a.is_active', '=', 1)
            ->where('c.is_active', '=', 1)
            ->groupBy('a.title')
            ->get(array('a.*', 'c.name'));

        return $return;
    }

    public static function returnAlbum($profile_id, $albums_id)
    {
        $return = DB::table('albums as a')
            ->join('albums_medias as b', 'b.album_id', '=', 'a.id')
            ->join('medias as c', 'c.id', '=', 'b.medias_id')
            ->where('a.profile_id', '=', $profile_id)
            ->where('a.id', '=', $albums_id)
            ->where('a.is_active', '=', 1)
            ->where('c.is_active', '=', 1)
            ->get(array('a.*', 'c.id as foto_id', 'c.name'));

        return $return;
    }

    public static function returnNameAlbum($profile_id, $albums_id)
    {
        $return = Album::whereRaw('
                profile_id = ?
                and id = ?
                and is_active = 1',
            array($profile_id, $albums_id)
        )->get();

        return $return;
    }

    public static function excludeAlbum($id_album, $profile_id)
    {
        $return = Album::whereRaw('id = ? and profile_id = ?', array($id_album, $profile_id))->update(array('is_active' => 0));

        return $return;
    }

    public static function alterAlbum($profile_id, $id_album, $nome_album, $nome_novo )
    {
        $return = Album::whereRaw('id = ? and profile_id = ? and title = ?', array($id_album, $profile_id, $nome_album))->update(array('title' => $nome_novo));

        return $return;
    }
}
