@if(isset($babyline) && count($babyline) > 0)
    <div class="baby-line">
        <h2 title="Baby Line">Baby Line</h2>
        <div id="carouselBabyLine" class="edit">
            <a class="buttons next" href="#">right</a>
            <a class="buttons prev" href="#">left</a>
            <div class="viewport">
                <ul class="overview lista-babyline-fotos">
                @foreach ($babyline as $babyline)
                    <li>
                        <div class="thumb">
                            <a href="{{ $babyline->picture['original'] }}"><img src="{{ $babyline->picture['main'] }}" /></a>
                        </div>
                        <p>{{ $babyline->number }}ª Semana</p>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif