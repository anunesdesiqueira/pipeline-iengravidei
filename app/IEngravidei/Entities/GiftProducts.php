<?php namespace IEngravidei\Entities;


class GiftProducts extends \Eloquent
{
    protected $table = 'giftlists_products';
    protected $guarded = array();
    public $timestamps = false;
}