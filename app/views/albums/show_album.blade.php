@extends('layouts.master')

@section('content')
<div class="holder album-fotos">
    @include('components.cabecalho.public')

    <h2 name="albuns" title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>

    @if(isset($photos))
        <ul class="lista-album lista-album-fotos">
        @foreach($photos as $photo)
            <li>
                <div class="thumb">
                    <a href="{{ $photo->picture['original'] }}">
                        <img src="{{ $photo->picture['main'] }}" />
                    </a>
                </div>
            </li>
        @endforeach
        </ul>
    @endif
</div>
@stop