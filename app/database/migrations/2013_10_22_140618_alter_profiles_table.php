<?php

use Illuminate\Database\Migrations\Migration;

class AlterProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function($table)
        {
            $table->timestamp('activated_at')->after('activated');	
            $table->string('profile_picture', 50)->after('public_id');
			$table->integer('country_id')->unsigned()->after('public_id');
			$table->integer('state_id')->unsigned()->after('public_id');
			$table->integer('city_id')->unsigned()->after('public_id');							
            $table->string('father_name', 200)->after('public_id');
            $table->date('birth')->after('public_id');
            $table->integer('pregnancy_weeks')->after('public_id');
			// $table->foreign('country_id')->references('id')->on('countries');
			// $table->foreign('state_id')->references('id')->on('states');
			// $table->foreign('city_id')->references('id')->on('cities');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function($table)
        {		
			$table->dropColumn('activated_at');
			$table->dropColumn('profile_picture');
			$table->dropColumn('country_id');
			$table->dropColumn('state_id');
			$table->dropColumn('city_id');
			$table->dropColumn('father_name');
			$table->dropColumn('birth');
			$table->dropColumn('pregnancy_weeks');
		});
	}

}