<?php

namespace IEngravidei\Entities;

class ProfileGiftProduct extends \Eloquent
{
    protected $table = 'profiles_giftlist_products';
    protected $guarded = array();
    public $timestamps = false;
}