<?php

namespace IEngravidei\Entities;

class Poll extends \Eloquent
{
    protected $table = 'profiles_polls';
    protected $guarded = array();
    public $timestamps = false;
}