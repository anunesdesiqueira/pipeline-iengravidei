@extends('layouts.master')

@section('content')
<div class="holder">
    <div class="busca-resultado">
        <h2 name="buscar">Resultado da Busca</h2>
        <div class="container">
            @include('components.busca.resultado')
        </div>

        <h3>Busca</h3>
        @include('components.busca.avancada')
    </div>
</div>
@stop