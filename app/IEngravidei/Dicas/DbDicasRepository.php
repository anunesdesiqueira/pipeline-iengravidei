<?php

namespace IEngravidei\Dicas;

use IEngravidei\Entities\Dicas;
use IEngravidei\Repositories\DicasRepositoryInterface;

class DbDicasRepository implements DicasRepositoryInterface
{
    public function getAll()
    {
        return Dicas::where('status', '=', 1)->where('pub_inc', '<', new \DateTime('today'))->orderBy('pub_inc', 'DESC')->get();
    }

    public function find($str)
    {
        return \DB::select("
            select * from `professional_tips`
            where `status` = 1
            and `pub_inc` < NOW()
            and (`title` LIKE '%{$str}%' OR `body` LIKE '%{$str}%' OR `author` LIKE '%{$str}%')
            order by `pub_inc` DESC
        ");
    }
}