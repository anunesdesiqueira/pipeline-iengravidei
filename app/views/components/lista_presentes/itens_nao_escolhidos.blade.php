@if(count($gifts) > 0)
    <ul class="itens-medium">
        @foreach($gifts as $gift)
        <li>
            <div class="thumb">
                <img src="{{ $gift->picture['medium'] }}" alt="{{ $gift->name }}"/>
            </div>
            <p>{{ $gift->description }}</p>
            <span>R$ {{ str_replace('.', ',', $gift->price) }}</span>
            {{ Form::button('adicionar',  array('rel' => $gift->id, 'class' => 'bt-itens-medium')) }}
        </li>
        @endforeach
    </ul>
    {{ $gifts->links() }}
@else
    <p class="list-presentes-msg">Não existem mais presentes para serem adicionados a está lista!</p>
@endif