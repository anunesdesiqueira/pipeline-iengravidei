<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <title>Validação de Cadastro</title>
</head>

<body>

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
    <tbody>
    <tr>
        <td height="135" align="center"><img src="{{ asset('assets/img/email/logo.gif') }}" style="display:block" /></td>
    </tr>
    <tr>
        <td align="center">
            <br/><br/>
            <font face="Arial" color="#000000" style="font-size:22px;"><strong>Validação de Cadastro</strong></font>
            <br/><br/><br/><br/>
            <font face="Verdana" color="#000000" style="font-size:13px;line-height:20px;">
                Olá <strong>{{ $name }},</strong><br/>
                Para finalizar seu cadastro, clique no link abaixo e ative sua conta.
            </font>
            <br/><br/><br/><br/>
            <a href="{{ URL::route('register.confirm') . '/' . $activation_code }}" target="_blank">
                <font face="Arial" color="#e65179" size="4"><strong>ATIVE SUA CONTA</strong></font>
            </a>
            <br/><br/><br/><br/><br/><br/><br/><br/>
        </td>
    </tr>
    <tr>
        <td align="center"><font face="Arial" color="#000000" size="3"><strong>OBRIGADO POR SEU CADASTRO.</strong></font><br/><br/><br/><br/></td>
    </tr>
    <tr>
        <td bgcolor="#da235c" height="30" align="center">
            <font size="2" face="Arial" color="#ffffff"><strong>Para mais informações acesse: <a href="http://www.iengravidei.com.br" target="_blank" style="color:#ffffff">http://www.iengravidei.com.br</a></strong></font>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>