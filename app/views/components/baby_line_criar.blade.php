<!--### Babyline ###-->
<div class="baby-line">

    <h2 title="Baby Line">Baby Line</h2>

    <div class="addFoto">
    {{ Form::open(array('file' => true, 'enctype' => 'multipart/form-data', 'id' => 'babyline-form')) }}
        <div class="fileName">Insira nova foto</div>
        <div class="uploadHolder">
            <div class="mascara"></div>
            {{ Form::file('picture') }}
        </div>
        <div class="comboHolder">
            <div class="mascara"><div class="optName">Escolha a semana</div></div>
            <select name="babyline_weeks">
                <option value="">Escolha a semana</option>
                @foreach ($babyline_weeks as $k => $v)
                    <option value="{{ $k }}">{{ $v }}ª Semana</option>
                @endforeach
            </select>
        </div>
        <input type="submit" value="Enviar" />
        <div class="clearfix"></div>
    {{ Form::close() }}
    </div>

    <div id="carouselBabyLine" class="edit">
        <a class="buttons next" href="#">right</a>
        <a class="buttons prev" href="#">left</a>
        <div class="viewport">
            <ul class="overview lista-babyline-fotos">
                @foreach ($babyline as $photo)
                    <li rel="foto{{ $photo->id }}">
                        <a href="javascript:deleteNodeBaby({{ $photo->id }})" class="delete">Deletar</a>
                        <div class="thumb">
                            <a href="{{ $photo->picture['original'] }}"><img src="{{ $photo->picture['main'] }}" /></a>
                        </div>
                        <p>{{ $photo->number }}ª Semana</p>
                    </li>
                @endforeach
                @for ($i = count($babyline); $i < 5; $i++)
                    <li class="example">
                        <div class="thumb">
                            <img src="{{ asset('assets/img/babyline-exemplo.jpg') }}" />
                        </div>
                        <p>Xª Semana</p>
                    </li>
                @endfor
            </ul>
        </div>
    </div>

</div>
<!--### End: Babyline ###-->