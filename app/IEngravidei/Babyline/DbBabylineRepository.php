<?php

namespace IEngravidei\Babyline;

use IEngravidei\Entities\Babyline;
use IEngravidei\Entities\BabylineWeeks;
use IEngravidei\Repositories\BabylineRepositoryInterface;

class DbBabylineRepository implements BabylineRepositoryInterface
{
    public function getWeeks()
    {
        return BabylineWeeks::orderBy('number', 'ASC')->lists('id','number');
    }

    public function findWeek($id, $number)
    {
        return Babyline::join('babyline_weeks', 'babyline_weeks.id', '=', 'babyline.id_week')
            ->where('babyline.id_profile', '=', $id)->where('babyline_weeks.number', '=', $number)->exists();
    }

    public function getList($id)
    {
        return Babyline::join('babyline_weeks', 'babyline_weeks.id', '=', 'babyline.id_week')
            ->where('babyline.id_profile', '=', $id)->orderBy('babyline_weeks.number', 'ASC')
            ->select('babyline_weeks.number', 'babyline.id', 'babyline.picture')->get();
    }

    public function createImage($input)
    {
        return Babyline::create(array(
                            'id_profile' => $input['id'],
                            'id_week' => $input['babyline_weeks'],
                            'picture' => $input['picture'],
                            'created_at' => date('Y-m-d H:i:s')
                        ));
    }

    public function deleteImage($input)
    {
        return Babyline::where('id', '=', $input['babyline_image'])
            ->where('id_profile', '=', $input['id'])->forceDelete();
    }
}