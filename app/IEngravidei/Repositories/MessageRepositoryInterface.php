<?php namespace IEngravidei\Repositories;

interface MessageRepositoryInterface
{
    public function count($id_profile);
    public function read($id_profile);
    public function send($input);
    public function history($from, $to);
    public function destroyFrom($id, $id_profile);
    public function destroyTo($id, $id_profile);
}