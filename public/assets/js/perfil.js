// Global vars
var number = 10; // mural limit
var offset = 0; // mural offset
var busy = false;

$(window).load(function () {
    loadindGetData();
});

// Scroll events
$(window).scroll(function() {
    if(($(document).height() - $(window).height() - $(document).scrollTop()) < 440 && !busy) {
        busy = true;
        loadindGetData();
    }
});

// Mascara para o campo de upload da BabyLine
$(document.body).on('change', 'form#babyline-form .uploadHolder input[type=file]', function () {
    $('.addFoto .fileName').text($(this).val());
    $('.addFoto .fileName').each(function () {
        var text = $(this).text();
        $(this).text(text.replace('C:\\fakepath\\', ''));
    });
});

$(document.body).on('submit', '#babyline-form', function(e) {
    e.preventDefault();

    var form = $(this),
        formAction = form.attr('action'),
        formdata = false,
        submitButton = $(this).find('input[type="submit"]');

    $(submitButton).prop('disabled', true);

    if (window.FormData) {
        formdata = new FormData(form[0]);
    }

    $.ajax({
        url         : baseUri + '/babyline/ajax/add',
        data        : formdata ? formdata : form.serialize(),
        dataType    : 'json',
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR) {
            if(data.status) {
                babylineList(data.list);
            } else {
                setError(data);
            }

            $(form).find('.fileName').html('Insira nova foto');
        }
    });

});

function deleteNodeBaby(fotoHolder) {
    $.ajax({
        url         : baseUri + '/babyline/ajax/deletar',
        data        : {'babyline_image' : fotoHolder},
        dataType    : 'json',
        cache       : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR){

            var form = $('#babyline-form'),
                msg = document.createElement('div');

            if(data.status)
            {
                $('li[rel=foto'+fotoHolder+']').remove();

                var ul = $('.overview'),
                    count = $('#carouselBabyLine ul li').length;

                for(var i = count; i < 5; i++)
                {
                    var li = document.createElement('li'),
                        div = document.createElement('div'),
                        img = document.createElement('img'),
                        p = document.createElement('p');

                    $(div).addClass('thumb');
                    $(div).append(img);

                    $(li).addClass('example');
                    $(img).attr('src', '/assets/img/babyline-exemplo.jpg');
                    $(p).html('Xª Semana');
                    $(li).append(div);
                    $(li).append(p);

                    $(ul).append(li);
                }

                $(msg).addClass('msg sucess');
                $(msg).html('Imagem deletada com sucesso');
                $(form).append(msg);

                $('#carouselBabyLine').tinycarousel_destroy();
                startCarousel();
            }
            else
            {
                $(msg).addClass('msg sucess');
                $(msg).html('Ops! Ocorreu um erro no envio, tente novamente');
                $(form).append(msg);
            }

            setMsgTimeout();
        }
    });
};

function setError(data) {
    var form = $('#babyline-form'),
        div = document.createElement('div'),
        items = [];

    $.each(data, function(key, val){
        items.push(val)
    });

    $(div).addClass('msg error');
    $(div).html(items.join('<br>'));
    $(form).append(div);
    setMsgTimeout();
}

function babylineList(data) {
    var ul = $('.overview'),
        items = [],
        form = $('#babyline-form'),
        msg = document.createElement('div'),
        count = data.length;

    $(msg).addClass('msg sucess');
    $(msg).html('Imagem enviada com sucesso');
    $(form).append(msg);

    $(ul).html('');

    $.each(data, function(key, val)
    {
        var li = document.createElement('li'),
            del = document.createElement('a'),
            div = document.createElement('div'),
            open = document.createElement('a'),
            img = document.createElement('img'),
            p = document.createElement('p');

        $(del).addClass('delete').attr('href','javascript:deleteNodeBaby('+val.id+')').html('Deletar');
        $(div).addClass('thumb');
        $(open).attr('href', val.picture['original']);
        $(open).append(img);
        $(div).append(open);

        $(li).attr('rel', 'foto'+val.id);
        $(img).attr('src', val.picture['main']);
        $(p).html(val.number+'ª Semana');
        $(li).append(del);
        $(li).append(div);
        $(li).append(p);

        $(ul).append(li);
    });

    for(var i = count; i < 5; i++) {
        var li = document.createElement('li'),
            div = document.createElement('div'),
            img = document.createElement('img'),
            p = document.createElement('p');

        $(div).addClass('thumb');
        $(div).append(img);

        $(li).addClass('example');
        $(img).attr('src', '/assets/img/babyline-exemplo.jpg');
        $(p).html('Xª Semana');
        $(li).append(div);
        $(li).append(p);

        $(ul).append(li);
    }

    $('#carouselBabyLine').tinycarousel_destroy();

    startCarousel();
    setMsgTimeout();
}

function setMsgTimeout()
{
    var msg = $('.msg'),
        submitButton = $('#babyline-form').find('input[type="submit"]');

    msg.fadeOut( 3000, function() {
        msg.remove();
        $(submitButton).prop('disabled', false);
    });
}
/* Babyline End */

/* Mural Start */
$(document.body).on('click', '.tab > div', function(e) {
    e.preventDefault();

    var tabName = $(this).attr('rel');
    if($('.tab > div').hasClass('active')) {
        $('.tab > div').removeClass('active');
    }

    $(this).addClass('active');
    $('.tabList').removeClass('active');
    $('.'+tabName).addClass('active');

    if( tabName == 'amigas') {
        $('.tab').css('border-bottom','1px solid #ab97c0');
    } else {
        $('.tab').css('border-bottom','1px solid #eee9d2');
    }
});

$(document.body).confirmOn({
    questionText: 'Você tem certeza que deseja excluir esse comentário?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.mural-logado .control .bt-mural-excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/mural/deleted','POST',{ hash : $(e.toElement).attr('rel') },'','',successMuralDel);
        $(e.toElement).parent().parent().parent().remove();
    }
});

function successMuralDel(data) {
    if('true' !== data.status) {
        console.log('Ocorreu erro ao delelar o status');
    }
}

/* Hide input and clear player */
$(document.body).on('click', 'form#form-mural .envio input[type=file]', function() {
    hideInputYoutube();
});

$(document.body).on('change', 'form#form-mural .envio input[type=file]', function(e) {
    e.preventDefault();

    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return;

    if (/^image/.test( files[0].type)) {
        var reader = new FileReader();
        reader.readAsDataURL(files[0]);

        reader.onloadend = function(){
            $(".loader-media #picture").css({
                'background-image' : 'url('+this.result+')',
                'background-position' : 'center center',
                'background-size' : 'cover',
                '-webkit-box-shadow' : '0 0 1px 1px rgba(0, 0, 0, .3)',
                'display' : 'inline-block',
                'width' : '100px',
                'height' : '100px',
                'margin' : '10px 0'
            });
        }
    }
});

/* Show input youtube */
$(document.body).on('click', 'form#form-mural .showForm', function(e) {
    e.preventDefault();

    var yInput = $('.holderForm .ytUrl');

    if(yInput.hasClass('active')) {
        hideInputYoutube();
    } else {
        showInputYoutube();
        resetInputImageMural();
    }
});

/* Youtube embed */
$(document.body).on('change', 'form#form-mural .holderForm .ytUrl input', function(e) {
    e.preventDefault();

    $('#yPlayer').remove();

    var yUrl = this.value;
    yUrl = yUrl.fulltrim();

    if(validateYoutubeUrl(yUrl)) {
        var yId = YouTubeGetID(yUrl);
        createEmbedFrameYoutube(yId);
    }
});

function hideInputYoutube()
{
    $('#yPlayer').remove();

    var yInput = $('.holderForm .ytUrl');
    yInput.removeClass('active');
    yInput.find('input').val('');
    yInput.hide();
}

function showInputYoutube()
{
    var yInput = $('.holderForm .ytUrl');
    yInput.addClass('active');
    yInput.show();
}

function resetInputImageMural()
{
    reset($('input[rel="fileMural"]'));
    $(".loader-media #picture").css({
        'background-image' : 'none',
        'display' : 'none',
        'width' : '0',
        'height' : '0',
        'margin' : '0'
    });
}

function createEmbedFrameYoutube(id)
{
    var yPlayer = document.createElement('div');
    var yId = id;
    var yIdPlayer = 'yPlayer';
    var yWidth = '350';
    var yHeight = '245';

    yPlayer.setAttribute('id', 'yPlayer');
    $('.loader-media').append(yPlayer);

    onYouTubeIframeAPIReady(yId,yIdPlayer,yWidth,yHeight);
}

function validateYoutubeUrl(Url)
{
    var patternUrl = /(http(s)?:\/\/(www\.)?)(youtube\.com|youtu\.be)\/(watch\?v=|v\/)?[%&=#\w-]*/g;

    if(Url.match(patternUrl)) {
        return true
    } else {
        return false
    }
}

function YouTubeGetID(url)
{
    var ID = '';
    url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);

    if(url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_]/i);
        ID = ID[0];
    } else {
        ID = url;
    }

    return ID;
}

/* Submit mural */
$(document.body).on('submit', 'form#form-mural', function(e) {
    e.preventDefault();

    busy = true;

    var form = $(this);
    var comentario = form.find('textarea[name="comentario"]').val();
    var imagem = form.find(':input[name="imagem"]').val();
    var youtube = form.find(':input[name="youtube"]').val();

    if('' == comentario && '' == imagem && '' == youtube && false == validateYoutubeUrl(youtube)) {
        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm" style="max-width: 520px;">' +
                    '<h6>Essa atualização de status está vazia. Escreva algo ou anexe um link ou uma foto para atualizar o seu status.</h6>' +
                    '<div class="container" style="width: 73px;">' +
                    '<a class="bt-popup-eventos-sim" href="javascript:void(0)">Fechar</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    } else {
        var form = $(this);
        var params = form.serialize();
        var formdata = false;

        if(window.FormData) {
            formdata = new FormData(form[0]);
        }

        $.ajax({
            url: baseUri + '/meu-perfil/mural/send',
            data: formdata ? formdata : params,
            dataType    : 'json',
            cache       : false,
            contentType : false,
            processData : false,
            type        : 'POST',
            beforeSend: function() {
                if (typeof loadingFormShowMural === 'function') {
                    loadingFormShowMural();
                }
            },
            complete: function() {
                if (typeof loadindFormHideMural === 'function') {
                    loadindFormHideMural();
                }
            },
            success: function (data) {
                if (typeof successFormMural === 'function') {
                    successFormMural(data);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log(xhr);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
});

$(document.body).on('click', '.popup-presentes-confirm .container a.bt-popup-eventos-sim', function(e){
    e.preventDefault();
    $.magnificPopup.close();
});

$(document.body).on('submit', 'form#form-buscar-todas-mamaes', function(e){
    e.preventDefault();

    busy = true;

    var todas = $(this).find(':input[name="buscar-todas-mamaes"]').val();
    var amigas = $('form#form-buscar-amigas').find(':input[name="buscar-amigas"]').val();
    CallMethod(baseUri + '/meu-perfil/mural','GET',{number : 10, offset : 0, 'buscar-todas-mamaes' : todas, 'buscar-amigas' : amigas},'','',successFormMural);
});

$(document.body).on('submit', 'form#form-buscar-amigas', function(e) {
    e.preventDefault();

    busy = true;
    var amigas = $(this).find(':input[name="buscar-amigas"]').val();
    var todas = $('form#form-buscar-todas-mamaes').find(':input[name="buscar-todas-mamaes"]').val();
    CallMethod(baseUri + '/meu-perfil/mural','GET',{number : 10, offset : 0, 'buscar-todas-mamaes' : todas, 'buscar-amigas' : amigas},'','',successFormMural);
});

function loadingFormShowMural() {
    var submit = document.getElementsByClassName('enviar-mural');
    $(submit).css({color : '#d2c6df'}).prop({ disabled : true });
}

function loadindFormHideMural() {
    $(document.getElementsByClassName('enviar-mural')).css({color : '#ffffff'}).prop({ disabled : false });
}

function successFormMural(data) {
    hideInputYoutube();
    resetInputImageMural();

    reset($('form#form-mural textarea[name="comentario"]'));

    var all = $('.mural-logado .todas ul');
    var friends = $('.mural-logado .amigas ul');

    all.html('');
    friends.html('');

    all.append(data.pageAllMoms);
    friends.append(data.pageFriends);

    readMore();
    busy = false;
    offset = number;
}

/* Load status msgs Mural */
function loadindGetData() {

    busy = true;

    var todas = $('form#form-buscar-todas-mamaes').find(':input[name="buscar-todas-mamaes"]').val();
    var amigas = $('form#form-buscar-amigas').find(':input[name="buscar-amigas"]').val();

    CallMethod(baseUri + '/meu-perfil/mural','GET',{number : number, offset : offset, 'buscar-todas-mamaes' : todas, 'buscar-amigas' : amigas},'','',successStatusMural);
}

function successStatusMural(data) {
    var all = $('.mural-logado .todas ul');
    var friends = $('.mural-logado .amigas ul');

    all.append(data.pageAllMoms);
    friends.append(data.pageFriends);

    readMore();
    FB.XFBML.parse();

    busy = false;
    offset = offset+number;
}
/* Mural End */


/* Dicas Start */
$(document.body).on('submit', '#form-dicas', function(e) {
    e.preventDefault();

    $.ajax({
        url         : '/dicas-profissionais/search',
        data        : $(this).serialize(),
        dataType    : 'json',
        cache       : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR) {
            if(data.status) {
                loadTipsSearch(data.list);
            } else {
                $('#form-dicas').append('<p>Nenhum resultado foi encontrado.</p>');
                $('#form-dicas').find('p').fadeOut( 3000, function() {
                    $('#form-dicas').find('p').remove();
                });
            }
        }
    });
});

function loadTipsSearch(data) {
    var ul = $('.dicas-profissionais').find('ul');

    $(ul).html('');

    $.each(data, function(key, val) {
        var li = document.createElement('li'),
            img = document.createElement('img'),
            h3 = document.createElement('h3'),
            span = document.createElement('span');

        if (1 === val.type)
            $(li).addClass('produto');

        $(img).attr('src', 'assets/dicas_profissionais/' + val.picture);
        $(h3).html(val.title);
        $(span).html('postado por: ' + val.author);

        $(li).append(img);
        $(li).append(h3);
        $(li).append(val.body);
        $(li).append(span);

        $(ul).append(li);
    });
}
/* Dicas End */