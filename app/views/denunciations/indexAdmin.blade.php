@extends('layouts.scaffold')

@section('main')

<div class="container">
    <div class="row-fluid">

        <div class="span3 sidebar denuncie">
            <h4>Denuncias</h4>
            <label>Periodo das Denuncias:</label>

            <div class="data">
                <input type="text" /> à <input type="text" />
            </div>

            <p>Refinar por:</p>

            <h4>Status</h4>

            <select>
                <option>Selecione</option>
                <option>Ativa</option>
                <option>Desativada</option>
            </select>

            <button type="submit" class="btn">Pesquisar</button>
        </div>

        <div class="span9 conteudo denuncie">
            <h4>Resultado da Pesquisa</h4>

            <p>STATUS: {{{ $status }}}</p>
            @if (iterator_count($denuncias) > 0)
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Gostaria Identificar</th>
                    <th>Tipo denuncia</th>
                    <th>Tipo conteúdo</th>
                    <th>Comentário</th>
                    <th>Anexo</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($denuncias as $denuncia)
                <tr>
                    <td>{{{ $denuncia['user_name']['S'] }}}</td>
                    <td>{{{ $denuncia['email_contato']['S'] }}}</td>
                    <td>{{{ $denuncia['usuario_identificado']['S'] }}}</td>
                    <td>{{{ $denuncia['tipo_denuncia']['S'] }}}</td>
                    <td>{{{ $denuncia['tipo_conteudo']['S'] }}}</td>
                    <td>{{{ $denuncia['mensagem']['S'] }}}</td>
                    <td><a href="">|ver anexo|</a></td>
                    <td>|Responder| |Finalizar| |Excluir|</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
                Nenhum registro encontrado!!
            @endif

            <div class="pagination pagination-centered">
                <ul>
                    <li><a href="#">Anterior</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">Próximo</a></li>
                </ul>
            </div>

        </div>
    </div>
</div>

@stop

