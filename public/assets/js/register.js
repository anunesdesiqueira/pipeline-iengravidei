$(function() {
    // Init
    validateComplete();
    $('input[name="birth"], input[name="date"]').mask("99/99/9999");
});

//Validar form complete
function validateComplete() {
    $('form#complete-profile').validate({
        groups: {
            nameGroup: "country_id state_id city_id"
        },
        rules: {
            birth: {
                required: true,
                brazilianDate: true
            },
            country_id: {
                required: true
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            pregnancy_weeks: {
                required: true
            },
            public_id: {
                required: true,
                maxlength: 30,
                notEqualToPublicId: true
            }
        },
        messages: {
            birth: {
                required: "*Digite a data do seu nascimento"
            },
            country_id: {
                required: "*Selecione o país, estado e a cidade"
            },
            state_id: {
                required: "*Selecione o país, estado e a cidade"
            },
            city_id: {
                required: "*Selecione o país, estado e a cidade"
            },
            pregnancy_weeks: {
                required: "*Selecione as semanas de gravidez"
            },
            public_id: {
                required: "*Crie seu endereço exclusivo",
                maxlength: "*O nome do usuário deve conter no máximo 30 caracteres"
            }
        }
    });
}

// Mascara para campo de upload complete
$(document.body).on('change', '.uploadCad input[type=file]', function(e) {
    $('.uploadCad .fileName').text($(this).val());
    $('.uploadCad .fileName').each(function() {
        var text = $(this).text();
        $(this).text(text.replace('C:\\fakepath\\', ''));
    });
});