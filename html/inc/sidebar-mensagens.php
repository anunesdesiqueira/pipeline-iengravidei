<aside>
	<h2>Mensagens</h2>
	<a href="#" class="nova-mensagem">Nova mensagem</a>
	<div class="busca">
		<input type="text" placeholder="PESQUISAR" />
		<button type="button"></button>
	</div>
	<ul>
		<li>
			<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
			<div class="description">
				<h3>Irene Panisi Toko</h3>
				<p><a href="#">MSG: lorem ipsum idel capsem lori vide...</a></p>
			</div>
		</li>
		<li>
			<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
			<div class="description">
				<h3>Maria Helena de Andrade</h3>
				<p><a href="#">MSG: lorem ipsum idel capsem lori vide ipsum idel capsem lori vide...</a></p>
			</div>
		</li>
		<li>
			<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
			<div class="description">
				<h3>Irene Panisi Toko</h3>
				<p><a href="#">MSG: lorem ipsum idel capsem lori vide...</a></p>
			</div>
		</li>
		<li>
			<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
			<div class="description">
				<h3>Irene Panisi Toko</h3>
				<p><a href="#">MSG: lorem ipsum idel capsem lori vide...</a></p>
			</div>
		</li>
		<li>
			<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
			<div class="description">
				<h3>Irene Panisi Toko</h3>
				<p><a href="#">MSG: lorem ipsum idel capsem lori vide...</a></p>
			</div>
		</li>
	</ul>
</aside>