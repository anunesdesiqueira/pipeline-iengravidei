<?php

namespace IEngravidei\Entities;

class ProfileFriendships extends \Eloquent
{
    protected $table = 'profiles_friendships';
    protected $guarded = array();

    public $timestamps = false;
}