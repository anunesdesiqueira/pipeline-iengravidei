/*
 * Lista de Presentes
 */
$(document.body).on('click', '.lista-presentes .container-presentes ul li button', function(e) {
    e.preventDefault();
    CallMethod(baseUri + '/'+ publicId +'/lista-de-presentes/cart','POST',{'prod' : $(this).attr('rel')},'','',successPublicCart);
});

$(document.body).on('click', '.lista-presentes .container-presentes-cart ul.itens-small li button', function(e) {
    e.preventDefault();
    CallMethod(baseUri + '/'+ publicId +'/lista-de-presentes/cart/remove','POST',{'prod' : $(this).attr('rel')},'','',successPublicCart);
});


$(document.body).on('submit', 'form#form-lista-presentes-filter-public', function(e) {
    e.preventDefault();

    var params = $(this).serialize();
    CallMethod(baseUri + '/'+ publicId +'/lista-de-presentes','POST',params,loadingShowFilters,loadindHideFilters,successPublicListProducts);
});

function loadingShowFilters() {
    var submit = document.getElementsByClassName('bt-lista-presentes-filter');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideFilters() {
    $(document.getElementsByClassName('bt-lista-presentes-filter')).css({color : '#867a42'}).prop({ disabled : false });
}

function successPublicCart(data) {
    if(data.page != undefined) {
        var container = $('.lista-presentes .container-presentes-cart');
        container.empty().html(data.page);
    }
}

$(document.body).on('click', '.lista-presentes .container-presentes ul.pagination a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'POST','','','',successPublicListProducts);
});

function successPublicListProducts(data) {
    if(data.page != undefined) {
        var container = $('.lista-presentes .container-presentes');
        container.empty().html(data.page);
    }
}

/* Friends */
$(document.body).on('click', '.adicione-amiga .solicitar-amizade, .atividades a.adicionar', function(e) {
    e.preventDefault();

    var content = $('.adicione-amiga');
    var div = document.createElement('div');
    var p = document.createElement('p');
    var slug = $(this).attr('href');
    var bt = $('.adicione-amiga .solicitar-amizade');

    $(div).addClass('botShadow');
    $(div).append(p);
    $(content).append(div);

    $.ajax({
        url: baseUri + '/meu-perfil/amigos/solicitar-amizade',
        type: "POST",
        data: { 'public_id' : slug },
        dataType: "json",
        success: function(data){
            if(data.status) {
                $(bt).remove();
                if(data.update == undefined) {
                    $(p).html('A solicitação foi enviada para a usuária, aguarde a liberação de acesso.');
                    $('.atividades .box-adicionar').empty().html('<div class="esperando-liberacao"><span></span>Aguarde a liberação</a></div>');
                } else {
                    location.reload();
                }
            } else {
                $(p).html('Ocorreu um erro! Tente mais tarde.');
                setMsgTimeoutFriends();
            }
        }
    });
});

function setMsgTimeoutFriends() {
    var msg = $('.botShadow');
    msg.fadeOut( 5000, function() {
        msg.remove();
    });
}

/* Recados Especiais */
$('#form-recados-especias').validate({
    rules: {
        specialname: {
            required: true
        },
        specialemail: {
            required: true,
            email: true
        },
        specialmsg: {
            required: true
        }
    },
    errorPlacement: function(error, element) {},
    invalidHandler: function( form ) {
        var div = document.createElement('div'),
            span = document.createElement('span'),
            msg = $('#form-recados-especias li .msg');

        $(span).addClass('erro').text('Preencha os campos marcados em vermelho.');
        $(msg).append(span);
        specialMsgTimeout();
    },
    submitHandler: function( form ) {
        var div = document.createElement('div'),
            span = document.createElement('span'),
            msg = $('#form-recados-especias li .msg');

        $.ajax({
            url: $(form).attr('action'),
            type: 'POST',
            data: $(form).serialize(),
            dataType: 'json',
            success: function(data) {
                if(undefined !== data.status) {
                    $('#form-recados-especias')[0].reset();
                    $(span).addClass('sucesso').text('Sua mensagem foi enviada. Aguarde a liberação da mamãe.');
                } else {
                    $(span).addClass('erro').text('Preencha os campos marcados em vermelho.');
                }
            }
        });
        $(msg).append(span);
        specialMsgTimeout();
    }
});

function specialMsgTimeout() {
    var msg = $('#form-recados-especias li .msg span');
    msg.fadeOut( 5000, function() { msg.remove(); });
}

/* Voted Poll */
$(document).on('submit', '#enquete-form', function(e) {
    e.preventDefault();

    $.ajax({
        url: baseUri + '/meu-perfil/enquete/votar',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
            if(data.status) {
                updatePoll(data.poll, data.vote);
            }
        }
    });
});

function updatePoll(poll, vote) {
    var ul = $('#enquete-form ul'),
        button = $('#voted-enquete');

    ul.html('');
    button.remove();

    $.each(poll, function(key, val) {
        var li = document.createElement('li'),
            label = document.createElement('label'),
            barra = document.createElement('div'),
            mascara = document.createElement('div'),
            porcentagem = document.createElement('div'),
            span = document.createElement('span'),
            por = Math.round(val.total*100/vote);

        $(barra).addClass('barra');
        $(mascara).addClass('mascara');
        $(porcentagem).addClass('porcentagem').css({ 'width' : por+'%' });

        $(label).html(val.answer);
        $(span).html(por+'%');

        $(barra).append(mascara);
        $(barra).append(porcentagem);

        $(li).append(label);
        $(li).append(barra);
        $(li).append(span);

        $(ul).append(li);
    });
}

/* Visitantes */
$('form#public-convite').validate({
    rules: {
        visitantes_nome: {
            required: true
        },
        visitantes_sobrenome: {
            required: true
        },
        visitantes_email: {
            required: true,
            email: true
        }
    },
    errorPlacement: function(error, element) {},
    invalidHandler: function( form ) {
        visitantesMsg("Preencha os campos marcados em vermelho.");
        visitantesMsgTimeout();
    },
    submitHandler: function(form) {
        var params = $(form).serialize();
        CallMethod($(form).attr('action'),'POST',params,'','',successCadastroVisitante);
        $(form)[0].reset();
    }
});

function successCadastroVisitante(data) {
    if(data.status !== undefined) {
        visitantesMsg("a solicitação foi enviada para a usuária, aguarde a liberação de acesso.");
        visitantesMsgTimeout();
    }
}

function visitantesMsg(msg) {
    var p = document.createElement('p');
    var container = $('.cadastro-visitante .floatLeft');

    $(p).addClass('msg');
    $(p).html(msg);

    container.append(p);
}

function visitantesMsgTimeout() {
    var msg = $('.cadastro-visitante .floatLeft p.msg');
    msg.fadeOut( 5000, function() {
        msg.remove();
    });
}