<p>
    <strong>Você buscou:</strong>
    @if(!is_null($terms['q']))
        PELO NOME: "{{ $terms['q'] }}"
    @endif
    @if(!is_null($terms['father_name']))
        PELO PAI: "{{ $terms['father_name'] }}"
    @endif
    @if(!is_null($terms['state_id']))
        PELO ESTADO: "{{ $terms['state_name'] }}"
    @endif
</p>
@if(count($search) > 0)
    <ul class="lista-resultado">
        @foreach($search as $s)
        <li>
            <div class="thumb"><a href="{{ $s->slug }}"><img src="{{ $s->profile_picture['small'] }}" /></a></div>
            <div class="description">
                <h4><a href="{{ $s->slug }}">{{ $s->name }}</a></h4>
                <p>{{ $s->state }}</p>
            </div>
        </li>
        @endforeach
    </ul>
    {{ $search->appends($terms)->links() }}
@else
    <p class="nao-encontrado">Nenhum resultado encontrado</p>
@endif