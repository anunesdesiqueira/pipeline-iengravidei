@extends('layouts.master')

@section('content')
<div class="holder">
    @include('components.cabecalho.header_active')
    <div class="mensagem">
        <aside>
            <h2 name="mensagens">Mensagens</h2>
            <div class="busca">
                {{ Form::open(array('id' => 'form-mensages-search')) }}
                    {{ Form::text('pesquisar', '', array('placeholder' => 'PESQUISAR')) }}
                    {{ Form::submit('', array('class' => 'bt-buscar')) }}
                {{ Form::close() }}
            </div>
            <div class="resultado-busca">
                @include('components.mensagens.friends')
            </div>
        </aside>

        @if(isset($profileFrom))
            <section class="lista-mensagens">
                <h2>{{ $profileFrom->name }}</h2>
                <a href="#" class="excluir-todas">Excluir todas</a>
                <div class="resultado-mensagens">
                    @include('components.mensagens.results')
                </div>
            </section>

            <section>
                {{ Form::open(array('id' => 'form-mensages-send')) }}
                    {{ Form::hidden('public', $publicId) }}
                    {{ Form::hidden('acao', 'lista') }}
                    <label>Escreva sua mensagem:</label>
                    {{ Form::textarea('body') }}
                    {{ Form::submit('Enviar', array('class' => 'bt_enviar_mensagem')) }}
                {{ Form::close() }}
            </section>
        @endif
    </div>
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/mensages.js') }}
    {{ HTML::script('assets/js/enquete-logged.js') }}
@stop