<?php

use KraftHaus\Bauhaus\Admin;

class GiftlistsProductsAdmin extends Admin
{

    public function configureList($mapper)
    {
        $mapper->identifier('name');
        $mapper->identifier('description');
        $mapper->identifier('price');
    }

    public function configureForm($mapper)
    {
        $mapper->text('name');
        $mapper->text('description');
        $mapper->number('price')->decimals('2')->separators(['.', ',']);
        $mapper->file('picture')->location('assets/dicas_profissionais')->naming('random');
        $mapper->select('status')->options([1 => 'Ativo', 0 => 'Inativo']);
    }

    public function configureFilters($mapper)
    {
        // ...
    }

}