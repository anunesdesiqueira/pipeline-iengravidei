<p>Convite Escolhido</p>
<ul class="convite-templates">
    <li>
        <label>
            <div class="thumb">
                <img src="{{asset('assets/email/'.$template->preview)}}" />
            </div>
        </label>
    </li>
</ul>
<a href="" data-href="{{ $template->preview }}" class="visualizar-convite-escolhido"><span class="ico-visualizar">&nbsp;</span>Visualizar</a>