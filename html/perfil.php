<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<section class="content">
	<div class="holder">
	
		<!--## CABECALHO 1 ##-->
		<div class="cabecalho">
			
			<div class="foto"><img src="img/marcacao-perfil.jpg" /></div>
			
			<div class="atividades">
				<h2>Ana Paula Morais</h2>
				<a href="#" class="ver-perfil"><span>&nbsp;</span>Ver Perfil</a>
				<ul class="listaBotoes">
					<li><a href="#" class="editar"><span>&nbsp;</span>Editar Perfil</a></li>
					<li><a href="#" class="recados"><span>&nbsp;</span>Recados Especiais</a></li>
					<li><a href="#" class="eventos"><span>&nbsp;</span>Eventos</a></li>
					<li><a href="#" class="album"><span>&nbsp;</span>Albúm de Fotos</a></li>
					<li><a href="#" class="lista-amigas"><span>&nbsp;</span>Lista de Amigas</a></li>
					<li><a href="#" class="mensagens"><span>&nbsp;</span>Mensagens</a></li>
					<li><a href="#" class="presentes"><span>&nbsp;</span>Lista de Presentes</a></li>
					<li><a href="#" class="notificacoes"><span>&nbsp;</span>Notificafacões</a></li>
				</ul>
			</div>
			
			<!-- Enquete -->
			<div class="enqueteBox">
				<h3>Enquete</h3>
				<p>Estou na dúvida de qual nome colocar, qual vocês preferem?</p>
				<ul>
					<li>
						<label>ALEX</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:50%;"></div>
						</div>
						<span>50%</span>
					</li>
					<li>
						<label>JOÃOZINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:25%;"></div>
						</div>
						<span>25%</span>
					</li>
					<li>
						<label>PEDRINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:10%;"></div>
						</div>
						<span>10%</span>
					</li>
					<li>
						<label>ASTROGILDO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:15%;"></div>
						</div>
						<span>15%</span>
					</li>
				</ul>				
				<input type="submit" value="Vote" />
				<button>Ver Resultado</button>
			</div>
			<!-- End: Enquete -->
			
			<!-- Compartilhe -->
			<div class="clearfix"></div>
			<a href="#" class="compartilhe">Compartilhe</a>
			<!-- End: Compartilhe -->
		</div>
		<!--## END: CABECALHO 1 ##-->
		
		<!--### Babyline ###-->
		<div class="baby-line">
		
			<h2 title="Baby Line">Baby Line</h2>
			
			<div class="addFoto">
				<div class="fileName">Insira nova foto</div>
				<div class="uploadHolder">
					<div class="mascara"></div>
					<input type="file" />
				</div>
				<div class="comboHolder">
					<div class="mascara"><div class="optName">Escolha a semana</div></div>
					<select>						
						<option value="1ª Semana">1ª Semana</option>
						<option value="2ª Semana">2ª Semana</option>
						<option value="3ª Semana">3ª Semana</option>
					</select>
				</div>
				<input type="submit" value="Enviar" />
				<div class="clearfix"></div>
				<div class="msg sucess">Imagem enviada com sucesso</div>
				<!--<div class="msg error">Ops! Ocorreu um erro no envio, tente novamente</div>-->
			</div>
			
			<div id="carouselBabyLine" class="edit">
				<a class="buttons next" href="#">right</a>
				<a class="buttons prev" href="#">left</a>				
				<div class="viewport">
					<ul class="overview">
						<li rel="foto1">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>1ª Semana</p>
						</li>
						<li rel="foto2">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>2ª Semana</p>
						</li>
						<li rel="foto3">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>3ª Semana</p>
						</li>
						<li rel="foto4">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>4ª Semana</p>
						</li>
						<li rel="foto5">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>5ª Semana</p>
						</li>
						<li rel="foto6">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>6ª Semana</p>
						</li>
						<li rel="foto7">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>7ª Semana</p>
						</li>
						<li rel="foto8">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>8ª Semana</p>
						</li>
						<li rel="foto9">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>9ª Semana</p>
						</li>
						<li rel="foto10">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>10ª Semana</p>
						</li>
						<li rel="foto11">
							<a href="#" class="delete">Deletar</a>
							<div class="thumb">
								<a href="#"><img src="img/marcacao-baby-line.jpg" /></a>
							</div>
							<p>11ª Semana</p>
						</li>						
					</ul>
				</div>
			</div>
			
		</div>
		<!--### End: Babyline ###-->
		
		<!--### Lista de Presentes ###-->
		<div class="listaPresentes">
			<h2 title="Lista de Presentes">Lista de Presentes</h2>			
			<ul>
				<li>
					<div class="thumb"><img src="img/marcacao-presentes.jpg" /></div>
					<p>Descrição do produto Máximo Duas Linhas</p>
					<span>R$100,00</span>
				</li>
				<li>
					<div class="thumb"><img src="img/marcacao-presentes.jpg" /></div>
					<p>Descrição do produto Máximo Duas Linhas</p>
					<span>R$100,00</span>
				</li>
				<li>
					<div class="thumb"><img src="img/marcacao-presentes.jpg" /></div>
					<p>Descrição do produto Máximo Duas Linhas</p>
					<span>R$100,00</span>
				</li>
				<li>
					<div class="thumb"><img src="img/marcacao-presentes.jpg" /></div>
					<p>Descrição do produto Máximo Duas Linhas</p>
					<span>R$100,00</span>
				</li>
			</ul>
			<div class="deSeuPresente">
				<p>Dê agora seu presente!</p>
				<a href="#">clique aqui</a>
			</div>
		</div>
		<!--### End: Lista de Presentes ###-->
		
		<!--### Album de Fotos do Perfil ###-->
		<div class="album-fotos perfil">
			<h2 title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>
			<div class="clearfix"></div>
			<ul class="lista-album">
				<li>				
					<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
					<a href="#" class="nome">Nome do álbum</a>
				</li>
				<li>				
					<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
					<a href="#" class="nome">Nome do álbum</a>
				</li>
				<li>
					<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
					<a href="#" class="nome">Nome do álbum</a>
				</li>
			</ul>
			<a href="#" class="verAlbuns">confira todos os albuns</a>
		</div>
		<!--### End: Album de Fotos do Perfil ###-->
		
		<!--### Mural Não Logado ###-->
		<div class="mural-nao-logado">			
			<h2 title="Mural"><span>Mural</span></h2>
			<ul>
				<li>
					<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
					<div class="description">
						<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Ut eget sapien vel lectus congue faucibus ac ut dolor. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
						<div class="media">
							<iframe width="350" height="245" src="//www.youtube.com/embed/yJR-artQVuI" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</li>
				<li>
					<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
					<div class="description">
						<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
						<div class="media">
							<img src="img/marcacao-mural.jpg" />								
						</div>
					</div>
				</li>
				<li>
					<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
					<div class="description">
						<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
					</div>
				</li>
				<li>
					<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
					<div class="description">
						<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
					</div>
				</li>
				<li>
					<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
					<div class="description">
						<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
					</div>
				</li>
				<li>
					<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
					<div class="description">
						<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
					</div>
				</li>
			</ul>			
		</div>
		<!--### End: Mural Não Logado ###-->
		
		<!--### Recados Especiais ###-->
		<div class="recados-especiais">			
			<h2 title="Recados Especiais"><span>Recados Especiais</span></h2>			
			<p>Envie um recadinho para a mamãe você também:</p>
			<ul class="formRecados">
				<li class="floatLeft"><input type="text" placeholder="nome" class="nome" /></li>
				<li><input type="text" placeholder="e-mail" class="email" /></li>
				<li><textarea placeholder="mensagem"></textarea></li>
				<li>
					<div class="msg">
						<span class="sucesso">Sua mensagem foi enviada. Aguarde a liberação da mamãe.</span>
						<!--<span class="erro">Preencha os campos marcados em vermelho.</span>-->
					</div>
					<input type="submit" value="Enviar" />
				</li>
			</ul>
			<ul class="listaRecadosEspeciais">
				<li>
					<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				</li>
				<li>
					<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
				</li>
				<li>
					<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.</p>
				</li>
				<li>
					<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet</p>
				</li>
			</ul>
		</div>
		<!--### End: Recados Especiais ###-->
		
		<div class="clearfix"></div>
		
		<!--### Mural Logado ###-->
		<div class="mural-logado">			
			<h2 title="Mural"><span>Mural</span></h2>
			
			<div class="holderForm">
				<h3>Envie seu comentário:</h3>
				
				<div class="envio">
					<label>Envie:</label>
					<div class="holderFoto">
						<div class="mascara"></div>
						<input type="file" rel="fileMural" />
					</div>
					<a href="#" class="showForm">Video</a>
				</div>
				
				<div class="clearfix"></div>				
				
				<div class="fileName"></div>
				
				<div class="ytUrl">
					<input type="text" placeholder="Copie e cole aqui o link do video do Youtube" />
				</div>
				
				<textarea></textarea>
				
				<ul>
					<li>Compartilhar com:</li>
					<li><label><input type="checkbox" /> Todas as mamães</label></li>
					<li><label><input type="checkbox" /> Amigas</label></li>
					<li><label><input type="checkbox" /> Facebook</label></li>
					<li><input type="submit" value="Enviar" /></li>
				</ul>
			</div>
			
			<div class="clearfix"></div>			
			
			<div class="tab">
				<div class="active" rel="todas">Todas as mamães</div>
				<div rel="amigas">Amigas</div>
			</div>
			
			<!-- Todas as mamães -->
			<div class="tabList todas active">
				<div class="busca">
					<input type="text" placeholder="BUSQUE POR" />
					<input type="submit" />
				</div>
				<ul>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Ut eget sapien vel lectus congue faucibus ac ut dolor. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
							
							<div class="media">
								<img src="img/marcacao-mural.jpg" />								
							</div>
							
							<div class="control">
								<a href="#">Compartilhar</a> 
								| 
								<a href="#">Excluir</a>
							</div>
							
						</div>
					</li>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
							
							<div class="media">
								<img src="img/marcacao-mural.jpg" />								
							</div>
							
						</div>
					</li>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
						</div>
					</li>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
						</div>
					</li>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
						</div>
					</li>
				</ul>
			</div>
			<!-- End: Todas as mamães -->	
			
			<!-- Amigas -->
			<div class="tabList amigas">
				<div class="busca">
					<input type="text" placeholder="BUSQUE POR" />
					<input type="submit" />
				</div>
				<ul>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Ut eget sapien vel lectus congue faucibus ac ut dolor. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
							
							<div class="media">
								<iframe width="350" height="245" src="//www.youtube.com/embed/yJR-artQVuI" frameborder="0" allowfullscreen></iframe>
							</div>
							
							<div class="control">
								<a href="#">Compartilhar</a> 
								| 
								<a href="#">Excluir</a>
							</div>
							
						</div>
					</li>
					<li>
						<div class="thumb"><img src="img/foto-mensagem-marcacao.jpg" /></div>
						<div class="description">
							
							<h4>Ana Paula Moraes <span>12/12 às 4:36pm</span></h4>
							
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendu.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor.</p>
							
							<div class="media">
								<iframe width="350" height="245" src="//www.youtube.com/embed/0Wecy-IgYaw" frameborder="0" allowfullscreen></iframe>
							</div>
							
						</div>
					</li>
				</ul>
			</div>			
			<!-- End: Amigas -->
			
		</div>
		<!--### End: Mural Logado ###-->
		
		<!--### Dicas Profissionais ###-->
		<div class="dicas-profissionais">
			
			<h2 title="Dicas Profissionais"><span>Dicas Profissionais</span></h2>
			
			<p>Dicas sobre a gestação, cuidados e melhores práticas</p>
			
			<div class="busca">
				<input type="text" />
				<input type="submit" />
			</div>
			
			<ul>
				<li>
					<img src="img/marcacao-dicas-foto1.jpg" />
					<h3>Como carregar seu bebê</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan. In accumsan vulputate...Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/><br/>Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan. In accumsan vulputate...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus</p>
					<span>postado por: dr. joão feliz</span>
				</li>				
				<li>
					<img src="img/marcacao-dicas-foto2.jpg" />
					<h3>Pediatra Alerta</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan. In accumsan vulputate...Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/><br/>Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan. In accumsan vulputate...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus</p>
					<span>postado por: dr. joão feliz</span>
				</li>
				<li class="produto">
					<img src="img/marcacao-dicas-foto4.jpg" />
					<h3>Produtos para o seu bebê</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan. In accumsan vulputate...Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/><br/>Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan. In accumsan vulputate...Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus</p>
					<span>postado por: dr. joão feliz</span>
				</li>
				<li>
					<img src="img/marcacao-dicas-foto3.jpg" />
					<h3>Pediatra Alerta</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget sapien vel lectus congue faucibus ac ut dolor. Pellentesque bibendum ligula nec nisi sodales accumsan.</p>
					<span>postado por: dr. joão feliz</span>
				</li>
				<li class="ativacao">
					<p>Quer ler todas as dicas dos profissionais?</p>
					<a href="#">Ative Agora</a>
				</li>
			</ul>
			
		</div>
		<!--### End: Dicas Profissionais ###-->
		
		<div class="clearfix"></div>
		
		<!--### Adicione como Amiga ###-->
		<div class="adicione-amiga">
			<div class="holderAdd">
				<div class="verMais">Quer ver mais?</div>
				<div class="floatLeft">
					<h6>Adicione como amiga <span>&nbsp;</span></h6>
					<p>Solicite abaixo liberação para acessar esse perfil completo.</p>
					<a href="#">Clique aqui</a>
				</div>
			</div>
			<div class="botShadow">
				<p>a solicitação foi enviada para a usuária, aguarde a liberação de acesso.</p>
			</div>
		</div>
		<!--### End: Adicione como Amiga ###-->
		
		<!--### Cadastre-se você também ###-->
		<div class="cadastre-vc-tambem">
			<div class="holderAdd">
				<div class="gostou">Gostou</div>
				<div class="floatLeft">
					<h6>Cadastre-se você também</h6>
					<p>Tenha acesso a essas e outras informações deste perfil!</p>
					<a href="#">Clique aqui</a>
				</div>
			</div>
			<div class="botShadow"></div>
		</div>
		<!--### End: Cadastre-se você também ###-->
		
		<!--### Cadastro Visitante ###-->
		<div class="cadastro-visitante">
			<div class="holderAdd">
				<div class="gostou">Quer ver mais?</div>
				<div class="floatLeft">
					<h6>Cadastro de visitante</h6>
					<p>Solicite abaixo a liberação para acessar esse perfil completo<br/>(fotos, mural,votar na enquete e muito mais).</p>
					<ul>
						<li><label>Nome:&nbsp;&nbsp; <input type="text" /></label></li>
						<li><label>Sobrenome: <input type="text" /></label></li>
						<li><label>E-mail: <input type="text" class="email" /></label></li>
						<li><input type="submit" value="CADASTRAR" /></li>
					</ul>
					<p class="msg">a solicitação foi enviada para a usuária, aguarde a liberação de acesso.</p>
				</div>				
			</div>
			<div class="botShadow"></div>
		</div>
		<!--### End: Cadastro Visitante ###-->
		
		<!--### Lista de Serviços do Perfil ###-->		
		<div class="lista-servicos-perfil">
			<ul>
				<li>
					<div class="thumb lista-presentes"></div>
					<h6 class="presentes">Lista de Presentes</h6>
					<p>Crie sua lista de presentes e receba tudo sem sair do site! Facilite a sua vida e de seus amigos! </p>
				</li>
				<li>
					<div class="thumb criador-eventos"></div>
					<h6 class="eventos">Criador de Eventos</h6>
					<p> Chá de Bebê, Chá de Fralda, Nascimento ou o que você quiser! Avise a todos e crie listas de presentes!</p>
				</li>
				<li>
					<div class="thumb meu-mural"></div>
					<h6 class="mural">Meu Mural</h6>
					<p>Aqui é o lugar para postar e guardar para sempre seus momentos especiais. Vídeo do ultrassom, sentimentos do dia-a-dia e ainda pode compartilhar tudo em suas redes sociais. </p>
				</li>
				<li>
					<div class="thumb album-fotos"></div>
					<h6 class="fotos">Álbum de Fotos</h6>
					<p>Crie álbuns exclusivos para guardar os momentos desses meses tão inesquecíveis!</p>
				</li>
			</ul>
			<a href="#"><span>&nbsp;</span>Confira lista completa</a>
		</div>		
		<!--### End: Lista de Serviços do Perfil ###-->
		
		<div class="cadastre-tambem">
			<h5>Cadastre-se você também.</h5>
			<p>Na 1ª Rede Social para Gestantes do mundo!</p>
			<a href="#">CLIQUE AQUI</a>
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>