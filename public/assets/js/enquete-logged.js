$(function() {
    //Init
    loadPoll();
});

function loadPoll() {
    if($('div').hasClass('enqueteBox')) {
        CallMethod(baseUri + '/meu-perfil/enquete/show','GET','','','',successLoadPoll);
    }
}

function successLoadPoll(data) {
    if(data.page != undefined) {
        var container = $('.enqueteBox');
        container.empty().html(data.page);
    }
}