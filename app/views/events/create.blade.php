@extends('layouts.master')

@section('content')
<div class="holder">
    @include('components.cabecalho.header_active')
    <h2 name="eventos" class="eventos-title" title="Crie seu evento"><span>Crie seu evento</span></h2>
    <div class="eventos">
        @include('components.eventos.listar')
        @include('components.eventos.create')
    </div>
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/events.js') }}
    {{ HTML::script('assets/js/enquete-logged.js') }}
@stop