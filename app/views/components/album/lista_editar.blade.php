@foreach ($photos as $photo)
    <li>
        <a href="#" rel="{{ $photo->id }}" class="excluir delete-photo">Excluir</a>
        <div class="thumb">
            <a href="{{ $photo->picture['original'] }}">
                <img src="{{ $photo->picture['main'] }}" />
            </a>
        </div>
    </li>
@endforeach