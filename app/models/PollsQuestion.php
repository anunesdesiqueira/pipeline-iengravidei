<?php

class PollsQuestion extends Eloquent {

    protected $table = 'polls_questions';
    protected $guarded = array('id', 'id');

    public static function criar_relacionamento($id_polls, $ids_questions)
    {
        $total = count($ids_questions);

        for($i=0; $i<$total; $i++)
        {
            $polls_questions = PollsQuestion::create(array('poll_id' => $id_polls, 'question_id' => $ids_questions[$i]));
        }

        return $polls_questions;
    }
}