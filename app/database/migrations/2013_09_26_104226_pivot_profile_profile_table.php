<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotProfileProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profile_profile', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('who_asked')->unsigned()->index();
			$table->integer('who_accepted')->unsigned()->index();
            $table->boolean('actived')->default(0);
			$table->foreign('who_asked')->references('id')->on('profiles')->onDelete('cascade');
			$table->foreign('who_accepted')->references('id')->on('profiles')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profile_profile');
	}

}
