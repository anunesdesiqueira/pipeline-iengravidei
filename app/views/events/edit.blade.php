@extends('layouts.master')

@section('content')
<div class="holder">
    @include('components.cabecalho.header_active')
    <h2 class="eventos-title" title="Crie seu evento"><span>Crie seu evento</span></h2>
    <div class="eventos">
        @include('components.eventos.listar')
        @include('components.eventos.edit')
    </div>

</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/events.js') }}
@stop