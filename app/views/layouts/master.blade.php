<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>IEngravidei</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    @yield('facebook_metas')
</head>

<body {{ isset($layout) && is_array($layout) ? $layout['background'] : "" }}>

@include('components.facebook')

<div class="background"></div>

<!-- Login -->
<div class="login">
    <div class="holder">
        @include($includeTop)
    </div>
</div>
<!-- Login -->

<header>
    <div class="search">
    {{ Form::open(array('route' => 'search', 'method' => 'GET', 'enctype' => 'multipart/form-data')) }}
        {{ Form::text('q', Input::old('q'), array('placeholder' =>'Procure pela Mamãe:')) }}
        {{ Form::submit() }}
    {{ Form::close() }}
    </div>
    <nav>
        <ul>
            <li class="home{{ 'home' === Route::currentRouteName() || 'user.my_profile' === Route::currentRouteName() ? ' active' : '' }}">
                <div class="marcacao"></div>
                {{link_to_route('home', 'Home')}}
            </li>
            <li class="sobre{{ 'sobrenos' === Route::currentRouteName() ? ' active' : '' }}">
                <div class="marcacao"></div>
                {{link_to_route('sobrenos', 'Sobre Nós')}}
            </li>
            <li class="planos{{ 'planos' === Route::currentRouteName() ? ' active' : '' }}">
                <div class="marcacao"></div>
                {{link_to_route('planos', 'Planos')}}
            </li>
            <li class="servicos{{ 'services' === Route::currentRouteName() ? ' active' : '' }}">
                <div class="marcacao"></div>
                {{link_to_route('services', 'Serviços')}}
            </li>
            <li class="contato{{ 'contato' === Route::currentRouteName() ? ' active' : '' }}">
                <div class="marcacao"></div>
                {{link_to_route('contato', 'Contato')}}
            </li>
        </ul>
    </nav>
</header>

@yield('cadastro')

<section class="content">
    @yield('content')
</section>

<footer>
    <div class="holder">
        <ul class="nav">
            <li>{{link_to_route('home', 'Home')}}</li>
            <li>{{link_to_route('sobrenos', 'Sobre Nós')}}</li>
            <li>{{link_to_route('planos', 'Planos')}}</li>
            <li>{{link_to_route('services', 'Serviços')}}</li>
            <li>{{link_to_route('contato', 'Contato')}}</li>
        </ul>
        <div class="sobrenos">
            <div class="thumb"></div>
            <div class="description">
                <p>Somos uma comunidade apaixonada pela maternidade. Pelo indescritível e sublime estado em que ficamos quando estamos esperando nosso filho ou nossa filha!<br/>Compartilhe, emocione-se, converse, aprenda, ensine e viva intensamente esse momento. Estamos juntos com você para a contagem regressiva.</p>
            </div>
        </div>
        <div class="contatos">
            <a href="#">Quer ser nosso parceiro?</a>
            <a href="#">Entre em contato?</a>
            <ul>
                <li>{{link_to_route('denuncie', 'Denúncie')}}</li>
                <li>|</li>
                <li>{{link_to_route('termosdeuso', 'Termos de uso')}}</li>
                <li>|</li>
                <li><a href="#" class="facebook">Facebook</a></li>
            </ul>
        </div>
        <p class="disclaimer">I-Engravidei é uma marca registrada - iengravidei  Inc.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copyright © 2013-2013 I-engravidei</p>
    </div>
</footer>
@yield('jsconfig')
    <script type="text/javascript">
        var baseUri = '{{ url() }}';
        var publicId = '{{ isset($publicId)? $publicId : "" }}';
    </script>

    {{ HTML::script('assets/js/main.js') }}

    @if(Auth::client()->check())
        {{ HTML::script('assets/js/enquete-logged.js') }}
    @endif

    @if(Session::has('login_errors'))
        <script type="text/javascript">
            $(function () {
                $.magnificPopup.open({
                    items: [{
                        src:'<div class="popup-presentes-confirm">' +
                            '<ul class="modal-errors">' +
                            '<li>*E-mail ou senha incorretos</li>' +
                            '</ul>' +
                            '</div>',
                        type: 'inline'
                    }]
                });
            });
        </script>
    @endif

@yield('customjs')
</body>
</html>