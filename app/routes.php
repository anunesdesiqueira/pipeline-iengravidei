<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Route links geral
 */
Route::get('/', array(
    'as'    => 'home',
    'uses'  => 'HomeController@index'
));

Route::get('sobre-nos', array(
    'as'    => 'sobrenos',
    'uses'  => 'InstitutionalController@sobre_nos'
));

Route::get('planos', array(
    'as'    => 'planos',
    'uses'  => 'InstitutionalController@planos'
));

Route::get('termos-de-uso', array(
    'as'    => 'termosdeuso',
    'uses'  => 'InstitutionalController@termos_de_uso'
));

Route::get('denuncie', array(
    'as'     => 'denuncie',
    'uses'   => 'DenunciationController@index'
));

Route::post('denuncie', array(
    'as'     => 'post.denuncie',
    'uses'   => 'DenunciationController@store'
));

Route::get('contato', array(
    'as'    => 'contato',
    'uses'  => 'ContactsController@index'
));

Route::get('servicos', array(
    'as'     => 'services',
    'uses'   => 'ServicesController@index'
));

Route::post('cadastro', array(
    'as'     => 'register.store',
    'uses'   => 'RegisterController@store'
));

Route::get('confirmar-cadastro/{code?}', array(
    'as'   => 'register.confirm',
    'uses' => 'RegisterController@confirm'
))->where('code', '[A-Za-z0-9]+');

Route::post('login', array(
    'as'     => 'userauth.login',
    'uses'   => 'UserAuthController@login'
));

Route::get('logout', array(
    'as' => 'userauth.logout',
    'uses' => 'UserAuthController@logout'
));

Route::get('facebook', function() {
    $facebook = new Facebook(Config::get('iengravidei.facebook'));
    $params = array(
        'redirect_uri' => url('facebook/callback'),
        'scope' => 'email',
    );
    return Redirect::to($facebook->getLoginUrl($params));
});

Route::any('facebook/callback', array(
    'as'    => 'register.facebook_callback',
    'uses'  => 'RegisterController@facebook'
));

Route::any('recuperar-senha', array(
    'as'    => 'userauth.request',
    'uses'  => 'UserAuthController@request'
));

Route::get('recuperar-senha/{token}', array(
    'as'    => 'userauth.reset',
    'uses'  => 'UserAuthController@reset'
));

Route::get('buscar', array(
    'as'    => 'search',
    'uses'  => 'SearchController@search'
));

/**
 * Route perfil public
 */
Route::get('/{publicId}', array(
    'as' => 'user.public',
    'uses' => 'ProfileController@publicProfile'
))->where('publicId', '[A-Za-z0-9]+');

Route::any('/{publicId}/lista-de-presentes', array(
    'as' => 'user.public.gifts',
    'uses' => 'GiftListsController@publicList'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::post('/{publicId}/lista-de-presentes/cart', array(
    'as' => 'user.public.giftsCart',
    'uses' => 'GiftListsController@publicCreateCart'
))->where(array('publicId' => '[A-Za-z0-9]+', 'slug' => '[A-Za-z0-9]+'));

Route::post('/{publicId}/lista-de-presentes/cart/remove', array(
    'as' => 'user.public.giftsCartRemove',
    'uses' => 'GiftListsController@publicRemoveItenCart'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::any('/{publicId}/lista-de-presentes/payment', array(
    'as' => 'user.public.giftsCartPayment',
    'uses' => 'PaymentController@publicPaymentGifts'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::any('/{publicId}/lista-de-presentes/success', array(
    'as' => 'user.public.giftsCartPaymentSuccess',
    'uses' => 'PaymentController@ExecutePaymentSuccess'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::any('/{publicId}/lista-de-presentes/cancel', array(
    'as' => 'user.public.giftsCartPaymentSuccess',
    'uses' => 'PaymentController@ExecutePaymentCancel'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::get('/{publicId}/albuns', array(
    'as'     => 'show_my_album',
    'uses'   => 'AlbumsController@show'
))->where(array('publicId' => '[A-Za-z0-9]+', 'slug' => '[A-Za-z0-9]+'));

Route::get('/{publicId}/albuns/{slug}', array(
    'as'     => 'show_my_album_photos',
    'uses'   => 'AlbumsController@showAlbum'
))->where(array('publicId' => '[A-Za-z0-9]+', 'slug' => '[A-Za-z0-9]+'));

Route::get('/{publicId}/mural', array(
    'as'     => 'show_mural',
    'uses'   => 'MuralController@listaStatusPublic'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::get('/{publicId}/mural/{hash}', array(
    'as'     => 'show_mural.status',
    'uses'   => 'ProfileController@publicProfile'
))->where(array('publicId' => '[A-Za-z0-9]+', 'hash' => '[A-Za-z0-9]+'));

Route::get('/{publicId}/recados', array(
    'as'     => 'show_recados',
    'uses'   => 'SpecialMessagesController@recadosPublic'
))->where(array('publicId' => '[A-Za-z0-9]+'));

Route::post('meu-perfil/enquete/votar', array(
    'as'     => 'poll.vote',
    'uses'   => 'PollController@vote'
));

Route::post('meu-perfil/visitantes/solicitar', array(
    'as'    => 'visitantes.solicitar',
    'uses'  => 'VisitantesController@solicitar'
));

/**
 * Routes logados
 */
Route::group(array('before' => 'auth.profile'), function() {

    Route::get('publicar-perfil', array(
        'as' => 'user.published',
        'uses' => 'RegisterController@published'
    ));

    Route::get('meu-perfil', array(
        'as'    => 'user.my_profile',
        'uses'  => 'ProfileController@myProfile'
    ));

    Route::get('meu-perfil/editar', array(
        'as'    => 'user.edit_profile',
        'uses'  => 'ProfileController@editProfile'
    ));

    Route::post('meu-perfil/editar/send', array(
        'as'    => 'user.edit_profile_send',
        'uses'  => 'ProfileController@postEditProfile'
    ));

    Route::post('meu-perfil/editar/password', array(
        'as'    => 'user.edit_profile_password',
        'uses'  => 'ProfileController@postEditPassword'
    ));

    Route::post('meu-perfil/editar/excluir', array(
        'as'    => 'user.edit_profile_excluir',
        'uses'  => 'ProfileController@excluir'
    ));

    Route::any('complete-seu-perfil', array(
        'as'    => 'register.complete',
        'uses'  => 'RegisterController@complete'
    ));

    Route::get('escolha-um-plano', array(
        'as'    => 'plan',
        'uses'  => 'RegisterController@plan'
    ));

    Route::get('escolha-um-plano/{plan}', array(
        'as'    => 'plan.register',
        'uses'  => 'RegisterController@planRegister'
    ));

    Route::get('ajax/cidades/{id?}', array(
        'as'   => 'ajax.get_cities',
        'uses' => 'RegisterController@getCities'
    ))->where('id', '[0-9]+');

    Route::get('ajax/verificar-usuario/{publicId?}', array(
        'as'   => 'ajax.check_username',
        'uses' => 'RegisterController@isPublicIdAvailable'
    ))->where('publicId', '[A-Za-z0-9]+');

    Route::get('notificacoes', array(
        'as'    => 'notifications',
        'uses'  => 'NotificationController@index'
    ));

    Route::any('meu-perfil/personalizar', array(
        'as'    => 'user.personalize',
        'uses'  => 'ProfileController@personalize'
    ));

    // Mensagens
    Route::get('meu-perfil/mensagens/{publicId?}', array(
        'as'   => 'messages',
        'uses' => 'MessagesController@show'
    ))->where('publicId', '[A-Za-z0-9]+');

    Route::post('meu-perfil/mensagens/enviar', array(
        'before' => 'csrf',
        'as'   => 'messages.send',
        'uses' => 'MessagesController@postSend'
    ));

    Route::post('meu-perfil/mensagen/excluir', array(
        'as'   => 'messages_del',
        'uses' => 'MessagesController@delMsg'
    ));

    Route::post('meu-perfil/mensagen/excluir/all', array(
        'as'   => 'messages_delAll',
        'uses' => 'MessagesController@delMsgAll'
    ));

    Route::get('meu-perfil/search/mensagens/amigas', array(
        'as'   => 'messages_search',
        'uses' => 'MessagesController@search'
    ));

    /* Eventos */
    Route::get('meu-perfil/eventos', array(
        'as'     => 'events.create',
        'uses'   => 'EventsController@create'
    ));

    Route::post('meu-perfil/eventos/crie-seu-evento', array(
        'before' => 'csrf',
        'as'     => 'events.create.post',
        'uses'   => 'EventsController@postCreate'
    ));

    Route::get('meu-perfil/eventos/lista-presentes', array(
        'as'     => 'events.create.listar',
        'uses'   => 'EventsController@refreshListar'
    ));

    Route::get('meu-perfil/eventos/lista-amigas', array(
        'as'     => 'events.create.amigas',
        'uses'   => 'EventsController@refreshFriends'
    ));

    Route::get('meu-perfil/eventos/editar/{hash}', array(
        'as'     => 'events.edit',
        'uses'   => 'EventsController@edit'
    ))->where('hash', '[A-Za-z0-9]+');

    Route::post('meu-perfil/eventos/editar/info', array(
        'as'     => 'events.edit.post.info',
        'uses'   => 'EventsController@postEditInfo'
    ));

    Route::post('meu-perfil/eventos/editar/convite', array(
        'as'     => 'events.edit.post.convite',
        'uses'   => 'EventsController@postEditConvite'
    ));

    Route::get('meu-perfil/eventos/editar/lista-amigas/{hash}', array(
        'as'     => 'events.edit.amigas',
        'uses'   => 'EventsController@refreshFriendsEdit'
    ));

    Route::get('meu-perfil/eventos/criados', array(
        'as'     => 'events.my_events',
        'uses'   => 'EventsController@my_events'
    ));

    Route::post('meu-perfil/eventos/reenviar-convite', array(
        'as'     => 'events.reenviar',
        'uses'   => 'EventsController@Reenviar'
    ));

    /* Recados Especiais */
    Route::get('meu-perfil/recados-especiais', array(
        'as'     => 'meus-recados',
        'uses'   => 'SpecialMessagesController@show'
    ));

    Route::get('meu-perfil/recados-especiais/em-aprovacao', array(
        'as'     => 'meus-recados.emAprovacao',
        'uses'   => 'SpecialMessagesController@mNotApprovad'
    ));

    Route::get('meu-perfil/recados-especiais/aprovados', array(
        'as'     => 'meus-recados.aprovados',
        'uses'   => 'SpecialMessagesController@mApproved'
    ));

    Route::post('meu-perfil/recados-especiais/send/{publicId}', array(
        'as'     => 'meus-recados.send',
        'uses'   => 'SpecialMessagesController@send'
    ));

    Route::post('meu-perfil/recados-especiais/deleted', array(
        'as'     => 'meus-recados.deleted',
        'uses'   => 'SpecialMessagesController@deleted'
    ));

    Route::post('meu-perfil/recados-especiais/approved', array(
        'as'     => 'meus-recados.approved',
        'uses'   => 'SpecialMessagesController@approved'
    ));

    /* Mural */
    Route::get('meu-perfil/mural', array(
        'as'     => 'mural.listStatus',
        'uses'   => 'MuralController@listStatus'
    ));

    Route::post('meu-perfil/mural/send', array(
        'as'     => 'mural.send',
        'uses'   => 'MuralController@send'
    ));

    Route::post('meu-perfil/mural/deleted', array(
        'as'     => 'mural.deleted',
        'uses'   => 'MuralController@deleted'
    ));

    /* Enquete */
    Route::get('meu-perfil/enquete', array(
        'as'     => 'poll.add',
        'uses'   => 'PollController@index'
    ));

    Route::post('meu-perfil/enquete', array(
        'before' => 'csrf',
        'as'     => 'poll.add',
        'uses'   => 'PollController@index'
    ));

    Route::post('meu-perfil/enquete/encerrar', array(
        'as'     => 'poll.finished',
        'uses'   => 'PollController@finished'
    ));

    Route::get('meu-perfil/enquete/show', array(
        'as'     => 'poll.show',
        'uses'   => 'PollController@show'
    ));

    /* Babyline */
    Route::post('babyline/ajax/add', array(
        'as' => 'babyline.add',
        'uses' => 'BabyLineController@add'
    ));

    Route::post('babyline/ajax/deletar', array(
        'as' => 'babyline.deletar',
        'uses' => 'BabyLineController@deletar'
    ));

    /* Lista de presentes */
    Route::get('meu-perfil/lista-de-presentes', array(
        'as' => 'gift.index',
        'uses' => 'GiftListsController@index'
    ));

    Route::get('meu-perfil/lista-de-presentes/criar', array(
        'as' => 'gift.createList',
        'uses' => 'GiftListsController@createList'
    ));

    Route::get('meu-perfil/eventos/lista-de-presentes/criar', array(
        'as' => 'gift.eventos.createList',
        'uses' => 'GiftListsController@createListEvent'
    ));

    Route::get('meu-perfil/lista-de-presentes/criar/itens', array(
        'as' => 'gift.createList.itens',
        'uses' => 'GiftListsController@createListProducts'
    ));

    Route::any('meu-perfil/lista-de-presentes/cart', array(
        'as' => 'gift.cart',
        'uses' => 'GiftListsController@createCart'
    ));

    Route::post('meu-perfil/lista-de-presentes/cart/remove', array(
        'as' => 'gift.cart.remove',
        'uses' => 'GiftListsController@removeItemCart'
    ));

    Route::post('meu-perfil/lista-de-presentes/corfimacao', array(
        'as' => 'gift.confirm',
        'uses' => 'GiftListsController@confirm'
    ));

    Route::get('meu-perfil/lista-de-presentes/corfimacao/itens', array(
        'as' => 'gift.confirm.itens',
        'uses' => 'GiftListsController@confirmItens'
    ));

    Route::post('meu-perfil/lista-de-presentes/save', array(
        'as' => 'gift.saveList',
        'uses' => 'GiftListsController@saveList'
    ));

    Route::post('meu-perfil/lista-de-presentes/editar', array(
        'as' => 'gift.editList',
        'uses' => 'GiftListsController@editList'
    ));

    Route::get('meu-perfil/eventos/lista-de-presentes/editar', array(
        'as' => 'gift.eventos.editListEvent',
        'uses' => 'GiftListsController@editListEvent'
    ));

    Route::post('meu-perfil/lista-de-presentes/editar/save', array(
        'as' => 'gift.saveEditList',
        'uses' => 'GiftListsController@saveEditList'
    ));

    Route::any('meu-perfil/lista-de-presentes/editar/escolhidos', array(
        'as' => 'gift.editList.escolhidos',
        'uses' => 'GiftListsController@editListEscolhidos'
    ));

    Route::any('meu-perfil/lista-de-presentes/editar/naoescolhidos', array(
        'as' => 'gift.editList.naoescolhidos',
        'uses' => 'GiftListsController@editListNaoEscolhidos'
    ));

    Route::post('meu-perfil/lista-de-presentes/produtos', array(
        'as' => 'gift.produtos',
        'uses' => 'GiftListsController@listProducts'
    ));

    /* Album */
    Route::get('/meu-perfil/album-de-fotos', array(
        'as'     => 'my_albums',
        'uses'   => 'AlbumsController@myAlbums'
    ));

    Route::get('/meu-perfil/album-de-fotos/editar/{slug}', array(
        'as'     => 'edit_my_albums',
        'uses'   => 'AlbumsController@edit'
    ));

    Route::any('photos/upload', array(
        'as'     => 'photos_uploads',
        'uses'   => 'AlbumsController@store'
    ));

    Route::post('meus-albuns/ajax/add-album', array(
        'as' => 'my_albums.add_album',
        'uses' => 'AlbumsController@addAlbum'
    ));

    Route::post('meus-albuns/ajax/del-album', array(
        'as' => 'my_albums.del_album',
        'uses' => 'AlbumsController@delAlbum'
    ));

    Route::post('meus-albuns/ajax/update-album', array(
        'as' => 'my_albums.upd_album',
        'uses' => 'AlbumsController@updAlbum'
    ));

    Route::post('meus-albuns/upload-photo', array(
        'as' => 'my_albums.upload_photo',
        'uses' => 'AlbumsController@photo'
    ));

    Route::post('meus-albuns/name-album/{slug}', array(
        'as' => 'my_albums.name_album',
        'uses' => 'AlbumsController@nameAlbum'
    ));

    Route::post('meus-albuns/deletar-photo/{slug}', array(
        'as' => 'my_albums.deletar_photo',
        'uses' => 'AlbumsController@delPhoto'
    ));

    /* Dicas */
    Route::post('dicas-profissionais/search', array(
        'as' => 'dicas.search',
        'uses' => 'DicasController@search'
    ));

    /* Friends */
    Route::get('meu-perfil/amigas', array(
        'as'    => 'show.friendships',
        'uses'  => 'FriendsController@showFriendships'
    ));

    Route::get('meu-perfil/amigos/solicitacoes', array(
        'as'    => 'friendships.solicitacoes',
        'uses'  => 'FriendsController@requestFriends'
    ));

    Route::get('meu-perfil/amigos/all', array(
        'as'    => 'friendships.all',
        'uses'  => 'FriendsController@allFriends'
    ));

    Route::post('meu-perfil/amigos/solicitar-amizade', array(
        'as'    => 'friendships.solicitar',
        'uses'  => 'FriendsController@addFriendship'
    ));

    Route::post('meu-perfil/amigos/atualizar', array(
        'as'    => 'friendships.atualizar',
        'uses'  => 'FriendsController@updateFriendship'
    ));

    /* Visitantes */
    Route::get('meu-perfil/visitantes', array(
        'as'    => 'visitantes',
        'uses'  => 'VisitantesController@index'
    ));

    Route::post('meu-perfil/visitantes/deletar', array(
        'as'    => 'visitantes.delete',
        'uses'  => 'VisitantesController@deletar'
    ));

    Route::post('meu-perfil/visitantes/convite', array(
        'as'    => 'visitantes.convite',
        'uses'  => 'VisitantesController@enviarConvite'
    ));
});

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/
Event::listen('404', function() {
    return Response::error('404');
});

Event::listen('500', function() {
    return Response::error('500');
});