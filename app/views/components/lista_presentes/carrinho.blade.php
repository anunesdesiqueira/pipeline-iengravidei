<h3>Itens Escolhidos</h3>
@if(isset($cartGifts))
    @if(count($cartGifts) > 1)
        <span>{{ count($cartGifts) }} presentes selecionados:</span>
    @else
        <span>{{ count($cartGifts) }} presente selecionado:</span>
    @endif
    <ul class="itens-small">
        @foreach($cartGifts as $gift)
        <li>
            <div class="thumb">
                <img src="{{ $gift->picture['small'] }}" />
            </div>
            <p>{{ $gift->name }}</p>
            <span>R$ {{ str_replace('.', ',', $gift->price) }}</span>
            {{ Form::button('remover',  array('rel' => $gift->id, 'class' => 'bt-itens-small')) }}
        </li>
        @endforeach
    </ul>
    <span class="total"><b>Total:</b> R$ {{ str_replace('.', ',', $cartGifts->total) }}</span>
@else
    <span>Nenhum presente selecionado.</span>
@endif