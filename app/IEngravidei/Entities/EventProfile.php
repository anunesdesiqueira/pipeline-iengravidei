<?php namespace IEngravidei\Entities;

class EventProfile extends \Eloquent
{
    protected $table = 'profiles_events';
    protected $guarded = array();

    public $timestamps = false;
}