<?php
class ProfilesRelationship extends Eloquent {

    protected $table = 'profiles_relationships';
    protected $guarded = array('id', 'profiles_relationships_id');

    public static function exclude_friends($profiles_relationships_id)
    {
        $return = ProfilesRelationship::whereRaw('id = ?', array($profiles_relationships_id))->update(array('is_active' => 2));

        return $return;
    }
}