@if ($paginator->getLastPage() > 1)
<ul class="paginacao">
    @if ($paginator->getCurrentPage() == 1)
        <li><a href="{{ $paginator->getUrl(1) }}">&lt;</a></li>
    @endif

    @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
    <li>
        <a href="{{ $paginator->getUrl($i) }}" {{ ($paginator->getCurrentPage() == $i) ? 'class="active"' : '' }}>
        {{ $i }}
        </a>
    </li>
    @endfor

    @if ($paginator->getCurrentPage() == $paginator->getLastPage())
        <li><a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}">&gt;</a></li>
    @endif
</ul>
@endif