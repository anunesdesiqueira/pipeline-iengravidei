<?php

use Illuminate\Database\Migrations\Migration;

class CreateAlbumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('albums', function($table) {
            $table->bigIncrements('id');
            $table->integer('profile_id');
            $table->string('name', 200);
            $table->string('description', 200);
            $table->string('cover_photo');
            $table->boolean('actived')->default(0);
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('albums');
	}

}