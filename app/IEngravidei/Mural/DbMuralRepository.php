<?php

namespace IEngravidei\Mural;

use IEngravidei\Entities\Mural as Mural;
use IEngravidei\Repositories\MuralRepositoryInterface;

class DbMuralRepository implements MuralRepositoryInterface
{
    public function createStatus($data)
    {
        return Mural::create(array(
            'id_profile' => $data['id'],
            'message' => $this->Status($data),
            'shared' => $this->SharedType($data['shared']),
            'hash' => sha1(microtime(). uniqid(rand(), true) . $data['id'] . date('Y-m-d H:i:s')),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function momDeleteStatus($id_profile, $hash)
    {
        return Mural::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->update(array('deleted_at' => date('Y-m-d H:i:s')));
    }

    public function momShowStatus($id_profile, $hash)
    {
        return Mural::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->whereNull('deleted_at')->first();
    }

    public function showFriendsMoms($friends, $number, $offset, $filter = null)
    {
        return Mural::select('profiles_status')
            ->select('profiles.id', 'profiles.profile_picture', 'profiles.public_id', 'profiles.name', 'profiles_status.message', 'profiles_status.hash', 'profiles_status.created_at')
            ->join('profiles', 'profiles.id', '=', 'profiles_status.id_profile')
            ->whereNull('profiles_status.deleted_at')
            ->whereNull('profiles.deleted_at')
            ->whereIn('profiles.id', $friends)
            ->whereNested(function ($query) use($filter) {
                if(!is_null($filter) && !empty($filter)) {
                    $query->whereRaw('(profiles.name LIKE ? OR profiles_status.message LIKE ? OR profiles_status.created_at LIKE ?)',  array("%{$filter}%", "%{$filter}%", "&{$filter}&"));
                }
            })
            ->orderBy('profiles_status.created_at', 'desc')
            ->take($number)
            ->skip($offset)
            ->get();
    }

    public function showAllMoms($friends, $number, $offset, $filter = null)
    {
        $like = '';
        if(!is_null($filter) && !empty($filter)) {
            $like = "AND (`p`.`name` LIKE '%".$filter."%' OR `s`.`message` LIKE '%".$filter."%' OR `s`.`created_at` LIKE '%".$filter."%')";
        }

        return \DB::select('
            (SELECT `p`.`id`, `p`.`profile_picture`, `p`.`public_id`, `p`.`name`, `s`.`message`, `s`.`hash`, `s`.`created_at`
                FROM `profiles_status` as `s`
                INNER JOIN `profiles` as `p` ON (`p`.`id` = `s`.`id_profile`)
                WHERE `s`.`shared` = ?
                AND `p`.`deleted_at` IS NULL
                AND `s`.`deleted_at` IS NULL
                '. $like .'
            ) UNION ALL (
            SELECT `p`.`id`, `p`.`profile_picture`, `p`.`public_id`, `p`.`name`, `s`.`message`, `s`.`hash`, `s`.`created_at`
                FROM `profiles_status` as `s`
                INNER JOIN `profiles` as `p`
                ON (`p`.`id` = `s`.`id_profile`)
                WHERE `s`.`shared` = ?
                AND `p`.`id` IN ("'.implode('","',$friends).'")
                AND `p`.`deleted_at` IS NULL
                AND `s`.`deleted_at` IS NULL
                '. $like .'
            )
            ORDER BY `created_at` DESC
            LIMIT ? OFFSET ?',
            array(0, 1, $number, $offset)
        );
    }

    private function Status($data)
    {
        $content = "";

        if(isset($data['comentario']) && !empty($data['comentario'])) {
            $content .= "<p>".$data['comentario']."</p>";
        }

        if(isset($data['youtube']) && !empty($data['youtube'])) {
            $yId = $this->YouTubeGetID(trim($data['youtube']));
            $content .= "<div class=\"media\">";
            $content .= "<iframe width=\"350\" height=\"245\" src=\"//www.youtube.com/embed/". $yId ."\" frameborder=\"0\" allowfullscreen></iframe>";
            $content .= "</div>";
        }

        if(isset($data['imagem']) && !empty($data['imagem'])) {
            $content .= "<div class=\"media\">";
            $content .= "<img src='". $data['imagem']['main'] ."'>";
            $content .= "</div>";
        }

        return $content;
    }

    private function YouTubeGetID($url)
    {
        $url = preg_replace('/(>|<)/', '', $url);
        $url = preg_split('/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/', $url);

        if(isset($url[2])) {
            $yId = preg_split('/[^0-9a-z_]/', $url[2]);
            $yId = $yId[0];
        } else {
            $yId = $url[1];
        }

        return $yId;
    }

    private function SharedType($shared)
    {
        switch ($shared) {
            case 'publico':
                $shared = 0;
                break;

            case 'privado':
                $shared = 1;
                break;
        }

        return $shared;
    }
}