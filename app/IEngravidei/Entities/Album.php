<?php namespace IEngravidei\Entities;

class Album extends \Eloquent
{
    protected $table = 'album';
    protected $guarded = array();

    public $timestamps = false;
}