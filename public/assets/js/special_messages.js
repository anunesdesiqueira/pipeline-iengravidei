$(function() {
    // Init
    loadEmAprocacao();
    loadAprovados();
});

$(document.body).on('click', '.recados .recados-em-aprovacao ul li a.aprovar', function(e) {
    e.preventDefault();
    CallMethod(baseUri + '/meu-perfil/recados-especiais/approved','POST',{'hash' : $(e.toElement).attr('data-hash')},'','',successRecadosListar);
});

$(document.body).confirmOn({
    questionText: 'Quer mesmo apagar este recado?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.recados .recados-em-aprovacao ul li a.excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/recados-especiais/deleted','POST',{'hash' : $(e.toElement).attr('data-hash')},'','',successEmAprovacaoExcluir);
    }
});

$(document.body).confirmOn({
    questionText: 'Quer mesmo apagar este recado?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.recados .aprovados ul li a.excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/recados-especiais/deleted','POST',{'hash' : $(e.toElement).attr('data-hash')},'','',successAprovadosExcluir);
    }
});

function successRecadosListar(data) {
    if(data.status) {
        loadEmAprocacao();
        loadAprovados();
    }
}

function successEmAprovacaoExcluir(data) {
    if(data.status) {
        loadEmAprocacao();
    }
}

function successAprovadosExcluir(data) {
    if(data.status) {
        loadAprovados();
    }
}

function loadEmAprocacao() {
    CallMethod(baseUri + '/meu-perfil/recados-especiais/em-aprovacao','GET','','','',successEmAprocacao);
}

function loadAprovados() {
    CallMethod(baseUri + '/meu-perfil/recados-especiais/aprovados','GET','','','',successAprovados);
}

// Pagination
$(document.body).on('click', '.recados .recados-em-aprovacao ul.pagination li a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'GET','','','',successEmAprocacao);
});

$(document.body).on('click', '.recados .aprovados ul.pagination li a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'GET','','','',successAprovados);
});

function successEmAprocacao(data) {
    if(data.page != undefined) {
        var container = $('.recados .recados-em-aprovacao');
        container.empty().html(data.page);
    }
}

function successAprovados(data) {
    if(data.page != undefined) {
        var container = $('.recados .aprovados');
        container.empty().html(data.page);
    }
}