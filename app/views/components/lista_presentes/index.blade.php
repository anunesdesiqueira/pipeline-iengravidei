<div class="container-listar">
    @include('components.lista_presentes.listar')
</div>
<div class="container-presentes">
    <div class="clear">&nbsp;</div>
    <h3>Histórico de compras</h3>
    <p>Filtre pela lista:</p>
    {{ Form::open( array('id' => 'form-lista-presentes-historico') ) }}
        <p>
            <label>Lista:</label>
            <div class="comboHolder">
                <div class="mascara"><div class="optName"></div></div>
                {{ Form::select('lista-historico', $lists) }}
            </div>
            {{ Form::submit('Buscar', array('class' => 'bt-editar')) }}
        </p>
    {{ Form::close() }}
    <div class="clear">&nbsp;</div>
    <hr class="stroke"/>
    <h3>Resultado da busca:</h3>
</div>
