var uploader = new plupload.Uploader({
    runtimes : 'html5,flash,silverlight,html4',

    browse_button : 'pickfiles', // you can pass in id...
    container: document.getElementById('container'), // ... or DOM Element itself

    url : '/meus-albuns/upload-photo',

    preinit : editAlbum,

    filters : {
        max_file_size : '2mb',
        chunk_size : '1mb',
        mime_types: [
            {title : "Image files", extensions : "jpg"}
        ]
    },

    // Flash settings
    flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',

    // Silverlight settings
    silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',


    init: {
        PostInit: function() {
            document.getElementById('filelist').innerHTML = '';

            document.getElementById('uploadfiles').onclick = function() {
                uploader.start();
                return false;
            };
        },

        FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
                document.getElementById('filelist').innerHTML += '<div id="' + file.id + '" class="lista">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
            });
        },

        UploadProgress: function(up, file) {
            document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
        },

        UploadComplete: function (up, files) {
            up.splice();
        },

        Error: function(up, err) {
            filelist.append("<div class='error'>Atenção: Você deve subir imagens com a extensão JPG</div>");
            setTimeout(function(){
                divError.fadeOut();
            }, 3000);
        }
    }
});

// Mostra box de Upload
$('.album-fotos a.criar').on('click', function(e){
    e.preventDefault();
    $('.cntUpFotos').slideDown();
});

// Fecha box de Upload
$('.cntUpFotos a.close').on('click', function(e){
    e.preventDefault();
    $('.cntUpFotos').slideUp('slow');
});

$('#uploadfiles').on('click', function(e)
{
    e.preventDefault();

    if ('' === $('input.nomeAlbum').val()) {
        $('input.nomeAlbum').addClass('error');
        $('label span.error').show();
    } else if (0 == $('#filelist').children().length) {
        $('div.semFotos').show();

        setTimeout(function() {
            $('div.semFotos').fadeOut();
        }, 3000);
    } else {
        uploader.start();
    }
});

$('input.nomeAlbum').keypress(function()
{
    $('input.nomeAlbum').removeClass('error');
    $('label span.error').hide();
});

$('.album-fotos .alterar').on('click', function(e) {

    var name = $('#name-album');

    if (name.val() != name.prop("defaultValue")) {
        $.ajax({
            url         : '/meus-albuns/name-album/'+name.attr('rel'),
            data        : { 'name' : name.val() },
            dataType    : 'json',
            cache       : false,
            type        : 'POST',
            success     : function(data, textStatus, jqXHR) {
                if(data.status) {
                    $('.msg').css('display', 'block');
                }
                setMsgTimeout();
            }
        });
    }
});

function setMsgTimeout()
{
    var msg = $('.msg');

    msg.fadeOut( 3000, function() {
        msg.css('display', 'none');
    });
}


$(function($)
{
    $('.lista-album-fotos .delete-photo').confirmOn({
        questionText: 'Quer mesmo apagar esta foto?',
        textYes: 'Sim',
        textNo: 'Não'
    }, 'click', function(e, confirmed) {
        if(confirmed)
        {
            var photo = $(e.toElement).attr('rel'),
                slug = $('.album-fotos .delete-album').attr('rel');

            $.ajax({
                url         : '/meus-albuns/deletar-photo/'+ slug,
                data        : { 'photo_image' : photo },
                dataType    : 'json',
                cache       : false,
                type        : 'POST',
                success     : function(data, textStatus, jqXHR){
                    if(data.status)
                    {
                        $(e.toElement).parent().remove();
                    }
                }
            });
        }
    });

    $('.album-fotos .delete-album').confirmOn({
        questionText: 'Quer mesmo apagar este álbum?',
        textYes: 'Sim',
        textNo: 'Não'
    },'click', function(e, confirmed) {
        if(confirmed)
        {
            var slug = $(e.toElement).attr('rel');

            $.ajax({
                url         : '/meus-albuns/ajax/del-album',
                data        : { 'slug' : slug },
                dataType    : 'json',
                cache       : false,
                type        : 'POST',
                success     : function(data, textStatus, jqXHR)
                {
                    if(data.status) {
                        window.location.href = '/meu-perfil/album-de-fotos';
                    }
                }
            });
        }
    });

});

function editAlbum(Uploader)
{
    Uploader.bind('FileUploaded', function(up, file, response)
    {
        $('#' + file.id + " span").html("100%");

        if( (Uploader.total.uploaded + 1) == Uploader.files.length)
        {
            var imgs = new Array(),
                n = 0;

            $.each(Uploader.files, function(i, f) {
                imgs[n] = f.name;
                n++
            });

            $.ajax({
                url         : '/meus-albuns/ajax/update-album',
                data        : { 'slug' : $('#name-album').attr('rel'), 'images' : imgs },
                dataType    : 'json',
                cache       : false,
                type        : 'POST',
                success     : function(data, textStatus, jqXHR)
                {
                    if (data.status)
                    {
                        PhotosList(data.list);

                        setTimeout(function(){
                            $('#container').slideUp('slow');
                            $('#filelist').find('.lista').remove();
                            $('input.nomeAlbum').val('');
                        }, 500);
                    }
                }
            });
        }
    });
}

function PhotosList(data)
{
    var ul = $('.lista-album-fotos');

    ul.html('');

    $.each(data, function(key, val)
    {
        var li = document.createElement('li'),
            del = document.createElement('a'),
            div = document.createElement('div'),
            img_a = document.createElement('a'),
            img = document.createElement('img');

        $(del).addClass('excluir delete-photo').attr({'href' : '#', 'rel' : val.id}).html('Excluir');
        $(div).addClass('thumb');
        $(img_a).attr('href', val.picture['original']);
        $(img).attr('src', val.picture['main']);
        $(img_a).append(img);
        $(div).append(img_a);

        $(li).append(del);
        $(li).append(div);

        ul.append(li);
    });

    $.getScript('/assets/js_old/album_edit.js_old');
}

//inicia uploader
uploader.init();