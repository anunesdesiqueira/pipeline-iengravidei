<?php

use IEngravidei\Repositories\PollRepositoryInterface as Poll;
use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;

class PollController extends BaseController
{
    public function __construct(Poll $poll, Profile $profile)
    {
        parent::__construct();

        $this->poll = $poll;
        $this->profile = $profile;
    }

    public function index()
    {
        if(Request::isMethod('post')) {
            $data = Input::all();

            $validation = \Validator::make($data, array(
                'question' => 'required',
                'answer' => 'required'
            ), array(
                'question.required' => '*Pergunta é obrigatória.',
                'answer.required' => '*Preencha pelo menos duas respostas.',
            ));

            if(!$validation->fails()) {
                $data['id_profile'] = $this->user->id;

                $pollActive = $this->poll->findPollActive($data['id_profile']);
                if($pollActive) {
                    $this->poll->upPoll($pollActive->slug, 0);
                }

                $this->poll->setPoll($data);
            }
        }

        $history = $this->poll->history($this->user->id);
        $poll = $this->poll->getPoll($this->user->id);

        if(Request::ajax()) {
            $page = View::make('components.poll.add', compact('history', 'poll'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('poll.add', compact('history', 'poll'));
        }
    }

    public function finished()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'slug' => 'required',
        ), array(
            'slug.required' => 'Enquete não encontrada',
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            if($this->poll->upPoll($data['slug'], 0)) {
                $history = $this->poll->history($this->user->id);
                $poll = $this->poll->getPoll($this->user->id);

                $page = View::make('components.poll.add', compact('history', 'poll'))->render();
                return Response::json(array('page' => $page));
            } else {
                return Response::json(array('error' => 'Ocorreu um erro!'));
            }
        }
    }

    public function show()
    {
        if($this->user->plan_roles['poll']) {
            $poll = $this->poll->getPoll($this->user->id);
            $page = View::make('components.poll.voting', compact('poll'))->render();
        } else {
            $page = View::make('components.poll.no_permission', compact('poll'))->render();
        }
        return Response::json(array('page' => $page));
    }

    public function vote()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'poll' => 'required',
            'enqt' => 'required'
        ), array(
            'poll.required' => 'Enquete é obrigatória',
            'enqt.required' => 'Escolha uma opção para votar.',
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $poll = $this->poll->getId($data['poll']);

            $data['id_poll'] = $poll->id;
            $data['ip'] = getenv("REMOTE_ADDR");

            if(Auth::check()) {
                $data['id_profile'] = $this->user->id;
            }

            if($this->poll->findLogAnswer($data['id_poll'], $data['ip'], isset($data['id_profile'])? $data['id_profile'] : ''))
                return Response::json(array('error' => 'Você já voltou nessa enquete.'));

            $voted = $this->poll->addLogVote($data);

            if($voted) {
                $getPoll = $this->poll->getPoll($poll->id_profile);
                return Response::json(array('status' => true, 'poll' => $getPoll->answers->toArray(), 'vote' => $getPoll->total));
            } else {
                return Response::json(array('error' => 'Ocorreu um erro!'));
            }
        }
    }
}