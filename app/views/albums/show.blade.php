@extends('layouts.master')

@section('content')
<div class="holder album-fotos">
    @include('components.cabecalho.public')

    <h2 name="albuns" title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>
    <div class="clearfix"></div>
    <ul class="lista-album">
        @foreach ($albums as $album)
        <li>
            <div class="thumb"><a href="{{ $album->slug }}"><img src="{{ $album->picture['main'] }}" /></a></div>
            <a href="{{ $album->slug }}" class="nome">{{ $album->name }}</a>
        </li>
        @endforeach
    </ul>
</div>
@stop