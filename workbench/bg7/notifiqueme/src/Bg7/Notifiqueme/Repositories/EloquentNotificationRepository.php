<?php namespace Bg7\Notifiqueme\Repositories;

use Bg7\Notifiqueme\Model\Eloquent\Notification as NotificationModel;


class EloquentNotificationRepository implements NotificationRepository
{
    protected $notificationModel;

    /**
     * @param NotificationModel $notificationModel
     */
    public function __construct(NotificationModel $notificationModel)
    {
        $this->notificationModel = $notificationModel;
    }

    /**
     * @param array $notificationData
     * @return bool
     */
    public function push(array $notificationData)
    {
        $v = $this->notificationModel->validate($notificationData);

        if( $v->fails())
        {
            return false;
        }

        $this->notificationModel->profile_id = $notificationData['profile_id'];
        $this->notificationModel->message = $notificationData['message'];
        $this->notificationModel->read = $notificationData['read'];
        return $this->notificationModel->save();
    }

    /**
     * @param $profileId
     * @return array|bool
     */
    public function pull($profileId)
    {
        $cleanNotifications = array();
        $notifications = $this->notificationModel->where('profile_id', '=', $profileId)->get();

        if( empty($notifications))
        {
            return false;
        }

        foreach( $notifications as $notification){
            $cleanNotifications[]['message'] = $notification->message;
        }

        return $cleanNotifications;

    }

    /**
     * @param $profileId
     * @return mixed
     */
    public function numberOfNotifications($profileId)
    {
        return $this->notificationModel->whereRaw('profile_id = ? and read = 0', array($profileId))->count();
    }
}