<?php

use Illuminate\Database\Migrations\Migration;

class CreateCountriesStatesAndCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('countries', function($table)
        {
            $table->increments('id');
            $table->string('name', 100);
        });	

        Schema::create('states', function($table)
        {
            $table->increments('id');
			$table->integer('country_id')->unsigned()->index();
            $table->string('short', 2);			
            $table->string('name', 100);

			$table->foreign('country_id')->references('id')->on('countries');
        });

        Schema::create('cities', function($table)
        {
            $table->increments('id');
			$table->integer('state_id')->unsigned()->index();
            $table->string('state_name', 100);			
            $table->string('name', 100);

			$table->foreign('state_id')->references('id')->on('states');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');				
		Schema::drop('states');		
		Schema::drop('countries');
	}

}