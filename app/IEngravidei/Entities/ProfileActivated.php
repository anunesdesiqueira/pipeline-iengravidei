<?php

namespace IEngravidei\Entities;

class ProfileActivated extends \Eloquent
{
    protected $table = 'profiles_activated';
    protected $guarded = array();

    public $timestamps = false;
}