@extends('layouts.master')

@section('content')
<div class="holder">

    <!--## CABECALHO 1 ##-->
    <div class="cabecalho">

        @include('components.my_profile_top')

        @include('components.enquete')

        <!-- Compartilhe -->
        <div class="clearfix"></div>
        <a href="#" class="compartilhe">Compartilhe</a>
        <!-- End: Compartilhe -->
    </div>
    <!--## END: CABECALHO 1 ##-->

    <div class="mensagem">

        <h2>Mensagens</h2>
        <h3>Nova mensagen</h3>

        <div class="clearfix"></div>

        <aside>
            <a href="#" class="nova-mensagem">Nova mensagem</a>
            <div class="busca">
                <input type="text" placeholder="PESQUISAR" />
                <button type="button"></button>
            </div>
            <ul>
                @foreach($friends as $friend)
                <li>
                    <div class="thumb"><img src="{{ $friend['profile_picture']['small'] }}" /></div>
                    <div class="description">
                        <h3>{{ $friend['name'] }}</h3>
                        <p><a href="{{ $friend['slug'] }}">MSG: {{ $friend['msg'] }}</a></p>
                    </div>
                </li>
                @endforeach
            </ul>
        </aside>

        <section>
            {{ Form::open(array('route' => 'messages.send.post', 'enctype' => 'multipart/form-data')) }}
                <label>Para: {{Form::text('to', $publicId)}}
                <div class="clearfix"></div>
                <label>Escreva sua mensagem:</label>
                {{ Form::textarea('body') }}
                <div class="clearfix"></div>
                <input type="submit" value="Enviar" />
            {{ Form::close() }}
        </section>

        <div class="clearfix"></div>

    </div>

</div>
@stop