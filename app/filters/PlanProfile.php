<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;

class PlanProfile
{
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    public function filter()
    {
        $user = Auth::client()->get();
        $nameRoute = Route::currentRouteName();

        if("userauth.logout" !== $nameRoute && "ajax.get_cities" !== $nameRoute && "ajax.check_username" !== $nameRoute && "plan.register" !== $nameRoute) {
            if($this->profile->existsProfileAdditional($user->id)) {
                $plan = $this->profile->getPlan($user->id);
                if($plan) {
                    $data = array(
                        'plan_name' => $plan->name,
                        'album_limited' => $plan->album_limited,
                        'album_photo_limited' => $plan->album_photo_limited,
                        'poll' => $plan->poll,
                        'professional_tips' => $plan->professional_tips,
                        'discount_club' => $plan->discount_club,
                        'page_customization' => $plan->page_customization,
                    );
                    $user->plan_roles = $data;
                } else {
                    if("plan" !== $nameRoute) {
                        return Redirect::route('plan');
                    }
                }
            } else {
                if("register.complete" !== $nameRoute) {
                    return Redirect::route('register.complete');
                }
            }
        }
    }
} 