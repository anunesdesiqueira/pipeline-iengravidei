<div class="holder">
    <div class="escolha-plano">
        <h2 name="escolha-um-plano">Escolha um plano para finalizar seu cadastro</h2>
        @if($errors->any())
            <ul>
                {{ implode('', $errors->all('<li>:message</li>'))}}
            </ul>
        @endif
        <ul>
            <li>
                <h3 class="free">100% Grátis</h3>
                <ul>
                    <li><span class="tickFree">&nbsp;</span>Perfil Individual</li>
                    <li><span class="tickFree">&nbsp;</span>Baby Line</li>
                    <li><span class="tickFree">&nbsp;</span>Mural</li>
                    <li><span class="tickFree">&nbsp;</span>Lista de Presentes</li>
                    <li><span class="tickFree">&nbsp;</span>Criador de Convites e Eventos</li>
                    <li><span class="tickFree">&nbsp;</span>Álbum de Fotos (3 álbuns)</li>
                </ul>
                <a href="{{ route('plan.register', array('plan' => 'Premium-Free')) }}" class="btnRed">Concluir Cadastro</a>
                {{-- <p>Parabéns!<br>Agora é só confirmar o cadastro<br>no seu e-mail e começar a usar.</p> --}}
            </li>
            <li>
                <h3 class="premium"><span>Apenas</span>R$14<span class="cents">,90</span><span class="mes">/mês</span></h3>
                <ul>
                    <li><span class="tickPremium">&nbsp;</span>Perfil Individual</li>
                    <li><span class="tickPremium">&nbsp;</span>Baby Line</li>
                    <li><span class="tickPremium">&nbsp;</span>Mural</li>
                    <li><span class="tickPremium">&nbsp;</span>Lista de Presentes</li>
                    <li><span class="tickPremium">&nbsp;</span>Criador de Convites e Eventos</li>
                    <li><span class="tickPremium">&nbsp;</span>Álbum de Fotos (12 álbuns)</li>
                    <li><span class="tickPremium">&nbsp;</span>Enquete</li>
                    <li><span class="tickPremium">&nbsp;</span>Dicas de Profissionais</li>
                    <li><span class="tickPremium">&nbsp;</span>Clube de Descontos</li>
                    <li><span class="tickPremium">&nbsp;</span>Personalização da Página</li>
                </ul>
                {{ Form::open(array('url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', 'method' => 'POST', 'target' => '_top')) }}
                    {{ Form::hidden('cmd', '_s-xclick') }}
                    {{ Form::hidden('hosted_button_id', 'R5MEQ4LSQTHLA') }}
                    {{ Form::submit('Assine Já', array('class' => 'btnBlue')) }}
                {{ Form::close() }}
            </li>
            {{--
            <li>
                <h3 class="top"><span>Apenas</span>R$14<span class="cents">,90</span><span class="mes">/mês</span></h3>
                <ul>
                    <li><span class="tickTop">&nbsp;</span>Perfil Individual</li>
                    <li><span class="tickTop">&nbsp;</span>Baby Line</li>
                    <li><span class="tickTop">&nbsp;</span>Mural</li>
                    <li><span class="tickTop">&nbsp;</span>Lista de Presentes</li>
                    <li><span class="tickTop">&nbsp;</span>Criador de Convites e Eventos</li>
                    <li><span class="tickTop">&nbsp;</span>Álbum de Fotos (ilimitado)</li>
                    <li><span class="tickTop">&nbsp;</span>Enquete</li>
                    <li><span class="tickTop">&nbsp;</span>Dicas de Profissionais</li>
                    <li><span class="tickTop">&nbsp;</span>Clube de Desconto VIP</li>
                    <li><span class="tickTop">&nbsp;</span>Personalização da Página</li>
                </ul>
                <a href="#" class="btnGreen bt-plano">Assine Já</a>
            </li>
            --}}
        </ul>
    </div>
</div>