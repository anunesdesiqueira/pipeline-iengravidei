<?php

namespace IEngravidei\Entities;

class Plans extends \Eloquent
{
    protected $table = 'plans';
    protected $guarded = array();

    public $timestamps = false;
}