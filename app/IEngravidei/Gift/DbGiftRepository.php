<?php
namespace IEngravidei\Gift;

use IEngravidei\Entities\GiftProducts;
use IEngravidei\Entities\ProfileGift;
use IEngravidei\Entities\ProfileGiftProduct;
use IEngravidei\Repositories\GiftRepositoryInterface;

class DbGiftRepository implements GiftRepositoryInterface
{
    public function findById($hash)
    {
        return ProfileGift::select('id', 'name', 'hash')
            ->whereNested(function ($query) use($hash) {
                $query->where('hash', '=', $hash)->orWhere('id', '=', $hash);
            })->first();
    }

    public function findByIdAndIdprofile($hash, $id_profile)
    {
        return ProfileGift::select('id', 'name', 'hash')->where('hash', '=', $hash)
                ->where('id_profile', '=', $id_profile)
                ->first();
    }

    public function getProfileList($data)
    {
        return ProfileGift::select('profiles_giftslist.name', 'profiles_giftslist.hash')
            ->join('profiles', 'profiles.id', '=', 'profiles_giftslist.id_profile')
            ->whereNested(function ($query) use($data) {
                if(isset($data['hash']))
                    $query->where('profiles_giftslist.hash', '=', $data['hash']);

                if(isset($data['type']))
                    $query->where('profiles_giftslist.type', '=', $data['type']);

            })->whereNested(function ($query) use($data) {
                $query->where('profiles.id', '=', $data['id'])
                    ->orWhere('profiles.public_id', '=', $data['id']);

            })->orderBy('profiles_giftslist.created_at')->lists('name','hash');
    }

    public function show($data)
    {
        $getLists = ProfileGift::select('profiles_giftslist.id')
            ->join('profiles', 'profiles.id', '=', 'profiles_giftslist.id_profile')
            ->where('profiles.id', '=', $data['id'])
            ->whereNested(function ($query) use($data) {
                if(isset($data['hash']))
                    $query->where('profiles_giftslist.hash', '=', $data['hash']);

            })->lists('profiles_giftslist.id');

        if(!empty($getLists)) {
            $products = ProfileGiftProduct::join('giftlists_products', 'giftlists_products.id', '=', 'profiles_giftlist_products.id_product')
                ->whereIn('id_giftslist', $getLists);

            if(isset($data['limit'])) {
                $products->take($data['limit']);
                $products->orderBy(\DB::raw('RAND()'))->get();
            }

            return $products->get();
        }

        return $getLists;
    }

    public function listProducts($products = null, $notProducts = null, $filter = null, $paginate = null)
    {
        $products = GiftProducts::where('status', '=', 1)
            ->whereNested(function ($query) use($products, $notProducts, $filter) {
                if(!is_null($products))
                    $query->whereIn('id', $products);

                if(!is_null($notProducts))
                    $query->whereNotIn('id', $notProducts);

                if(!is_null($filter)) {
                    $range = $this->range($filter);
                    $query->where('price', '>', $range[0]);
                    if(0 != $range[1])
                        $query->where('price', '<', $range[1]);
                }
            })->orderBy('name');

        if(!is_null($paginate))
            return $products->paginate($paginate);
        else
            return $products->get();
    }

    public function selectlistProductsId($id, $filter = null, $paginate = 3)
    {
        return ProfileGiftProduct::join('giftlists_products', 'giftlists_products.id', '=', 'profiles_giftlist_products.id_product')
            ->where('id_giftslist', '=', $id)
            ->whereNested(function ($query) use($filter) {
                if(!is_null($filter)) {
                    $range = $this->range($filter);
                    $query->where('price', '>', $range[0]);
                    if(0 != $range[1])
                        $query->where('price', '<', $range[1]);
                }
            })->paginate($paginate);
    }

    public function createList($data)
    {
        $createList = ProfileGift::create(array(
            'id_profile' => $data['id'],
            'name' => $data['name'],
            'hash' => sha1(microtime(). uniqid(rand(), true) . $data['name'] + $data['id']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        foreach($data['products'] as $product)
        {
            ProfileGiftProduct::create(array(
                'id_giftslist' => $createList->id,
                'id_product' => $product
            ));
        }

        return $createList;
    }

    public function updateList($data)
    {
        ProfileGiftProduct::where('id_giftslist', '=', $data['id_giftslist'])->forceDelete();

        foreach($data['products'] as $product)
        {
            ProfileGiftProduct::create(array(
                'id_giftslist' => $data['id_giftslist'],
                'id_product' => $product
            ));
        }

        return true;
    }

    private function range($price)
    {
        $min = explode(",", current($price));
        $max = explode(",", end($price));
        return array($min[0], ($max[0] == 499.99 ? 0 : $max[1]));
    }
}