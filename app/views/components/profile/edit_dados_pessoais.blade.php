{{ Form::open(array('file' => true, 'id' => 'edit-perfil')) }}
<h2 name="editar">dados pessoais</h2>
<ul class="dadosPessoais">
    <li class="nome">
        <label>
            <span>Seu nome*:</span>
            {{ Form::text('name', $profile->name) }}
        </label>
    </li>
    <li class="papai">
        <label>
            <span>Nome do papai:</span>
            {{ Form::text('father_name', $profile->father_name) }}
        </label>
    </li>
    <li class="bebe">
        <label>
            <span>Nome do bebê:</span>
            {{ Form::text('baby_name', $profile->baby_name) }}
        </label>
    </li>
    <li class="email">
        <label>
            <span>E-mail*:</span>
            {{ Form::text('email', $profile->email, array('disabled' => 'disabled')) }}
        </label>
    </li>
    <li class="nasc">
        <label>
            <span>Sua data de nascimento*:</span>
            {{ Form::text('birth', $profile->birth) }}
        </label>
    </li>
    <li>
        <label for="country_id" class="error"></label>
        <label for="state_id" class="error"></label>
        <label for="city_id" class="error"></label>
        <label>
            <span>País*:</span>
            <div class="comboHolder">
                <div class="mascara"><div class="optName"></div></div>
                {{ Form::select('country_id', array('' => 'Selecione') + $countries, $profile->country_id, array('id' => 'country')) }}
            </div>
        </label>
        <label>
            <span>Estado*:</span>
            <div class="comboHolder">
                <div class="mascara"><div class="optName"></div></div>
                {{ Form::select('state_id', array('' => 'Selecione') + $states, $profile->state_id, array('id' => 'states')) }}
            </div>
        </label>
        <label>
            <span>Cidade*:</span>
            <div class="comboHolder">
                <div class="mascara"><div class="optName"></div></div>
                {{ Form::select('city_id', array('' => 'Selecione') + $cities, $profile->city_id, array('id' => 'cities')) }}
            </div>
        </label>
    </li>
    <li>
        <label>
            <span>Estou com*:</span>
            <div class="comboHolder">
                <div class="mascara"><div class="optName"></div></div>
                {{ Form::select('pregnancy_weeks', array('' => 'Selecione') + $pregnancy_weeks, $profile->pregnancy_weeks) }}
            </div>
            <span>Semana(s)</span>
        </label>
        <label for="pregnancy_weeks" class="error error-select"></label>
    </li>
    <li>
        <label>
            <span>Troque sua foto:</span>
            <div class="uploadHolder">
                <div class="mascara"><div class="fileName"></div></div>
                {{ Form::file('picture') }}
            </div>
        </label>
    </li>
    <li class="campoObr">Campo obrigatório*</li>
    <li><input type="submit" value="salvar alterações" class="bt_salvar" /></li>
</ul>
{{ Form::close() }}