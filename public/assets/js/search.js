/* Search pagination */
$(document.body).on('click', '.busca-resultado .container ul.pagination a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'GET','','','',successSearch);
});

function successSearch(data) {
    if(data.page !== undefined) {
        var container = $('.busca-resultado .container');
        container.empty().html(data.page);
    }
}