<h3>Lista de Presentes</h3>
{{ Form::open( array('id' => 'form-lista-presentes-listar') ) }}
    <p>
        <label>Listas já criadas</label>
        <div class="comboHolder">
            <div class="mascara">
                <div class="optName"></div>
            </div>
            {{ Form::select('listascriadas', $lists) }}
        </div>
        {{ Form::submit('Editar Lista', array('class' => 'bt-editar')) }}
    </p>
{{ Form::close() }}
<a class="bt-criar-lista" href="{{ route('gift.createList') }}" title="Criar Lista">Criar Lista</a>