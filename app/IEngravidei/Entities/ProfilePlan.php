<?php

namespace IEngravidei\Entities;

class ProfilePlan extends \Eloquent
{
    protected $table = 'profiles_plans';
    protected $guarded = array();

    public $timestamps = false;
}