<?php
use IEngravidei\Repositories\DenunciationRepositoryInterface as Denunciation,
    IEngravidei\Libraries\Upload as Upload;

class DenunciationController extends BaseController {


    public function __construct(Denunciation $denunciation, Upload $upload)
    {
        parent::__construct();
        $this->denunciation = $denunciation;
        $this->upload = $upload;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('denunciations.index');
	}

    public function store()
    {
        $input = Input::all();

        if (Input::hasFile('image')) {
            $maxSize = 2 * pow(1024,2);
            $uploaded = $this->upload->toApp(Input::file('image'), array('image/jpeg', 'image/png'), 'uploads/denunciations', $maxSize);

            if ( is_array($uploaded)) {
                $this->upload->toS3('iengravidei.denuncias', $uploaded['name'], $uploaded['fullPath']);
                $input['s3_object'] = $uploaded['name'];
            }
        }

        if (Input::has('anonymous')) {
            $input['author'] = 'anonymous';

            if( "N" === $input['anonymous'] ) {
                $input['author'] = $input['email'];
                if( Auth::check()) {
                    $input['author'] = Auth::user()->name;
                }
            }
        }

        unset($input['_token']);
        unset($input['anonymous']);
        unset($input['image']);

        $created = $this->denunciation->create($input);

        if( true === $created)
        {
            return Redirect::route('denuncie')
                ->with('flashMessage', 'Denúncia realizada com sucesso!');
        }
        return Redirect::route('denuncie')
            ->withInput()
            ->withErrors($created);

    }



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function adminIndex()
    {
        $denuncia = new Denunciation();
        $client = $denuncia->conect();
        $denuncias = $denuncia->return_all($client);
        $status = 'Ativo';

        return View::make('denuncia.indexAdmin', compact('denuncias', 'status'));
    }

    /*public function create_denunciation()
    {
        $input = Input::all();

        $rules = array(
            'denuncias' => 'Required|Min:2',
            'conteudo' => 'Required|Min:2'
        );

        $v = Validator::make($input, $rules);

        if( $v->passes() )
        {
            if(!is_null($input['image']))
            {
                $caminho = "../public/uploads/denuncias/";

                $arquivos[] = $input['image'];

                $retorno = $this->executa_upload($arquivos, $caminho);

                if($retorno == FALSE)
                {
                    $messages = "Arquivo ou extensão não permitidos.";
                }
                else
                {
                    $input['name_image'] = $retorno;
                }
            }


            if(!isset($messages))
            {
                $user = Sentry::getUser();

                if(!is_null($user))
                {
                    $input['user_id'] = $user->id;
                    $input['user_name'] = $user->first_name." ".$user->last_name;
                    $input['hash_user'] = $user->hash_user;
                }

                $denuncia = new Denunciation();
                $client = $denuncia->conect();
                $grava_denuncias = $denuncia->cadastra_denuncia($client, $input);

                if($grava_denuncias)
                {
                    $messages = "Denúncia realizada com sucesso.";
                }
            }

            return View::make('denuncia.index', compact('messages'));
        }
        else
        {
            $messages = "Mensagem não enviada. Preencha os campos Tipo de denúncia e Tipo de conteúdo.";

            return View::make('denuncia.index', compact('messages'));
        }
    }*/

    /**
     * Executa upload.
     *
     * @param array  arquivos
     * @param string caminho
     * @return Response
     */
    /*public function executa_upload($arquivos, $caminho)
    {
        $total = count($arquivos);

        for($i=0;$i<$total;$i++)
        {
            $extension =$arquivos[$i]->getClientOriginalExtension();

            $retorno = $this->valida_extensao($extension);

            if($retorno)
            {
                $filename = md5($arquivos[$i]->getClientOriginalName().date('d-m-Y h:i:s').rand(0,654654654787987)).".".$extension;

                $upload_success = $arquivos[$i]->move($caminho, $filename);

                if( $upload_success == FALSE ) {
                    //return Response::json('success', 200);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        return $filename;
    }*/


    /**
     * Verifica extensão
     *
     *
     * */
    /*public function valida_extensao($extensao)
    {
        $permitidas[0] = "jpg";
        $permitidas[1] = "jpeg";
        $permitidas[2] = "gif";
        $permitidas[3] = "png";
        $permitidas[4] = "psd";
        $permitidas[5] = "bmp";
        $permitidas[6] = "raw";
        $permitidas[7] = "tiff";
        $permitidas[8] = "mp3";
        $permitidas[9] = "mp4";
        $permitidas[10] = "wmv";
        $permitidas[11] = "mkv";
        $permitidas[12] = "avi";
        $permitidas[13] = "mpeg";
        $permitidas[14] = "pdf";

        if(in_array($extensao, $permitidas))
        {
            return TRUE;
        }

        return FALSE;
    }*/

}