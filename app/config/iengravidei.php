<?php
	// application/config/iengravidei.php
	return array(
		'mail' => array(
			'subject' => array(
				'confirm' 			=> 'Confirme seu cadastro!',			
				'reset_password' 	=> 'Recuperar senha',
                'visitante'         => 'Visite a minha página no IEngravidei',
			),
			'from' => array(
				'address' 	=> 'dizaovp@gmail.com',
				'name' 		=> 'IEngravidei'
			)
		),
		'pictures' => array(
			'profile' => array(
				'path' => '/public/assets/avatares/',
				'public_path' => '/assets/avatares/',
				'thumbnails' => array(
					'small' 	=> array(60, 60),
					'medium' 	=> array(100, 100),
					'main' 		=> array(120, 180)
				)
			),
            'mural' => array(
                'path' => '/public/assets/mural/',
                'public_path' => '/assets/mural/',
                'thumbnails' => array(
                    'small' 	=> array(82, 66),
                    'medium' 	=> array(175, 132),
                    'main' 		=> array(350, 265)
                )
            ),
			'custom_layout' => array(
				'path' => '/public/assets/layout/',
				'public_path' => '/assets/layout/'
			),
            'babyline' => array(
                'path' => '/public/assets/babyline/',
                'public_path' => '/assets/babyline/',
                'thumbnails' => array(
                    'small' 	=> array(60, 60),
                    'medium' 	=> array(100, 100),
                    'main' 		=> array(138, 91)
                )
            ),
            'album' => array(
                'path' => '/public/assets/album/',
                'public_path' => '/assets/album/',
                'thumbnails' => array(
                    'small' 	=> array(60, 60),
                    'medium' 	=> array(100, 100),
                    'main' 		=> array(232, 285)
                )
            ),
            'gift' => array(
                'path' => '/public/assets/gifts/',
                'public_path' => '/assets/gifts/',
                'thumbnails' => array(
                    'small' 	=> array(50, 50),
                    'medium' 	=> array(103, 103),
                    'main' 		=> array(189, 189)
                )
            ),
            'tmp' => array(
                'path' => '/public/assets/tmp/',
                'public_path' => '/assets/tmp/',
            ),
		),
        'facebook' => array(
            'appId' => '185225351684493',
            'secret' => '38af93e3ec09bcedcd10e1254cec5bf3'
        )
	);