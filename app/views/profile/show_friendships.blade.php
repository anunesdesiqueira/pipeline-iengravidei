@extends('layouts.master')

@section('content')

<section class="content">
    <div class="holder">

        <!--## CABECALHO 1 ##-->
        <div class="cabecalho">

            @include('components.my_profile_top')

            @include('components.enquete_criar')

            <!-- Compartilhe -->
            <div class="clearfix"></div>
            <a href="#" class="compartilhe">Compartilhe</a>
            <!-- End: Compartilhe -->
        </div>
        <!--## END: CABECALHO 1 ##-->

        <div class="lista-amigas">

            <h2>lista amigas</h2>

            <ul>
                @foreach($friends as $friend)
                <li id="f{{ $friend->id }}">
                    <div class="foto-excluir">

                        <div class="thumb"><img src="{{ $friend->picture['small'] }}" /></div>
                        <a data-url="{{ URL::route('friendships.remove') }}" data-id="{{ $friend->id }}" class="excluir">Excluir</a>
                    </div>
                    <div class="description">
                        <h3>{{ $friend->name }}</h3>
                        {{link_to_route('messages.send', 'Enviar mensagem', array($friend->public_id), array('class' => 'msg')) }}
                    </div>
                </li>
                @endforeach
            </ul>

        </div>

    </div>
</section>

@stop
