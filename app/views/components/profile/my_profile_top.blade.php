<div class="foto">
    <img src="{{ $user->profile_picture['main'] }}" />
</div>

<div class="atividades">
	<h2>{{ $user->name }}</h2>
	<a href="{{ route('user.public', array('publicId' => $user->public_id)) }}" class="ver-perfil">
        <span>&nbsp;</span>Ver Perfil
    </a>

    @if(!$published)
        @include('components.published')
    @endif

	<ul class="listaBotoes">
		<li>
            <a href="{{ route('user.edit_profile') }}" class="editar">
                <span>&nbsp;</span>Editar Perfil
            </a>
        </li>
		<li>
            <a href="{{ route('user.personalize') }}" class="editar">
                <span>&nbsp;</span>Personalizar
            </a>
        </li>
		<li>
            <a href="{{ route('meus-recados') }}" class="recados">
                <span>&nbsp;</span>Recados Especiais
                @if($notification->countSpecial > 0)
                    <div class="nav-counter">{{ $notification->countSpecial }}</div>
                @endif
            </a>
        </li>
		<li>
            <a href="{{ route('events.create') }}" class="eventos">
                <span>&nbsp;</span>Eventos
            </a>
        </li>
		<li>
            <a href="{{ route('my_albums') }}" class="album">
                <span>&nbsp;</span>Albúm de Fotos
            </a>
        </li>
		<li>
            <a href="{{ route('show.friendships') }}" class="lista-amigas">
                <span>&nbsp;</span>Lista de Amigas
                @if($notification->countFriends > 0)
                    <div class="nav-counter">{{ $notification->countFriends }}</div>
                @endif
            </a>
        </li>
		<li>
            <a href="{{ route('messages') }}" class="mensagens">
                <span>&nbsp;</span>Mensagens
                @if($notification->countMsg > 0)
                    <div class="nav-counter">{{ $notification->countMsg }}</div>
                @endif
            </a>
        </li>
		<li>
            <a href="{{ route('gift.index') }}" class="presentes">
                <span>&nbsp;</span>Lista de Presentes
            </a>
        </li>
        <li>
            <a href="{{ route('visitantes') }}" class="notificacoes">
                <span>&nbsp;</span>Visitantes
                @if($notification->countVisitantes > 0)
                    <div class="nav-counter">{{ $notification->countVisitantes }}</div>
                @endif
            </a>
        </li>
	</ul>
</div>