<?php

class GiftlistsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('giftlists')->truncate();

		$giftlists = array(

            array(
                'hash'       => sha1(uniqid(rand(), true)),
                'name'       => 'Chá de bebe do Lucas',
                'profile_id' => 6,
            ),

            array(
                'hash'       => sha1(uniqid(rand(), true)),
                'name'       => 'Chá de bebe da Maria',
                'profile_id' => 6,
            ),

            array(
                'hash'       => sha1(uniqid(rand(), true)),
                'name'       => 'Chá de bebe do Pedro',
                'profile_id' => 6,
            ),
		);

		// Uncomment the below to run the seeder
        DB::table('giftlists')->truncate();
        DB::table('giftlists')->insert($giftlists);
	}

}
