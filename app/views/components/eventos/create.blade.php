<div class="crie">
    {{ Form::open(array('route' => 'events.create.post','id' => 'form-event-create')) }}
        <h3 name="create-convite">Crie um novo evento</h3>
        @include('components.eventos.types')
        <div class="presente-selecao">
            @include('components.eventos.presentes')
        </div>
        @include('components.eventos.dados')
        @include('components.eventos.personalize')
        <div class="escolha-amigos">
            @include('components.eventos.select_friends')
        </div>
        {{ Form::hidden('mails_externos') }}
    {{ Form::close(); }}
</div>