<?php

class GiftList extends Eloquent {
	protected $fillable = array('name','hash','profile_id');
	protected $table = 'giftlists';

	public static $rules = array(
	);

	public function gifts()
	{	
		return $this->belongsToMany('Product','giftlists_products');
	}
}
