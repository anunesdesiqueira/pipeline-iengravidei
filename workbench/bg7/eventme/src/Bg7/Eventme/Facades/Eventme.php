<?php namespace Bg7\Eventme\Facades;

use Illuminate\Support\Facades\Facade;

class Eventme extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'eventme';
    }
}