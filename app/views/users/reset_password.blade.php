@extends('layouts.master')

@section('content')

<div class="holder">
    @if (Session::has('error'))
        <h2>{{ trans(Session::get('reason')) }}</h2>
    @endif

    <div class="primeiro-acesso-cadastro">
        <h2>Para criar uma nova senha preeencha os campos abaixo!</h2>

        @if($errors->any())
        <ul>
            {{ implode('', $errors->all('<li>:message</li>'))}}
        </ul>
        @endif

        {{ Form::open(array('method' => 'POST')) }}
            <ul class="remider">
                {{ Form::hidden('token', $token) }}
                <li>
                    <label class="email">E-mail*: {{ Form::text('email', Input::old('email')) }}</label>
                </li>
                <li>
                    <label class="nova">Nova senha*: {{ Form::password('password', Input::old('password')) }}</label>
                </li>
                <li>
                    <label class="confirmar">Confirmar nova senha*: {{ Form::password('password_confirmation', Input::old('password_confirmation')) }}</label>
                </li>
                <li class="obs">Campo obrigatório*</li>
                <li class="botao"><button class="btn">Salvar alterações</button></li>
            </ul>
        {{ Form::close() }}
    </div>
</div>

@stop