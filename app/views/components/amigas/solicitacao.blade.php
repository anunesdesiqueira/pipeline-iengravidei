@if(count($requests))
    <h2>Solicitação novas amizades</h2>
    <ul class="listar">
    @foreach ($requests as $request)
        <li rel="{{ $request['public_id'] }}">
            <div class="foto">
                <div class="thumb">
                    <a href="{{ route('user.public', array('publicId' => $request['public_id'])) }}">
                        <img src="{{ $request['profile_picture']['small'] }}" />
                    </a>
                </div>
            </div>
            <div class="description">
                <h3><a href="{{ route('user.public', array('publicId' => $request['public_id'])) }}">{{ $request['name'] }}</a></h3>
                <a href="#" class="excluir">Excluir</a>
                <a href="#" class="aceitar">Aceitar</a>
            </div>
        </li>
    @endforeach
    </ul>
    {{ $requests->links() }}
    <hr class="stroke-amigas">
@endif
