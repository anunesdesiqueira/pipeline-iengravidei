@extends('layouts.master')

@section('cadastro')
<!-- Só aparece na HOME -->
<section class="home">
    <div class="holder">
        <h1><span>iengravidei</span></h1>
        <div class="box-cadastro">
            <ul>
                <li class="facebook">Crie um novo login ou <a href="#">Facebook</a></li>
                <li><label>Seu Nome: <input type="text" style="width:370px;" /></label></li>
                <li><label>Nome do Bebê<span>*</span>: <input type="text" style="width:330px;" /></label></li>
                <li><label>E-mail: <input type="text" style="width:399px;" /></label></li>
                <li>
                    <label>Senha: <input type="text" style="width:114px;" /></label>
                    <label style="padding-left:8px;">Confirmar Senha: <input type="text" style="width:114px;" /></label>
                </li>
                <li><p>* CAMPO OBRIGATÓRIO</p></li>
                <li><input type="submit" value="CADASTRAR" /></li>
            </ul>
        </div>
        <a href="#" class="veja-mais">Veja mais</a>
    </div>
</section>
<!-- End: Só aparece na HOME -->
@stop

@section('content')
<div class="holder">

    <div class="cabecalho"><img src="<?php echo asset('assets/img/marcacao-header.png') ?>" /></div>

    <div class="notificacoes">
        <h2>notificações</h2>

        <ul>

            @foreach($notifications as $month => $item)
            <li>
                <h3>{{ $month }}</h3>
                <ul>

                    @foreach($item as $notification)
                    <li>
                        <a href="#" class="excluir">Excluir</a>
                        <span>{{ $notification['day_time']}}</span>
                        <a href="#">{{ $notification['message']}}</a>
                    </li>
                    @endforeach

                </ul>
            </li>
            @endforeach

        </ul>

    </div>

</div>
@stop

