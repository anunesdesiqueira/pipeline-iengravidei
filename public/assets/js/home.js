// Ativa plugin do zoom na home
/*$('#zoom').elevateZoom({
    zoomType: "lens",
    lensShape: "round",
    lensSize: 200
});*/

$('.image-popup-pagina').magnificPopup({
    type: 'image',
    closeOnContentClick: true,
    closeBtnInside: false,
    fixedContentPos: true,
    mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
    image: {
        verticalFit: true
    },
    zoom: {
        enabled: true,
        duration: 300 // don't foget to change the duration also in CSS
    }
});
