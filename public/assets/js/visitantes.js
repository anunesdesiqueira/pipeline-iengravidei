$(document.body).confirmOn({
    questionText: 'Quer mesmo excluir essa solicitação?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.visitantes .visitantes-lista li a.excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/visitantes/deletar','POST',{ 'hash' : $(e.toElement).attr('data-url') },'','',successVisitanteExcluir);
    }
});

// Pagination
$(document.body).on('click', '.visitantes ul.pagination li a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'GET','','','',successVisitantes);
});

$(document.body).on('click', '.visitantes .visitantes-lista li a.liberar-acesso', function(e) {
    e.preventDefault();
    $(this).html('Enviando...')
    CallMethod(baseUri + '/meu-perfil/visitantes/convite','POST',{ hash : $(this).attr('data-url') },'','',successEnviarConvite);
});

function successVisitanteExcluir(data) {
    if(data.status !== undefined) {
        CallMethod(baseUri + '/meu-perfil/visitantes','GET','','','',successVisitantes);
    }
}

function successVisitantes(data) {
    if(data.page != undefined) {
        var container = $('.visitantes');
        container.empty().html(data.page);
    }
}

function successEnviarConvite(data) {
    if(data.status !== undefined) {
        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                    '<h6>Convite enviado com sucesso.</h6>' +
                    '</div>',
                type: 'inline'
            }]
        });
    }

    $('.visitantes .visitantes-lista li a.liberar-acesso').html('LIBERAR ACESSO');
}

