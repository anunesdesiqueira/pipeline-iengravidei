/* Enquetes */

// Edit form dados
function validatorFormPoll()
{
    $('form#form-poll-create').validate({
        rules: {
            question : {
                required: true
            },
            'answer[]' : {
                required: true
            }
        },
        messages: {
            question : {
                required: "*Pergunta é obrigatório."
            },
            'answer[]' : {
                required: "*Preecha no minimo 2 campos de repostas."
            }
        }
    });
}

jQuery.validator.addMethod('requiredAnswer', function(value, element) {
    console.log(value);
}, "*Você precisa pelo menos criar 2 repostas para sua enquete.");

$(function() {
    validatorFormPoll();
});

/* Create Poll */
$(document.body).on('submit', 'form#form-poll-create', function(e) {
    e.preventDefault();
    var params = $(this).serialize();
    CallMethod(baseUri + '/meu-perfil/enquete','POST',params,loadingShowEnquetes,loadindHideEnquetes,successEnquetes);
});

function loadingShowEnquetes() {
    var button = document.getElementsByClassName('btn-enquete-ativar');
    $(button).val('Criando...');
    $(button).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideEnquetes() {
    var button = document.getElementsByClassName('btn-enquete-ativar');
    $(button).val('Ativar');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
}

function successEnquetes(data) {
    if(data.page != undefined) {
        $.magnificPopup.open({
            items: [{
                src:'<div class="enquete-popup">' +
                    '<h6>Enquete criada com sucesso!</h6>' +
                    '<div class="container">' +
                    '<a class="bt-popup-enquete-continuar" href="javascript:void(0)">Continuar</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
        var container = $('.enquete');
        container.empty().html(data.page);

        //Restart poll
        loadPoll();
    }
}

// Close Poll
$(document.body).confirmOn({
    questionText: 'Você tem certeza que deseja encerrar a enquete?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.btn-enquete-encerrar', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/enquete/encerrar','POST',{ slug : $(e.toElement).attr('rel') },loadingShowActive,loadindHideActive,successActive);
    }
});

function loadingShowActive() {
    var button = document.getElementsByClassName('btn-enquete-encerrar');
    $(button).val('Encerrando...');
    $(button).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideActive() {
    var button = document.getElementsByClassName('btn-enquete-encerrar');
    $(button).val('Encerrar');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
}

function successActive(data) {
    if(data.page != undefined) {
        var container = $('.enquete');
        container.empty().html(data.page);

        //Restart poll
        loadPoll();
    }
}

$(document.body).on('click', '.bt-popup-enquete-continuar', function(e) {
    e.preventDefault();
    $.magnificPopup.close();
});