<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\PollRepositoryInterface as Poll;
use IEngravidei\Repositories\AlbumRepositoryInterface as Album;
use IEngravidei\Repositories\GiftRepositoryInterface as Gift;

class AlbumsController extends BaseController
{
	public function __construct(Profile $profile, Poll $poll, Album $album, Gift $gif)
	{
        parent::__construct();

        $this->profile = $profile;
        $this->poll = $poll;
        $this->album = $album;
        $this->gift = $gif;
	}

    public function myAlbums()
    {
        $albums = $this->album->getAlbums($this->user->id);
        $albums = $this->createPathImagesAlbum($albums);
        $albums = $this->createUrl($albums, 'edit_my_albums');

        $limitAlbum = "all";
        $countAlbums = $this->album->countAlbuns($this->user->id);
        $upAlbum = 'album-container';
        $uriAlbum = 'my_albums.add_album';

        return View::make('albums.my_albums', compact('albums', 'limitAlbum','uriAlbum','upAlbum','countAlbums'));
    }

    public function addAlbum()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'name' => 'required|max:100',
            'images' => 'required',
            'limit' => 'required',
        ), array(
            'name.required'  => 'Digite o nome do Álbum',
            'images.required'  => 'Opps! Não pode estar vazio',
            'limit.required' => 'Opps! Não pode estar vazio',
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $data['id'] = $this->user->id;
            $createAlbum = $this->album->createAlbum($data);

            if($createAlbum) {
                $pathAlbum = $this->getPath($this->user->id, $createAlbum->slug);
                if($this->moveFilesTmpPath($data['images'], $createAlbum->id, $pathAlbum)) {
                    $limit = null;

                    if("All" !== $data['limit'])
                        $limit = $data['limit'];

                    $albums = $this->album->getAlbums($this->user->id, $limit);
                    $albums = $this->createPathImagesAlbum($albums);
                    $albums = $this->createUrl($albums, 'edit_my_albums');
                    $countAlbums = $this->album->countAlbuns($this->user->id);

                    $pages = array();
                    $pages['page'] = View::make('components.album.lista', compact('albums'))->render();
                    $pages['pageCriar'] = View::make('components.album.bt_criar', compact('countAlbums'))->render();
                    if(!is_null($limit)) {
                        $pages['pageTodos'] = View::make('components.album.bt_todos', compact('countAlbums'))->render();
                    }

                    return Response::json($pages);
                }
            }
        }
    }

    public function photo()
    {
        if (Input::hasFile('file')) {
            $file = Input::file('file');
            $photoUpload = $this->uploadAlbumPhoto($file);
            return $photoUpload;
        }
    }

    public function edit($slug)
    {
        $album = $this->album->getAlbum($slug);
        $photos = $this->album->getPhotos($slug);
        $photos = $this->createPathImagesAlbum($photos);

        $limitAlbum = "all";
        $countAlbums = $this->album->countAlbuns($this->user->id);
        $upAlbum = 'album-container-editar';
        $uriAlbum = 'my_albums.upd_album';
        $countAlbumFotosAdd = count($photos);

        return View::make('albums.my_albums_edit', compact('album','limitAlbum','countAlbums','upAlbum','uriAlbum','countAlbumFotosAdd','photos'));
    }

    public function nameAlbum($slug)
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'name' => 'required'
        ), array(
            'name.required' => 'O nome é obrigatório!'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $data['id_profile'] = $this->user->id;
            $data['slug'] = $slug;

            $update = $this->album->updateName($data);
            if ($update)
                return Response::json(array('status' => true));
            else
                return Response::json(array('status' => false));
        }
    }

    public function delAlbum()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'slug' => 'required'
        ), array(
            'slug.required' => 'É obrigatório!'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $data['id_profile'] = $this->user->id;

            $delete = $this->album->deleteAlbum($data);
            if ($delete)
                return Response::json(array('status' => true));
            else
                return Response::json(array('status' => false));
        }
    }

    public function updAlbum()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'slug' => 'required',
            'images' => 'required'
        ), array(
            'slug.required'  => 'Opps! Não pode estar vazio',
            'images.required'  => 'Opps! Não pode estar vazio'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $data['id'] = $this->user->id;
            $getAlbum = $this->album->getAlbum($data['slug']);

            if($getAlbum) {
                $pathAlbum = $this->getPath($data['id'], $data['slug']);
                if($this->moveFilesTmpPath($data['images'], $getAlbum->id, $pathAlbum)) {
                    $photos = $this->album->getPhotos($data['slug']);
                    $photos = $this->createPathImagesAlbum($photos);
                    $totalAlbumPhotos = $this->user->plan_roles['album_photo_limited'] - count($photos);

                    $page = View::make('components.album.lista_editar', compact('photos'))->render();
                    return Response::json(array('page' => $page, 'totalAlbumPhotos' => $totalAlbumPhotos));
                }
            }
        }
    }

    public function delPhoto($slug)
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'photo_image' => 'required|integer'
        ), array(
            'photo_image.required' => 'A imagem é obrigatória',
            'photo_image.numeric' => 'Apenas números'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $data['id_profile'] = $this->user->id;
            $data['slug'] = $slug;
            $delete = $this->album->deletePhoto($data);

            if($delete) {
                $photos = $this->album->getPhotos($data['slug']);
                $photos = $this->createPathImagesAlbum($photos);
                $totalAlbumPhotos = $this->user->plan_roles['album_photo_limited'] - count($photos);

                $page = View::make('components.album.lista_editar', compact('photos'))->render();
                return Response::json(array('page' => $page, 'totalAlbumPhotos' => $totalAlbumPhotos));
            }
        }
    }

    private function getPath($id_profile, $slug)
    {
        $config = Config::get('iengravidei.pictures.album');
        $path 	= base_path() . $config['path'];
        $userPath = $path.$id_profile;

        if(!is_dir($userPath)) {
            mkdir($userPath, '0777');
            chmod($userPath, 0777);
        }

        if(!is_dir($userPath.'/'.$slug)) {
            mkdir($userPath.'/'.$slug, '0777');
            chmod($userPath.'/'.$slug, 0777);
        }

        return $id_profile.'/'.$slug.'/';
    }

    private function moveFilesTmpPath($imgs, $id_album, $pathAlbumUser)
    {
        $configTmp      = Config::get('iengravidei.pictures.tmp');
        $configAlbum    = Config::get('iengravidei.pictures.album');
        $pathTmp        = base_path() . $configTmp['path'];
        $pathAlbum      = base_path() . $configAlbum['path'];

        $filesTmp = scandir($pathTmp);

        foreach ($filesTmp as $file) {
            if (in_array($file, array(".",".."))) continue;

            foreach ($imgs as $img) {
                if ($img == $file) {
                    $ext = explode('.', $file);
                    $string = str_random(8);
                    $name = "{$string}.{$ext[1]}";

                    foreach($configAlbum['thumbnails'] as $sizes) {
                        $thumbName = "{$string}-{$sizes[0]}x{$sizes[1]}.{$ext[1]}";
                        Image::make($pathTmp.$file)->fit($sizes[0], $sizes[1])->save($pathAlbum.$pathAlbumUser.$thumbName);
                    }
                    if (copy($pathTmp.$file, $pathAlbum.$pathAlbumUser.$name)) {
                        $this->album->createAlbumPhoto($pathAlbumUser.$name, $id_album);
                        $delete[] = $pathTmp.$file;
                    }

                }
            }
        }

        foreach ($delete as $file) {
            unlink($file);
        }
        return true;
    }

    /**
     * Route public album
     */
    public function show($publicId)
    {
        $profile = $this->profile->find($publicId);
        $profile = $this->createPathImagesProfile(array($profile));
        $profile = $profile[0];
        $profile->month = $this->calcGestation($profile->pregnancy_weeks);
        $profile->countdown = (40 - $profile->pregnancy_weeks);

        $layout = $this->setBackgroundLayout($profile->layout);

        $gifts = $this->gift->show(array('id' => $profile->id, 'limit' => 4));

        if(isset($this->user->id)) {
            if($this->user->id === $profile->id)
                $permissions['profile_user'] = true;

            if($this->profile->getFriend($this->user->id, $profile->id) || isset($permissions['profile_user'])) {
                $permissions['amiga'] = true;

                $albums = $this->album->getAlbums($profile->id);
                $albums = $this->createPathImagesAlbum($albums);
                $albums = $this->createUrlPublic($albums, $profile->public_id, 'show_my_album_photos');
            }
        }

        $poll = $this->poll->getPoll($profile->id);
        if(!empty($poll->question)) {
            if($this->poll->findLogAnswer($poll->question->id, getenv("REMOTE_ADDR"), isset($this->user->id)? $this->user->id : '')) {
                $permissions['voted'] = true;
            }
        }
        return View::make('albums.show', compact('albums','profile', 'layout', 'poll','gifts','publicId', 'permissions'));
    }

    public function showAlbum($publicId, $slug)
    {
        $profile = $this->profile->find($publicId);
        $profile = $this->createPathImagesProfile(array($profile));
        $profile = $profile[0];
        $profile->month = $this->calcGestation($profile->pregnancy_weeks);
        $profile->countdown = (40 - $profile->pregnancy_weeks);

        $layout = $this->setBackgroundLayout($profile->layout);

        $gifts = $this->gift->show(array('id' => $profile->id, 'limit' => 4));

        $album = $this->album->getAlbum($slug);

        if(isset($this->user->id)) {
            if($this->user->id === $profile->id)
                $permissions['profile_user'] = true;

            if($this->profile->getFriend($this->user->id, $profile->id) || isset($permissions['profile_user'])) {
                $permissions['amiga'] = true;

                $photos = $this->album->getPhotos($slug);
                $photos = $this->createPathImagesAlbum($photos);
            }
        }

        $poll = $this->poll->getPoll($profile->id);
        if(!empty($poll->question)) {
            if($this->poll->findLogAnswer($poll->question->id, getenv("REMOTE_ADDR"), isset($this->user->id)? $this->user->id : '')) {
                $permissions['voted'] = true;
            }
        }
        return View::make('albums.show_album', compact('album','photos','profile', 'layout', 'poll','gifts','publicId', 'permissions'));
    }
}