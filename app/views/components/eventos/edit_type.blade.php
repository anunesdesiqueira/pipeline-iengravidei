<p>Tipo de evento:</p>
<ul class="lista-tipo-evento">
    <li class="{{ $type['class'] }}">
        <label>
            <div class="thumb"></div>
            {{ $type['name'] }}
        </label>
    </li>
</ul>