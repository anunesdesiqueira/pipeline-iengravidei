<?php

use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('messages', function($table) {
            $table->bigIncrements('id');
            $table->integer('parent_id');
            $table->integer('from_id');
            $table->integer('to_id');
            $table->text('body');
            $table->enum('type', array('private', 'wall'));
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('messages');
	}

}