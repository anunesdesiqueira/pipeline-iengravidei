<?php

class BaseController extends Controller
{
    public function __construct()
    {
        $includeTop = 'components.top';
        if(Auth::client()->check()) {
            $this->beforeFilter('plan.profile');
            $this->beforeFilter('published.profile');
            $this->beforeFilter('notification.profile');

            $includeTop = 'components.top_logged';

            $this->user = Auth::client()->get();
            $user = $this->createPathImagesProfile(array($this->user));
            $user = $this->createUrl($user, 'user.public');
            $user = $user[0];
            $layout = $this->setBackgroundLayout($user->layout);

            View::share(compact('user', 'layout'));
        }

        View::share(compact('includeTop'));
    }

	public function uploadProfilePicture($img, $id)
	{
		$config = Config::get('iengravidei.pictures.profile');
		$ext 	= $img->getClientOriginalExtension();
		$path 	= base_path() . $config['path'];
		$string = "{$id}-" . str_random(6);		
		$name 	= "{$string}.{$ext}";
		foreach($config['thumbnails'] as $names => $sizes) {
			$thumbName = "{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
			Image::make($img->getRealPath())->fit($sizes[0], $sizes[1])->save($path . $thumbName);
		}
		$img->move($path, $name);
		return $name;
	}

    public function uploadBabylinePicture($img, $id)
    {
        $config = Config::get('iengravidei.pictures.babyline');
        $ext 	= $img->getClientOriginalExtension();
        $path 	= base_path() . $config['path'];
        $string = "{$id}-" . str_random(6);
        $name 	= "{$string}.{$ext}";
        foreach($config['thumbnails'] as $names => $sizes) {
            $thumbName = "{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
            Image::make($img->getRealPath())->fit($sizes[0], $sizes[1])->save($path . $thumbName);
        }
        $img->move($path, $name);
        return $name;
    }

    public function uploadAlbumPhoto($img)
    {
        $config = Config::get('iengravidei.pictures.tmp');
        $path 	= base_path() . $config['path'];
        $name   = $img->getClientOriginalName();
        $img->move($path, $name);
        return $name;
    }

	public function uploadCustomLayoutPicture($img, $id)
	{
		$config = Config::get('iengravidei.pictures.custom_layout');
		$ext 	= $img->getClientOriginalExtension();
		$path 	= base_path() . $config['path'];
		$string = "{$id}-" . str_random(6);
		$name 	= "{$string}.{$ext}";
		$img->move($path, $name);
		return $name;
	}

    public function uploadMuralPhoto($img, $id)
    {
        $config = Config::get('iengravidei.pictures.mural');
        $ext 	= $img->getClientOriginalExtension();
        $path 	= base_path() . $config['path'];
        $string = "{$id}-" . str_random(6);
        $name 	= "{$string}.{$ext}";
        foreach($config['thumbnails'] as $names => $sizes) {
            $thumbName = "{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";

            list($width) = getimagesize($img->getRealPath());

            if($width > $sizes[0]){
                Image::make($img->getRealPath())->widen($sizes[0])->save($path . $thumbName);
            } else {
                Image::make($img->getRealPath())->save($path . $thumbName);
            }
        }
        $img->move($path, $name);
        return $name;
    }

    public function createPathImageMural($name)
    {
        $config = Config::get('iengravidei.pictures.mural');
        $photo = $name;
        $ext = pathinfo($photo, PATHINFO_EXTENSION);
        $string = basename($photo, ".{$ext}");
        $images = array('original' => $config['public_path'].$photo);
        foreach($config['thumbnails'] as $names => $sizes) {
            $images[$names] = "{$config['public_path']}{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
        }
        return $images;
    }

    public function createPathImagesProfile($data)
    {
        foreach ($data as $k => $v) {
            $config = Config::get('iengravidei.pictures.profile');

            if(!is_null($v->profile_picture)) {
                $photo = $v->profile_picture;
                $ext = pathinfo($photo, PATHINFO_EXTENSION);
                $string = basename($photo, ".{$ext}");

                $images = array('original' => $config['public_path'].$photo);

                foreach($config['thumbnails'] as $names => $sizes) {
                    $images[$names] = "{$config['public_path']}{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
                }
            } else {
                $images = array(
                    'original' => "{$config['public_path']}avatar_perfil_no_photo.jpg",
                    'small' => "{$config['public_path']}avatar_perfil_no_photo_60_x_60.jpg",
                    'medium' => "{$config['public_path']}avatar_perfil_no_photo_100_x_100.jpg",
                    'main' => "{$config['public_path']}avatar_perfil_no_photo_120_x_180.jpg"
                );
            }

            $data[$k]->profile_picture = $images;
        }

        return $data;
    }

    public function createPathImagesBabyline($data)
    {
        foreach ($data as $k => $v) {
            if(!is_null($v->picture)) {
                $config = Config::get('iengravidei.pictures.babyline');
                $photo = $v->picture;
                $ext = pathinfo($photo, PATHINFO_EXTENSION);
                $string = basename($photo, ".{$ext}");
                $images = array('original' => $config['public_path'].$photo);
                foreach($config['thumbnails'] as $names => $sizes) {
                    $images[$names] = "{$config['public_path']}{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
                }

                $data[$k]->picture = $images;
            }
        }
        return $data;
    }

    public function createPathImagesAlbum($albums)
    {
        foreach ($albums as $k => $album)
        {
            if (!is_null($album->picture))
            {
                $config = Config::get('iengravidei.pictures.album');
                $photo = $album->picture;
                $ext = pathinfo($photo, PATHINFO_EXTENSION);
                $string = basename($photo, ".{$ext}");

                $images = array('original' => $config['public_path'].$photo);

                foreach($config['thumbnails'] as $names => $sizes)
                {
                    $images[$names] = "{$config['public_path']}{$album->id_profile}/{$album->slug}/{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
                }

                $albums[$k]->picture = $images;
            }
        }

        return $albums;
    }

    public function createPathImagesProducts($products)
    {
        foreach ($products as $k => $product) {
            if (!is_null($product->picture))
            {
                $config = Config::get('iengravidei.pictures.gift');
                $photo = $product->picture;
                $ext = pathinfo($photo, PATHINFO_EXTENSION);
                $string = basename($photo, ".{$ext}");
                $images = array('original' => $config['public_path'].$photo);

                foreach($config['thumbnails'] as $names => $sizes)
                {
                    $images[$names] = "{$config['public_path']}{$string}-{$sizes[0]}x{$sizes[1]}.{$ext}";
                }

                $products[$k]->picture = $images;
            }
        }

        return $products;
    }

    public function createUrl($data, $route)
    {
        foreach ($data as $k => $v)
        {
            if(!is_null($v->public_id))
                $slug = $v->public_id;
            else
                $slug = $v->slug;

            $data[$k]->slug = route($route, $slug);
        }

        return $data;
    }

    public function createUrlPublic($data, $publicId, $route)
    {
        foreach ($data as $k => $v)
        {
            $slug = $v->slug;

            $data[$k]->slug = route($route, array($publicId, $slug));
        }

        return $data;
    }

    public function setBackgroundLayout($layout) {
        if($layout !== 0) {
            return array(
                'layout' => $layout,
                'background' => 'style="background-image: url(/assets/img/bg/pattern-' . $layout . '-body.png)"',
            );
        }
    }

    public function calcGestation($pregnancy_weeks)
    {
        if (!empty($pregnancy_weeks) || !is_null($pregnancy_weeks))
        {
            if ($pregnancy_weeks >= 1 && $pregnancy_weeks <= 4)
            {
                return 1;
            }
            elseif ($pregnancy_weeks >= 5 && $pregnancy_weeks <= 9)
            {
                return 2;
            }
            elseif ($pregnancy_weeks >= 10 && $pregnancy_weeks <= 13)
            {
                return 3;
            }
            elseif ($pregnancy_weeks >= 14 && $pregnancy_weeks <= 17)
            {
                return 4;
            }
            elseif ($pregnancy_weeks >= 18 && $pregnancy_weeks <= 22)
            {
                return 5;
            }
            elseif ($pregnancy_weeks >= 23 && $pregnancy_weeks <= 26)
            {
                return 6;
            }
            elseif ($pregnancy_weeks >= 27 && $pregnancy_weeks <= 31)
            {
                return 7;
            }
            elseif ($pregnancy_weeks >= 32 && $pregnancy_weeks <= 35)
            {
                return 8;
            }
            elseif ($pregnancy_weeks >= 36 && $pregnancy_weeks <= 40)
            {
                return 9;
            }
            else
            {
                return null;
            }
        }
    }

    public function truncate($str,$size)
    {
        if(strlen($str) > $size){
            $substr = substr($str,0,$size);
            $ex = explode(" ",$substr);
            $ex = array_slice($ex, 0,count($ex)-1);
            return implode(" ",$ex) . "...";
        }else{
            return $str;
        }
    }

    public function addDecimal($array)
    {
        $add = 0;

        foreach($array as $v)
        {
            $add = $add + $v->price;
        }

        return money_format('%.2n', $add);
    }
}