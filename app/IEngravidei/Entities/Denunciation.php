<?php namespace IEngravidei\Entities;

class Denunciation extends \Eloquent {

    protected static $validType = array(
      'PPI'    => 'Pedofilia e pornografia infantil',
      'ES'     => 'Exploração sexual',
      'AIC'    => 'Apologia e incitação ao crime',
      'NZM'    => 'Neonazismo',
      'AIPCA'  => 'Apologia e incitação a práticas cruéis contra animais',
      'CDICH'  => 'Calúnia, difamação, injúria e crimes contra a honra',
      'DA'     => 'Direitos autorais',
      'FI'     => 'Falsa identidade',
      'PRN'    => 'Pornografia',
      'VS'     => 'Vírus e Spam',
      'RXISR'  => 'Racismo, xenofobia e intolerância sexual ou religiosa',
      'OTR'    => 'Outros'
    );

    protected static $validPosted = array(
        'AUD'   => 'Áudio',
        'IMG'   => 'Imagens',
        'USE'   => 'Url/Sites Externos',
        'CTC'   => 'Comentários/Textos/Conversa',
        'VID'   => 'Vídeos',
        'PFR'   => 'Perfil',
        'OTR'   => 'Outros'
    );


    protected $table = "denunciations";

    public function validate($input)
    {
        $rules = array(
            'author'        => 'required',
            'type'          => 'required|in:'.implode(',',array_keys(self::$validType)),
            'email'         => 'required|email',
            'posted'        => 'required|in:'.implode(',',array_keys(self::$validPosted))
        );

        $messages = array(
            'author.required'       => 'Escolha a opção GOSTARIA DE SE IDENTIFICAR AO FAZER ESSA DENÚNCIA',
            'type.required'         => 'Escolha QUE TIPO DE DENÚNCIA VOCÊ QUER FAZER',
            'type.in'               => 'TIPO DE DENÚNCIA VOCÊ QUER FAZER inválido',
            'email.required'        => 'Digite um E-mail para contato',
            'email.email'           => 'Digite um E-mail válido',
            'posted.required'       => 'Escolha  QUE TIPO DE CONTEÚDO FOI POSTADO',
            'posted.in'             => 'TIPO DE CONTEÚDO FOI POSTADO inválido'
        );

        return \Validator::make($input, $rules, $messages);
    }
}