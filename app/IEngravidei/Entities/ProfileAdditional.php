<?php

namespace IEngravidei\Entities;

class ProfileAdditional extends \Eloquent
{
    protected $table = 'profiles_additional';
    protected $guarded = array();
    public $timestamps = false;
    public $key = 'id_profile';
}