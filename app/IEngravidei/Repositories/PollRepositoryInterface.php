<?php

namespace IEngravidei\Repositories;

interface PollRepositoryInterface
{
    public function getId($slug);
    public function findPollActive($idProfile);
    public function getPoll($idUser);
    public function setPoll($data);
    public function upPoll($id, $status);
    public function history($idUser);
    public function addLogVote($data);
    public function findLogAnswer($idPoll, $id, $id_profile);
}