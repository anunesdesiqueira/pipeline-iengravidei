<?php

use Illuminate\Database\Migrations\Migration;

class CreateDenunciationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('denunciations', function($table) {
            $table->increments('id');
            $table->string('author');
            $table->string('type', 80);
            $table->string('email', 150);
            $table->string('s3_object', 200);
            $table->text('comments');
            $table->string('posted', 80);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('denunciations');
    }

}