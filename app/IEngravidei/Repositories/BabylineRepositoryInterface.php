<?php

namespace IEngravidei\Repositories;

interface BabylineRepositoryInterface
{
    public function getWeeks();
    public function findWeek($id, $number);
    public function getList($id);
    public function createImage($input);
    public function deleteImage($id);
}