<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\GiftRepositoryInterface as Gift;

class PaymentController extends BaseController
{
    private $_apiContext;

    private $_ClientId='ATgz1RC-k5j-zbte6V7tn4h0fygAVp8zbuuHdygD0hantFastSh4RQ9IyI_3';
    private $_ClientSecret='ELcKDhCkDy09qvtkhFQULM3FK1sIl0pHyqgVZ4IrH1MMEM8D2bxvw1eRS4Xk';

    private $_paymentId;

    public function __construct(Profile $profile, Gift $gift)
    {
        parent::__construct();
        $this->profile = $profile;
        $this->gift = $gift;

        $this->_apiContext = Paypalpayment::ApiContext(
            Paypalpayment::OAuthTokenCredential(
                $this->_ClientId,
                $this->_ClientSecret
            )
        );

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => __DIR__.'/../storage/logs/PayPal.log',
            'log.LogLevel' => 'FINE'
        ));
    }

    public function publicPaymentGifts($publicId)
    {
        $cart = Session::get('iengravidei.buypcart');

        if(count($cart) > 0) {
            $cartGifts = $this->gift->listProducts($cart);
        } else {
            return Response::view('errors.404', array(), 500);
        }

        $returnUrl = URL::to($publicId . '/lista-de-presentes/success');
        $cancelUrl = URL::to($publicId . '/lista-de-presentes/cancel');

        $payer = Paypalpayment::Payer();
        $payer->setPayment_method('paypal');

        $currency = "BRL";

        $items = array();
        $i = 0;
        $total = 0;

        foreach($cartGifts as $gift) {
            $items[$i] = Paypalpayment::Item();
            $items[$i]->setQuantity("1");
            $items[$i]->setName($gift->name);
            $items[$i]->setPrice($gift->price);
            $items[$i]->setCurrency($currency);

            $total = $total + $gift->price;
            $i++;
        }

        $total = number_format(round((float)$total,2),2);

        $item_list = Paypalpayment::ItemList();
        $item_list->setItems($items);

        $amount = Paypalpayment::Amount();
        $amount->setCurrency($currency);
        $amount->setTotal($total);

        $transaction = Paypalpayment::Transaction();
        $transaction->setAmount($amount);
        $transaction->setItem_list($item_list);

        $redirectUrls = Paypalpayment::RedirectUrls();
        $redirectUrls->setReturnUrl($returnUrl);
        $redirectUrls->setCancelUrl($cancelUrl);

        $payment = Paypalpayment::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirect_urls($redirectUrls);
        $payment->setTransactions(array($transaction));

        try {
            $response = $payment->create($this->_apiContext);

            if(Session::has('iengravidei.paymentId')) {
                Session::put('iengravidei.paymentId', '');
            }
            Session::push('iengravidei.paymentId', $response->id);

        } catch (\PPConnectionException $ex) {
            return "Exception: " . $ex->getMessage() . PHP_EOL;
            var_dump($ex->getData());
            exit(1);
        }

        return Redirect::to($this->getLink($payment->getLinks(), "approval_url"));
    }

    public function ExecutePaymentSuccess()
    {
        $paymentId = Session::get('iengravidei.paymentId');
        $payer_id = Input::get('PayerID');

        $payment = Paypalpayment::get($paymentId[0], $this->_apiContext);
        $execution = Paypalpayment:: PaymentExecution();
        $execution->setPayer_id($payer_id);
        $payment->execute($execution, $this->_apiContext);
        $executePayment = $payment->toArray();

        $data = array(
            'id_payment' => $executePayment['id'],
            'created_at' => $executePayment['create_time'],
            'updated_at' => $executePayment['update_time'],
            'payer_status' => $executePayment['payer']['status'],
            'payer_email' => $executePayment['payer']['payer_info']['email'],
            'payer_first_name' => $executePayment['payer']['payer_info']['first_name'],
            'payer_last_name' => $executePayment['payer']['payer_info']['last_name'],
            'payer_id' => $executePayment['payer']['payer_info']['payer_id'],
            'total' => $executePayment['transactions'][0]['amount']['total'],
        );

        var_dump($data); die;
    }

    public function ExecutePaymentCancel()
    {

    }

    /**
     * Utility method that returns the first url of a certain
     * type. Returns empty string if no match is found
     *
     * @param array $links
     * @param string $type
     * @return string
     */
    private function getLink(array $links, $type) {
        foreach($links as $link) {
            if($link->getRel() == $type) {
                return $link->getHref();
            }
        }
        return "";
    }
}