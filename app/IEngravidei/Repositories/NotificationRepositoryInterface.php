<?php namespace IEngravidei\Repositories;

interface NotificationRepositoryInterface
{
    public function push($profileId, $message);
    public function pull($profileId);
    public function numberOfNotifications($profileId);

}