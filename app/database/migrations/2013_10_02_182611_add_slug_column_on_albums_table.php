<?php

use Illuminate\Database\Migrations\Migration;

class AddSlugColumnOnAlbumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('albums', function($table)
        {
            $table->string('slug');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('albums', function($table)
        {
            $table->dropColumn('slug');

        });
	}

}