<?php

class NotificationsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('notifications')->truncate();

		$notifications = array(

            array(
                'profile_id' => 6,
                'message'    => 'Bem vinda ao IEngravidei',
                'created_at' => '2013-02-10 18:25:30',
            ),

            array(
                'profile_id' => 6,
                'message'    => 'Bianca Lopes te adicionou como amiga',
                'created_at' => '2013-02-15 20:21:32',
            ),

            array(
                'profile_id' => 6,
                'message'    => 'Palmeiras conquista o TRI da série B.',
                'created_at' => '2013-12-15 20:21:32',
            ),

            array(
                'profile_id' => 7,
                'message'    => 'Bem vinda ao IEngravidei',
                'created_at' => '2013-01-15 21:21:32',
            ),

            array(
                'profile_id' => 7,
                'message'    => 'Giselle Pinho aceitou sua solicitação de amizade',
                'created_at' => '2013-02-15 21:21:32',
            ),

		);

		// Uncomment the below to run the seeder
        DB::table('notifications')->truncate();
		DB::table('notifications')->insert($notifications);
	}

}
