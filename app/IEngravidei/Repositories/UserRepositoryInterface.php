<?php

namespace IEngravidei\Repositories;

interface UserRepositoryInterface
{
    public function find($id);
    public function create($input);
    public function friendships($id);
    public function getById($id);
    public function getByPublicId($publicId);
    public function getByEmail($email);
    public function getByFacebookId($facebookId);
	public function createFromFacebook($uid, $me, $access_token);
    public function setActive($activationCode);
    public function completeProfile($user);	
    public function editProfile($user);	
	public function validateProfileEditing($input);
    public function saveLayout($profileId, $layout);
}