<p>Dados do evento</p>
{{ Form::open(array('id' => 'form-event-edit-dados')) }}
    {{ Form::hidden('hash', $event->hash) }}
    <ul class="form-dados-pessoais">
        <li>
            <label>
                <span>Nome do evento*:</span>
                {{ Form::text('name', $event->name) }}

            </label>
        </li>
        <li>
            <label>
                <span>Data do evento*:</span>
                {{ Form::text('date', $event->date) }}

            </label>
        </li>
        <li>
            <label>
                <span>Nome da mamãe*:</span>
                {{ Form::text('mom_name', $event->mom) }}
            </label>
        </li>
        <li>
            <label>
                <span>Nome do papai:</span>
                {{ Form::text('dad_name', $event->father) }}
            </label>
        </li>
        <li>
            <label>
                <span>Endereço*:</span>
                {{ Form::text('address', $event->address) }}
            </label>
        </li>
        <li>
            <label>
                <span>Horário*:</span>
                {{ Form::text('time', $event->time) }}
            </label>
        </li>
        <li>
            <label>
                <span>Comentários:</span>
                {{ Form::textarea('comments', $event->comments) }}
            </label>
        </li>
    </ul>
    {{ Form::submit('Salvar alterações', array('class' => 'salvar-alteracoes-evento')) }}
{{ Form::close(); }}