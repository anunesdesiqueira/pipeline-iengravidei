<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<!-- Só aparece na HOME -->
<section class="home">
	<div class="holder">		
		<h1><span>iengravidei</span></h1>
		<div class="box-cadastro">
			<ul>
				<li class="facebook">Crie um novo login ou <a href="#">Facebook</a></li>
				<li><label>Seu Nome: <input type="text" style="width:370px;" /></label></li>
				<li><label>Nome do Bebê<span>*</span>: <input type="text" style="width:330px;" /></label></li>
				<li><label>E-mail: <input type="text" style="width:399px;" /></label></li>
				<li>
					<label>Senha: <input type="text" style="width:114px;" /></label>
					<label style="padding-left:8px;">Confirmar Senha: <input type="text" style="width:114px;" /></label>
				</li>
				<li><p>* CAMPO OBRIGATÓRIO</p></li>
				<li><input type="submit" value="CADASTRAR" /></li>
			</ul>
		</div>
		<a href="#" class="veja-mais">Veja mais</a>
	</div>
</section>
<!-- End: Só aparece na HOME -->

<section class="content">
	<div class="holder">
	
		<div class="contatoList">
			<h2>Contato</h2>
			<h3>Perguntas Frequentes</h3>
			<ul>
				<li>
					<a href="#">1 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">2 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">3 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">4 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">5 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">6 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">7 - Como faço para criar um login gratuíto <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">8 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
				<li>
					<a href="#">9 - Lorem ipsim lorem capesim dist item <span>+</span></a>
					<div class="description">
						<p>Sed ut perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui  ut</p>
						<p>Perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
						<p>perspiciatis unde omnis iste natus errr sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione</p>
					</div>
				</li>
			</ul>			
			
			<div class="cadastre-tambem">
				<h5>Cadastre-se você também.</h5>
				<p>Na 1ª Rede Social para Gestantes do mundo!</p>
				<a href="#">CLIQUE AQUI</a>
			</div>
			
		</div>
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>