<?php

namespace IEngravidei\Entities;

class ProfileFacebook extends \Eloquent
{
    protected $table = 'profiles_facebook';
    protected $guarded = array();

    public $timestamps = false;
}