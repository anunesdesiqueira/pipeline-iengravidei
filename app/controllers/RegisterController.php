<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\BabylineRepositoryInterface as Babyline;

class RegisterController extends BaseController
{
    public function __construct(Profile $profile, Babyline $babyline)
    {
		parent::__construct();
        $this->profile = $profile;
        $this->babyline = $babyline;
    }

	public function store()
	{
        $data = Input::all();
        unset($data['_token']);

        $validation = Validator::make($data, array(
            'name' => 'required|max:200',
            'email' => 'required|email|max:200|unique:profiles',
            'password'  => 'required|between:5,16|confirmed'
        ), $this->profile->messages);

        if($validation->fails()) {
            return Redirect::route('home')->withInput()->withErrors($validation);
        } else {
            $register = $this->profile->profileCreate($data);

            $this->sendConfirmationEmail($register);

            return Redirect::route('home')
                ->with('flashMessage', 'Registro realizado com sucesso! Para ativar seu cadastro, clique no link que enviamos por e-mail.');
        }
	}

    private function sendConfirmationEmail($data)
	{
		$profile = array(
            'email' => $data->profile->email,
            'name' => $data->profile->name,
            'activation_code' => $data->activated->activation_code
        );

        $config = Config::get('iengravidei.mail');

		Mail::send('emails.confirm', $profile, function($message) use ($profile, $config) {
            $message->to($profile['email'], $profile['name'])->subject($config['subject']['confirm']);
		});
	}

	public function confirm($code)
	{
		$profile = $this->profile->setActive($code);

        if($profile) {
            Auth::login($profile);
            return Redirect::route('register.complete');
		} else {
            return Redirect::route('home')->with('flashMessage', 'O código é inválido ou seu cadastro já está ativo!');
        }
	}

	public function facebook()
	{
		$code = Input::get('code');
		if (strlen($code) == 0)
            return Redirect::to('/')->with('flashMessage', 'Aconteceu um problema na comunicação com o Facebook!');

		$facebook = new Facebook(Config::get('iengravidei.facebook'));
		$uid = $facebook->getUser();

		if ($uid == 0)
			return Redirect::to('/')->with('message', 'There was an error');

		$profile = $this->profile->findFacebook($uid);

		if ($profile) {
            $this->profile->profileUpdateFacebook($uid, $facebook->getAccessToken());
		} else {
            $profile = $this->profile->profileCreateByFacebook($uid, $facebook->api('/me'), $facebook->getAccessToken());
		}

        Auth::login($profile);

        return Redirect::route('home');
	}

    public function complete()
    {
        $data = Input::all();

        if($data) {
            $data['id'] = $this->user->id;

            $validation = Validator::make($data, array(
                'profile_picture' => 'image|max:200|mimes:jpg,jpeg,png,gif',
                'father_name' => 'max:155',
                'baby_name' => 'max:155',
                'birth' => 'required|date_format:d/m/Y',
                'country_id' => 'required|integer',
                'state_id' => 'required|integer',
                'city_id' => 'required|integer',
                'pregnancy_weeks' => 'required|numeric',
                'public_id' => 'required|unique:profiles,public_id,{$data["id"]}|alpha_num|max:30'
            ), array(
                'profile_picture.image' => 'O arquivo enviado não é uma imagem',
                'profile_picture.max' => 'O arquivo deve ser menor que :max',
                'profile_picture.mimes' => 'A imagem de perfil deve ser do tipo JPG, PNG ou GIF',
                'father_name.max' => 'O nome do pai deve conter no máximo :max caracteres',
                'baby_name.max' => 'O nome do bebê deve conter no máximo :max caracteres',
                'birth.required' => 'A data de nascimento é obrigatória',
                'birth.date_format' => 'O formato da data de nascimento é inválida',
                'country_id.required' => 'País é obrigatório',
                'country_id.integer' => 'O campo país é inválido',
                'state_id.required' => 'Estado é obrigatório',
                'state_id.integer' => 'O campo estado é inválido',
                'city_id.required' => 'Cidade é obrigatório',
                'city_id.integer' => 'O campo cidade é inválido',
                'pregnancy_weeks.required' => 'Semanas de gravidez é obrigatório',
                'pregnancy_weeks.numeric' => 'Digite apenas números para informar as semanas de gravidez',
                'public_id.required' => 'O nome de usuário é obrigatório',
                'public_id.unique' => 'O nome de usuário escolhido já está sendo utilizado',
                'public_id.alpha_num' => 'O nome de usuário deve conter apenas letras e números',
                'public_id.max' => 'O nome do usuário deve conter no máximo :max caracteres',
            ));

            if ($validation->fails()) {
                return Redirect::route('register.complete')->withInput()->withErrors($validation);
            } else {
                if(isset($data['profile_picture'])) {
                    $data['profile_picture'] = $this->uploadProfilePicture($data['profile_picture'], $data['id']);
                }

                $update = $this->profile->profileUpdate($data);

                if($update) {
                    return Redirect::route('plan');
                } else {
                    return Redirect::route('register.complete')->with('flashMessage', 'Ops! Ocorreu um erro na atualização dos dados.');
                }
            }
        }

        $countries = Cache::rememberForever('countries', function() {
            return DB::table('countries')->lists('name', 'id');
        });

        $states = Cache::rememberForever("states", function() {
            return DB::table('states')->lists('name', 'id');
        });

        $pregnancy_weeks = Cache::rememberForever('pregnancy_weeks', function() {
            return $this->babyline->getWeeks();
        });

        return View::make('users.complete', compact('countries', 'states', 'pregnancy_weeks'));
    }

    public function getCities($id)
    {
        return Response::json(DB::table('cities')->where('state_id', '=', $id)->lists('name', 'id'));
    }

    public function isPublicIdAvailable($public_id)
    {
        return Response::json(array('available' => $this->profile->getPublicId($public_id) ? 0 : 1));
    }

    public function plan()
    {
        return View::make('users.plan');
    }

    public function planRegister($plan)
    {
        $plan = $this->profile->findPlan($plan);
        $setPlan = $this->profile->setPlan($this->user->id, $plan->id);

        if($setPlan)
            return Redirect::route('user.my_profile');
        else
            return Redirect::route('users.plan')->with('flashMessage', 'Ops! Ocorreu um erro na escolha do plano.');
    }

    public function published()
    {
        $this->profile->setPublished($this->user->id);
        return Redirect::route('user.my_profile');
    }
}