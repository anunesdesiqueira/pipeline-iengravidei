<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<section class="content">
	<div class="holder">
		
		<!--## CABECALHO 1 ##-->
		<div class="cabecalho">
			
			<div class="foto"><img src="img/marcacao-perfil.jpg" /></div>
			
			<div class="atividades">
				<h2>Ana Paula Morais</h2>
				<a href="#" class="ver-perfil"><span>&nbsp;</span>Ver Perfil</a>
				<ul class="listaBotoes">
					<li><a href="#" class="editar"><span>&nbsp;</span>Editar Perfil</a></li>
					<li><a href="#" class="recados"><span>&nbsp;</span>Recados Especiais</a></li>
					<li><a href="#" class="eventos"><span>&nbsp;</span>Eventos</a></li>
					<li><a href="#" class="album"><span>&nbsp;</span>Albúm de Fotos</a></li>
					<li><a href="#" class="lista-amigas"><span>&nbsp;</span>Lista de Amigas</a></li>
					<li><a href="#" class="mensagens"><span>&nbsp;</span>Mensagens</a></li>
					<li><a href="#" class="presentes"><span>&nbsp;</span>Lista de Presentes</a></li>
					<li><a href="#" class="notificacoes"><span>&nbsp;</span>Notificafacões</a></li>
				</ul>
			</div>
			
			<!-- Enquete -->
			<div class="enqueteBox">
				<h3>Enquete</h3>
				<p>Estou na dúvida de qual nome colocar, qual vocês preferem?</p>
				<ul>
					<li>
						<label>ALEX</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:50%;"></div>
						</div>
						<span>50%</span>
					</li>
					<li>
						<label>JOÃOZINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:25%;"></div>
						</div>
						<span>25%</span>
					</li>
					<li>
						<label>PEDRINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:10%;"></div>
						</div>
						<span>10%</span>
					</li>
					<li>
						<label>ASTROGILDO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:15%;"></div>
						</div>
						<span>15%</span>
					</li>
				</ul>				
				<input type="submit" value="Vote" />
				<button>Ver Resultado</button>
			</div>
			<!-- End: Enquete -->
			
			<!-- Compartilhe -->
			<div class="clearfix"></div>
			<a href="#" class="compartilhe">Compartilhe</a>
			<!-- End: Compartilhe -->
		</div>
		<!--## END: CABECALHO 1 ##-->
		
		<div class="recados">
			<h2>recados especiais</h2>
			
			<ul>
				<li>
					<a href="#" class="excluir">Excluir</a>
					<span class="data">12/02/2015</span>
					<div class="clearfix"></div>
					<h3>Regina Maria Oliveira</h3>
					<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
				</li>
				<li>
					<a href="#" class="excluir">Excluir</a>
					<span class="data">12/02/2015</span>
					<div class="clearfix"></div>
					<h3>Regina Maria Oliveira</h3>
					<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
				</li>
				<li>
					<a href="#" class="excluir">Excluir</a>
					<span class="data">12/02/2015</span>
					<div class="clearfix"></div>
					<h3>Regina Maria Oliveira</h3>
					<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
				</li>
				<li>
					<a href="#" class="excluir">Excluir</a>
					<span class="data">12/02/2015</span>
					<div class="clearfix"></div>
					<h3>Regina Maria Oliveira</h3>
					<p>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
				</li>
			</ul>
			
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>