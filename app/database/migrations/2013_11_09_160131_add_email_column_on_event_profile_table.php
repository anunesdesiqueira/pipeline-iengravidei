<?php

use Illuminate\Database\Migrations\Migration;

class AddEmailColumnOnEventProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('event_profile', function($table)
        {
            $table->string('email')->nullable()->after('profile_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('event_profile', function($table)
        {
            $table->dropColumn('email');
        });
	}

}