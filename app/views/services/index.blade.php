@extends('layouts.master')

@if(!Auth::client()->check())
    @section('cadastro')
        @include('components.register')
    @stop
@endif

@section('content')
<div class="holder">
    @if(Auth::client()->check())
        @include('components.cabecalho.header_active')
    @endif
    <div class="servicos-exclusivos">
        <h2 name="servicos">Serviços Exclusivos</h2>
        <p>Crie seu perfil e veja tudo que a 1ª Rede Social para Gestantes do Mundo tem para você!</p>
        <ul>
            <li>
                <div class="thumb-servicos ico-mural"></div>
                <div class="description">
                    <h3 class="t-mural">Meu Mural</h3>
                    <p>Aqui é o lugar para postar e guardar para sempre seus momentos especiais. Vídeo do ultrassom, sentimentos do dia-a-dia e ainda pode compartilhar tudo em suas redes sociais. </p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-presentes"></div>
                <div class="description">
                    <h3 class="t-presentes">Lista de Presentes</h3>
                    <p>Crie sua lista de presentes e receba tudo sem sair do site! Facilite a sua vida e de seus amigos!</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-descontos"></div>
                <div class="description">
                    <h3 class="t-descontos">Clube de Descontos</h3>
                    <p>Dependendo do seu plano, você tem descontos em lojas parceiras que podem ser muito maiores que o valor da assinatura. Afinal, quem não gosta de economizar?</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-baby"></div>
                <div class="description">
                    <h3 class="t-baby">Baby Line</h3>
                    <p>Uma linha para postar uma foto por semana e ver o desenvolvimento da barriga. Muito legal!</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-profissionais"></div>
                <div class="description">
                    <h3 class="t-profissionais">Dicas Profissionais</h3>
                    <p>Médicos, terapeutas, decoradoras e muitos outros profissionais irão dar as melhroes dicas para você.</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-enquete"></div>
                <div class="description">
                    <h3 class="t-enquete">Enquete</h3>
                    <p>Crie enquetes para pedir a opinião dos amigos: Que nome colocar? Que escola indicam? Tudo muito fácil!</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-recados"></div>
                <div class="description">
                    <h3 class="t-recados">Recados Especiais</h3>
                    <p>Suas amigas, cadastradas ou não, poderão deixar mensagens num só lugar, desejando tudo de bom para você e seu bebê!</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-convites"></div>
                <div class="description">
                    <h3 class="t-convites">Personalização de Convites</h3>
                    <p>Escolha o fundo de tela  com detalhes para meninos ou meninas ou até escolha uma foto sua! Todos os seus amigos verão a página com o seu jeito.</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-fotos"></div>
                <div class="description">
                    <h3 class="t-fotos">Álbum de Fotos</h3>
                    <p>Crie álbuns exclusivos para guardar os momentos desses meses inesquecíveis!</p>
                </div>
            </li>
            <li>
                <div class="thumb-servicos ico-eventos"></div>
                <div class="description">
                    <h3 class="t-eventos">Criador de Eventos</h3>
                    <p>Crie listas de presentes e convites lindos e super fofos para chamar os amigos para o Chá de Bebê, Chá de Fralda, Nascimento ou o que você quiser!</p>
                </div>
            </li>
        </ul>
        @if(!Auth::client()->check())
            @include('components.cadastro')
        @endif
    </div>
</div>
@stop