// Personalizar
$(document).on('click', '.customizacao .botoes button', function(e) {
    e.preventDefault();
    var radio = $('.customizacao ul li input[type="radio"]:checked').val();
    $('body').css({ 'background-image' : 'url(/assets/img/bg/pattern-'+radio+'-body.png)' });
});