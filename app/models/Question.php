<?php

class Question extends Eloquent {

    protected $table = 'questions';
    protected $guarded = array('id', 'id');

    public static function criar_pergunta($nome)
    {
        $questions = Question::create(array('enunciate' => $nome));

        return $questions->id;
    }
}