<?php

class ProfessionalTips extends \Eloquent
{

	protected $fillable = [
        'title',
        'body',
        'picture',
        'youtube',
        'author',
        'type',
        'pub_inc',
        'status'
    ];

}