<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use Illuminate\Support\Facades\Auth;

class UserAuthController extends BaseController {

    public function __construct(Profile $profile)
    {
        parent::__construct();
        $this->profile = $profile;
    }

    public function login()
	{
        $credentials = array(
			'email'     => Input::get('email'),
			'password'  => Input::get('password'),
            'deleted_at' => null
		);

		$remember = Input::has('remember') AND "1" === Input::get('remember') ? true : false;

		if ($this->profile->getActive($credentials['email']) && Auth::client()->attempt($credentials, $remember)) {
            $user = Auth::client()->get();

            if ($this->profile->existsProfileAdditional($user->id)) {
                $rolePlans = $this->profile->getPlan($user->id);

                if($rolePlans) {
                    return Redirect::route('user.my_profile');
                } else {
                    return Redirect::route('plan');
                }
            } else {
                return Redirect::route('register.complete');
            }
		}

		return Redirect::route('home')->withInput()->with('login_errors', true);
	}

    public function request()
    {
        if (Input::server("REQUEST_METHOD") == "POST") {
            $validation = \Validator::make(Input::all(), array(
                'email' => 'required|email'
            ), array(
                'email.required' => 'Digite o E-mail',
                'email.email' => 'Digite um E-mail valido',
            ));

            if($validation->fails()) {
                return Redirect::route('home')->withInput()->withErrors($validation);
            } else {
                $credentials = array('email' => Input::get('email'));
                $user = $this->profile->find($credentials['email']);

                Password::client()->remind($credentials, function($message, $user) {
                    $message->subject(Config::get('iengravidei.mail.subject.reset_password'));
                });

                return Redirect::route('home')->with('flashMessage', 'Lembrete de senha enviado com sucesso!');
            }
        }
        return Redirect::route('home');
    }

    public function reset($token)
    {
        if (Input::server("REQUEST_METHOD") == "POST") {
            $validation = \Validator::make(Input::all(), array(
                'email'     => 'required|email',
                'password'  => 'required|between:5,16|confirmed'
            ));

            if($validation->passes()) {
                $credentials = array('email' => Input::get('email'));

                return Password::client()->reset($credentials, function($user, $password) {
                    $user->password = Hash::make($password);
                    $user->save();
                    return Redirect::route('home')->with('flashMessage', 'Senha alterada com sucesso!');
                });
            }
        }

        return View::make('users.reset_password')->with('token', $token);
    }

    public function logout()
    {
        $user = Auth::client()->get();

        unset($user->profile_picture);
        unset($user->slug);
        unset($user->plan_roles);

        Auth::client()->logout();

        return Redirect::route('home');
    }
}