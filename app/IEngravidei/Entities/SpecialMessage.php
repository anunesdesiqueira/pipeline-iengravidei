<?php namespace IEngravidei\Entities;

class SpecialMessage extends \Eloquent
{
    protected $table = 'profiles_special_messages';
    protected $guarded = array();
}