<?php

namespace IEngravidei\Libraries;

use Aws\Common\Aws;
use Hashids\Hashids;
use PHPImageWorkshop\ImageWorkshop;

class UploadPhotos implements UploadInterface
{

    public function toApp($file, $destinationPath, $publicId = null)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = $this->generateFilename() . '.' .$extension;
        $originalPath = $destinationPath;

        if( !is_null($publicId))
        {
            $originalPath = $destinationPath . '/' . $publicId . '/originals';
            $thumbPath = $destinationPath . '/' . $publicId . '/thumbs';
            $originalBucketName = sprintf("photos.%s.%s", $publicId, 'originals');
            $thumbBucketName = sprintf("photos.%s.%s", $publicId, 'originals');

        }

        $upload = $file->move($originalPath, $filename);

        if( $upload instanceof \Symfony\Component\HttpFoundation\File\File)
        {
            $fullPath = public_path() . '/'. $originalPath. '/' . $filename;
            $this->generateThumbnail($fullPath, $thumbPath, $filename, true);
            $this->toAwsS3($originalBucketName, $filename, $fullPath, 'private');

            //$this->toAwsS3($thumbBucketName, $filename, $fullPath, 'private');

            return array(
                'fullPath'  => $fullPath,
                'filename'  => $filename
            );
        }
        return false;

    }

    public function toAwsS3($bucketName, $objectName, $fullPath, $acl)
    {
        $s3Client = \App::make('aws')->get('s3');
        /*var_dump($s3Client);
        $result = $s3Client->createBucket(array(
            'Bucket'              => $bucketName,
            'LocationConstraint'  => \Aws\Common\Enum\Region::US_WEST_2
        ));
        var_dump($result);*/
        try
        {
            $s3Client->upload($bucketName, $objectName, fopen($fullPath, 'r'), $acl);

        }
        catch(S3Exception $e)
        {
            error_log("Erro ao subir arquivo da denuncia no S3");
            $s3Client = false;
        }

    }

    public function validInstanceOfUploadedFile($file)
    {
        return $file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile;
    }

    public function validMimeType($file, $mimes = array())
    {
        return in_array($file->getMimeType(), $validMimes);
    }

    public function checkFileSize($file, $maxSize)
    {
        return $file->getSize() > $maxSize;
    }

    public function generateFilename()
    {
        $hashids = new Hashids("Aik5eengChohxae5AeSh0aebGah1gairsahpoP7uahRaiVi7");
        return $hashids->encrypt(rand(1, $hashids->get_max_int_value()));
        //return $hashids->encrypt();
    }

    public function generateThumbnail($fullPath, $thumbPath, $filename, $crop = true, $options = array('image_quality' => 95, 'width' => 232, 'height' => 285))
    {

        $layer = ImageWorkshop::initFromPath($fullPath);

        if( true === $crop)
        {
            // Determine the largest expected side automatically
            ($options['width'] > $options['height']) ? $largestSide = $options['width'] : $largestSide = $options['height'];

            // Get a squared layer
            $layer->cropMaximumInPixel(0, 0, "MM");

            // Resize the squared layer with the largest side of the expected thumb
            $layer->resizeInPixel($largestSide, $largestSide);

            // Crop the layer to get the expected dimensions
            $layer->cropInPixel($options['width'], $options['height'], 0, 0, 'MM');
        }

        if( false === $crop)
        {
            $layer->resizeInPixel($options['width'], $options['height'], true, 0, 0, 'MM');
        }

        $layer->save($thumbPath, $filename, true, null, $options['image_quality']);


    }

}