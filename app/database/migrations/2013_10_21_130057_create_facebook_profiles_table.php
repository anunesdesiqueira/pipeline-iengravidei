<?php

use Illuminate\Database\Migrations\Migration;

class CreateFacebookProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('facebook_profiles', function($table)
        {
			$table->bigIncrements('id');
            $table->string('username');
            $table->string('email');
            $table->string('access_token');
            $table->timestamps();
        });

        Schema::table('profiles', function($table)
        {
			$table->bigInteger('facebook_uid')->after('id')->unsigned();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facebook_profiles');
		
        Schema::table('profiles', function($table)
        {
            $table->dropColumn('facebook_uid');
        });		
	}

}