/* Edit lists */
$(document.body).on('submit', 'form#form-lista-presentes-listar', function(e)
{
    e.preventDefault();
    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar',
        'POST',
        params,
        loadingShowEdit,
        loadindHideEdit,
        successEdit
    );
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-escolhidos ul.pagination a', function(e)
{
    e.preventDefault();
    showItensEdit($(this).attr('href'), successItensEscolhidosEdit);
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-nao-escolhidos ul.pagination a', function(e)
{
    e.preventDefault();
    showItensEdit($(this).attr('href'), successItensNaoEscolhidosEdit);
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-escolhidos ul li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar/escolhidos',
        'POST',
        {'prod' : $(this).attr('rel')},
        loadingShowEdit,
        loadindHideEdit,
        successItensRemoveEdit
    )
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-nao-escolhidos ul li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar/naoescolhidos',
        'POST',
        {'prod' : $(this).attr('rel')},
        loadingShowEdit,
        loadindHideEdit,
        successItensAddEdit
    )
});

$(document.body).on('submit', 'form#form-lista-presentes-editar', function(e)
{
    e.preventDefault();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar/save',
        'POST',
        params,
        loadingShowEdit,
        loadindHideEdit,
        successSaveEdit
    );
});

function loadingShowEdit() {}

function loadindHideEdit() {}

function successEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/escolhidos', successItensEscolhidosEdit);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/naoescolhidos', successItensNaoEscolhidosEdit);
    }
}

function successItensEscolhidosEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-escolhidos');
        container.empty().html(data.page);
    }
}

function successItensRemoveEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-escolhidos');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/naoescolhidos', successItensNaoEscolhidosEdit);
    }
}

function successItensNaoEscolhidosEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-nao-escolhidos');
        container.empty().html(data.page);
    }
}

function successItensAddEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-nao-escolhidos');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/escolhidos', successItensEscolhidosEdit);
    }
}

function successSaveEdit(data)
{
    if(data.status == 'true')
    {
        $.magnificPopup.open({
            items: [{
                src: '<div class="popup-presentes-edit">' +
                    '<h6>Deseja continuar alterando essa lista?</h6>' +
                    '<div class="container">' +
                    '<a class="bt-popup-sim" href="javascript:void(0)">Sim</a>' +
                    '<a class="bt-popup-nao" href="javascript:void(0)">Não</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    }
    else if(data.error == 2)
    {
        msgEdit('A lista precisa ter pelo um presente!', $('.container-presentes'));
    }
    else
    {
        msgEdit('Ocorreu um erro, tente mais tarde!', $('.container-presentes'));
    }
}

$(document.body).on('click', '.popup-presentes-edit .bt-popup-sim', function(e)
{
    e.preventDefault();

    $.magnificPopup.close();
});

$(document.body).on('click', '.popup-presentes-edit .bt-popup-nao', function(e)
{
    e.preventDefault();

    $.magnificPopup.close();
    showItensEdit(baseUri + '/meu-perfil/lista-de-presentes', sucessLoadListar);
});

/* Criar Lista */
$(document.body).on('click', '.lista-presentes .container-listar a.bt-criar-lista', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/criar',
        'GET',
        '',
        loadingShowCriar,
        loadindHideCriar,
        successCriar
    );
});

function loadingShowCriar()
{
    var criar = document.getElementsByClassName('bt-criar-lista');
    $(criar).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideCriar()
{
    $(document.getElementsByClassName('bt-criar-lista')).css({color : '#867a42'}).prop({ disabled : false });
}

function successCriar(data)
{
    if(data.page != undefined)
    {
        var lista = $('.lista-presentes');
        var container = lista.find('.container-presentes');

        lista.find('.container-listar').remove();

        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/criar/itens', successListProducts);
    }
}

/* Filters prices */
$(document.body).on('submit', 'form#form-lista-presentes-filter', function(e)
{
    e.preventDefault();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/criar/itens',
        'GET',
        params,
        loadingShowFilters,
        loadindHideFilters,
        successListProducts
    );

});

function loadingShowFilters()
{
    var submit = document.getElementsByClassName('bt-lista-presentes-filter');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideFilters()
{
    $(document.getElementsByClassName('bt-lista-presentes-filter')).css({color : '#867a42'}).prop({ disabled : false });
}

/* Carrinho */
$(document.body).on('click', '.lista-presentes .container-presentes-produtos ul.itens-main li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/cart',
        'POST',
        {'prod' : $(this).attr('rel')},
        loadingShowCart,
        loadindHideCart,
        successCart
    )
});

$(document.body).on('click', '.lista-presentes .container-presentes-cart ul.itens-small li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/cart/remove',
        'POST',
        {'prod' : $(this).attr('rel'), 'action' : 'cart'},
        loadingShowCart,
        loadindHideCart,
        successCart
    )
});

function loadingShowCart() {}

function loadindHideCart() {}

function successCart(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-cart');
        container.empty().html(data.page);
    }
}

/* Pagination */
$(document.body).on('click', '.lista-presentes .container-presentes-produtos ul.pagination a', function(e)
{
    e.preventDefault();

    CallMethod(
        $(this).attr('href'),
        'GET',
        '',
        loadingShowListProducts,
        loadindHideListProducts,
        successListProducts
    );
});

/* Criar lista */
$(document.body).on('submit', 'form#form-lista-presentes-criar', function(e)
{
    e.preventDefault();

    if(!$(this).find('input[type="checkbox"]').is(':checked')) {
        msgEdit('Leia os termos e aceite para poder prosseguir!', $('form#form-lista-presentes-criar'));
        return false;
    }

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/corfimacao',
        'POST',
        $(this).serialize(),
        loadingShowListProducts,
        loadindHideListProducts,
        successConfirm
    );
});

/* Confirmar Paginação */
$(document.body).on('click', '.lista-presentes .container-presentes-produtos-confirm ul.pagination a', function(e)
{
    e.preventDefault();
    showItensEdit($(this).attr('href'), successItensCorfirm);

});

/* Confirmar lista */
$(document.body).on('submit', 'form#form-lista-presentes-criar-confirmacao', function(e)
{
    e.preventDefault();

    if('' == $(this).find('input[name="name"]').val())
    {
        msgEdit('Escolha um nome para essa lista!', $('form#form-lista-presentes-criar-confirmacao'));
        return false;
    }

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/save',
        'POST',
        $(this).serialize(),
        loadingShowListProducts,
        loadindHideListProducts,
        successCreateSave
    );
});

$(document.body).on('click', '.popup-presentes-confirm .bt-popup-sim', function(e)
{
    e.preventDefault();

    $.magnificPopup.close();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/criar',
        'GET',
        '',
        loadingShowCriar,
        loadindHideCriar,
        successCriar
    );
});

$(document.body).on('click', '.popup-presentes-confirm .bt-popup-nao', function(e)
{
    e.preventDefault();

    $.magnificPopup.close();

    showItensEdit(baseUri + '/meu-perfil/lista-de-presentes', sucessLoadListar);
});

/*$(document.body).on('click', 'form#form-create-lista-presentes ul li button', function(e)
 {
 e.preventDefault();

 CallMethod(
 baseUri + '/meu-perfil/lista-de-presentes/cart/remove',
 'POST',
 {'prod' : $(this).attr('rel'), 'action' : 'confirm'},
 loadingShowCart,
 loadindHideCart,
 successRemove
 )
 });*/

function loadingShowListProducts()
{
    var submit = document.getElementsByClassName('bt-salvar');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideListProducts()
{
    $(document.getElementsByClassName('bt-salvar')).css({color : '#867a42'}).prop({ disabled : false });
}

function successListProducts(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-produtos');
        container.empty().html(data.page);
    }
}

function successConfirm(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/corfimacao/itens', successItensCorfirm);
        $('form#form-lista-presentes-criar-confirmacao').find('input[type="text"]').focus();
    }
    else
    {
        msgEdit('Escolha os presentes da sua lista!', $('form#form-lista-presentes-criar'));
    }
}

function successItensCorfirm(data)
{
    var container = $('.lista-presentes .container-presentes-produtos-confirm');

    if(data.page != undefined)
    {
        container.empty().html(data.page);
    }
    else
    {
        container.empty().html('Ocorreu um erro ao carregar o conteúdo!');
    }
}

/*function successRemove(data)
 {
 if(data.page != undefined)
 {
 var container = $('.lista-presentes');
 container.find('.confirm-produtos').empty().html(data.page);
 }
 }*/

function successCreateSave(data)
{
    if('true' == data.status)
    {
        $.magnificPopup.open({
            items: [{
                src: '<div class="popup-presentes-confirm">' +
                    '<h6>Deseja criar uma outra Lista de Presentes?</h6>' +
                    '<div class="container">' +
                    '<a class="bt-popup-sim" href="javascript:void(0)">Sim</a>' +
                    '<a class="bt-popup-nao" href="javascript:void(0)">Não</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    }
    else if(1 == data.error)
    {
        msgEdit('A lista está vazia, adicione os presentes em sua lista!', $('form#form-lista-presentes-criar-confirmacao'));
    }
    else
    {
        msgEdit('Ocorreu um erro, tente mais tarde!', $('form#form-lista-presentes-criar-confirmacao'));
    }
}

function sucessLoadListar(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes');
        container.empty().html(data.page);
        loadSelectMask();
        $('select[name="listascriadas"]').focus();
    }
}

function showItensEdit(url, callBack)
{
    CallMethod(
        url,
        'GET',
        '',
        loadingShowEdit,
        loadindHideEdit,
        callBack
    );
}

function msgEdit(text, obj)
{
    var span = document.createElement('span');
    var container = obj;

    $(span).addClass('presentes-msg');
    $(span).html(text);

    $(container).append(span);

    $(container).find('.presentes-msg').fadeOut( 3000, function() {
        $(container).find('.presentes-msg').remove();
    });
}