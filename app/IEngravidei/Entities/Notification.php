<?php namespace IEngravidei\Entities;

class Notification extends \Eloquent {

    protected $table = 'notifications';

    public function validate($input)
    {

        $rules = array(
            'profile_id'    =>  'required|integer',
            'message'       =>  'required|max:200'
        );

        $messages = array(
            'profile_id.required'   =>  'Perfil do usuário inválido',
            'profile_id.integer'    =>  'Perfil do usuário inválido',
            'message.required'      =>  'Campo :attribute é obrigatório',
            'message.max'           =>  'O Limite para o campo :attribute é 200 caracteres'
        );

        return \Validator::make($input, $rules, $messages);

    }

}