<?php

namespace IEngravidei\Entities;

class PollLogs extends \Eloquent
{
    protected $table = 'profiles_polls_logs';
    protected $guarded = array();
    public $timestamps = false;
}