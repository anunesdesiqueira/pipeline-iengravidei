<div class="listaPresentes">
    <h2 title="Lista de Presentes">Lista de Presentes</h2>

    <aside>
        @include('components.lista_presentes.filtros')
        <div class="container-presentes-cart">
            @include('components.lista_presentes.carrinho')
        </div>
    </aside>

    <section>
        <h4>Crie sua lista de presentes</h4>
        {{ Form::open( array('route' => 'gift.confirm', 'id' => 'form-lista-presentes') ) }}
            <p>Os produtos são ilustrativos e representam uma média aproximada de valor de mercado para cada item. Ao receber um
                presente de um amigo, você receberá um e-mail informando o presente selecionado e a mensagem escrita por ele. E receberá os valores em
                Reais (descontadas taxa de serviço do site e do meio de pagamento - cartão de crédito/PayPal) diretamente em sua conta corrente
                cadastrada, com total segurança e certificação de pagamento seguro. Para maiores detalhes, veja aqui as políticas de serviços
                e segurança - clique aqui.</p>

            <label>{{ Form::checkbox('terms', 'aceito', false) }} Li e aceito os termos desse serviço.</label>

            <h4>Escolha os presentes que deseja incluir na lista</h4>

            <div class="list">
                @include('components.lista_presentes.produtos')
            </div>

            {{ Form::submit('Criar lista de presentes') }}
        {{ Form::close() }}
    </section>
</div>