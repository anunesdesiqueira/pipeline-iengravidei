<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<section class="content">
	<div class="holder">
	
		<div class="escolha-plano">
		
			<h2>Escolha um plano para finalizar seu cadastro</h2>
			
			<ul>
				<li>
					<h3 class="free">100% Grátis</h3>
					<ul>
						<li><span class="tickFree">&nbsp;</span>Perfil Individual</li>
						<li><span class="tickFree">&nbsp;</span>Baby Line</li>
						<li><span class="tickFree">&nbsp;</span>Mural</li>
						<li><span class="tickFree">&nbsp;</span>Lista de Presentes</li>
						<li><span class="tickFree">&nbsp;</span>Criador de Convites e Eventos</li>
						<li><span class="tickFree">&nbsp;</span>Álbum de Fotos (3 álbuns)</li>
					</ul>
					<a href="#" class="btnRed">Concluir Cadastro</a>
					<p>Parabéns!<br/>Agora é só confirmar o cadastro<br/>no seu e-mail e começar a usar.</p>
				</li>
				<li>
					<h3 class="premium"><span>Apenas</span>R$14<span class="cents">,90</span><span class="mes">/mês</span></h3>
					<ul>
						<li><span class="tickPremium">&nbsp;</span>Perfil Individual</li>
						<li><span class="tickPremium">&nbsp;</span>Baby Line</li>
						<li><span class="tickPremium">&nbsp;</span>Mural</li>
						<li><span class="tickPremium">&nbsp;</span>Lista de Presentes</li>
						<li><span class="tickPremium">&nbsp;</span>Criador de Convites e Eventos</li>
						<li><span class="tickPremium">&nbsp;</span>Álbum de Fotos (12 álbuns)</li>
						<li><span class="tickPremium">&nbsp;</span>Enquete</li>
						<li><span class="tickPremium">&nbsp;</span>Dicas de Profissionais</li>
						<li><span class="tickPremium">&nbsp;</span>Clube de Descontos</li>
					</ul>
					<a href="#" class="btnBlue">Assine Já</a>
				</li>
				<li>
					<h3 class="top"><span>Apenas</span>R$14<span class="cents">,90</span><span class="mes">/mês</span></h3>
					<ul>
						<li><span class="tickTop">&nbsp;</span>Perfil Individual</li>
						<li><span class="tickTop">&nbsp;</span>Baby Line</li>
						<li><span class="tickTop">&nbsp;</span>Mural</li>
						<li><span class="tickTop">&nbsp;</span>Lista de Presentes</li>
						<li><span class="tickTop">&nbsp;</span>Criador de Convites e Eventos</li>
						<li><span class="tickTop">&nbsp;</span>Álbum de Fotos (ilimitado)</li>
						<li><span class="tickTop">&nbsp;</span>Enquete</li>
						<li><span class="tickTop">&nbsp;</span>Dicas de Profissionais</li>
						<li><span class="tickTop">&nbsp;</span>Clube de Desconto VIP</li>
						<li><span class="tickTop">&nbsp;</span>Personalização da Página</li>
					</ul>
					<a href="#" class="btnGreen">Assine Já</a>
				</li>
			</ul>
			
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>