<?php

namespace IEngravidei\Entities;

class BabylineWeeks extends \Eloquent
{
    protected $table = 'babyline_weeks';
    protected $guarded = array();

    public $timestamps = false;
}