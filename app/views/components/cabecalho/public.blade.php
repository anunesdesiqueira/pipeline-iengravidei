<div class="cabecalho">
    <div class="foto">
        <a href="{{ route('user.public', array('publicId' => $profile->public_id)) }}" title="{{ $profile->name }}">
            <img src="{{ $profile->profile_picture['main'] }}" />
        </a>
    </div>
    <div class="atividades">
        <h2><a href="{{ route('user.public', array('publicId' => $profile->public_id)) }}" title="{{ $profile->name }}">{{ $profile->name }}</a></h2>
        <div class="clearfix"></div>
        <ul class="listaDadosPessoais">
            @if(!empty($profile->father_name) && !is_null($profile->father_name))
                <li><strong>Nome do Papai:</strong> {{ $profile->father_name }}</li>
            @endif
            @if(!empty($profile->city) && !is_null($profile->city))
                <li><strong>Cidade:</strong> {{ $profile->city }} - {{ $profile->state_name }}</li>
            @endif
            @if(!empty($profile->pregnancy_weeks) && !is_null($profile->pregnancy_weeks))
                <li><strong>Estou com</strong> {{ $profile->pregnancy_weeks }} <strong>semanas | Meses:</strong> {{ $profile->month }} meses</li>
                <li><strong>Contagem Regressiva:</strong> {{ $profile->countdown }} semanas</li>
            @endif
        </ul>

        <div class="box-adicionar">
            @if(!isset($permissions['visitante']) && !isset($permissions['amiga']) && !isset($permissions['convidado']))
                @if(isset($permissions['solicitacao']) && $permissions['solicitacao'])
                    <div class="esperando-liberacao"><span></span>Aguarde a liberação</a></div>
                @else
                    <a href="{{ $publicId }}" class="adicionar"><span></span>Adicionar como amiga</a>
                @endif
            @endif
        </div>

        @if(isset($permissions['amiga']) && !isset($permissions['profile_user']) && !isset($permissions['convidado']))
            <a href="#" class="enviarMsg"><span></span>Enviar mensagem</a>
            @include('components.mensagens.form_mensagem')
        @endif

        <div class="clearfix"></div>

        <ul class="listaBotoes perfilPublic">
            <li>
                <a href="{{ route('user.public', array('publicId' => $profile->public_id)) }}" class="editar">
                    <span>&nbsp;</span>Ver Perfil
                </a>
            </li>
            @if(isset($permissions['amiga']) || isset($permissions['convidado']))
                <li>
                    <a href="{{ route('show_my_album', array('publicId' => $publicId)) }}" class="album">
                        <span>&nbsp;</span>Albúm de Fotos
                    </a>
                </li>
            @endif
        </ul>

        @if(count($gifts) > 0)
            <a href="{{ route('user.public.gifts', array('publicId' => $publicId)) }}" class="listaPresente">Lista de Presentes</a>
        @endif

    </div>
    @include('components.poll.public')
</div>