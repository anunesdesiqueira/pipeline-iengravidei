<h3>Confira os itens escolhidos:</h3>
<div class="container-presentes-produtos-confirm">Carregando os itens escolhidos...</div>
{{ Form::open( array('id' => 'form-lista-presentes-criar-confirmacao') ) }}
    <p><label>Escolha um nome para essa lista: {{ Form::text('name', null, array('class' => 'text-input')) }}</label></p>
    {{ Form::submit('Salvar lista', array('class' => 'bt-salvar')) }}
{{ Form::close() }}