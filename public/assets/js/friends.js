$(function() {
    // Init
    loadRequests();
    loadFriends();
});

function loadRequests() {
    CallMethod(baseUri + '/meu-perfil/amigos/solicitacoes','GET','','','',successSolicitacao);
}

function loadFriends() {
    CallMethod(baseUri + '/meu-perfil/amigos/all','GET','','','',successAmigas);
}

$(document.body).confirmOn({
    questionText: 'Quer mesmo excluir essa amiga?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.amigas .lista-solitacoes ul li a.excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/amigos/atualizar','POST',{ 'public_id' : $(e.toElement).parent().parent().attr('rel'), 'friendship' : 2 },'','',successSolicitacaoExcluir);
    }
});

$(document.body).confirmOn({
    questionText: 'Quer mesmo excluir essa amiga?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', '.amigas .minhas-amigas .lista ul li a.excluir', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/amigos/atualizar','POST',{ 'public_id' : $(e.toElement).parent().parent().attr('rel'), 'friendship' : 2 },'','',successAmigasExcluir);
    }
});

$(document.body).on('click', '.amigas .lista-solitacoes ul li a.aceitar', function(e) {
    e.preventDefault();
    CallMethod(baseUri + '/meu-perfil/amigos/atualizar','POST',{ 'public_id' : $(this).parent().parent().attr('rel'), 'friendship' : 1 },'','',successSolicitacaoAceitar);
});

// Pagination
$(document.body).on('click', '.amigas .lista-solitacoes ul.pagination li a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'GET','','','',successSolicitacao);
});

$(document.body).on('click', '.amigas .minhas-amigas .lista ul.pagination li a', function(e) {
    e.preventDefault();
    CallMethod($(this).attr('href'),'GET','','','',successAmigas);
});

function successSolicitacaoExcluir(data) {
    if(data.status) {
        loadRequests();
    }
}

function successAmigasExcluir(data) {
    if(data.status) {
        loadFriends();
    }
}

function successSolicitacaoAceitar(data) {
    if(data.status) {
        loadRequests();
        loadFriends();
    }
}

function successSolicitacao(data) {
    if(data.page != undefined) {
        var container = $('.amigas .lista-solitacoes');
        container.empty().html(data.page);
    }
}

function successAmigas(data) {
    if(data.page != undefined) {
        var container = $('.amigas .minhas-amigas .lista');
        container.empty().html(data.page);
    }
}