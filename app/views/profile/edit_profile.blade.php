@extends('layouts.master')

@section('content')
    <div class="holder">
        @include('components.cabecalho.header_active')
        <div class="formEditar">
            @include('components.profile.edit_dados_pessoais')
            @include('components.profile.edit_perfil_senha')
            {{-- @include('components.profile.edit_plano') --}}
            @include('components.profile.excluir_perfil')
        </div>
    </div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/editar-perfil.js') }}
@stop