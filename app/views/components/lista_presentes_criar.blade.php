<!--### Lista de Presentes ###-->
<div class="lista-presentes" style="padding: 0;">
    <h2 class="lista-presentes-title">Lista de Presentes</h2>
    <a href="{{ route('gift.index') }}" class="criar">criar lista</a>
    <div class="clearfix"></div>
    <ul class="itens-main" style="margin-left: 39px">
        @foreach ($gifts as $gift)
            <li>
                <div class="thumb">
                    <img src="{{ $gift->picture['main'] }}" />
                </div>
                <p>{{ $gift->name }}</p>
                <span>R$ {{ str_replace('.', ',', $gift->price) }}</span>
            </li>
        @endforeach
        @for ($i = count($gifts); $i < 4; $i++)
            <li>
                <div class="thumb">
                    <img src="{{ asset('assets/img/lista-presente-exemplo.jpg') }}" />
                </div>
                <p>Escolha um produto</p>
                <span>R$ XX,XX</span>
            </li>
        @endfor
    </ul>
</div>
<!--### End: Lista de Presentes ###-->