$(function () {
    validateEditBaseDados();
    validateEditSenha();
});

// Mascara para o campo de upload
$(document.body).on('change', '.uploadHolder input[type=file]', function () {
    $('.uploadHolder .fileName').text($(this).val());
    $('.uploadHolder .fileName').each(function () {
        var text = $(this).text();
        $(this).text(text.replace('C:\\fakepath\\', ''));
    });
});

// Edit dados pessoais
$(document.body).on('submit', 'form#edit-perfil', function(e) {
    e.preventDefault();

    var form = $(this);
    var formAction = form.attr('action');
    var params = form.serialize();
    var formdata = false;

    if(window.FormData) {
        formdata = new FormData(form[0]);
    }

    $.ajax({
        url: baseUri + '/meu-perfil/editar/send',
        data: formdata ? formdata : params,
        dataType    : 'json',
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        beforeSend: function() {
            if (typeof loadingFormShowEdit === 'function') {
                loadingFormShowEdit();
            }
        },
        complete: function() {
            if (typeof loadindFormHideEdit === 'function') {
                loadindFormHideEdit();
            }
        },
        success: function (data) {
            if (typeof successEditing === 'function') {
                successEditing(data);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
});

function loadingFormShowEdit() {
    var submit = document.getElementsByClassName('bt_salvar');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHideEdit() {
    $(document.getElementsByClassName('bt_salvar')).css({color : '#867a42'}).prop({ disabled : false });
}

function successEditing(data) {
    if(data.status) {
        var photo = $('.cabecalho .foto img');
        var name = $('.cabecalho .atividades h2');

        photo.attr('src', baseUri + data.profile_picture['main']);
        name.html(data.profile_name);

        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                    '<h6>Dados atualizados com sucesso.</h6>' +
                    '<div class="container" style="width: 73px;">' +
                    '<a class="bt-popup-editar" href="javascript:void(0)">Fechar</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    }
}

$(document.body).on('click', '.bt-popup-editar', function(e) {
    e.preventDefault();
    $.magnificPopup.close();
});

// Edit senha
$(document).on('submit', 'form#edit-perfil-senha', function(e) {
    e.preventDefault();
    var params = $(this).serialize();
    CallMethod(baseUri + '/meu-perfil/editar/password','POST',params,loadingFormShowPassword,loadindFormHidePassword,successPassword);
});

function loadingFormShowPassword() {
    var submit = document.getElementsByClassName('bt_alterar_senha');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHidePassword() {
    $(document.getElementsByClassName('bt_alterar_senha')).css({color : '#867a42'}).prop({ disabled : false });
}

function successPassword(data) {
    if(data.status != undefined) {
        $('form#edit-perfil-senha').find('input[type="password"]').val("");

        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                    '<h6>Senha atualizada com sucesso.</h6>' +
                    '<div class="container" style="width: 73px;">' +
                    '<a class="bt-popup-editar" href="javascript:void(0)">Fechar</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    }
}

// Deletar usuário
$(document).on('change', 'form#excluir-perfil input[type="radio"]', function(e) {
    var opt = $('input[type="radio"]:checked').val();
    var submit = $('form#excluir-perfil input[type="submit"]');

    if('1' === opt) {
        submit.prop('disabled',false);
    } else {
        submit.prop('disabled',true);
    }
});

$(document.body).confirmOn({
    questionText: 'Quer mesmo excluir seu perfil?',
    textYes: 'Sim',
    textNo: 'Não'
},'click', 'form#excluir-perfil input[type="submit"]', function(e, confirmed) {
    if(confirmed) {
        CallMethod(baseUri + '/meu-perfil/editar/excluir','POST',{ opt : 1 },loadingFormShowExcluir,loadindFormHideExcluir,successExcluir);
    }
});

function loadingFormShowExcluir() {
    var submit = document.getElementsByClassName('bt_perfil_excluir');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHideExcluir() {
    $(document.getElementsByClassName('bt_perfil_excluir')).css({color : '#867a42'}).prop({ disabled : false });
}

function successExcluir(data) {
    if(data.status != undefined) {
        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                    '<h6>Seu perfil foi excluído com sucesso.</h6>' +
                    '<div class="container" style="width: 73px;">' +
                    '<a href="'+ baseUri +'">Fechar</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    }
}

function validateEditBaseDados() {
    $('form#edit-perfil').validate({
        rules: {
            name: {
                required: true
            },
            birth: {
                required: true,
                brazilianDate: true
            },
            country_id: {
                required: true
            },
            state_id: {
                required: true
            },
            city_id: {
                required: true
            },
            pregnancy_weeks: {
                required: true
            }
        },
        messages: {
            name: {
                required: "*Digite seu nome"
            },
            birth: {
                required: "*Digite a data do seu nascimento"
            },
            country_id: {
                required: "*Selecione o país"
            },
            state_id: {
                required: "*Selecione o estado"
            },
            city_id: {
                required: "*Selecione a cidade"
            },
            pregnancy_weeks: {
                required: "*Selecione as semanas de gravidez"
            }
        }
    });
}

function validateEditSenha() {
    $('form#edit-perfil-senha').validate({
        rules: {
            password_atual : {
                required: true,
                minlength: 5,
                maxlength: 16
            },
            password : {
                required: true,
                minlength: 5,
                maxlength: 16
            },
            password_confirmation : {
                required: true,
                minlength: 5,
                maxlength: 16,
                equalTo: "#password"
            }
        },
        messages: {
            password_atual: {
                required: "*Digite sua senha atual",
                minlength: "*Senha deve possuir no minimo 5 caracteres",
                maxlength: "*Senha deve possuir no máximo 16 caracteres"
            },
            password: {
                required: "*Digite sua nova senha",
                minlength: "*Senha deve possuir no minimo 5 caracteres",
                maxlength: "*Senha deve possuir no máximo 16 caracteres"
            },
            password_confirmation: {
                required: "*Confirme sua senha",
                minlength: "*Senha deve possuir no minimo 5 caracteres",
                maxlength: "*Senha deve possuir no máximo 16 caracteres",
                equalTo: "*Senhas não conferem"
            }
        }
    });
}