<?php namespace IEngravidei\Photo;

use IEngravidei\Repositories\PhotoRepositoryInterface;
use IEngravidei\Entities\Photo;

class DbPhotoRepository implements PhotoRepositoryInterface
{
    public function __construct()
    {
        $this->photo = new Photo;
    }
    public function create($input)
    {
        $validator = $this->photo->validate($input);

        if($validator->passes())
        {
            return $this->photo->create($input);
        }

        return $validator;
    }
}