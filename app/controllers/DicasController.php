<?php

use IEngravidei\Repositories\DicasRepositoryInterface as Dicas;

class DicasController extends BaseController
{
    public function __construct(Dicas $dicas)
    {
        $this->dicas = $dicas;
    }

    public function search()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'search' => 'required'
        ), array(
            'search.required' => 'Não pode estar vazio!',
        ));

        if($validation->fails())
        {
            return $validation->messages()->toJson();
        }
        else
        {
            $list = $this->dicas->find($data['search']);
            if (count($list) > 0)
                return Response::json(array('status' => true,'list' => $list));
            else
                return Response::json(array('status' => false));
        }
    }
}