<div class="recados-especiais">
    <h2 title="Recados Especiais"><span>Recados Especiais</span></h2>
    <p>Envie um recadinho para a mamãe você também:</p>
    {{ Form::open(array('action' => array('SpecialMessagesController@send', $publicId), 'id' => 'form-recados-especias')) }}
    <ul class="formRecados">
        <li class="floatLeft">
            {{ Form::text('specialname', Input::old('specialname'), array('placeholder' => 'nome', 'class' => 'nome', 'required')) }}
        </li>
        <li>
            {{ Form::text('specialemail', Input::old('specialemail'), array('placeholder' => 'e-mail', 'class' => 'email', 'required')) }}
        </li>
        <li>
            {{ Form::textarea('specialmsg', Input::old('specialmsg'), array('placeholder' => 'mensagem', 'required')) }}
        </li>
        <li>
            <div class="msg"></div>
            {{ Form::submit('Enviar') }}
        </li>
    </ul>
    {{ Form::close() }}
    <ul class="listaRecadosEspeciais"></ul>
</div>