<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<section class="content">
	<div class="holder album-fotos">
		
		<!--## CABECALHO 1 ##-->
		<div class="cabecalho">
			
			<div class="foto"><img src="img/marcacao-perfil.jpg" /></div>
			
			<div class="atividades">
				<h2>Ana Paula Morais</h2>
				<a href="#" class="ver-perfil"><span>&nbsp;</span>Ver Perfil</a>
				<ul class="listaBotoes">
					<li><a href="#" class="editar"><span>&nbsp;</span>Editar Perfil</a></li>
					<li><a href="#" class="recados"><span>&nbsp;</span>Recados Especiais</a></li>
					<li><a href="#" class="eventos"><span>&nbsp;</span>Eventos</a></li>
					<li><a href="#" class="album"><span>&nbsp;</span>Albúm de Fotos</a></li>
					<li><a href="#" class="lista-amigas"><span>&nbsp;</span>Lista de Amigas</a></li>
					<li><a href="#" class="mensagens"><span>&nbsp;</span>Mensagens</a></li>
					<li><a href="#" class="presentes"><span>&nbsp;</span>Lista de Presentes</a></li>
					<li><a href="#" class="notificacoes"><span>&nbsp;</span>Notificafacões</a></li>
				</ul>
			</div>
			
			<!-- Enquete -->
			<div class="enqueteBox">
				<h3>Enquete</h3>
				<p>Estou na dúvida de qual nome colocar, qual vocês preferem?</p>
				<ul>
					<li>
						<label>ALEX</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:50%;"></div>
						</div>
						<span>50%</span>
					</li>
					<li>
						<label>JOÃOZINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:25%;"></div>
						</div>
						<span>25%</span>
					</li>
					<li>
						<label>PEDRINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:10%;"></div>
						</div>
						<span>10%</span>
					</li>
					<li>
						<label>ASTROGILDO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:15%;"></div>
						</div>
						<span>15%</span>
					</li>
				</ul>				
				<input type="submit" value="Vote" />
				<button>Ver Resultado</button>
			</div>
			<!-- End: Enquete -->
			
			<!-- Compartilhe -->
			<div class="clearfix"></div>
			<a href="#" class="compartilhe">Compartilhe</a>
			<!-- End: Compartilhe -->
		</div>
		<!--## END: CABECALHO 1 ##-->
		
		<h2 title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>
		
		<a href="#" class="criar">criar álbum</a>
		
		<div class="clearfix"></div>
		
		<div id="container" class="cntUpFotos">
			<h3>Criar novo álbum</h3>			
			<a class="close">X</a>
			<label>
				Nome do Álbum: <input type="text" class="nomeAlbum" />
				<span class="error">Não esqueça o nome do álbum :)</span>
			</label>
			<div class="semFotos">Você deve adicionar pelo menos uma foto para criar um novo álbum.</div>
			<div id="filelist"></div>			
			<div class="clearfix"></div>
			<div class="teste"></div>
			<a id="pickfiles" href="javascript:;">Selecione as fotos</a> 
			<a id="uploadfiles" href="javascript:;">Criar o álbum</a>
		</div>
		
		<div class="clearfix"></div>
		
		<ul class="lista-album">
			<li>
				<a href="#" class="editar">Editar álbum</a>
				<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
				<a href="#" class="nome">Nome do álbum</a>
			</li>
			<li>
				<a href="#" class="editar">Editar álbum</a>
				<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
				<a href="#" class="nome">Nome do álbum</a>
			</li>
			<li>
				<a href="#" class="editar">Editar álbum</a>
				<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
				<a href="#" class="nome">Nome do álbum</a>
			</li>
			<li>
				<a href="#" class="editar">Editar álbum</a>
				<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
				<a href="#" class="nome">Nome do álbum</a>
			</li>
			<li>
				<a href="#" class="editar">Editar álbum</a>
				<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
				<a href="#" class="nome">Nome do álbum</a>
			</li>
			<li>
				<a href="#" class="editar">Editar álbum</a>
				<div class="thumb"><a href="#"><img src="img/marcacao-album.jpg" /></a></div>
				<a href="#" class="nome">Nome do álbum</a>
			</li>
		</ul>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>