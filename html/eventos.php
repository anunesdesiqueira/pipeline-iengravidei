<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<section class="content">
	<div class="holder">
		
		<!--## CABECALHO 1 ##-->
		<div class="cabecalho">
			
			<div class="foto"><img src="img/marcacao-perfil.jpg" /></div>
			
			<div class="atividades">
				<h2>Ana Paula Morais</h2>
				<a href="#" class="ver-perfil"><span>&nbsp;</span>Ver Perfil</a>
				<ul class="listaBotoes">
					<li><a href="#" class="editar"><span>&nbsp;</span>Editar Perfil</a></li>
					<li><a href="#" class="recados"><span>&nbsp;</span>Recados Especiais</a></li>
					<li><a href="#" class="eventos"><span>&nbsp;</span>Eventos</a></li>
					<li><a href="#" class="album"><span>&nbsp;</span>Albúm de Fotos</a></li>
					<li><a href="#" class="lista-amigas"><span>&nbsp;</span>Lista de Amigas</a></li>
					<li><a href="#" class="mensagens"><span>&nbsp;</span>Mensagens</a></li>
					<li><a href="#" class="presentes"><span>&nbsp;</span>Lista de Presentes</a></li>
					<li><a href="#" class="notificacoes"><span>&nbsp;</span>Notificafacões</a></li>
				</ul>
			</div>
			
			<!-- Enquete -->
			<div class="enqueteBox">
				<h3>Enquete</h3>
				<p>Estou na dúvida de qual nome colocar, qual vocês preferem?</p>
				<ul>
					<li>
						<label>ALEX</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:50%;"></div>
						</div>
						<span>50%</span>
					</li>
					<li>
						<label>JOÃOZINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:25%;"></div>
						</div>
						<span>25%</span>
					</li>
					<li>
						<label>PEDRINHO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:10%;"></div>
						</div>
						<span>10%</span>
					</li>
					<li>
						<label>ASTROGILDO</label>
						<input type="radio" name="enqt" />
						<div class="barra">
							<div class="mascara"></div>
							<div class="porcentagem" style="width:15%;"></div>
						</div>
						<span>15%</span>
					</li>
				</ul>				
				<input type="submit" value="Vote" />
				<button>Ver Resultado</button>
			</div>
			<!-- End: Enquete -->
			
			<!-- Compartilhe -->
			<div class="clearfix"></div>
			<a href="#" class="compartilhe">Compartilhe</a>
			<!-- End: Compartilhe -->
		</div>
		<!--## END: CABECALHO 1 ##-->
		
		<h2 class="eventos-title" title="Crie seu evento"><span>Crie seu evento</span></h2>
		
		<div class="eventos">
			
			<div class="lista">
				<h3>Lista de eventos</h3>
				<p>Gerencie sua lista de eventos e confira as listas ativas ou  anteriores.</p>			
				<a href="listar-eventos.php" class="btnList">Lista de eventos</a>
			</div>
			
			<div class="crie">
			
				<h3>Crie um novo evento</h3>
				
				<p>Que tipo de evento você deseja criar?</p>
				
				<ul class="lista-tipo-evento">
					<li class="cha">
						<label>
							<div class="thumb"></div>
							<input type="radio" name="evento" /> Chá de Bebê
						</label>
					</li>
					<li class="nascimento">
						<label>
							<div class="thumb"></div>
							<input type="radio" name="evento" /> Nascimento
						</label>
					</li>
					<li class="fralda">
						<label>
							<div class="thumb"></div>
							<input type="radio" name="evento" /> Chá de Fralda
					</li>
					<li class="outros">
						<label>
							<div class="thumb"></div>
							<input type="radio" name="evento" /> Outros
						</label>
					</li>
				</ul>
				
				<p>Presente para esse evento</p>
				
				<div class="presente-selecao">
					<label>Selecione sua lista de presentes</label>					
					<div class="combo">
						<div class="mascara"></div>
						<select>							
							<option value="Lista 1">Lista 1</option>
							<option value="Lista 2">Lista 2</option>
							<option value="Lista 3">Lista 3</option>
						</select>
					</div>					
					<span>ou crie uma nova lista</span>					
					<button>Nova lista</button>
				</div>
				
				<p>Dados pessoais</p>
				
				<ul class="form-dados-pessoais">
					<li>
						<label>
							<span>Nome do evento:</span>
							<input type="text" />
						</label>
					</li>
					<li>
						<label>
							<span>Data do evento:</span>
							<input type="text" />
						</label>
					</li>
					<li>
						<label>
							<span>Nome da mamãe:</span>
							<input type="text" />
						</label>
					</li>
					<li>
						<label>
							<span>Nome do papai:*</span>
							<input type="text" />
						</label>
					</li>
					<li>
						<label>
							<span>Endereço:</span>
							<input type="text" />
						</label>
					</li>
					<li>
						<label>
							<span>Horário:</span>
							<input type="text" />
						</label>
					</li>					
					<li>
						<label>
							<span>Comentários:*</span>
							<textarea></textarea>
						</label>
					</li>
				</ul>
				
				<span class="obs">obs.: campo não obrigatório</span>
				
				<p>personalize e envie seu convite</p>
				
				<ul class="convite-templates">
					<li>
						<label>
							<div class="thumb"><img src="img/template-convite-a.gif" /></div>
							<div class="radioHolder">
								<div class="mascara"></div>
								<input type="radio" name="tpl" value="tpl1" />
							</div> 
							<span>Escolher este template</span>
						</label>
					</li>
					<li>
						<label>
							<div class="thumb"><img src="img/template-convite-b.gif" /></div>
							<div class="radioHolder">
								<div class="mascara"></div>
								<input type="radio" name="tpl" value="tpl2" />
							</div> 
							<span>Escolher este template</span>
						</label>
					</li>
					<li>
						<label>
							<div class="thumb"><img src="img/template-convite-c.gif" /></div>
							<div class="radioHolder">
								<div class="mascara"></div>
								<input type="radio" name="tpl" value="tpl3" />
							</div> 
							<span>Escolher este template</span>
						</label>
					</li>
					<li class="last">
						<label>
							<div class="thumb"><img src="img/template-convite-d.gif" /></div>
							<div class="radioHolder">
								<div class="mascara"></div>
								<input type="radio" name="tpl" value="tpl4" />
							</div> 
							<span>Escolher este template</span>
						</label>
					</li>
				</ul>
				
				<div class="visuTplError">Por favor, selecione um template para visualizar</div>
				
				<a href="#" class="visualizar-convite"><span class="ico-visualizar">&nbsp;</span>Visualizar</a>
				
				<p>Escolha seus amigos</p>
				
				<div class="escolha-amigos">
				
					<p>Escolha na lista as amigas do iengravidei que deseja convidar:</p>
					
					<div class="col1">
						<h4>lista de amigos</h4>
						<h4 class="last">selecionados</h4>
						<div class="clearfix"></div>
						<div class="holder notSelected">
							<ul class="list-connected">
								<li id="user1">Marcos Andrade de Lima</li>
								<li id="user2">Irene Panisi Toko</li>
								<li id="user3">Karina Delfim Côrrea</li>
								<li id="user4">Júlia Naomi Panisi Toko</li>
								<li id="user5">Paula Mariano</li>
								<li id="user6">Bianca Lopez</li>
								<li id="user7">Giselle Pinho</li>
							</ul>
						</div>
						<div class="holder selected last">
							<ul class="list-connected">							
							</ul>
						</div>
						<p>Escolha na lista as amigas do iengravidei que deseja convidar:</p>
						<label>E-mails externos: <input type="text" name="emails" id="emails" /></label>
						<a href="#" class="addMail">&nbsp;</a>
						
						<div class="clearfix"></div>					
						
						<div class="emailsInvalidos">
							<h5>Os e-mails abaixo não são válidos, por favor confira e adicione novamente:</h5>
							<p></p>
						</div>
						
						<h4>selecionados</h4>
						
						<div class="emails-selecionados">
							<ul></ul>
						</div>
						
					</div>
					
					<div class="col2">
						<h4>Lista de convidados selecionados</h4>
						<div class="convidados-selecionados">
							<ul></ul>							
						</div>
					</div>
					
					<div class="clearfix"></div>
					
					<a href="#" class="enviar-convite"><span class="ico-enviar">&nbsp;</span>Enviar convite</a>
					
				</div>				
				
			</div>
			
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>