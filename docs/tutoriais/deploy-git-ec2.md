# Deploy usando git na instância EC2

## Adicionar identidade ssh para se conectar a instância
```
ssh-add path/to/iengravidei.pem
```

## Adicionar o endereço do Repositório remoto na instância
```
git remote add aws ssh://ec2-user@54.232.208.71/home/ec2-user/iEngravidei.git
```

## Enviando os arquivos via git para o repositório remoto
```
git push aws +master:refs/heads/master
git push aws +develop:refs/heads/develop
```
__Obs__

Ao dar push no repositório remoto, automaticamente os arquivos são enviados para o /var/www  
assim o projeto já pode ser acessado via browser [iEngravidei - Portal](http://54.232.208.71/public/) ou [iEngravidei - Admin](http://54.232.208.71/public/admin/)
