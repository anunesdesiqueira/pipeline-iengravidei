<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotEventProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_profile', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('event_id')->unsigned()->index();
			$table->integer('profile_id')->unsigned()->index();
            $table->boolean('confirmed')->default(0);
			//$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			//$table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_profile');
	}

}
