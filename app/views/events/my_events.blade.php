<div class="lista noBorder">
    <h3>Gerenciador de eventos</h3>

    <button title="Fechar (Esc)" type="button" class="close mfp-close">×</button>

    <h4>Eventos em andamento</h4>

    @if (count($actives) > 0 )
    <ul class="listEventos">
        @foreach ($actives as $active)
        <li>
            <ul>
                <li><strong>Nome do evento:</strong> {{ $active->name }}</li>
                <li><strong>Data Publicação:</strong> {{ date('d/m/Y', strtotime($active->created_at)) }}</li>
                <li><strong>Data do Evento:</strong> {{ date('d/m/Y', strtotime($active->date)) }}</li>
                <li><strong>Endereço:</strong> {{ $active->address }}</li>
            </ul>
            {{link_to_route('events.edit', 'Editar evento', array('hash' => $active->hash), array('class'=>'firstBnt bt-editar-convite')) }}
            <a href="javascript:void(0)" class="btn-reenviar-convites" rel="{{ $active->hash }}">Reenviar convite</a>
        </li>
        @endforeach
    </ul>
    @else
        <br>
        <p>Nenhum evento ativo no momento</p>
    @endif

    <h4>Eventos já finalizados</h4>

    @if (count($finisheds) > 0 )
    <ul class="listEventos">
        @foreach ($finisheds as $finished)
        <li>
            <ul>
                <li><strong>Nome do evento:</strong>{{ $finished->name }}</li>
                <li><strong>Data Publicação:</strong>{{ date('d/m/Y', strtotime($finished->created_at)) }}</li>
                <li><strong>Data do Evento:</strong> {{ date('d/m/Y', strtotime($finished->date)) }}</li>
                <li><strong>Endereço:</strong>{{ $finished->address }}</li>
            </ul>
            {{-- <a href="#" class="firstBnt">Visualizar</a> --}}
        </li>
        @endforeach
    </ul>
    @else
        <br>
        <p>Nenhum evento finalizado</p>
    @endif
</div>