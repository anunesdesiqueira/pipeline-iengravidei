<?php namespace IEngravidei\SpecialMessage;

use IEngravidei\Repositories\SpecialMessageRepositoryInterface;
use IEngravidei\Entities\SpecialMessage as SpecialMessage;

class DbSpecialMessageRepository implements SpecialMessageRepositoryInterface
{
    public function send($data)
    {
        return SpecialMessage::create(array(
            'id_profile' => $data['id'],
            'name' => $data['specialname'],
            'email' => $data['specialemail'],
            'message' => $data['specialmsg'],
            'hash' => sha1(microtime(). uniqid(rand(), true) . $data['specialemail']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function count($id_profile)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)
            ->where('status', '=', 0)->where('read', '=', 0)->whereNull('deleted_at')->count();
    }

    public function listApproved($id_profile, $paginate = 5)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)->where('status', '=', 1)
            ->whereNull('deleted_at')->orderBy('created_at', 'DESC')->paginate($paginate);
    }

    public function listNotApproved($id_profile, $paginate = 5)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)->where('status', '=', 0)
            ->whereNull('deleted_at')->orderBy('created_at', 'DESC')->paginate($paginate);
    }

    public function listPublic($id_profile, $number, $offset)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)->where('status', '=', 1)
                ->whereNull('deleted_at')->orderBy('created_at', 'DESC')
                ->take($number)
                ->skip($offset)
                ->get();
    }

    public function read($id_profile)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)->where('read', '=', 0)
            ->update(array('read' => 1, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function deleted($hash, $id_profile)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->update(array('updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => date('Y-m-d H:i:s')));
    }

    public function approved($hash, $id_profile)
    {
        return SpecialMessage::where('id_profile', '=', $id_profile)->where('hash', '=', $hash)
            ->update(array('updated_at' => date('Y-m-d H:i:s'), 'status' => 1));
    }
}