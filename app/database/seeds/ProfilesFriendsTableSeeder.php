<?php
class ProfilesFriendsTableSeeder extends Seeder {

    public function run()
    {


        $profilesFriends = array(

            array(
                'who_asked'    => 1,
                'who_accepted' => 2,
                'actived'      => 1,
            ),

            array(
                'who_asked'     => 1,
                'who_accepted'  => 3,
                'actived'       => 1,
            ),

            array(
                'who_asked'      => 4,
                'who_accepted'   => 1,
                'actived'        => 1,
            ),


            array(
                'who_asked'       => 1,
                'who_accepted'    => 5,
                'actived'         => 1,
            ),

        );
        DB::table('profile_profile')->truncate();
        DB::table('profile_profile')->insert($profilesFriends);
    }

}

