<?php

namespace IEngravidei\Entities;

class Babyline extends \Eloquent
{
    protected $table = 'babyline';
    protected $guarded = array();

    public $timestamps = false;
}