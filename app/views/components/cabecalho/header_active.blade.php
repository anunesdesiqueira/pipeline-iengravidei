<div class="cabecalho">
    @include('components.profile.my_profile_top')
    <div class="enqueteBox"></div>
    <div class="clearfix"></div>
    <div class="fb-share-button" data-href="{{ route('user.public', array('publicId' => $user->public_id)) }}" data-width="99px" data-type="button"></div>
</div>