@if(isset($recados_especiais) && count($recados_especiais) > 0)
    @foreach ($recados_especiais as $recado)
    <li>
        <h4>{{ $recado->name }} <span>{{ date('d/m à\\s h:i a', strtotime($recado->created_at)) }}</span></h4>
        <p>{{ $recado->message }}</p>
    </li>
    @endforeach
@endif