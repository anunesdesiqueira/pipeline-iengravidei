<div id="mural-{{ $showStatus->hash }}" class="mural-content-status mfp-hide">
    <div class="thumb">
        <a href="{{ route('user.public', array('publicId' => $profile->public_id)) }}" title="{{ $profile->name }}">
            <img src="{{ $profile->profile_picture['small'] }}" />
        </a>
    </div>
    <h4><a href="{{ route('user.public', array('publicId' => $profile->public_id)) }}" title="{{ $profile->name }}">{{ $profile->name }}</a></h4>
    <span>{{ date('d/m à\\s h:i a', strtotime($showStatus->created_at)) }}</span>
    <hr>
    {{ $showStatus->message }}
    <div class="clearfix"></div>
</div>