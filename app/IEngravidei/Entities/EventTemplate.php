<?php namespace IEngravidei\Entities;

class EventTemplate extends \Eloquent
{
    protected $table = 'events_templates';
    protected $guarded = array();

    public $timestamps = false;
}