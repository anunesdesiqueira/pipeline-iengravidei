@extends('layouts.master')

@section('content')
    <div class="holder">
        @include('components.cabecalho.public')
        @include('components.babyline.public')
        @include('components.lista_presentes.public')

        @if(isset($permissions['visitante']))
            @include('components.cadastrar_visitante')
        @else
            @if(isset($permissions['amiga']) || isset($permissions['convidado']))
                @include('components.album.public')
                @include('components.mural.public')
                @include('components.recados_especiais.public')
            @else
                @include('components.amigas.public')
            @endif
        @endif
    </div>

    @if(isset($showStatus))
        @include('components.mural.show_status_public')
    @endif

@stop

@section('customjs')
    {{ HTML::script('assets/js/public.js') }}
    @if(isset($permissions['amiga']) || isset($permissions['convidado']))
        {{ HTML::script('assets/js/load-mensages.js') }}
    @endif

    @if(isset($showStatus))
        <script type="text/javascript">
            $(function () {
                $.magnificPopup.open({
                    preloader: true,
                    items: {
                        src: '#mural-{{ $showStatus->hash }}',
                        type: 'inline'
                    },
                    callbacks: {
                        close: function() {
                            window.history.replaceState('', '', '/{{ $profile->public_id }}');
                        }
                    }
                });
            });
        </script>
    @endif
@stop

@section('facebook_metas')
    <meta property="og:url" content="{{ URL::full() }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $profile->name }}" />
    <meta property="og:description" content="Criei meu perfil no site iengravidei e achei o máximo. Super fofo e com várias funcionalidades para todo mundo que vai ter bebê. Dá uma olhada e depois me conta. Beijos" />
    <meta property="og:site_name" content="IEngravidei"/>
    <meta property="og:image" content="{{ asset($profile->profile_picture['main']) }}">
    <meta property="og:image:width" content="120">
    <meta property="og:image:height" content="180">
@stop