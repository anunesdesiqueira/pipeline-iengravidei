<?php

use Illuminate\Database\Migrations\Migration;

class AddCustomLayoutColumnOnProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('profiles', function($table)
        {
            $table->string('layout')->after('profile_picture');
			$table->string('layout_image')->after('profile_picture');            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('profiles', function($table)
        {
            $table->dropColumn('layout');
			$table->dropColumn('layout_image');            
        });
	}

}