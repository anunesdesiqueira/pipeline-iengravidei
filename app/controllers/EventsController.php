<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\EventRepositoryInterface as Event;
use IEngravidei\Repositories\GiftRepositoryInterface as Gift;

class EventsController extends BaseController
{
    public function __construct(Profile $profile, Event $event, Gift $gift)
    {
        parent::__construct();
        $this->profile = $profile;
        $this->event = $event;
        $this->gift = $gift;
    }

    public function create()
    {	
        $data = array('id' => $this->user->id);
        $friends = $this->profile->getListAllFriends($data['id']);
        $giftlists = $this->gift->getProfileList($data);
        $templates = $this->event->listTemplate();

        if(Request::ajax()) {
            $page = View::make('components.eventos.create', compact('friends', 'giftlists', 'templates'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('events.create', compact('friends', 'giftlists', 'templates'))->render();
        }
    }

    public function refreshListar()
    {
        $giftlists = $this->gift->getProfileList(array('id' => $this->user->id));
        $page = View::make('components.eventos.presentes', compact('giftlists'))->render();
        return Response::json(array('page' => $page));
    }

    public function refreshFriends()
    {
        $data = array('id' => $this->user->id);

        $friends = $this->profile->getListAllFriends($data['id']);
        $page = View::make('components.eventos.select_friends', compact('friends'))->render();
        return Response::json(array('page' => $page));
    }

    public function edit($hash)
    {
        $data = array('id' => $this->user->id);

        $event = $this->event->findEvent($hash);
        $event->date = implode("/",array_reverse(explode("-",$event->date)));
        $event->time = date('H:i', strtotime($event->time));
        $event->friends = (array) json_decode($event->friends);
        $event->mails_externos = (array) json_decode($event->mails_externos);
        $type = $this->event->findType($event->type);
        $giftlist = $this->gift->findById($event->id_giftslist);
        $template = $this->event->findTemplate($event->id_template);
        $friends = $this->profile->getListAllFriends($data['id']);

        if(Request::ajax()) {
            $page = View::make('components.eventos.edit', compact('event', 'type', 'giftlist', 'template', 'friends'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('events.edit', compact('event', 'type', 'giftlist', 'template', 'friends'));
        }
    }

    public function refreshFriendsEdit($hash)
    {
        $data = array('id' => $this->user->id);

        $event = $this->event->findEvent($hash);
        $event->friends = (array) json_decode($event->friends);
        $event->mails_externos = (array) json_decode($event->mails_externos);
        $friends = $this->profile->getListAllFriends($data['id']);

        $page = View::make('components.eventos.edit_select_friends', compact('event', 'friends'))->render();
        return Response::json(array('page' => $page));
    }

    public function my_events()
    {
        $actives = $this->event->listEventsActives($this->user->id);
        $finisheds = $this->event->listEventsFinished($this->user->id);

        $page = View::make('events.my_events', compact('actives', 'finisheds'))->render();
        return Response::json(array('page' => $page));
    }

    public function postCreate()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'type'        => 'required',
                'list'        => 'required',
                'name'        => 'required',
                'date'        => 'required|date_format:d/m/Y',
                'mom_name'    => 'required',
                'address'     => 'required',
                'time'        => 'required|date_format:H:i',
                'template_id' => 'required',
                //'friends'     => 'required'
            ), array(
                'type.required'        => 'Escolha o tipo do evento',
                'type.in'              => 'Tipo de evento inválido',
                'list.required'        => 'Escolha uma lista de presentes',
                'name.required'        => 'Digite o nome do evento',
                'date.required'        => 'Digite a data evento',
                'date.date_format'     => 'A data do evento é inválida',
                'date.required'        => 'Digite a data do evento',
                'date.date_format'     => 'A data é inválida',
                'time.required'        => 'Digite o horário do evento',
                'mom_name.required'    => 'Digite o nome da mamãe',
                'address.required'     => 'Digite o endereço do evento',
                'template_id.required' => 'Escolha um template para o convite',
                'slug.required'        => 'URL amigável deve ser informada',
                'slug.unique'          => 'Slug inválido',
                //'friends.required'     => 'Selecione pelo menos uma amiga ou email externo.'
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $idList = $this->gift->findById($data['list']);
                $idTemplate = $this->event->findTemplate($data['template_id']);

                $data['id'] = $this->user->id;
                $data['list'] = $idList->id;
                $data['template_id'] = $idTemplate->id;
                $data['friends'] = isset($data['friends'])? json_encode($data['friends']) : '';

                $createEvent = $this->event->create($data);

                $data['list_hash'] = $idList->hash;
                $data['public_id'] = $this->user->public_id;

                if($createEvent) {
                    $friends = array_merge((array) json_decode($data['friends']), (array) json_decode($data['mails_externos']));
                    $subject = 'Convite Especial - '.$data['name'];
                    $content = View::make('emails.events.'.$idTemplate->template, compact('data'));

                    if($this->sendMailGun($friends, $subject, $content)) {
                        return Response::json(array('status' => 'true'));
                    }
                }
            }
        }
    }

    public function postEditInfo()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'name'        => 'required',
                'date'        => 'required|date_format:d/m/Y',
                'mom_name'    => 'required',
                'address'     => 'required',
                'time'        => 'required|date_format:H:i',
                'hash'        => 'required'
            ), array(
                'name.required'        => 'Digite o nome do evento',
                'date.required'        => 'Digite a data evento',
                'date.date_format'     => 'A data do evento é inválida',
                'date.required'        => 'Digite a data do evento',
                'date.date_format'     => 'A data é inválida',
                'time.required'        => 'Digite o horário do evento',
                'mom_name.required'    => 'Digite o nome da mamãe',
                'address.required'     => 'Digite o endereço do evento',
                'hash.required'        => 'Evento não encontrado',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $updateInfo = $this->event->updateDataInfo($data);

                if($updateInfo)
                    return Response::json(array('status' => 'true'));
                else
                    return Response::json(array('error' => 1));
            }
        }
    }

    public function postEditConvite() {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'hash' => 'required'
            ), array(
                'hash.required' => 'Evento não encontrado',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $friends = array();

                $event = $this->event->findEvent($data['hash']);

                if(isset($data['friends']) && !empty($data['friends'])) {
                    $event->friends = array_merge( (array) json_decode($event->friends), $data['friends']);
                    $event->friends = json_encode($event->friends);
                    $friends = array_merge($friends, $data['friends']);
                }

                if(isset($data['mails_externos']) && !empty($data['mails_externos'])) {
                    $data['mails_externos'] = (array) json_decode($data['mails_externos']);

                    $event->mails_externos = array_merge( (array) json_decode($event->mails_externos), $data['mails_externos']);
                    $event->mails_externos = json_encode($event->mails_externos);
                    $friends = array_merge($friends, $data['mails_externos']);
                }

                $updateConvites = $this->event->updateConvites($data['hash'], $event->friends, $event->mails_externos);

                if($updateConvites) {
                    $idTemplate = $this->event->findTemplate($event->id_template);
                    $data = $event->toArray();
                    $data['mom_name'] = $data['mom'];

                    $idList = $this->gift->findById($event->id_giftslist);
                    $data['list_hash'] = $idList->hash;
                    $data['public_id'] = $this->user->public_id;
                    $data['date'] = date('d/m/Y', strtotime($data['date']));
                    $data['time'] = date('H:i', strtotime($data['time']));

                    $subject = 'Convite Especial - '.$event['name'];
                    $content = View::make('emails.events.'.$idTemplate->template, compact('data'));

                    if($this->sendMailGun($friends, $subject, $content)) {
                        return Response::json(array('status' => 'true'));
                    }
                }
            }
        }
    }

    public function Reenviar()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make($data, array(
                'hash' => 'required'
            ), array(
                'hash.required' => 'Evento não encontrado',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $event = $this->event->findEvent($data['hash']);
                $event->friends = (array) json_decode($event->friends);
                $event->mails_externos = (array) json_decode($event->mails_externos);

                $friends = array_merge($event->friends, $event->mails_externos);

                $idTemplate = $this->event->findTemplate($event->id_template);
                $data = $event->toArray();
                $data['mom_name'] = $data['mom'];

                $idList = $this->gift->findById($event->id_giftslist);
                $data['list_hash'] = $idList->hash;
                $data['public_id'] = $this->user->public_id;
                $data['date'] = date('d/m/Y', strtotime($data['date']));
                $data['time'] = date('H:i', strtotime($data['time']));

                $subject = 'Convite Especial - '.$event['name'];
                $content = View::make('emails.events.'.$idTemplate->template, compact('data'));

                if($this->sendMailGun($friends, $subject, $content)) {
                    return Response::json(array('status' => 'true'));
                } else {
                    return Response::json(array('error' => 1));
                }
            }
        }
    }

    private function sendMailGun($friends, $subject, $content)
    {
        foreach ($friends as $friend) {
            Mailgun::message(function($mail) use ($friend, $subject, $content) {
                $mail->to = $friend;
                $mail->from = 'postmaster@sandbox67935.mailgun.org';
                $mail->subject = $subject;
                $mail->html = $content;
            })->deliver();
        }
        return true;
    }
}