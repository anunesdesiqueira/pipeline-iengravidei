<!--### Mural Logado ###-->
<div class="mural-logado">
    <h2 title="Mural"><span>Mural</span></h2>

    {{ Form::open(array('id' => 'form-mural')) }}
    <div class="holderForm">
        <h3>Envie seu comentário:</h3>

        <div class="envio">
            <label>Envie:</label>
            <div class="holderFoto">
                <div class="mascara"></div>
                {{ Form::file('imagem', array('rel' => 'fileMural')) }}
            </div>
            <a href="#" class="showForm">Video</a>
        </div>

        <div class="clearfix"></div>

        <div class="ytUrl">
            {{ Form::text('youtube', Input::old('youtube'), array('placeholder' => 'Copie e cole aqui o link do video do Youtube')) }}
        </div>

        {{ Form::textarea('comentario', Input::old('comentario')) }}

        <div class="loader-media"><div id="picture"></div></div>

        <ul>
            <li>Compartilhar com:</li>
            <li><label><input type="radio" name="shared" value="publico" checked /> Todas as mamães</label></li>
            <li><label><input type="radio" name="shared" value="privado" /> Amigas</label></li>
            <li><label><input type="checkbox" name="facebook" checked /> Facebook</label></li>
            <li>{{ Form::submit('Enviar', array('class' => 'enviar-mural')) }}</li>
        </ul>
    </div>
    {{ Form::close() }}

    <div class="clearfix"></div>

    <div class="tab">
        <div class="active" rel="todas">Todas as mamães</div>
        <div rel="amigas">Amigas</div>
    </div>

    <!-- Todas as mamães -->
    <div class="tabList todas active">
        <div class="busca">
            {{ Form::open(array('id' => 'form-buscar-todas-mamaes')) }}
                {{ Form::text('buscar-todas-mamaes', Input::old('buscar-todas-mamaes'), array('placeholder' => 'BUSQUE POR')) }}
                {{ Form::submit('') }}
            {{ Form::close() }}
        </div>
        <ul></ul>
    </div>
    <!-- End: Todas as mamães -->

    <!-- Amigas -->
    <div class="tabList amigas">
        <div class="busca">
            {{ Form::open(array('id' => 'form-buscar-amigas')) }}
                {{ Form::text('buscar-amigas', Input::old('buscar-amigas'), array('placeholder' => 'BUSQUE POR')) }}
                {{ Form::submit('') }}
            {{ Form::close() }}
        </div>
        <ul></ul>
    </div>
    <!-- End: Amigas -->

</div>
<!--### End: Mural Logado ###-->