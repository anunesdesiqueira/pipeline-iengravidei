@extends('layouts.master')

@section('content')
<div class="holder">
    @include('components.cabecalho.header_active')
    <div class="recados">
        <h2 name="recados-especiais">recados especiais</h2>
        <h3>Em aprovação</h3>
        <div class="recados-em-aprovacao">carregando..</div>
        <h3>Já Aprovado</h3>
        <div class="aprovados">carregando..</div>
    </div>
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/special_messages.js') }}
    {{ HTML::script('assets/js/enquete-logged.js') }}
@stop