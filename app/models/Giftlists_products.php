<?php

class GiftlistsProducts extends \Eloquent
{

	protected $fillable = [
        'name',
        'description',
        'price',
        'picture',
        'status',
    ];

}