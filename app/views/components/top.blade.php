<p>Bem-vindo. Faça seu login ou cadastre-se.</p>
{{ Form::open(array('route' => 'userauth.login', 'id' => 'sign-in-profile')) }}
    <ul>
        <li>
            <label>LOGIN: <input type="text" name="email" /></label>
            <label class="check"><input type="checkbox" name="remember" value="1" /> LEMBRAR MINHA SENHA</label>
        </li>
        <li>
            <label>SENHA: <input type="password" name="password" /></label>
            <a href="#" class="esqueci-senha">ESQUECI MINHA SENHA</a>
        </li>
        <li><input type="submit" value="ENTRAR" /></li>
    </ul>
{{ Form::close() }}