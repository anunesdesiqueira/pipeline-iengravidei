@if(isset($gifts) && count($gifts) > 0)
    <div class="lista-presentes" style="padding: 0;">
        <h2 class="lista-presentes-title">Lista de Presentes</h2>
        <ul class="itens-main" style="margin: 39px 0 0 39px;">
        @foreach ($gifts as $gift)
            <li>
                <div class="thumb">
                    <img src="{{ $gift->picture['main'] }}" />
                </div>
                <p>{{ $gift->name }}</p>
                <span>R$ {{ str_replace('.', ',', $gift->price) }}</span>
            </li>
        @endforeach
        </ul>

        <a href="{{ route('user.public.gifts', array('publicId' => $publicId)) }}">
            <div class="deSeuPresente">
                <p>Dê agora seu presente!</p>
                <span>clique aqui</span>
            </div>
        </a>
    </div>
@endif