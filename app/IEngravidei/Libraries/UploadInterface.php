<?php  namespace IEngravidei\Libraries;

interface UploadInterface
{
    public function toApp($file, $destinationPath, $publicId = null);
    public function toAwsS3($bucketName, $objectName, $fullPath, $acl);
    public function validInstanceOfUploadedFile($file);
    public function validMimeType($file, $mimes = array());
    public function checkFileSize($file, $maxSize);
    public function generateFilename();

}