<div class="col-left">
    @include('components.lista_presentes.filtros')
    <div class="container-presentes-cart">
        @include('components.lista_presentes.carrinho')
    </div>
</div>

<div class="col-center">
    <h3>Crie sua lista de presentes</h3>
    {{ Form::open( array('id' => 'form-lista-presentes-criar') ) }}
        <p>Os produtos são ilustrativos e representam uma média aproximada de valor de mercado para cada item. Ao receber um
            presente de um amigo, você receberá um e-mail informando o presente selecionado e a mensagem escrita por ele. E receberá os valores em
            Reais (descontadas taxa de serviço do site e do meio de pagamento - cartão de crédito/PayPal) diretamente em sua conta corrente
            cadastrada, com total segurança e certificação de pagamento seguro. Para maiores detalhes, veja aqui as políticas de serviços
            e segurança - clique aqui.
        </p>
        <p><label>{{ Form::checkbox('terms', 'aceito', false) }} Li e aceito os termos desse serviço.</label></p>
        <h6>Escolha os presentes que deseja incluir na lista</h6>
        <div class="container-presentes-produtos">Carregando a lista de presentes...</div>
        {{ Form::submit('Criar lista de presentes', array('class' => 'bt-salvar')) }}
    {{ Form::close() }}
</div>