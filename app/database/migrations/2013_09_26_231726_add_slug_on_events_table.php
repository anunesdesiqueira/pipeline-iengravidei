<?php

use Illuminate\Database\Migrations\Migration;

class AddSlugOnEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('events', function($table)
        {
            $table->string('slug');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('events', function($table)
        {
            $table->dropColumn('events');
        });

	}

}