<h2 name="album-de-fotos" title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>

<h3>Edição de Álbum</h3>

<label>Nome do álbum: <input type="text" id="name-album" rel="{{ $album->slug }}" value="{{ $album->name }}" /></label>
<a href="javascript:void(0)" class="alterar">Alterar nome</a>
<a href="javascript:void(0)" class="excluir delete-album" rel="{{ $album->slug }}">Excluir álbum</a>

<a href="#" class="criar">adicionar fotos</a>

<div class="clearfix"></div>

<div id="album-container-editar" class="cntUpFotos">
    <h3>Adicionar fotos no álbum</h3>
    <a class="close">X</a>
    <div class="semFotos">Selecione ao menos uma foto para adicionar ao álbum.</div>
    <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
    <div class="clearfix"></div>
    <div class="teste"></div>
    <a id="pickfiles" href="javascript:;">Selecione as fotos</a>
    <a id="uploadfiles" href="javascript:;">Adicionar fotos selecionadas</a>
</div>

<div class="msg sucess">Nome do álbum alterado!</div>

<div class="clearfix"></div>

<ul class="lista-album lista-album-fotos">
    @include('components.album.lista_editar')
</ul>

