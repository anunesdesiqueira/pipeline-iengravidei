<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;

class FriendsController extends BaseController {

	public function __construct(Profile $profile)
	{
        parent::__construct();
        $this->profile = $profile;
	}

	public function showFriendships()
	{
        return View::make('friends.index');
	}

    public function requestFriends()
    {
        $requests = $this->profile->getAllRequireFriend($this->user->id);
        $requests = $this->createPathImagesProfile($requests);

        $page = View::make('components.amigas.solicitacao', compact('requests'))->render();
        return Response::json(array('page' => $page));
    }

    public function allFriends()
    {
        $friends = $this->profile->getAllFriends($this->user->id);
        $friends = $this->createPathImagesProfile($friends);
        $friends = $this->createUrl($friends, 'messages');

        $page = View::make('components.amigas.lista_amigas', compact('friends'))->render();
        return Response::json(array('page' => $page));
    }

    public function addFriendship()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'public_id' => 'required'
        ), array(
            'public_id.required' => 'Opss! Usuário não encontrado!'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $profile = $this->profile->find($data['public_id']);

            if($this->profile->getRequireFriend($profile->id, $this->user->id)) {
                if ($this->profile->updateRequireFriend($this->user->id, $profile->id, 1))
                    return Response::json(array('status' => 'true', 'update' => 'true'));
                else
                    return Response::json(array('status' => 'false'));
            } else {
                if ($this->profile->requireFriend($this->user->id, $profile->id))
                    return Response::json(array('status' => 'true'));
                else
                    return Response::json(array('status' => 'false'));
            }
        }
    }

    public function updateFriendship()
    {
        $data = Input::all();

        $validation = \Validator::make($data, array(
            'public_id' => 'required',
            'friendship' => 'required'
        ), array(
            'friendship.required' => 'Opss! Valor não encontrado!',
            'public_id.required' => 'Opss! Usuário não encontrado!'
        ));

        if($validation->fails()) {
            return $validation->messages()->toJson();
        } else {
            $profile = $this->profile->find($data['public_id']);

            if($this->profile->updateRequireFriend($this->user->id, $profile->id, $data['friendship'])) {
                if(1 == $data['friendship']) {
                    return Response::json(array('status' => 'true'));
                }
                return Response::json(array('status' => 'true'));
            } else {
                return Response::json(array('status' => 'false'));
            }
        }
    }
}