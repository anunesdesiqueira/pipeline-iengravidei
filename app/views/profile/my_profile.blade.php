@extends('layouts.master')

@section('content')
    <div class="holder">
        @include('components.cabecalho.header_active')
        @include('components.baby_line_criar')
        @include('components.lista_presentes_criar')
        <div class="album-fotos">
            <h2 title="Álbum de Fotos"><span>Álbum de Fotos</span></h2>
            @include('components.album_fotos_criar')
            <div class="bt-confira-todos">
                @include('components.album.bt_todos')
            </div>
        </div>
        @include('components.mural')
        @include('components.dicas_profissionais')
    </div>
@stop

@section('customjs')
    <script type="text/javascript">
        @if(isset($upAlbum))
            var upAlbum = document.getElementById('{{ $upAlbum }}');
        @else
            var upAlbum = 0;
        @endif

        var totalAlbumPhotos = {{{ isset($countAlbumFotosAdd) && isset($user->plan_roles['album_photo_limited']) ? $user->plan_roles['album_photo_limited'] - $countAlbumFotosAdd : isset($user->plan_roles['album_photo_limited']) ? $user->plan_roles['album_photo_limited'] : 0 }}};
        var limitAlbum = "{{{ isset($limitAlbum) ? $limitAlbum : '' }}}";
        var totalAlbum = {{{ isset($user->plan_roles['album_limited']) ? $user->plan_roles['album_limited'] : 0}}};
        var uriAlbum = "{{{ isset($uriAlbum) ? route($uriAlbum) : '' }}}";
    </script>
    {{ HTML::script('assets/js/album.js') }}
    {{ HTML::script('assets/js/perfil.js') }}

    @if(isset($countAlbums) && $countAlbums < $user->plan_roles['album_limited'] || isset($countAlbumFotosAdd))
        <script type="text/javascript">uploader.init();</script>
    @endif
@stop