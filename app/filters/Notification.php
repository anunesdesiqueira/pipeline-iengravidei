<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\SpecialMessageRepositoryInterface as SpecialMessage;
use IEngravidei\Repositories\VisitantesRepositoryInterface as Visitantes;
use IEngravidei\Repositories\MessageRepositoryInterface as Message;

class Notification
{
    public function __construct(Profile $profile, SpecialMessage $specialMessage, Visitantes $visitantes, Message $message)
    {
        $this->profile = $profile;
        $this->specialMessage = $specialMessage;
        $this->visitantes = $visitantes;
        $this->message = $message;
    }

    public function filter()
    {
        $user = Auth::client()->get();

        $notification = new \stdClass();
        $notification->countFriends = $this->profile->getCountRequire($user->id);
        $notification->countSpecial = $this->specialMessage->count($user->id);
        $notification->countVisitantes = $this->visitantes->count($user->id);
        $notification->countMsg = $this->message->count($user->id);

        View::share(compact('notification'));
    }
}