<div class="crie">
    <h3 name="edit-convite">Edição de um evento</h3>
    @include('components.eventos.edit_type')
    @include('components.eventos.edit_presentes')
    @include('components.eventos.edit_dados')
    @include('components.eventos.edit_convites')
    <div class="escolha-amigos">
        @include('components.eventos.edit_select_friends')
    </div>
</div>