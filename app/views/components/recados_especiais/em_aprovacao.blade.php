@if(count($mNotApproved) > 0)
    <ul>
        @foreach ($mNotApproved as $msg)
        <li>
            <a href="#" data-hash="{{ $msg->hash }}" class="excluir">Excluir</a>
            <a href="#" data-hash="{{ $msg->hash }}" class="aprovar">Aprovar</a>
            <span class="data">{{ date('d/m/Y', strtotime($msg->created_at)) }}</span>
            <div class="clearfix"></div>
            <h3>{{ $msg->name }}</h3>
            <p>{{ $msg->message }}</p>
        </li>
        @endforeach
    </ul>
    {{ $mNotApproved->links() }}
@else
    <p>Você não possue nenhum recado especial para aprovação.</p>
@endif