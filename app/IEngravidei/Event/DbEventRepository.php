<?php namespace IEngravidei\Event;

use IEngravidei\Repositories\EventRepositoryInterface;
use IEngravidei\Entities\EventProfile as ProfileEvent;
use IEngravidei\Entities\EventTemplate as EventTemplate;

class DbEventRepository implements EventRepositoryInterface
{
    public function findEvent($hash)
    {
        return ProfileEvent::where('hash', '=', $hash)->first();
    }

    public function create(array $data)
    {
        return ProfileEvent::create(array(
            'id_profile' => $data['id'],
            'id_giftslist' => $data['list'],
            'id_template' => $data['template_id'],
            'type' => $this->type($data['type']),
            'name' => $data['name'],
            'time' => $data['time'],
            'date' => implode("-",array_reverse(explode("/",$data['date']))),
            'mom' => $data['mom_name'],
            'father' => $data['dad_name'],
            'address' => $data['address'],
            'comments' => $data['comments'],
            'friends' => $data['friends'],
            'mails_externos' => $data['mails_externos'],
            'hash' => sha1(microtime(). uniqid(rand(), true) . $data['id'] . $data['name']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function updateDataInfo(array $data)
    {
        return ProfileEvent::where('hash', '=', $data['hash'])->update(
            array(
                'name' => $data['name'],
                'time' => $data['time'],
                'date' => implode("-",array_reverse(explode("/",$data['date']))),
                'mom' => $data['mom_name'],
                'father' => $data['dad_name'],
                'address' => $data['address'],
                'comments' => $data['comments'],
                'updated_at' => date('Y-m-d H:i:s')
            )
        );
    }

    public function updateConvites($hash, $friends, $mails_externos)
    {
        return ProfileEvent::where('hash', '=', $hash)->update(
            array(
                'friends' => $friends,
                'mails_externos' => $mails_externos,
                'updated_at' => date('Y-m-d H:i:s')
            )
        );
    }

    public function listEventsActives($id_profile)
    {
        return ProfileEvent::where('id_profile', '=', $id_profile)->where('date', '>', date('Y-m-d'))->get();
    }

    public function listEventsFinished($id_profile)
    {
        return ProfileEvent::where('id_profile', '=', $id_profile)->where('date', '<', date('Y-m-d'))->get();
    }

    public function listTemplate()
    {
        return EventTemplate::orderBy('id')->get();
    }

    public function findTemplate($value)
    {
        return EventTemplate::select('id', 'template', 'preview')
            ->whereNested(function ($query) use($value) {
                $query->where('hash', '=', $value)->orWhere('id', '=', $value);
            })->first();
    }

    public function findType($type)
    {
        return $this->type($type);
    }

    private function type($type)
    {
        switch ($type)
        {
            case 'cha-de-bebe':
                $type = 1;
                break;

            case 'nascimento':
                $type = 2;
                break;

            case 'cha-de-fralda':
                $type = 3;
                break;

            case 'outros':
                $type = 4;
                break;

            case 1:
                $type = array('class' => 'cha', 'name' => 'Chá de Bebê');
                break;

            case 2:
                $type = array('class' => 'nascimento', 'name' => 'Nascimento');
                break;

            case 3:
                $type = array('class' => 'fralda', 'name' => 'Chá de Fralda');
                break;

            case 4:
                $type = array('class' => 'outros', 'name' => 'Outros');
                break;
        }
        return $type;
    }
}