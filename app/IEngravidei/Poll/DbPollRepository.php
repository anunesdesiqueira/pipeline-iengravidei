<?php

namespace IEngravidei\Poll;

use IEngravidei\Entities\Poll;
use IEngravidei\Entities\PollAnswers;
use IEngravidei\Entities\PollLogs;
use IEngravidei\Repositories\PollRepositoryInterface;

class DbPollRepository implements PollRepositoryInterface
{
    public function getId($slug)
    {
        return Poll::where('profiles_polls.slug', '=', $slug)->first();
    }

    public function findPollActive($idProfile)
    {
        return Poll::where('profiles_polls.id_profile', '=', $idProfile)->where('status', '=', 1)->first();
    }

    public function getPoll($idUser)
    {
        $poll = new \stdClass();

        $poll->question = Poll::select('profiles_polls.id', 'profiles_polls.question', 'profiles_polls.slug')
            ->join('profiles', 'profiles.id', '=', 'profiles_polls.id_profile')
            ->where('profiles_polls.status', '=', 1)
            ->whereNested(function ($query) use($idUser) {

                $query->where('profiles.id', '=', $idUser)
                      ->orWhere('profiles.public_id', '=', $idUser)
                      ->orWhere('profiles.email', '=', $idUser);

            })->first();

        if(!empty($poll->question)) {
            $poll->answers = PollAnswers::select('profiles_polls_answers.id', 'profiles_polls_answers.answer', \DB::raw('COUNT(profiles_polls_logs.id_answer) as total'))
                ->leftJoin('profiles_polls_logs', 'profiles_polls_logs.id_answer', '=', 'profiles_polls_answers.id')
                ->where('profiles_polls_answers.id_poll', '=', $poll->question->id)
                ->orderBy('profiles_polls_answers.id', 'asc')
                ->groupBy('profiles_polls_answers.id')
                ->get();

            $poll->total = 0;

            foreach ($poll->answers as $answer) {
                $poll->total = $poll->total + $answer->total;
            }
        }
        return $poll;
    }

    public function setPoll($data)
    {
        $poll = Poll::create(array(
            'id_profile' => $data['id_profile'],
            'question' => $data['question'],
            'slug' => sha1(microtime(). uniqid(rand(), true) . $data['id_profile'].$data['question']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        if($poll) {
            foreach ($data['answer'] as $answer) {
                if(!empty($answer) && !is_null($answer)) {
                    PollAnswers::create(array('id_poll' => $poll->id,'answer' => $answer));
                }
            }
        }

        return true;
    }

    public function upPoll($id, $status)
    {
        return Poll::whereNested(function ($query) use($id) {
            $query->where('id', '=', $id)->orWhere('slug', '=', $id);
        })->update(array('status' => $status, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function history($idUser)
    {
        $results = \DB::select(
            '(select * from (
                select
                    `profiles_polls`.`id` as `poll`, `profiles_polls`.`question` as `question`,
                    `profiles_polls_answers`.`answer` as `answer`, `profiles_polls`.`created_at` as `created_at`,
                    `profiles_polls`.`updated_at` as `updated_at`, COUNT(`profiles_polls_logs`.`id_answer`)
                from `profiles_polls`
                inner join `profiles_polls_answers` on `profiles_polls_answers`.`id_poll` = `profiles_polls`.`id`
                left join `profiles_polls_logs` on `profiles_polls_logs`.`id_answer` = `profiles_polls_answers`.`id`
                where `profiles_polls`.`id_profile` = ?
                and `profiles_polls`.`status` = ?
                group by `profiles_polls_answers`.`id`
                order by COUNT(`profiles_polls_logs`.`id_answer`) DESC
            ) as counter
            group by `poll`)', array($idUser, 0)
        );

        return $results;
    }

    public function addLogVote($data)
    {
        return PollLogs::create(array(
            'id_poll' => $data['id_poll'],
            'id_answer' => $data['enqt'],
            'id_profile' => isset($data['id_profile'])? $data['id_profile'] : null,
            'ip' => $data['ip'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function findLogAnswer($idPoll, $ip, $id_profile)
    {
        return PollLogs::where('id_poll', '=', $idPoll)
            ->whereNested(function ($query) use($ip, $id_profile) {
                if(isset($id_profile) && !empty($id_profile)) {
                    $query->where('id_profile', '=', $id_profile);
                } else {
                    $query->where('ip', '=', $ip);
                }
            })->exists();
    }
}