<?php

namespace IEngravidei\Entities;

class PollAnswers extends \Eloquent
{
    protected $table = 'profiles_polls_answers';
    protected $guarded = array();
    public $timestamps = false;
}