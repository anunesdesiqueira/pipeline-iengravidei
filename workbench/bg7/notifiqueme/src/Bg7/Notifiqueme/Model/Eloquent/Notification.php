<?php namespace Bg7\Notifiqueme\Model\Eloquent;

/**
 * Class Notification
 * @package Bg7\Notifiqueme\Model\Eloquent
 */
class Notification extends \Eloquent
{
    public function validate($input)
    {
        $rules = array(
            'profile_id'    =>  'integer',
            'message'       =>  'required|max:200',
        );

        $messages = array(
            'profile_id.integer'    =>  'ID do perfil obrigatório',
            'message.required'      =>  'Campo obrigatório',
            'message.max'           =>  'Mensagem não pode conter mais do que 200 caracteres'
        );

        return \Validator::make($input, $rules, $messages);
    }

}