<div class="clear">&nbsp;</div>
<h3>Editar lista: <span>{{ $list->name }}</span></h3>
<p class="top-margin-editar">Você pode adicionar ou excluir a qualquer momento.</p>
<h6>Confira os itens escolhidos:</h6>
<div class="container-presentes-itens-escolhidos">Carregando os itens escolhidos...</div>
<hr class="stroke"/>
<h6>Itens não escolhidos:</h6>
<p class="bottom-margin-editar">Você pode adicionar novos itens a sua lista, basta selecionar-los.</p>
<div class="container-presentes-itens-nao-escolhidos">Carregando os itens não escolhidos...</div>
{{ Form::open(array('id' => 'form-lista-presentes-editar')) }}
    {{ Form::hidden('hash', $list->hash) }}
    {{ Form::submit('Salvar lista', array('class' => 'bt-salvar')) }}
{{ Form::close() }}