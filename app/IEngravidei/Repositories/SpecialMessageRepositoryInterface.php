<?php namespace IEngravidei\Repositories;

interface SpecialMessageRepositoryInterface
{
    public function send($data);
    public function count($id_profile);
    public function listApproved($id_profile, $paginate = 5);
    public function listNotApproved($id_profile, $paginate = 5);
    public function listPublic($id_profile, $number, $offset);
    public function read($id_profile);
    public function deleted($hash, $id_profile);
    public function approved($hash, $id_profile);
}