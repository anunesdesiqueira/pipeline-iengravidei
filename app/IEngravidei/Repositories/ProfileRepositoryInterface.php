<?php

namespace IEngravidei\Repositories;

interface ProfileRepositoryInterface
{
    public function find($id);
    public function findFacebook($uid);
    public function search($input);
    public function profileCreate($input);
    public function profileUpdate($input);
    public function profileCreateByFacebook($uid, $me, $access_token);
    public function profileUpdateFacebook($uid, $access_token);
    public function existsProfileAdditional($id);
    public function findPlan($plan);
    public function getPlan($id);
    public function setPlan($id, $id_plan);
    public function upPlan($id, $id_plan);
    public function setLayout($data);
    public function getActive($email);
    public function setActive($code);
    public function getPublished($id);
    public function setPublished($id);
    public function getPublicId($public_id);
    public function setPublicId($id, $public_id);
    public function getAllFriends($id, $paginate = 12);
    public function getListAllFriends($id);
    public function getListAllFriendsId($id);
    public function getFriend($from_user, $to_user);
    public function getAllRequireFriend($to_user, $paginate = 6);
    public function getRequireFriend($from_user, $to_user);
    public function getCountRequire($from_user);
    public function updateRequireFriend($from_user, $to_user, $status);
    public function requireFriend($from_user, $to_user);
    public function excluirProfile($id);
}