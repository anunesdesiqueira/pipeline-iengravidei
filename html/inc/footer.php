<footer>
	<div class="holder">
		
		<ul class="nav">
			<li><a href="#">Home</a></li>
			<li><a href="#">Sobre Nós</a></li>
			<li><a href="#">Planos</a></li>
			<li><a href="#">Serviços</a></li>
			<li><a href="#">Contato</a></li>
		</ul>
		
		<div class="sobrenos">
			<div class="thumb"></div>
			<div class="description">
				<p>Somos uma comunidade apaixonada pela maternidade. Pelo indescritível e sublime estado em que ficamos quando estamos esperando nosso filho ou nossa filha!<br/>Compartilhe, emocione-se, converse, aprenda, ensine e viva intensamente esse momento. Estamos juntos com você para a contagem regressiva.</p>
			</div>
		</div>
		
		<div class="contatos">
			<a href="#">Quer ser nosso parceiro?</a>
			<a href="#">Entre em contato?</a>
			<ul>
				<li><a href="denuncia.php">Denúncie</a></li>
				<li>|</li>
				<li><a href="#">Termos de uso</a></li>
				<li>|</li>
				<li><a href="#" class="facebook">Facebook</a></li>
			</ul>
		</div>
		
		<p class="disclaimer">I-Engravidei é uma marca registrada - iengravidei  Inc.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copyright © 2013-2013 I-engravidei</p>
		
	</div>
</footer>