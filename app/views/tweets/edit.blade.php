@extends('layouts.scaffold')

@section('main')
<div class="container">
    <div class="row-fluid">

        <div class="span12 conteudo usuarios">
            <h4>Criar Tweet</h4>
            {{ Form::model($tweet, array('method' => 'PATCH', 'route' => array('tweets.update', $tweet->id))) }}
            <ul>
                <li>
                    {{ Form::label('author', 'Autor:', array('class' => 'help-inline')) }}
                    {{ Form::text('author') }}
                </li>

                <li>
                    {{ Form::label('body', 'Tweet',array('class' => 'help-inline')) }}
                    {{ Form::textarea('body') }}
                </li>

                <li>
                    {{ Form::submit('Salvar', array('class' => 'btn')) }}
                </li>
            </ul>


            {{ Form::close() }}
            @if ($errors->any())
            <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </ul>
            @endif
        </div>

    </div>
</div>
</div>
@stop


