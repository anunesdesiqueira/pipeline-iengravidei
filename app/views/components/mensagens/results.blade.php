@if(isset($history) && count($history) > 0 )
    <ul>
        @foreach($history as $message)
            <li>
                <div class="thumb"><img src="{{ $message->profile_picture['small'] }}" /></div>
                <div class="description">
                    <span>{{ date('d/m/Y', strtotime($message->created_at)) }}</span>
                    <h4>{{ $message->name }}</h4>
                    <p>{{ $message->body }}</p>
                    <a href="#" data-type="{{ $message->send }}" rel="{{ $message->idMsg }}" class="excluir">Excluir</a>
                </div>
            </li>
        @endforeach
    </ul>
@else
    <p>Você ainda não enviou ou recebeu nenhuma mensagem dessa sua amiga</p>
@endif