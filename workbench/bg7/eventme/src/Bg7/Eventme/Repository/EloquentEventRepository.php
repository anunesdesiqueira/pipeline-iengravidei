<?php namespace Bg7\Eventme\Repository;
use Bg7\Eventme\Model\Event as EventModel;

class EloquentEventRepository implements EventRepository
{
    protected $eventModel;

    public function __construct(EventModel $eventModel)
    {
        $this->eventModel = $eventModel;
    }
    public function all()
    {
        return $this->eventModel->all();

    }

    public function find($id)
    {
        return $this->eventModel->findOrFail($id);
    }

    public function create($data)
    {
        $v = $this->eventModel->validate($data);

        if( $v->fails() )
        {
            return $v->messages();
        }

        $this->eventModel->name = $data['name'];
        $this->eventModel->when = $data['when'];
        $this->eventModel->where = $data['where'];
        $this->eventModel->profile_id = $data['profile_id'];

        return $this->eventModel->save();


    }
}