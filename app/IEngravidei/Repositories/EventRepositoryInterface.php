<?php

namespace IEngravidei\Repositories;

interface EventRepositoryInterface
{
    public function findEvent($hash);
    public function create(array $data);
    public function updateDataInfo(array $data);
    public function updateConvites($hash, $friends, $mails_externos);
    public function listEventsActives($id_profile);
    public function listEventsFinished($id_profile);
    public function listTemplate();
    public function findTemplate($value);
    public function findType($type);
}