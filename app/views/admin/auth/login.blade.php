<!DOCTYPE html>
<html>
<head>
    <title>iEngravidei | Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->

    <link href="<?php echo asset('admin/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
    <link href="<?php echo asset('admin/css/default.css'); ?>" rel="stylesheet" media="screen">
</head>
<body>

<div class="container login">
    @if ($errors->has('login'))
        <div class="alert alert-error">{{ $errors->first('login', ':message') }}</div>
    @endif

    {{ Form::open(array('class'=> 'form-signin')) }}
    <!--<form class="form-signin">-->
        <h1><span>Chat54</span></h1>
        <h2 class="form-signin-heading">Login | Administração</h2>
        <input type="text" class="input-block-level" placeholder="Email" name="email">
        <input type="password" class="input-block-level" placeholder="Senha" name="password">
        <label class="checkbox">
            <input type="checkbox" value="remember-me"> Lembrar
        </label>
        <button class="btn btn-large btn-primary" type="submit">Log in</button>
        <div class="clearfix"></div>
    <!--</form>-->
    {{ Form::close() }}

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo asset('admin/js_old/bootstrap.min.js_old'); ?>"></script>

</body>
</html>