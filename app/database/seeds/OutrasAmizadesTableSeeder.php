<?php

class OutrasAmizadesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('outrasamizades')->truncate();

		$outrasamizades = array(
            array(
                'who_asked'    => 6,
                'who_accepted' => 7,
                'actived'      => 1,
            ),

            array(
                'who_asked'     => 6,
                'who_accepted'  => 2,
                'actived'       => 1,
            ),

            array(
                'who_asked'      => 6,
                'who_accepted'   => 3,
                'actived'        => 1,
            ),


            array(
                'who_asked'       => 6,
                'who_accepted'    => 5,
                'actived'         => 1,
            ),

            array(
                'who_asked'    => 7,
                'who_accepted' => 6,
                'actived'      => 1,
            ),

            array(
                'who_asked'     => 7,
                'who_accepted'  => 2,
                'actived'       => 1,
            ),

            array(
                'who_asked'      => 7,
                'who_accepted'   => 3,
                'actived'        => 1,
            ),


            array(
                'who_asked'       => 7,
                'who_accepted'    => 5,
                'actived'         => 1,
            ),

            array(
                'who_asked'    => 3,
                'who_accepted' => 6,
                'actived'      => 1,
            ),

            array(
                'who_asked'     => 3,
                'who_accepted'  => 2,
                'actived'       => 1,
            ),

            array(
                'who_asked'      => 3,
                'who_accepted'   => 1,
                'actived'        => 1,
            ),


            array(
                'who_asked'       => 3,
                'who_accepted'    => 5,
                'actived'         => 1,
            ),

		);

		// Uncomment the below to run the seeder
		DB::table('profile_profile')->insert($outrasamizades);
	}

}
