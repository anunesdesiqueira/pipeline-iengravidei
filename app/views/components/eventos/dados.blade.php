<p>Dados pessoais</p>
<ul class="form-dados-pessoais">
    <li>
        <label>
            <span>Nome do evento*:</span>
            {{ Form::text('name') }}
        </label>
    </li>
    <li>
        <label>
            <span>Data do evento*:</span>
            {{ Form::text('date') }}
        </label>
    </li>
    <li>
        <label>
            <span>Nome da mamãe*:</span>
            {{ Form::text('mom_name') }}
        </label>
    </li>
    <li>
        <label>
            <span>Nome do papai:</span>
            {{ Form::text('dad_name') }}
        </label>
    </li>
    <li>
        <label>
            <span>Endereço*:</span>
            {{ Form::text('address') }}
        </label>
    </li>
    <li>
        <label>
            <span>Horário*:</span>
            {{ Form::text('time') }}
        </label>
    </li>
    <li>
        <label>
            <span>Comentários:</span>
            {{ Form::textarea('comments') }}
        </label>
    </li>
</ul>
<span class="obs">obs.: campo obrigatório*</span>