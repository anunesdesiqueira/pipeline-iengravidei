<?php

namespace IEngravidei;

use Illuminate\Support\ServiceProvider;

class IEngravideiServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(
            'IEngravidei\Repositories\ProfileRepositoryInterface',
            'IEngravidei\Profile\DbProfileRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\BabylineRepositoryInterface',
            'IEngravidei\Babyline\DbBabylineRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\DicasRepositoryInterface',
            'IEngravidei\Dicas\DbDicasRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\PollRepositoryInterface',
            'IEngravidei\Poll\DbPollRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\GiftRepositoryInterface',
            'IEngravidei\Gift\DbGiftRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\SpecialMessageRepositoryInterface',
            'IEngravidei\SpecialMessage\DbSpecialMessageRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\MuralRepositoryInterface',
            'IEngravidei\Mural\DbMuralRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\VisitantesRepositoryInterface',
            'IEngravidei\Visitantes\DbVisitantesRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\UserRepositoryInterface',
            'IEngravidei\User\DbUserRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\DenunciationRepositoryInterface',
            'IEngravidei\Denunciation\DbDenunciationRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\NotificationRepositoryInterface',
            'IEngravidei\Notification\DbNotificationRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\MessageRepositoryInterface',
            'IEngravidei\Message\DbMessageRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\EventRepositoryInterface',
            'IEngravidei\Event\DbEventRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\AlbumRepositoryInterface',
            'IEngravidei\Album\DbAlbumRepository'
        );

        $this->app->bind(
            'IEngravidei\Repositories\PhotoRepositoryInterface',
            'IEngravidei\Photo\DbPhotoRepository'
        );
    }
}
