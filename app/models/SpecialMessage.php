<?php

class SpecialMessage extends Eloquent {
	protected $fillable = array('name', 'email', 'special_message_text', 'profile_id');
	protected $table = 'special_messages';

	public static $rules = array(
		'name' => 'required',
		'email' => 'required|email',
		'special_message_text' => 'required'
	);

}
