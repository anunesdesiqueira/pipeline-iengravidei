<?php

class AlbumMedia extends Eloquent {

    protected $table = 'albums_medias';
    protected $guarded = array('id', 'id');

    public static function registersPicturesAlbum($id_media, $id_album)
    {
        $return = new AlbumMedia();

        $return->album_id = $id_album;
        $return->medias_id = $id_media;

        return $return->save();
    }
}
