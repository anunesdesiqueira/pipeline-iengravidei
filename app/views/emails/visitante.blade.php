<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Convites Visitantes</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><img src="{{ asset('assets/img/email/visitantes/images/vistantes-convite_01.jpg') }}" width="600" height="138" style="display:block"/></td>
                </tr>
                <tr>
                    <td>
                        <table width="600" border="0" cellspacing="2" cellpadding="2">
                            <tr>
                                <td align="center" style="font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight: bold; text-align: center; color:#1a1a1a">Olá {{ $nome }}, tudo bom?</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; text-align: center; color:#1a1a1a">Adoraria que você visitasse a minha página no iengravidei. </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><img src="{{ asset('assets/img/email/visitantes/images/vistantes-convite_03.jpg') }}" width="600" height="255" style="display:block"/></td>
                </tr>
                <tr>
                    <td><img src="{{ asset('assets/img/email/visitantes/images/vistantes-convite_04.jpg') }}" width="600" height="144" style="display:block"/></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="600" border="0" cellspacing="2" cellpadding="2">
                            <tr>
                                <td width="88" rowspan="2" align="right"><img src="{{ asset('assets/img/email/visitantes/images/vistantes-convite_07.jpg') }}" width="32" height="50" /></td>
                                <td align="center"><img src="{{ asset('assets/img/email/visitantes/images/vistantes-convite_09.jpg') }}" width="185" height="23" /></td>
                                <td width="88">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="460" align="center">
                                    <p><a href="{{ URL::route('user.public', array('publicId' => $publicId)) }}?param_data={{ $hash }}" style="font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight: bold; text-decoration:none; color:#1a1a1a">www.iengravidei.com.br/{{ $publicId }}</a></p>
                                </td>
                                <td width="88">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="background-color:#da235c;">
                        <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; font-weight: bold; line-height: 2px; color:#ffffff">Para mais informações acesse:
                            <a style="color:#ffffff; text-decoration: none;" href="http://www.iengravidei.com.br">http://www.iengravidei.com.br</a>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>