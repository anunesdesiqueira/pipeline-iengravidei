<?php

use Illuminate\Database\Migrations\Migration;

class CreateGiftlistsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('giftlists', function($table)
        {
            $table->increments('id');
            $table->string('hash',60);
            $table->string('name',45);
            $table->integer('profile_id')->references('id')->on('profiles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('giftlists');
    }

}