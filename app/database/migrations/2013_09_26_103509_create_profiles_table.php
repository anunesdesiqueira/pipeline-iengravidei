<?php

use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('profiles', function($table) {
            $table->increments('id');
            $table->string('email', 200);
            $table->string('name', 200);
            $table->string('baby_name', 200);
            $table->boolean('activated')->default(0);
            $table->string('activation_code', 60);
            $table->string('reset_password_code', 60);
            $table->string('password', 60);
            $table->integer('plan_id')->default(1);
            $table->string('hash', 60);
            $table->string('public_id', 20);
            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('profiles');
	}

}