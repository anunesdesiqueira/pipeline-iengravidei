{{ Form::open(array('id' => 'edit-perfil-senha')) }}
    <h2>Trocar senha</h2>
    <ul class="dadosPessoais">
        <li class="senha">
            <label>
                <span>Atual*:</span>
                <input type="password" name="password_atual" />
            </label>
        </li>
        <li class="senha">
            <label>
                <span>Nova*:</span>
                <input type="password" name="password" id="password" class="required" />
            </label>
        </li>
        <li class="senha">
            <label>
                <span>Digite novamente*:</span>
                <input type="password" name="password_confirmation" />
            </label>
        </li>
        <li class="campoObr">Campo obrigatório*</li>
        <li>
            <input type="submit" value="Confirmar" class="bt_alterar_senha" />
        </li>
    </ul>
{{ Form::close() }}