<?php namespace IEngravidei\Entities;

class Message extends \Eloquent
{
    protected $table = 'profiles_messages';

    public function validate($input)
    {

        $rules = array(
            'from_id' => 'required|integer',
            'to_id'   => 'required|integer',
            'body'    => 'required',
            'type'    => 'required|in:private,wall',
        );

        $messages = array(
            'from_id.required'    => 'O remetente deve ser especificado.',
            'from_id.integer'     => 'Remente inválido',
            'to_id.required'      => 'Destinatário deve ser especificado',
            'to_id.integer'       => 'Destinatário inválido',
            'body.required'       => 'O texto da mensagem deve ser digitado',
            'type.required'       => 'O tipo da mensagem deve ser especificado',
            'type.in'             => 'Tipo da mensagem inválido',

        );

        return \Validator::make($input, $rules, $messages);

    }

    public function addressee()
    {
        return $this->belongsTo('IEngravidei\Entities\Profile', 'from_id');
    }
}