@if(count($mApproved) > 0)
    <ul>
        @foreach ($mApproved as $msg)
        <li>
            <a href="#" data-hash="{{ $msg->hash }}" class="excluir">Excluir</a>
            <span class="data">{{ date('d/m/Y', strtotime($msg->created_at)) }}</span>
            <div class="clearfix"></div>
            <h3>{{ $msg->name }}</h3>
            <p>{{ $msg->message }}</p>
        </li>
        @endforeach
    </ul>
    {{ $mApproved->links() }}
@else
    <p>Você ainda não possue nenhum recado especial aprovado.</p>
@endif