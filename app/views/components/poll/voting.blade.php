<h3>Enquete</h3>
@if(!empty($poll->question))
    <p>{{ $poll->question->question }}</p>
    <ul>
    @foreach ($poll->answers as $answer)
        <li>
            <label>{{ $answer->answer }}</label>
            <div class="barra">
                <div class="mascara"></div>
                <div class="porcentagem" style="width:{{ 0 === $poll->total ? 0 : round($answer->total*100/$poll->total) }}%;"></div>
            </div>
            <span>{{ 0 === $poll->total ? 0 : round($answer->total*100/$poll->total) }}%</span>
        </li>
    @endforeach
    </ul>
    <button onclick="window.location.href='{{ route('poll.add') }}'">Editar</button>
@else
    <a href="{{ route('poll.add') }}" class="criar">criar enquete</a>
    <div class="enquete-criar">Faça já uma enquete!</div>
@endif