<?php

namespace IEngravidei\Message;

use IEngravidei\Entities\Message;
use IEngravidei\Repositories\MessageRepositoryInterface;

class DbMessageRepository implements MessageRepositoryInterface
{
    public function __construct()
    {
        $this->message = new Message;
    }

    public function count($id_profile)
    {
        return Message::where('to_id', '=', $id_profile)
            ->where('read', '=', 0)->whereNull('deleted_to_at')->count();
    }

    public function read($id_profile)
    {
        return Message::where('to_id', '=', $id_profile)
            ->update(array('read' => 1, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function send($input)
    {
        $this->message->parent_id = $input['parent_id'];
        $this->message->from_id = $input['from_id'];
        $this->message->to_id = $input['to_id'];
        $this->message->body = $input['body'];
        $this->message->type = $input['type'];
        $this->message->read = 0;

        return $this->message->save();
    }

    public function history($fromId, $toId)
    {
        $results = \DB::select(
            '(select `profiles_messages`.`id` as `idMsg`, `profiles_messages`.`from_id`, `profiles_messages`.`to_id`, `profiles_messages`.`body`, `profiles_messages`.`created_at`, `profiles`.`id`, `profiles`.`name`, `profiles`.`public_id`, `profiles`.`profile_picture`
                from `profiles_messages` inner join `profiles` on `profiles`.`id` = `profiles_messages`.`from_id`
                where `from_id` = ? and `to_id` = ? and `deleted_to_at` is null)
            union
            (select `profiles_messages`.`id` as `idMsg`, `profiles_messages`.`from_id`, `profiles_messages`.`to_id`, `profiles_messages`.`body`, `profiles_messages`.`created_at`, `profiles`.`id`, `profiles`.`name`, `profiles`.`public_id`, `profiles`.`profile_picture`
                from `profiles_messages` inner join `profiles` on `profiles`.`id` = `profiles_messages`.`from_id`
                where `from_id` = ? and `to_id` = ? and `deleted_from_at` is null)
            order by `created_at` desc',
            array($fromId, $toId, $toId, $fromId)
        );
        return $results;
    }

    public function destroyFrom($id, $id_profile)
    {
        return Message::where('id', '=', $id)->where('from_id', '=', $id_profile)
            ->update(array('deleted_from_at' => date('Y-m-d H:i:s')));
    }

    public function destroyTo($id, $id_profile)
    {
        return Message::where('id', '=', $id)->where('to_id', '=', $id_profile)
            ->update(array('deleted_to_at' => date('Y-m-d H:i:s')));
    }
}