<div class="criar">
    <h2 name="enquete">Crie uma nova enquente</h2>
    {{ Form::open(array('route' => 'poll.add', 'enctype' => 'multipart/form-data', 'id' => 'form-poll-create')) }}
        <ul>
            <li>
                <label>Digite sua pergunta*:</label>
                {{ Form::text('question', Input::old('question'), array('class' => 'pergunta')) }}
                <span>Ex.: Ajude a escolher o nome / Em qual escola devo colocar?</span>
            </li>
            <li>
                <label>Respostas para votação*:</label>
                {{ Form::text('answer[]') }}
                {{ Form::text('answer[]') }}
                {{ Form::text('answer[]') }}
                {{ Form::text('answer[]') }}
            </li>
            <li>
                {{ Form::submit('Ativar', array('class' => 'btn-enquete-ativar')) }}
            </li>
        </ul>
    {{ Form::close() }}
</div>