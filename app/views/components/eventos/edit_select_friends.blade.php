{{ Form::open(array('id' => 'form-event-edit-friends')) }}
    {{ Form::hidden('hash', $event->hash) }}
        <div class="col1">
            <p>Adicione mais amigos</p>
            <span class="info">Escolha na lista as amigas do iengravidei que deseja convidar:</span>
            <h4>lista de amigos / Visitantes</h4>
            <div class="clearfix"></div>
            <select id="list-friends" name="friends[]" multiple="multiple">
                @foreach($friends as $k => $friend)
                    <?php
                        $disable = false;
                        foreach($event->friends as $sel)
                        {
                            if($k == $sel)
                                $disable = true;
                        }
                    ?>
                    <option value="{{ $k }}" <?php echo ($disable)? 'disabled selected' : ''; ?>>{{ $friend }}</option>
                @endforeach
                @foreach($event->mails_externos as $mail)
                <option value="{{ $mail }}" disabled selected>{{ $mail }}</option>
                @endforeach
            </select>
            <span class="info mail-margin">Adicione outras pessoas:</span>
            <label>E-mails externos: {{ Form::text('emails', Input::old('emails'), array('id' => 'emails')) }}</label>
            <a href="#" class="addMail">&nbsp;</a>
            <div class="clearfix"></div>
            <div class="emailsInvalidos">
                <h5>Os e-mails abaixo não são válidos, por favor confira e adicione novamente:</h5>
                <p></p>
            </div>
        </div>
        <div class="col2">
            <p>Lista de convidados</p>
            <span class="info">Confira a lista atualizada de convidados</span>
            <span class="atencao">Atenção: Para concluir a atualização da Lista de Convidados envie o convite abaixo.<span>
        </div>
        <div class="clearfix"></div>

        <div class="visuTplError"><label for="list-friends" class="error"></label></div>

        <button type="submit" class="enviar-convite">
            <span class="ico-enviar">&nbsp;</span>Enviar convite
        </button>
    {{ Form::hidden('mails_externos') }}
{{ Form::close(); }}