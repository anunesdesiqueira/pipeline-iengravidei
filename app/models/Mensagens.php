<?php

require '../libraries/aws.phar';

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Enum\KeyType;
use Aws\DynamoDb\Enum\Type;
use Aws\DynamoDb\Enum\AttributeAction;

class Mensagens
{
    protected $table  = 'mensagens';
    protected $key    = 'AKIAIE462V7GHWBNFQXQ';
    protected $secret = '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';
    protected $region = 'sa-east-1';

    public function conect()
    {
        $client = DynamoDbClient::factory(array(
            'key'    => $this->key,
            'secret' => $this->secret,
            'region' => $this->region
        ));

        return $client;
    }

    /*
     * A tabela no Dynamodb só precisa de 2 campos para serem as chaves primarias;
     * Nesse caso utilizei o campo "id" como um hash de 256 caracteres e o campo "data_cadastro" com timestampo da função time()
     * Fique a vontade para utilizar do jeito que você quiser
     * */
    public function cria_tabela($client)
    {
        // Create an "errors" table
        $client->createTable(array(
            'TableName' => 'messages',
            'AttributeDefinitions' => array(
                array(
                    'AttributeName' => 'id',
                    'AttributeType' => 'S'
                ),
                array(
                    'AttributeName' => 'data_cadastro',
                    'AttributeType' => 'N'
                )
            ),
            'KeySchema' => array(
                array(
                    'AttributeName' => 'id',
                    'KeyType'       => 'HASH'
                ),
                array(
                    'AttributeName' => 'data_cadastro',
                    'KeyType'       => 'RANGE'
                )
            ),
            'ProvisionedThroughput' => array(
                'ReadCapacityUnits'  => 10,
                'WriteCapacityUnits' => 5
            )
        ));
    }

    /*
     * Retorna todos os amigos que o usuário já enviou mensagens
     * */
    public function return_people_messages($client, $hash_user)
    {
        $iterator = $client->getIterator('Scan', array(
            'TableName' => 'messages',
            'Count' => true,
            'ScanFilter' => array(
                'active' => array(
                    'AttributeValueList' => array(
                        array('N' => '1')
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'tipo' => array(
                    'AttributeValueList' => array(
                        array('S' => 'private')
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'parent_id' =>  array(
                    'AttributeValueList' => array(
                        array('S' => 'P')
                    ),
                    'ComparisonOperator' => 'EQ'
                )
            ),
            'ReturnValues' => 'none'
        ));

        return $iterator;
    }

    /*
     * Retorna mensagem para a pessoa selecionada
     * */
    public function return_messages_people_selected($client, $field, $value, $hash_user )
    {
        $iterator = $client->getIterator('Scan', array(
            'TableName' => 'messages',
            'Count' => true,
            'ScanFilter' => array(
                'active' => array(
                    'AttributeValueList' => array(
                        array('N' => '1')
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'tipo' => array(
                    'AttributeValueList' => array(
                        array('S' => 'private')
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                $field =>  array(
                    'AttributeValueList' => array(
                        array('S' => $value)
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'delete' =>  array(
                    'AttributeValueList' => array(
                        array('S' => $hash_user)
                    ),
                    'ComparisonOperator' => 'NOT_CONTAINS'
                )
            ),
            'ReturnValues' => 'none'
        ));

        return $iterator;
    }

    /*
     * Retorna mensagem para a pessoa selecionada
     * */
    public function delete_messages_selected($client, $id, $date, $field, $value )
    {
        $response = $client->updateItem(array(
            "TableName" => "messages",
            "Key" => array(
                "id" => array(
                    Type::STRING => $id
                ),
                "data_cadastro" => array(
                    Type::NUMBER => $date
                )
            ),
            "AttributeUpdates" => array(
                $field => array(
                    "Action" => AttributeAction::PUT,
                    "Value" => array(
                        Type::STRING => $value
                    )
                )
            )
        ));

        print_r($response);
    }

    /*
     * Envia a mensagem
     *
     * Lembrando que os campos podem ser criados a qualquer momento, basta apenas acrescentá-los ao array
     * */
    public function send_messages($client, $input)
    {
        $time = time();
        $data = date('d-m-Y h:m:s').$input['hash_user'].$input['addressee_hash_user'];
        $id = hash('sha512', $data);

        $result = $client->putItem(array(
            'TableName' => 'messages',
            'Item' => array(
                'id'                  => array('S' => $id),
                'data_cadastro'       => array('N' => $time),
                'hash_user'           => array('S' => $input['hash_user']),
                'name_user'           => array('S' => $input['name_user']),
                'addressee_hash_user' => array('S' => $input['addressee_hash_user']),
                'addressee_name_user' => array('S' => $input['addressee_name_user']),
                'message'             => array('S' => $input['message']),
                'tipo'                => array('S' => $input['tipo']),
                'from'                => array('S' => $input['from']),
                'to'                  => array('S' => $input['to']),
                'place'               => array('S' => $input['place']),
                'are_friends'         => array('N' => $input['are_friends']),
                'parent_id'           => array('S' => $input['parent_id']),
                'sender_name_user'    => array('S' => $input['name_user']),
                'delete'              => array('S' => "0"),
                'active'              => array('N'   => 1)
            )
        ));

        return $result;
    }








    /*
     * Envia a mensagem
     *
     * Lembrando que os campos podem ser criados a qualquer momento, basta apenas acrescentá-los ao array
     * */
    public function send_messages_OLD($client, $input)
    {
        $time = time();
        $data = date('d-m-Y h:m:s').$input['user1'].$input['amigos1'];
        $id = hash('sha512', $data);

        $result = $client->putItem(array(
            'TableName' => 'messages',
            'Item' => array(
                'id'                  => array('S' => $id),
                'data_cadastro'       => array('N' => $time),
                'hash_user'           => array('S' => $input['user1']),
                'name_user'           => array('S' => $input['user2']),
                'addressee_hash_user' => array('S' => $input['amigos1']),
                'addressee_name_user' => array('S' => $input['amigos2']),
                'message'             => array('S' => $input['mensagem']),
                'tipo'                => array('S' => $input['tipo']),
                'from'                => array('S' => $input['user1']),
                'to'                  => array('S' => $input['amigos1']),
                'place'               => array('S' => $input['tipo']),
                'are_friends'         => array('N' => $input['are_friends']),
                'parent_id'           => array('S' => $input['parent_id']),
                'active'              => array('N'   => 1)
            )
        ));

        return $result;
    }

    /*
     * Retorna ID da conversa
     * */
    public function return_id_messages($client, $type, $hash_user, $hash_user2)
    {
        $iterator = $client->getIterator('Scan', array(
            'TableName' => 'messages',
            'Count' => true,
            'ScanFilter' => array(
                'active' => array(
                    'AttributeValueList' => array(
                        array('N' => '1')
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'tipo' => array(
                    'AttributeValueList' => array(
                        array('S' => $type)
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'from' =>  array(
                    'AttributeValueList' => array(
                        array('S' => $hash_user)
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'to' =>  array(
                    'AttributeValueList' => array(
                        array('S' => $hash_user2)
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'parent_id' =>  array(
                    'AttributeValueList' => array(
                        array('S' => "P")
                    ),
                    'ComparisonOperator' => 'EQ'
                )
            ),
            'ReturnValues' => 'none'
        ));

        return $iterator;
    }

    /*
     * Imprimir os resuldados:
     * foreach ($iterator as $item) { echo $item['nome_campo']['N']; }
     * N = numeric | S = String
     * */
    public function return_all($client, $user, $type)
    {
        $iterator = $client->getIterator('Scan', array(
            'TableName' => 'messages',
            'Count' => true,
            'ScanFilter' => array(
                'active' => array(
                    'AttributeValueList' => array(
                        array('N' => '1')
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'tipo' => array(
                    'AttributeValueList' => array(
                        array('S' => $type)
                    ),
                    'ComparisonOperator' => 'EQ'
                ),
                'addressee_hash_user' =>  array(
                    'AttributeValueList' => array(
                        array('S' => $user)
                    ),
                    'ComparisonOperator' => 'EQ'
                )
            ),
            'ReturnValues' => 'none'
        ));

        return $iterator;
    }



    /*
     * Você pode imprimir as tabelas dessa forma:
     * foreach($iterator as $table){ echo $table; }
     *
     * */
    public static function lista_tabelas($client)
    {
        $iterator = $client->getIterator('ListTables');

        return $iterator;
    }

}