<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\VisitantesRepositoryInterface as Visitantes;
use IEngravidei\Repositories\PollRepositoryInterface as Poll;
use IEngravidei\Repositories\BabylineRepositoryInterface as Babyline;
use IEngravidei\Repositories\AlbumRepositoryInterface as Album;
use IEngravidei\Repositories\DicasRepositoryInterface as Dicas;
use IEngravidei\Repositories\GiftRepositoryInterface as Gift;
use IEngravidei\Repositories\SpecialMessageRepositoryInterface as SpecialMessage;
use IEngravidei\Repositories\MuralRepositoryInterface as Mural;

class ProfileController extends BaseController
{
    public function __construct(Profile $profile, Visitantes $visitantes, Poll $poll, Babyline $babyline, Album $album, Dicas $dicas, Gift $gift, SpecialMessage $specialMessage, Mural $mural)
    {
        parent::__construct();

        $this->profile = $profile;
        $this->poll = $poll;
        $this->babyline = $babyline;
        $this->album = $album;
        $this->dicas = $dicas;
        $this->gift = $gift;
        $this->specialMessage = $specialMessage;
        $this->visitantes = $visitantes;
        $this->mural = $mural;
    }

    public function myProfile()
    {
        $babyline_weeks = Cache::rememberForever("babyline_weeks", function() {
            return $this->babyline->getWeeks();
        });
        $babyline = $this->babyline->getList($this->user->id);
        $babyline = $this->createPathImagesBabyline($babyline);

        $gifts = $this->gift->show(array('id' => $this->user->id, 'limit' => 4));
        $gifts = $this->createPathImagesProducts($gifts);

        $limitAlbum = 3;
        $albums = $this->album->getAlbums($this->user->id, $limitAlbum);
        $albums = $this->createPathImagesAlbum($albums);
        $albums = $this->createUrl($albums, 'edit_my_albums');
        $countAlbums = $this->album->countAlbuns($this->user->id);
        $upAlbum = 'album-container';
        $uriAlbum = 'my_albums.add_album';

        $dicas = $this->dicas->getAll();

        return View::make('profile.my_profile', compact('babyline_weeks','babyline','gifts','albums','limitAlbum','uriAlbum','upAlbum','countAlbums','dicas'));
    }

    public function editProfile()
    {
        $profile = $this->profile->find($this->user->id);
        $profile->birth = implode("/",array_reverse(explode("-",$profile->birth)));

        $countries = Cache::rememberForever("countries", function() {
            return DB::table('countries')->lists('name', 'id');
        });

        $states = Cache::rememberForever("states", function() {
            return DB::table('states')->lists('name', 'id');
        });

        $cities = Cache::rememberForever("citiesFromStateId" . $profile->state_id, function() use ($profile) {
            return DB::table('cities')->where('state_id', '=', $profile->state_id)->lists('name', 'id');
        });

        $pregnancy_weeks = Cache::rememberForever('pregnancy_weeks', function() {
            return $this->babyline->getWeeks();
        });

        return View::make('profile.edit_profile', compact('profile', 'countries', 'states', 'cities', 'pregnancy_weeks'));
    }

    public function postEditProfile()
    {
        $data = Input::all();

        if($data) {
            $validation = Validator::make(Input::all(), array(
                'name' => 'required|max:200',
                'father_name' => 'max:200',
                'baby_name' => 'max:200',
                'birth' => 'required|date_format:d/m/Y',
                'country_id' => 'required|integer',
                'state_id' => 'required|integer',
                'city_id' => 'required|integer',
                'pregnancy_weeks' => 'required|numeric',
                'picture' => 'image|max:200|mimes:jpg,jpeg'
            ), $this->profile->messages);

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                unset($data['_token']);
                $data['id'] = $this->user->id;

                if (isset($data['picture']) && $data['picture'])
                    $data['profile_picture'] = $this->uploadProfilePicture($data['picture'], $data['id']);

                $result = $this->profile->profileUpdate($data);

                if($result) {
                    $profile = $this->profile->find($this->user->id);
                    $profile = $this->createPathImagesProfile(array($profile));
                    $profile = $profile[0];
                    return Response::json(array('status' => 'true','profile_picture' => $profile->profile_picture, 'profile_name' => $profile->name));
                }
            }
        }
    }

    public function postEditPassword()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'password_atual' => 'required|between:5,16|',
                'password'  => 'required|between:5,16|confirmed'
            ), array(
                'password_atual.required' => 'Digite sua senha atual',
                'password_atual.between' => 'Senha deve possuir no minimo 5 e no máximo 16 caracteres',
                'password.required' => 'Digite sua nova senha',
                'password.between' => 'Senha deve possuir no minimo 5 e no máximo 16 caracteres',
                'password.confirmed'    => 'Senhas não conferem',
            ));

            if ($validation->fails()) {
                return Redirect::route('user.personalize')->withInput()->withErrors($validation);
            } else {
                $user = $this->profile->find($this->user->id);

                $old_password = $data['password_atual'];
                $password = $data['password'];

                if(\Hash::check($old_password, $this->user->password)) {
                    $user->password = Hash::make($password);

                    if($user->save())
                        return Response::json(array('status' => 'true'));
                }
            }
        }
    }

    public function excluir()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'opt' => 'required',
            ), array(
                'opt.required' => 'Ops ocorreu um erro!',
            ));

            if ($validation->fails()) {
                return Redirect::route('user.personalize')->withInput()->withErrors($validation);
            } else {
                $excluir = $this->profile->excluirProfile($this->user->id);
                if($excluir) {
                    unset($this->user->profile_picture);
                    unset($this->user->slug);
                    unset($this->user->plan_roles);

                    Auth::client()->logout();

                    return Response::json(array('status' => 1));
                }
            }
        }
    }

    public function personalize()
    {
        $layout = $this->setBackgroundLayout($this->user->layout);
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'tpl' => 'required|integer'
            ), array(
                'tpl.required' => 'Layout não selecionado!',
                'tpl.numeric' => 'Layout precisa ser um número.'
            ));

            if ($validation->fails()) {
                return Redirect::route('user.personalize')->withInput()->withErrors($validation);
            } else {
                $data['id'] = $this->user->id;
                $this->profile->setLayout($data);
                $layout = $this->setBackgroundLayout($data['tpl']);
            }
        }

        return View::make('profile.personalize', compact('layout'));
    }

    public function publicProfile($publicId, $hash = null)
    {
        $profile = $this->profile->find($publicId);
        $profile = $this->createPathImagesProfile(array($profile));
        $profile = $profile[0];
        $profile->month = $this->calcGestation($profile->pregnancy_weeks);
        $profile->countdown = (40 - $profile->pregnancy_weeks);
        $layout = $profile->layout;
        $convite = Input::get('param_data');

        if(isset($this->user->id)) {
            if($this->user->id === $profile->id) {
                $permissions['profile_user'] = true;
            }

            if($this->profile->getFriend($this->user->id, $profile->id) || isset($permissions['profile_user'])) {
                $permissions['amiga'] = true;
            }

            if(!isset($permissions['profile_user']) && !isset($permissions['amiga'])) {
                $permissions['solicitacao'] = $this->profile->getRequireFriend($this->user->id, $profile->id);
            }
        }

        if(!is_null($convite)) {
            if($this->visitantes->getVisitante($profile->id, $convite)) {
                $permissions['convidado'] = true;
            }
        }

        if(isset($permissions['profile_user']) || isset($permissions['amiga']) || isset($permissions['convidado'])) {
            $albums = $this->album->getAlbums($profile->id, 3);
            $albums = $this->createPathImagesAlbum($albums);
            $albums = $this->createUrlPublic($albums, $profile->public_id, 'show_my_album_photos');
            $countAlbums = $this->album->countAlbuns($profile->id);

            View::share(compact('albums','countAlbums'));
        } else if(!isset($permissions['solicitacao'])) {
            $permissions['visitante'] = true;
        }

        $poll = $this->poll->getPoll($profile->id);
        if(!empty($poll->question)) {
            if($this->poll->findLogAnswer($poll->question->id, getenv("REMOTE_ADDR"), isset($this->user->id)? $this->user->id : '')) {
                $permissions['voted'] = true;
            }
        }

        $babyline = $this->babyline->getList($profile->id);
        $babyline = $this->createPathImagesBabyline($babyline);

        $gifts = $this->gift->show(array('id' => $profile->id, 'limit' => 4));
        $gifts = $this->createPathImagesProducts($gifts);

        if(!is_null($hash)) {
            $showStatus = $this->mural->momShowStatus($profile->id, $hash);
            View::share(compact('showStatus'));
        }

        return View::make('profile.public', compact('profile','babyline','gifts','layout','poll','publicId','permissions'));
    }
}