<!--### Cadastro Visitante ###-->
<div class="cadastro-visitante">
    <div class="holderAdd">
        <div class="gostou">Quer ver mais?</div>
        <div class="floatLeft">
            <h6>Cadastro de visitante</h6>
            <p>Solicite abaixo a liberação para acessar esse perfil completo<br/>(fotos, mural, recados especiais e muito mais).</p>
            {{ Form::open(array('route' => 'visitantes.solicitar', 'id' => 'public-convite')) }}
                {{ Form::hidden('publicId', $publicId) }}
                <ul>
                    <li><label>Nome:&nbsp;&nbsp; {{ Form::text('visitantes_nome') }}</label></li>
                    <li><label>Sobrenome: {{ Form::text('visitantes_sobrenome') }}</label></li>
                    <li><label>E-mail: {{ Form::text('visitantes_email', '', array('class' => 'email')) }}</label></li>
                    <li>{{ Form::submit('CADASTRAR') }}</li>
                </ul>
            {{ Form::close() }}
        </div>
    </div>
    <div class="botShadow"></div>
</div>
<!--### End: Cadastro Visitante ###-->

<!--### Lista de Serviços do Perfil ###-->
<div class="lista-servicos-perfil">
    <ul>
        <li>
            <div class="thumb lista-presentes"></div>
            <h6 class="presentes">Lista de Presentes</h6>
            <p>Crie sua lista de presentes e receba tudo sem sair do site! Facilite a sua vida e de seus amigos! </p>
        </li>
        <li>
            <div class="thumb criador-eventos"></div>
            <h6 class="eventos">Criador de Eventos</h6>
            <p> Chá de Bebê, Chá de Fralda, Nascimento ou o que você quiser! Avise a todos e crie listas de presentes!</p>
        </li>
        <li>
            <div class="thumb meu-mural"></div>
            <h6 class="mural">Meu Mural</h6>
            <p>Aqui é o lugar para postar e guardar para sempre seus momentos especiais. Vídeo do ultrassom, sentimentos do dia-a-dia e ainda pode compartilhar tudo em suas redes sociais. </p>
        </li>
        <li>
            <div class="thumb album-fotos"></div>
            <h6 class="fotos">Álbum de Fotos</h6>
            <p>Crie álbuns exclusivos para guardar os momentos desses meses tão inesquecíveis!</p>
        </li>
    </ul>
    <a href="{{ route('services') }}"><span>&nbsp;</span>Confira lista completa</a>
</div>
<!--### End: Lista de Serviços do Perfil ###-->

<div class="cadastre-tambem">
    <h5>Cadastre-se você também.</h5>
    <p>Na 1ª Rede Social para Gestantes do mundo!</p>
    <a href="{{ route('home') }}">CLIQUE AQUI</a>
</div>