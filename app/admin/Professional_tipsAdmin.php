<?php

use KraftHaus\Bauhaus\Admin;

class ProfessionalTipsAdmin extends Admin
{

    public function configureList($mapper)
    {
        $mapper->identifier('title');
        $mapper->identifier('pub_inc');
    }

    public function configureForm($mapper)
    {
        $mapper->text('title');
        $mapper->wysiwyg('body');
        $mapper->file('picture')->location('assets/dicas_profissionais')->naming('random');
        $mapper->textarea('youtube');
        $mapper->text('author');
        $mapper->select('type')->options([0 => 'Normal', 1 => 'Produto']);
        $mapper->datetime('pub_inc');
        $mapper->select('status')->options([1 => 'Ativo', 0 => 'Inativo']);
    }

    public function configureFilters($mapper)
    {
        // ...
    }

}