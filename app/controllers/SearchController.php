<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;

class SearchController extends BaseController
{
    public function __construct(Profile $profile)
	{
        parent::__construct();
        $this->profile = $profile;
	}

    public function search()
    {
        $data = Input::all();

        if($data) {
            $search = $this->profile->search($data);
            $search = $this->createPathImagesProfile($search);
            $search = $this->createUrl($search, 'user.public');
        }

        $terms = array();

        if(isset($data['father_name']) && !empty($data['father_name']))
            $terms['father_name'] = $data['father_name'];
        else
            $terms['father_name'] = null;

        if(isset($data['state_id']) && !empty($data['state_id'])) {
            $terms['state_id'] = $data['state_id'];
            $states = DB::table('states')->where('states.id', '=', $data['state_id'])->first();
            $terms['state_name'] = $states->name;
        } else {
            $terms['state_id'] = null;
        }

        if(isset($data['q']) && !empty($data['q']))
            $terms['q'] = $data['q'];
        else
            $terms['q'] = null;

        $states = Cache::rememberForever('states', function() {
            return DB::table('states')->orderBy('name', 'ASC')->lists('name', 'id');
        });

        if(Request::ajax()) {
            $page = View::make('components.busca.resultado', compact('search', 'terms'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('search.search', compact('states', 'search', 'terms'));
        }
    }
}