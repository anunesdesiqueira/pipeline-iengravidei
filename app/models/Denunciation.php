<?php

require '../libraries/aws.phar';

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Enum\KeyType;
use Aws\DynamoDb\Enum\Type;
use Aws\DynamoDb\Enum\AttributeAction;

class Denunciation
{
    protected $table  = 'denunciation';
    protected $key    = 'AKIAIE462V7GHWBNFQXQ';
    protected $secret = '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';
    protected $region = 'sa-east-1';

    public function conect()
    {
        $client = DynamoDbClient::factory(array(
            'key'    => $this->key,
            'secret' => $this->secret,
            'region' => $this->region
        ));

        return $client;
    }

    /*
     * A tabela no Dynamodb só precisa de 2 campos para serem as chaves primarias;
     * Nesse caso utilizei o campo "id" como um hash de 256 caracteres e o campo "data_cadastro" com timestampo da função time()
     * Fique a vontade para utilizar do jeito que você quiser
     * */
    public function cria_tabela($client)
    {
        // Create an "errors" table
        $client->createTable(array(
            'TableName' => 'denunciation',
            'AttributeDefinitions' => array(
                array(
                    'AttributeName' => 'id',
                    'AttributeType' => 'S'
                ),
                array(
                    'AttributeName' => 'data_cadastro',
                    'AttributeType' => 'N'
                )
            ),
            'KeySchema' => array(
                array(
                    'AttributeName' => 'id',
                    'KeyType'       => 'HASH'
                ),
                array(
                    'AttributeName' => 'data_cadastro',
                    'KeyType'       => 'RANGE'
                )
            ),
            'ProvisionedThroughput' => array(
                'ReadCapacityUnits'  => 10,
                'WriteCapacityUnits' => 5
            )
        ));
    }

    public function cadastra_denuncia($client, $dados)
    {
        $time = time();
        $data = date('d-m-Y h:m:s').$dados['denuncias'].rand(65464,4648948);
        $denid = hash('sha512', $data);

        $result = $client->putItem(array(
            'TableName' => 'denunciation',
            'Item' => array(
                'id'                   => array('S' => $denid),
                'data_cadastro'        => array('N' => $time),
                'user_id'              => array('S' => isset($dados['user_id'])?$dados['user_id']:" "),
                'user_name'            => array('S' => isset($dados['user_name'])?$dados['user_name']:" "),
                'hash_user'            => array('S' => isset($dados['hash_user'])?$dados['hash_user']:" "),
                'tipo_denuncia'        => array('S' => isset($dados['denuncias'])?$dados['denuncias']:" "),
                'foto'                 => array('S' => isset($dados['name_image'])?$dados['name_image']:" "),
                'mensagem'             => array('S' => !empty($dados['comentario'])?$dados['comentario']:" "),
                'email_contato'        => array('S' => isset($dados['email'])?$dados['email']:" "),
                'tipo_conteudo'        => array('S' => isset($dados['conteudo'])?$dados['conteudo']:" "),
                'usuario_identificado' => array('S' => isset($dados['identificacao'])?$dados['identificacao']:" "),
                'active'               => array('N'   => 1)
            )
        ));

        return $result;
    }

    /*
     * Imprimir os resuldados:
     * foreach ($iterator as $item) { echo $item['nome_campo']['N']; }
     * N = numeric | S = String
     * */
    public function return_all($client)
    {
        $iterator = $client->getIterator('Scan', array(
            'TableName' => 'denunciation',
            'Count' => true,
            'ScanFilter' => array(
                'ativo' => array(
                    'AttributeValueList' => array(
                        array('N' => '1')
                    ),
                    'ComparisonOperator' => 'EQ'
                )
            )
        ));

        return $iterator;
    }

    /*
     * Você pode imprimir as tabelas dessa forma:
     * foreach($iterator as $table){ echo $table; }
     *
     * */
    public static function lista_tabelas($client)
    {
        $iterator = $client->getIterator('ListTables');

        return $iterator;
    }

}