/* Convites Eventos */

//Globals
var git_list_name;

//Hash location
function scrollToAnchorEnvents(aid)
{
    var aTag = $("h3[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

//Select mask
function loadMask()
{
    $('select').each(function(){
        var valorCombo = $(this).find('option:selected').html();
        $(this).prev('.mascara').find('.optName').text(valorCombo);
    });
}

//Inout mask validator
function loadMaskInput()
{
    // Mask inputs
    $('input[name="birth"], input[name="date"]').mask("99/99/9999");
    $('input[name="time"]').mask('99:99');
}

//Exibir msgs
function msgEdit(text, obj)
{
    var span = document.createElement('span');
    var container = obj;

    $(span).addClass('presentes-msg');
    $(span).html(text);

    $(container).append(span);

    $(container).find('.presentes-msg').fadeOut( 3000, function() {
        $(container).find('.presentes-msg').remove();
    });
}

//Exibe itens por GET
function showItensEdit(url, callBack)
{
    CallMethod(
        url,
        'GET',
        '',
        loadingShowEdit,
        loadindHideEdit,
        callBack
    );
}

function loadingShowEdit() {}

function loadindHideEdit() {}

//Carrega a input com os email externos validos selecionados
function emailExternos()
{
    var emails = $('input[name="mails_externos"]');
    var selecionados = $('.ms-container .ms-selection ul.ms-list li.ms-email-select');
    var json = [];

    emails.val('');

    if(selecionados.length > 0) {
        selecionados.each(function(index, value)
        {
            json.push($(value).data('id'));
        });

        emails.val(JSON.stringify(json));
    }
}

// Validation

// Create form convites
function validatorFormCreate()
{
    $('form#form-event-create').validate({
        rules: {
            type : {
                required: true
            },
            list : {
                required: true
            },
            name : {
                required: true
            },
            date : {
                required: true
            },
            mom_name : {
                required: true
            },
            address : {
                required: true
            },
            time : {
                required: true
            },
            template_id : {
                required: true
            },
            'friends[]' : {
                selectFriends: true
            }
        },
        messages: {
            type : {
                required: "*O tipo de evento é obrigatório."
            },
            list : {
                required: "*Selecione uma lista ou crie um nova."
            },
            name : {
                required: "*Nome do evento é obrigatório."
            },
            date : {
                required: "*Data do evento é obrigatório."
            },
            mom_name : {
                required: "*Nome da mamãe é obrigatório."
            },
            address : {
                required: "*Endereço é obrigatório."
            },
            time : {
                required: "*Horário é obrigatório."
            },
            template_id : {
                required: "*Por favor, selecione um template."
            },
            'friends[]' : {
                selectFriends: "*Selecione pelo menos uma amiga ou email externo."
            }
        }
    });
}

// Edit form dados
function validatorFormEdit()
{
    $('form#form-event-edit-dados').validate({
        rules: {
            name : {
                required: true
            },
            date : {
                required: true
            },
            mom_name : {
                required: true
            },
            address : {
                required: true
            },
            time : {
                required: true
            }
        },
        messages: {
            name : {
                required: "*Nome do evento é obrigatório."
            },
            date : {
                required: "*Data do evento é obrigatório."
            },
            mom_name : {
                required: "*Nome da mamãe é obrigatório."
            },
            address : {
                required: "*Endereço é obrigatório."
            },
            time : {
                required: "*Horário é obrigatório."
            }
        }
    });
}

// Edit form friends
function validationFormEditFriends()
{
    $('form#form-event-edit-friends').validate({
        rules : {
            'friends[]' : {
                selectFriends: true
            }
        },
        messages: {
            'friends[]' : {
                selectFriends: "*Selecione pelo menos uma amiga ou email externo."
            }
        }
    });
}

// Check select friends
jQuery.validator.addMethod('selectFriends', function(value, element)
{
    emailExternos();
    var emails = $('input[name="mails_externos"]');
    return emails.val() != '' || value != null && $(value).length > 0;

}, "*Selecione pelo menos uma amiga ou email externo.");

$(function()
{
    // Init
    loadMask();
    validatorFormCreate();
    loadMaskInput();
});

// Presente select mask
$(document.body).on('change', '.presente-selecao .combo select', function(e)
{
    e.preventDefault();
    $(this).prev('.mascara').find('.optName').text($(this).find('option:selected').html());
});

// Convite radio template mask
$(document.body).on('click', '.convite-templates li', function(e)
{
    e.preventDefault();
    $('.visuTplError label[for="template_id"]').html('');
    $('.convite-templates li input[type="radio"]').prop('checked', false).prev('.mascara').removeClass('active');
    $(this).find('input[type="radio"]').prop('checked', true).prev('.mascara').addClass('active');
});

// Visualiza o template do convite selecionado
$(document.body).on('click', 'a.visualizar-convite', function(e)
{
    e.preventDefault();

    var radioTemplate = $('.convite-templates li').find('input[type="radio"]:checked');
    var radioCkeck = radioTemplate.val();

    if(radioCkeck == null) {
        $('.visuTplError label[for="template_id"]').html('Por favor, selecione um template para visualizar');
    } else {
        $.magnificPopup.open({
            items: {
                src: baseUri + '/assets/email/zoom-'+radioTemplate.attr('data-href')
            },
            type: 'image'
        });
    }
});

$(document.body).on('click', 'a.visualizar-convite-escolhido', function(e)
{
    e.preventDefault();

    var template = $(this).attr('data-href');

    $.magnificPopup.open({
        items: {
            src: baseUri + '/assets/email/zoom-'+template
        },
        type: 'image'
    });
});

/* Select Friends */
$('#list-friends').multiSelect();

// Add email select list
$(document).on('click', 'a.addMail', function(e)
{
    e.preventDefault();
    $('.emailsInvalidos').hide();

    checkEmails();
    $('input[name="emails"]').val('');
});

// Remove email
$(document).on('click', '.ms-container .ms-selection li.ms-email-select', function(e)
{
    e.preventDefault();
    $(this).remove();
});

// Check e-mails e adiciona a lista
function checkEmails()
{
    var emails = document.getElementById("emails").value;
    var emailArray = emails.split(/[,; ]+/);
    var invEmails = "";
    for(i = 0; i <= (emailArray.length - 1); i++) {
        if(checkEmail(emailArray[i])) {
            $("<li class='ms-email-select ms-selected' data-type='email' data-id='"+emailArray[i]+"'><span>" + emailArray[i] + "</span></li>")
                .appendTo(".ms-container .ms-selection ul.ms-list");
        } else {
            invEmails += emailArray[i] + "\n";
        }
    }

    if(invEmails != false) {
        $('.emailsInvalidos').show();
        $('.emailsInvalidos p').html(invEmails.replace(/\s+/g, ", ").replace(/(^, )|(, $)/g, " "));
    }
}

// Regex que valida o e-mail
function checkEmail(email)
{
    var regExp = /\b[a-zA-Z0-9][a-zA-Z0-9.-]+[a-zA-Z0-9][@][a-zA-Z0-9][a-zA-Z0-9.-]+[a-zA-Z0-9][.](com|org|edu|net|co)[.]?[a-zA-Z]{0,2}\b/g;
    return regExp.test(email);
}

/* Lista eventos */
$(document.body).on('click', '.eventos .lista a.btnList', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/eventos/criados',
        'GET',
        '',
        loadingShowLista,
        loadindHideLista,
        successLista
    );
});

function loadingShowLista()
{
    var button = document.getElementsByClassName('btnList');
    $(button).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideLista(){}

function successLista(data)
{
    if(data.page != undefined)
    {
        var container = $('.eventos .lista-eventos');
        container.css('display', 'block').empty().html(data.page);
    }
}

$(document.body).on('click', '.eventos .lista-eventos .close', function(e)
{
    var container = $('.eventos .lista-eventos');
    var button = document.getElementsByClassName('btnList');

    container.css('display', 'none').empty().html('');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
});
/* Fim: Lista de eventos */

/* Criar o convite do evento e enviar */
$(document.body).on('submit', 'form#form-event-create', function(e)
{
    e.preventDefault();

    emailExternos();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/eventos/crie-seu-evento',
        'POST',
        params,
        loadingShowCreate,
        loadindHideCreate,
        successCreate
    );
});

function loadingShowCreate()
{
    var button = document.getElementsByClassName('enviar-convite');
    $(button).html('<span class="ico-enviar">&nbsp;</span>Enviando convite...');
    $(button).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideCreate()
{
    var button = document.getElementsByClassName('enviar-convite');
    $(button).html('<span class="ico-enviar">&nbsp;</span>Enviar convite');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
}

function successCreate(data)
{
    var form = $('form#form-event-create');

    if(data.status == 'true') {

        git_list_name = '';

        form.find('input[type="text"], textarea').val("");
        form.find('input[type="radio"]').prop('checked', false);
        form.find('input[type="radio"]').prop('checked', false).prev('.mascara').removeClass('active');

        showItensEdit(baseUri + '/meu-perfil/eventos/lista-amigas', successRefreshFriends);

        selectOptionList();
        msgEdit('Convite enviado com sucesso!', form);
    } else {
        msgEdit('Ocorreu um erro no envio dos convites!', form);
    }
}

// Recarrega a lista de amigas
function successRefreshFriends(data)
{
    if(data.page != undefined)
    {
        var container = $('.eventos .escolha-amigos');
        container.empty().html(data.page);
        $('#list-friends').multiSelect();
    }
}
/* Fim: Criar o convite do evento */

/* Criar lista de presentes para eventos */
$(document.body).on('click', '.eventos .presente-selecao button', function(e)
{
    e.preventDefault();

    $.magnificPopup.open({
        items: [{
            src : baseUri + '/meu-perfil/eventos/lista-de-presentes/criar',
            type : 'ajax'
        }],
        closeOnBgClick : false,
        callbacks: {
            ajaxContentAdded: function()
            {
                showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/criar/itens', successListProducts);
            }
        }
    });

});

/* Pagination */
$(document.body).on('click', '.lista-presentes .container-presentes-produtos ul.pagination a', function(e)
{
    e.preventDefault();

    CallMethod(
        $(this).attr('href'),
        'GET',
        '',
        loadingShowListProducts,
        loadindHideListProducts,
        successListProducts
    );
});

/* Filters prices */
$(document.body).on('submit', 'form#form-lista-presentes-filter', function(e)
{
    e.preventDefault();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/criar/itens',
        'GET',
        params,
        loadingShowFilters,
        loadindHideFilters,
        successListProducts
    );

});

/* Carrinho */
$(document.body).on('click', '.lista-presentes .container-presentes-produtos ul.itens-main li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/cart',
        'POST',
        {'prod' : $(this).attr('rel')},
        loadingShowEdit,
        loadindHideEdit,
        successCart
    )
});

$(document.body).on('click', '.lista-presentes .container-presentes-cart ul.itens-small li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/cart/remove',
        'POST',
        {'prod' : $(this).attr('rel'), 'action' : 'cart'},
        loadingShowEdit,
        loadindHideEdit,
        successCart
    )
});

/* Criar lista */
$(document.body).on('submit', 'form#form-lista-presentes-criar', function(e)
{
    e.preventDefault();

    if(!$(this).find('input[type="checkbox"]').is(':checked'))
    {
        msgEdit('Leia os termos e aceite para poder prosseguir!', $('form#form-lista-presentes-criar'));
        return false;
    }

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/corfimacao',
        'POST',
        $(this).serialize(),
        loadingShowListProducts,
        loadindHideListProducts,
        successConfirm
    );
});

//Confirmar Paginação
$(document.body).on('click', '.lista-presentes .container-presentes-produtos-confirm ul.pagination a', function(e)
{
    e.preventDefault();
    showItensEdit($(this).attr('href'), successItensCorfirm);

});

//Confirmar lista
$(document.body).on('submit', 'form#form-lista-presentes-criar-confirmacao', function(e)
{
    e.preventDefault();

    if('' == $(this).find('input[name="name"]').val())
    {
        msgEdit('Escolha um nome para essa lista!', $('form#form-lista-presentes-criar-confirmacao'));
        return false;
    }

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/save',
        'POST',
        $(this).serialize(),
        loadingShowListProducts,
        loadindHideListProducts,
        successCreateSave
    );
});

function loadingShowFilters()
{
    var submit = document.getElementsByClassName('bt-lista-presentes-filter');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideFilters()
{
    $(document.getElementsByClassName('bt-lista-presentes-filter')).css({color : '#867a42'}).prop({ disabled : false });
}

function loadingShowListProducts()
{
    var submit = document.getElementsByClassName('bt-salvar');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideListProducts()
{
    $(document.getElementsByClassName('bt-salvar')).css({color : '#867a42'}).prop({ disabled : false });
}

function successCart(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-cart');
        container.empty().html(data.page);
    }
}

function successListProducts(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-produtos');
        container.empty().html(data.page);
    }
}

function successConfirm(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/corfimacao/itens', successItensCorfirm);
        $('form#form-lista-presentes-criar-confirmacao').find('input[type="text"]').focus();
    }
    else
    {
        msgEdit('Escolha os presentes da sua lista!', $('form#form-lista-presentes-criar'));
    }
}

function successItensCorfirm(data)
{
    var container = $('.lista-presentes .container-presentes-produtos-confirm');

    if(data.page != undefined)
    {
        container.empty().html(data.page);
    }
    else
    {
        container.empty().html('Ocorreu um erro ao carregar o conteúdo!');
    }
}

function successCreateSave(data)
{
    if('true' == data.status)
    {
        git_list_name = data.list;
        showItensEdit(baseUri + '/meu-perfil/eventos/lista-presentes', successRefresh);
        $.magnificPopup.close();
    }
    else if(1 == data.error)
    {
        msgEdit('A lista está vazia, adicione os presentes em sua lista!', $('form#form-lista-presentes-criar-confirmacao'));
    }
    else
    {
        msgEdit('Ocorreu um erro, tente mais tarde!', $('form#form-lista-presentes-criar-confirmacao'));
    }
}

function successRefresh(data)
{
    if(data.page != undefined)
    {
        var container = $('.eventos .presente-selecao');
        container.empty().html(data.page);
        selectOptionList();
    }
}

function selectOptionList()
{
    $('select[name="list"]').find('option').each(function(i)
    {
        if($(this).val() == git_list_name)
        {
            $(this).prop('selected', true);
        }
    });

    loadMask();
}
/* Fim: Criar no lista de presentes */

/* Editar lista de eventos */

//Abre a lista para edicão
$(document.body).on('click', '.eventos .lista ul.listEventos li a.bt-editar-convite', function(e)
{
    e.preventDefault();

    CallMethod(
        $(this).attr('href'),
        'GET',
        '',
        loadingShowEditConvite,
        loadindHideEditConvite,
        successEditConvite
    );
});

function loadingShowEditConvite(){}

function loadindHideEditConvite()
{
    var container = $('.eventos .lista-eventos');
    var button = document.getElementsByClassName('btnList');

    container.css('display', 'none').empty().html('');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
}

function successEditConvite(data)
{
    if(data.page != undefined)
    {
        var container = $('.eventos .crie');
        container.empty().html(data.page);
        $('#list-friends').multiSelect();
        scrollToAnchorEnvents('edit-convite');
        loadMaskInput();
        validatorFormEdit();
        validationFormEditFriends();
    }
}

//Salva a edicão dos dados
$(document).on('submit', 'form#form-event-edit-dados', function(e)
{
    e.preventDefault();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/eventos/editar/info',
        'POST',
        params,
        loadingFormShowEditInfo,
        loadindFormHideEditInfo,
        successFormEditInfo
    );
});

function loadingFormShowEditInfo()
{
    var submit = document.getElementsByClassName('salvar-alteracoes-evento');
    $(submit).val('Salvando...');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHideEditInfo()
{
    var submit = document.getElementsByClassName('salvar-alteracoes-evento');
    $(submit).val('Salvar alterações');
    $(submit).css({color : '#867a42'}).prop({ disabled : false });
}

function successFormEditInfo(data)
{
    if(data.status == 'true') {
        msgEdit('Dados do evento atualizado com sucesso!', $('form#form-event-edit-dados'));
    } else {
        msgEdit('Ocorreu um erro, tente mais tarde!', $('form#form-event-edit-dados'));
    }
}

// Add novas amigas ou e-mails e reenvia o convite
$(document).on('submit', 'form#form-event-edit-friends', function(e)
{
    e.preventDefault();

    emailExternos();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/eventos/editar/convite',
        'POST',
        params,
        loadingFormShowEditConvites,
        loadindFormHideEditConvites,
        successFormEditConvites
    );
});

function loadingFormShowEditConvites()
{
    var button = document.getElementsByClassName('enviar-convite');
    $(button).html('<span class="ico-enviar">&nbsp;</span>Enviando convite...');
    $(button).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHideEditConvites()
{
    var button = document.getElementsByClassName('enviar-convite');
    $(button).html('<span class="ico-enviar">&nbsp;</span>Enviar convite');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
}

function successFormEditConvites(data)
{
    if(data.status != undefined) {
        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                        '<h6>Deseja continuar editando esse evento?</h6>' +
                        '<div class="container">' +
                            '<a class="bt-popup-eventos-sim" href="javascript:void(0)">Sim</a>' +
                            '<a class="bt-popup-eventos-nao" href="javascript:void(0)">Não</a>' +
                        '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
        showItensEdit(baseUri + '/meu-perfil/eventos/editar/lista-amigas/' + $('form#form-event-edit-friends').find('input[name="hash"]').val(), successRefreshFriends);
    }
    else
    {
        msgEdit('Ocorreu um erro, tente mais tarde!', $('form#form-event-edit-friends'));
    }
}
$(document.body).on('click', '.bt-popup-eventos-sim', function(e)
{
    e.preventDefault();

    $.magnificPopup.close();
});

$(document.body).on('click', '.bt-popup-eventos-nao', function(e)
{
    e.preventDefault();

    $.magnificPopup.close();

    showItensEdit(baseUri + '/meu-perfil/eventos', successCreateReload);
});

function successCreateReload(data)
{
    if(data.page != undefined)
    {
        var container = $('.eventos .crie');
        container.empty().html(data.page);
        $('#list-friends').multiSelect();
        selectOptionList();
        loadMaskInput();
        validatorFormCreate();
        scrollToAnchorEnvents('create-convite');
    }
}

//Editar lista de presentes escolhida
$(document.body).on('click', '.eventos .bt-edit-lista-presentes', function(e)
{
    e.preventDefault();

    $.magnificPopup.open({
        items: [{
            src : baseUri + '/meu-perfil/eventos/lista-de-presentes/editar?listascriadas=' + $(this).attr('rel'),
            type : 'ajax'
        }],
        closeOnBgClick : false,
        callbacks: {
            ajaxContentAdded: function()
            {
                showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/escolhidos', successItensEscolhidosEdit);
                showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/naoescolhidos', successItensNaoEscolhidosEdit);
            }
        }
    });
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-escolhidos ul.pagination a', function(e)
{
    e.preventDefault();
    showItensEdit($(this).attr('href'), successItensEscolhidosEdit);
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-nao-escolhidos ul.pagination a', function(e)
{
    e.preventDefault();
    showItensEdit($(this).attr('href'), successItensNaoEscolhidosEdit);
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-escolhidos ul li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar/escolhidos',
        'POST',
        {'prod' : $(this).attr('rel')},
        loadingShowEdit,
        loadindHideEdit,
        successItensRemoveEdit
    )
});

$(document.body).on('click', '.lista-presentes .container-presentes-itens-nao-escolhidos ul li button', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar/naoescolhidos',
        'POST',
        {'prod' : $(this).attr('rel')},
        loadingShowEdit,
        loadindHideEdit,
        successItensAddEdit
    )
});

$(document.body).on('submit', 'form#form-lista-presentes-editar', function(e)
{
    e.preventDefault();

    var params = $(this).serialize();

    CallMethod(
        baseUri + '/meu-perfil/lista-de-presentes/editar/save',
        'POST',
        params,
        loadingShowEdit,
        loadindHideEdit,
        successSaveEdit
    );
});

function loadingShowEdit() {}

function loadindHideEdit() {}

function successItensEscolhidosEdit(data) {
    if(data.page != undefined) {
        var container = $('.lista-presentes .container-presentes-itens-escolhidos');
        container.empty().html(data.page);
    }
}

function successItensRemoveEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-escolhidos');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/naoescolhidos', successItensNaoEscolhidosEdit);
    }
}

function successItensNaoEscolhidosEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-nao-escolhidos');
        container.empty().html(data.page);
    }
}

function successItensAddEdit(data)
{
    if(data.page != undefined)
    {
        var container = $('.lista-presentes .container-presentes-itens-nao-escolhidos');
        container.empty().html(data.page);
        showItensEdit(baseUri + '/meu-perfil/lista-de-presentes/editar/escolhidos', successItensEscolhidosEdit);
    }
}

function successSaveEdit(data)
{
    if(data.status == 'true')
    {
        msgEdit('A lista atualizada com sucesso!', $('form#form-lista-presentes-editar'));
    }
    else if(data.error == 2)
    {
        msgEdit('A lista precisa ter pelo um presente!', $('form#form-lista-presentes-editar'));
    }
    else
    {
        msgEdit('Ocorreu um erro, tente mais tarde!', $('form#form-lista-presentes-editar'));
    }
}
/* Fim: Editar lista de eventos */

/* Reenviar lista de convites */
$(document.body).on('click', '.listEventos .btn-reenviar-convites', function(e)
{
    e.preventDefault();

    CallMethod(
        baseUri + '/meu-perfil/eventos/reenviar-convite',
        'POST',
        {'hash' : $(this).attr('rel')},
        loadingShowReenviar(this),
        loadindHideReenviar,
        successReenviar
    );
});

function loadingShowReenviar(button)
{
    $(button).html('Reenviando convite...');
    $(button).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindHideReenviar()
{
    var button = document.getElementsByClassName('btn-reenviar-convites');
    $(button).html('Reenviar convite');
    $(button).css({color : '#867a42'}).prop({ disabled : false });
}

function successReenviar(data){}

/* Fim: Reenviar lista de convites */