<?php

namespace IEngravidei\Profile;

use IEngravidei\Entities\Profile;
use IEngravidei\Entities\ProfileFacebook;
use IEngravidei\Entities\ProfileActivated;
use IEngravidei\Entities\ProfileAdditional;
use IEngravidei\Entities\ProfilePublished;
use IEngravidei\Entities\ProfileFriendships;
use IEngravidei\Entities\Plans;
use IEngravidei\Entities\ProfilePlan;
use IEngravidei\Repositories\ProfileRepositoryInterface;

class DbProfileRepository implements ProfileRepositoryInterface
{
    public function __construct()
    {
        $this->profile = new Profile;
    }

    public function find($id)
    {
        return Profile::join('profiles_activated', 'profiles_activated.id_profile', '=', 'profiles.id')
            ->join('profiles_published', 'profiles_published.id_profile', '=', 'profiles.id')
            ->leftJoin('profiles_additional', 'profiles_additional.id_profile', '=', 'profiles.id')
            ->leftJoin('cities', 'cities.id', '=', 'profiles_additional.city_id')
            ->whereNull('profiles.deleted_at')
            ->where('profiles_activated.activated', '=', 1)
            ->whereNested(function ($query) use($id) {
                $query->where('profiles.id', '=', $id)
                    ->orWhere('profiles.public_id', '=', $id)
                    ->orWhere('profiles.email', '=', $id);
            })
            ->select(
                'profiles.id', 'profiles.name', 'profiles.email', 'profiles.public_id', 'profiles.profile_picture', 'profiles.layout',
                'profiles_additional.birth', 'profiles_additional.country_id', 'profiles_additional.state_id',
                'profiles_additional.city_id', 'cities.name as city', 'cities.state_name', 'profiles_additional.pregnancy_weeks',
                'profiles_additional.baby_name', 'profiles_additional.father_name'
            )->first();
    }

    public function findFacebook($uid)
    {
        return Profile::join('profiles_activated', 'profiles_activated.id_profile', '=', 'profiles.id')
            ->join('profiles_facebook', 'profiles_facebook.id_profile', '=', 'profiles.id')
            ->where('profiles.deleted_at', '=', NULL)
            ->where('profiles_facebook.id_facebook', '=', $uid)
            ->where('profiles_activated.activated', '=', 1)
            ->select('profiles.id', 'profiles.name', 'profiles.email', 'profiles.password', 'profiles.public_id')->first();
    }

    public function search($input)
    {
        return Profile::join('profiles_activated', 'profiles_activated.id_profile', '=', 'profiles.id')
            ->join('profiles_published', 'profiles_published.id_profile', '=', 'profiles.id')
            ->leftJoin('profiles_additional', 'profiles_additional.id_profile', '=', 'profiles.id')
            ->leftJoin('states', 'states.id', '=', 'profiles_additional.state_id')
            ->whereNull('profiles.deleted_at')
            ->where('profiles_activated.activated', '=', 1)
            ->where('profiles_published.published', '=', 1)
            ->whereNested(function ($query) use($input) {
                if(isset($input['q']) && !empty($input['q']))
                    $query->where('profiles.name', 'like', "%{$input['q']}%");

                if(isset($input['father_name']) && !empty($input['father_name']))
                    $query->where('profiles_additional.father_name', 'like', "%{$input['father_name']}%");

                if(isset($input['state_id']) && !empty($input['state_id']))
                    $query->where('states.id', 'like', "%{$input['state_id']}%");

            })
            ->orderBy('profiles.id')
            ->groupBy('profiles.id')
            ->select('profiles.*', 'states.name as state')
            ->paginate(12);
    }

    public function profileCreate($input)
    {
        $data = new \stdClass();

        /* Create profile */
        $data->profile = Profile::create(array(
            'email' => $input['email'],
            'name' => $input['name'],
            'password' => \Hash::make($input['password']),
            'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . $input['email']),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        /* Create Activated */
        $data->activated = ProfileActivated::create(array(
            'id_profile' => $data->profile->id,
            'activation_code' => sha1(microtime(). uniqid(rand(), true) . $input['email']),
            'activated_at' => '0000-00-00 00:00:00'
        ));

        /* Defined Published */
        ProfilePublished::create(array(
            'id_profile' => $data->profile->id,
            'published_at' => '0000-00-00 00:00:00'
        ));

        return $data;
    }

    public function profileUpdate($input)
    {
        $profile = Profile::find($input['id']);

        if (isset($input['name']) && !empty($input['name']))
            $profile->name = $input['name'];

        if (isset($input['profile_picture']) && !empty($input['profile_picture']))
            $profile->profile_picture = $input['profile_picture'];

        if (isset($input['public_id']) && !empty($input['public_id']))
            $profile->public_id = $input['public_id'];

        $profile->updated_at = date('Y-m-d H:i:s');
        $profile->save($input);

        $profileAdditional = ProfileAdditional::where('id_profile', '=', $input['id'])->first();

        if(empty($profileAdditional))
        {
            $profileAdditional = new ProfileAdditional;
            $profileAdditional->id_profile = $input['id'];
            $profileAdditional->created_at = date('Y-m-d H:i:s');
        }

        $profileAdditional->pregnancy_weeks = $input['pregnancy_weeks'];
        $profileAdditional->city_id = $input['city_id'];
        $profileAdditional->state_id = $input['state_id'];
        $profileAdditional->country_id = $input['country_id'];
        $profileAdditional->birth = implode("-",array_reverse(explode("/",$input['birth'])));
        $profileAdditional->updated_at = date('Y-m-d H:i:s');

        if (isset($input['father_name']))
            $profileAdditional->father_name = $input['father_name'];

        if (isset($input['baby_name']))
            $profileAdditional->baby_name = $input['baby_name'];

        return $profileAdditional->save($input);
    }

    public function profileCreateByFacebook($uid, $me, $access_token)
    {
        $this->profile = $this->profile->whereRaw('email = ?', array($me['email']))->first();

        if(!$this->profile)
        {
            $user = $this->profileCreate(array('email' => $me['email'],'name' => $me['name'],'password' => str_random(6)));
            $this->profile = $user->profile;
        }

        /* Set Active */
        ProfileActivated::where('id_profile', '=', $this->profile->id)->where('activated', '=', 0)
            ->update(array('activated' => 1, 'activated_at' => date('Y-m-d H:i:s')));

        /* Create facebook access */
        ProfileFacebook::create(array(
            'id_profile' => $this->profile->id,
            'id_facebook' => $uid,
            'username' => $me['username'],
            'email' => $me['email'],
            'access_token' => $access_token,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        return $this->profile;
    }

    public function profileUpdateFacebook($uid, $access_token)
    {
        return ProfileFacebook::where('id_facebook', '=', $uid)
            ->update(array('access_token' => $access_token, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function existsProfileAdditional($id)
    {
        return ProfileAdditional::where('id_profile', '=', $id)->exists();
    }

    public function findPlan($plan)
    {
        return Plans::whereNested(function ($query) use($plan)
            {
                $query->where('name', '=', $plan)->orWhere('id', '=', $plan);
            })->first();
    }

    public function getPlan($id)
    {
        return ProfilePlan::select('plans.name', 'plans_roles.album_limited', 'plans_roles.album_photo_limited', 'plans_roles.poll', 'plans_roles.professional_tips', 'plans_roles.discount_club', 'plans_roles.page_customization')
            ->join('plans', 'plans.id', '=', 'id_plan')
            ->join('plans_roles', 'plans_roles.plans_id', '=', 'id_plan')
            ->where('id_profile', '=', $id)
            ->first();
    }

    public function setPlan($id, $id_plan)
    {
        return ProfilePlan::create(array(
            'id_profile' => $id,
            'id_plan' => $id_plan,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ));
    }

    public function upPlan($id, $id_plan)
    {
        return ProfilePlan::where('id_profile', '=', $id)->update(array('id_plan' => $id_plan, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function setLayout($data)
    {
        $profile = Profile::find($data['id']);
        $profile->layout = $data['tpl'];
        $profile->updated_at = date('Y-m-d H:i:s');
        $profile->save($data);
    }

    public function setActive($code)
    {
        $activated = ProfileActivated::where('activation_code', '=', $code)
            ->where('activated', '=', 0)->update(array('activated' => 1, 'activated_at' => date('Y-m-d H:i:s')));

        if($activated)
            return Profile::join('profiles_activated', 'id_profile', '=', 'profiles.id')->where('activation_code', '=', $code)
                ->select('profiles.id','profiles.name','profiles.email','profiles.password')->first();
        else
            return false;
    }

    public function getActive($email)
    {
        return Profile::join('profiles_activated', 'id_profile', '=', 'profiles.id')
            ->where('email', '=', $email)->where('deleted_at', '=', NULL)->where('activated', '=', 1)->exists();
    }

    public function getPublished($id)
    {
        return ProfilePublished::where('id_profile', '=', $id)
            ->where('published', '=', 1)->exists();
    }

    public function setPublished($id)
    {
        return ProfilePublished::where('id_profile', '=', $id)
            ->update(array('published' => 1, 'published_at' => date('Y-m-d H:i:s')));
    }

    public function getPublicId($public_id)
    {
        return Profile::where('public_id', '=', $public_id)->exists();
    }

    public function setPublicId($id, $public_id)
    {
        return Profile::where('id', '=', $id)->update(array('public_id' => $public_id));
    }

    public function getAllFriends($id, $paginate = 12)
    {
        return ProfileFriendships::join('profiles', function ($join) {
                    $join->on('profiles.id', '=', 'profiles_friendships.from_user')->orOn('profiles.id', '=', 'profiles_friendships.to_user');
                })->where('profiles.id', '!=', $id)
                ->whereRaw('profiles_friendships.status = ? AND (profiles_friendships.from_user = ? OR profiles_friendships.to_user = ?)', array(1, $id, $id))
                ->select('profiles.id', 'profiles.name', 'profiles.email', 'profiles.public_id', 'profiles.profile_picture')
                ->orderBy('profiles.name')
                ->paginate($paginate);
    }

    public function getListAllFriends($id)
    {
        return ProfileFriendships::join('profiles', function ($join) {
                    $join->on('profiles.id', '=', 'profiles_friendships.from_user')->orOn('profiles.id', '=', 'profiles_friendships.to_user');
                })->where('profiles.id', '!=', $id)
                ->where('profiles_friendships.status', '=', 1 )
                ->whereRaw('(profiles_friendships.from_user = ? OR profiles_friendships.to_user = ?)', array($id, $id))
                ->orderBy('profiles.name')
                ->lists('name', 'email');
    }

    public function getListAllFriendsId($id)
    {
        return ProfileFriendships::join('profiles', function ($join) {
                    $join->on('profiles.id', '=', 'profiles_friendships.from_user') ->orOn('profiles.id', '=', 'profiles_friendships.to_user');
                })->where('profiles.id', '!=', $id)
                ->where('profiles_friendships.status', '=', 1 )
                ->whereRaw('(profiles_friendships.from_user = ? OR profiles_friendships.to_user = ?)', array($id, $id))
                ->orderBy('profiles.name')
                ->select('profiles.id')
                ->lists('id');
    }

    public function getAllSearchFriends($id, $search)
    {
        return ProfileFriendships::join('profiles', function ($join) {
                    $join->on('profiles.id', '=', 'profiles_friendships.from_user')->orOn('profiles.id', '=', 'profiles_friendships.to_user');
                })->where('profiles_friendships.status', '=', 1)
                ->whereRaw('profiles.name LIKE ? AND (profiles_friendships.from_user = ? OR profiles_friendships.to_user = ?)', array("%{$search}%", $id, $id))
                ->select('profiles.id', 'profiles.name', 'profiles.public_id', 'profiles.profile_picture')
                ->orderBy('profiles.name')
                ->distinct()
                ->get();
    }

    public function getFriend($from_user, $to_user)
    {
        return ProfileFriendships::where('status', '=', 1)
                ->whereNested(function ($query) use($from_user) {
                    $query->where('from_user', '=', $from_user)->orWhere('to_user', '=', $from_user);
                })->whereNested(function ($query) use($to_user) {
                    $query->where('to_user', '=', $to_user)->orWhere('from_user', '=', $to_user);
                })->exists();
    }

    public function getAllRequireFriend($to_user, $paginate = 6)
    {
        return ProfileFriendships::join('profiles', 'profiles.id', '=', 'profiles_friendships.from_user')
                ->where('profiles_friendships.status', '=', 0)
                ->where('profiles_friendships.to_user', '=', $to_user)
                ->select('profiles.id', 'profiles.name', 'profiles.public_id', 'profiles.profile_picture')
                ->orderBy('profiles.name')
                ->paginate($paginate);
    }

    public function getRequireFriend($from_user, $to_user)
    {
        return ProfileFriendships::whereNested(function ($query) use($from_user, $to_user) {
                    $query->where('from_user', '=', $from_user);
                    $query->where('to_user', '=', $to_user);
                })->where('status', '=', 0)->exists();
    }

    public function getCountRequire($from_user)
    {
        return ProfileFriendships::where('profiles_friendships.to_user', '=', $from_user)
                ->where('profiles_friendships.status', '=', 0)
                ->distinct()
                ->count();
    }

    public function updateRequireFriend($from_user, $to_user, $status)
    {
        return ProfileFriendships::whereNested(function ($query) use($from_user) {
                    $query->where('from_user', '=', $from_user)->orWhere('to_user', '=', $from_user);
                })->whereNested(function ($query) use($to_user) {
                    $query->where('to_user', '=', $to_user)->orWhere('from_user', '=', $to_user);
                })->update(array('status' => $status, 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function requireFriend($from_user, $to_user)
    {
        return ProfileFriendships::create(array(
            'from_user' => $from_user,
            'to_user' => $to_user,
            'status' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));
    }

    public function excluirProfile($id) {
        return Profile::where('id', '=', $id)->update(array('deleted_at' => date('Y-m-d H:i:s')));
    }

    public $messages = array(
        'name.required'  => 'Digite Seu Nome',
        'email.required'    => 'Digite o E-mail',
        'email.email'    => 'E-mail inválido',
        'email.max'     => 'E-mail deve ter no máximo 200 caracteres',
        'email.unique'  => 'E-mail já cadastrado',
        'password.required' => 'Digite sua senha',
        'password.between' => 'Senha deve possuir no minimo 5 e no máximo 16 caracteres',
        'password.confirmed'    => 'Senhas não conferem',
        'picture.required' => 'A imagem de perfil é obrigatória',
        'picture.required_if' => 'Você precisa selecionar uma imagem',
        'picture.image' => 'O arquivo enviado não é uma imagem',
        'picture.max' => 'O arquivo deve ser menor que :max',
        'picture.mimes' => 'A imagem de perfil deve ser do tipo JPG',
        'public_id.required' => 'O nome de usuário é obrigatório',
        'public_id.unique' => 'O nome de usuário escolhido já está sendo utilizado',
        'public_id.alpha_num' => 'O nome de usuário deve conter apenas letras e números',
        'public_id.max' => 'O nome do usuário deve conter no máximo :max caracteres',
        'pregnancy_weeks.required' => 'Semanas de gravidez é obrigatório',
        'pregnancy_weeks.numeric' => 'Digite apenas números para informar as semanas de gravidez',
        'country_id.required' => 'O campo país é obrigatório',
        'country_id.integer' => 'O campo país é inválido',
        'state_id.required' => 'O campo estado é obrigatório',
        'state_id.integer' => 'O campo estado é inválido',
        'city_id.required' => 'O campo cidade é obrigatório',
        'city_id.integer' => 'O campo cidade é inválido',
        'birth.required' => 'A data de nascimento é obrigatória',
        'birth.date_format' => 'O formato da data de nascimento é inválida',
        'father_name.max' => 'O nome do pai deve conter no máximo :max caracteres',
        'baby_name.max' => 'O nome do bebê deve conter no máximo :max caracteres',
        'layout.required' => 'Escolha um dos layouts para continuar',
        'layout.in' => 'Opção de layout inválida',
    );
}