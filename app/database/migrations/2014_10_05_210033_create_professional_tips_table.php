<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionalTipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('professional_tips', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->text('body');
            $table->text('picture');
            $table->text('youtube');
            $table->string('author');
            $table->tinyInteger('type');
            $table->dateTime('pub_inc');
            $table->tinyInteger('status');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('professional_tips');
	}

}
