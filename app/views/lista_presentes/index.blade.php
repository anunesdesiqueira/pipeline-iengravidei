@extends('layouts.master')

@section('content')
    <div class="holder">
        @include('components.cabecalho.header_active')
        <h2 name="lista-de-presentes" class="lista-presentes-title" title="Lista de Presentes"><span>Lista de Presentes</span></h2>
        <div class="lista-presentes">
            @if(count($lists) > 0)
                @include('components.lista_presentes.index')
            @else
                <div class="container-presentes">
                    @include('lista_presentes.create')
                </div>
            @endif
        </div>
    </div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/lista-presentes.js') }}
    {{ HTML::script('assets/js/enquete-logged.js') }}
    @if(count($lists) < 1)
        {{ HTML::script('assets/js/lista-presentes-load.js') }}
    @endif
@stop