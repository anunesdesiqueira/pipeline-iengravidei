<?php

use Illuminate\Database\Migrations\Migration;

class AddColumnProfileIdOnEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('events', function($table)
        {
            $table->bigInteger('profile_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}