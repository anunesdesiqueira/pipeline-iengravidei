<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\VisitantesRepositoryInterface as Visitantes;

class VisitantesController extends BaseController {

	public function __construct(Profile $profile, Visitantes $visitantes)
	{
        parent::__construct();
        $this->profile = $profile;
        $this->visitantes = $visitantes;
	}

    public function index()
    {
        $this->visitantes->read($this->user->id);
        $solicitacoes = $this->visitantes->solicitacoes($this->user->id);

        if(Request::ajax()) {
            $page = View::make('components.visitantes.listar', compact('solicitacoes'))->render();
            return Response::json(array('page' => $page));
        } else {
            return View::make('visitantes.index', compact('solicitacoes'));
        }
    }

    public function solicitar()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'visitantes_nome'   => 'required',
                'visitantes_sobrenome'   => 'required',
                'visitantes_email'  => 'required|email|max:155',
                'publicId' => 'required',
            ), array(
                'visitantes_nome.required' => 'O nome não pode estar vazio.',
                'visitantes_sobrenome.required' => 'O sobrenome não pode estar vazio.',
                'visitantes_email.required' => 'O email não pode estar vazio.',
                'publicId.required' => 'Ocorreu um erro',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $public = $this->profile->find($data['publicId']);
                $data['id'] = $public->id;

                $solicitacao = $this->visitantes->add($data);

                if($solicitacao)
                    return Response::json(array('status' => 'true'));
            }
        }
    }

    public function deletar()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'hash'   => 'required',
            ), array(
                'hash.required' => 'Ocorreu um erro.',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $deletar = $this->visitantes->deletar($this->user->id, $data['hash']);

                if($deletar)
                    return Response::json(array('status' => 'true'));
            }
        }
    }

    public function enviarConvite()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'hash'   => 'required',
            ), array(
                'hash.required' => 'Ocorreu um erro.',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $visitante = $this->visitantes->getVisitante($this->user->id, $data['hash']);
                if($visitante) {
                    $this->sendVisitorEmail($visitante);
                    $update = $this->visitantes->update($this->user->id, $data['hash']);

                    if($update)
                        return Response::json(array('status' => 'true'));
                }
            }
        }
    }

    private function sendVisitorEmail($data)
    {
        $visitante = array(
            'publicId' => $this->user->public_id,
            'nome' => $data->nome,
            'email' => $data->email,
            'hash' => $data->hash,
        );

        $config = Config::get('iengravidei.mail');

        Mail::send('emails.visitante', $visitante, function($message) use ($visitante, $config) {
            $message->to($visitante['email'], $visitante['nome'])->subject($config['subject']['visitante']);
        });
    }
}