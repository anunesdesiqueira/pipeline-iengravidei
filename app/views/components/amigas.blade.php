@if(count($showFriendsMoms) > 0)
    @foreach($showFriendsMoms as $mom)
        <li>
            <div class="thumb">
                <a href="{{ route('user.public', array('publicId' => $mom->public_id)) }}" title="{{ $mom->name }}">
                    <img src="{{ $mom->profile_picture['small'] }}" />
                </a>
            </div>
            <div class="description">
                <h4>{{ $mom->name }} <span>{{ date('d/m à\\s h:i a', strtotime($mom->created_at)) }}</span></h4>
                {{ $mom->message }}
            </div>
        </li>
    @endforeach
@endif