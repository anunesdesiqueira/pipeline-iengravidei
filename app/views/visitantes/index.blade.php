@extends('layouts.master')

@section('content')
<div class="holder">
    @include('components.cabecalho.header_active')
    <div class="visitantes">
        @include('components.visitantes.listar')
    </div>
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/visitantes.js') }}
    {{ HTML::script('assets/js/enquete-logged.js') }}
@stop