<?php

use IEngravidei\Repositories\MuralRepositoryInterface as Mural;
use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;

class MuralController extends BaseController
{
	public function __construct(Mural $mural, Profile $profile)
	{
        parent::__construct();

        $this->mural = $mural;
        $this->profile = $profile;
	}

    public function listStatus()
    {
        $number = Input::get('number');
        $offset = Input::get('offset');
        $filterAll = Input::get('buscar-todas-mamaes', null);
        $filterFriends = Input::get('buscar-amigas', null);

        $idFriends = $this->profile->getListAllFriendsId($this->user->id);

        $showAllMoms = $this->mural->showAllMoms(array_merge($idFriends, array($this->user->id)), $number, $offset, $filterAll);
        if(count($showAllMoms) > 0) {
            $showAllMoms = $this->createPathImagesProfile($showAllMoms);
            $showAllMoms = $this->verifyOptions($showAllMoms);
        }

        if(count($idFriends) > 0) {
            $showFriendsMoms = $this->mural->showFriendsMoms($idFriends, $number, $offset, $filterFriends);
            $showFriendsMoms = $this->createPathImagesProfile($showFriendsMoms);
        } else {
            $showFriendsMoms = array();
        }

        $allMoms = View::make('components.todas_mamaes', compact('showAllMoms'))->render();
        $friends = View::make('components.amigas', compact('showFriendsMoms'))->render();

        return Response::json(array('pageAllMoms' => $allMoms, 'pageFriends' => $friends));
    }

    public function send()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'shared'   => 'required'
            ), array(
                'shared.required' => 'Escolha umas das opções: "Todas as mamães" ou " Amigas"'
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $data['id'] = $this->user->id;

                if(isset($data['imagem']) && $data['imagem']) {
                    $data['imagem'] = $this->uploadMuralPhoto($data['imagem'], $data['id']);
                    $data['imagem'] = $this->createPathImageMural($data['imagem']);
                }

                $postStatus = $this->mural->createStatus($data);

                if($postStatus) {
                    $idFriends = $this->profile->getListAllFriendsId($data['id']);

                    $showAllMoms = $this->mural->showAllMoms(array_merge($idFriends, array($this->user->id)), 10, 0);
                    if(count($showAllMoms) > 0) {
                        $showAllMoms = $this->createPathImagesProfile($showAllMoms);
                        $showAllMoms = $this->verifyOptions($showAllMoms);
                    }

                    if(count($idFriends) > 0) {
                        $showFriendsMoms = $this->mural->showFriendsMoms($idFriends, 10, 0);
                        $showFriendsMoms = $this->createPathImagesProfile($showFriendsMoms);
                        $showFriendsMoms = $this->verifyOptions($showFriendsMoms);
                    } else {
                        $showFriendsMoms = array();
                    }

                    $allMoms = View::make('components.todas_mamaes', compact('showAllMoms'))->render();
                    $friends = View::make('components.amigas', compact('showFriendsMoms'))->render();

                    return Response::json(array('pageAllMoms' => $allMoms, 'pageFriends' => $friends));
                }
            }
        }
    }

    public function deleted()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'hash'   => 'required'
            ), array(
                'hash.required' => 'Hash não pode estar vazio'
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $deleted = $this->mural->momDeleteStatus($this->user->id, $data['hash']);
                if($deleted) {
                    return Response::json(array('status' => 'true'));
                } else {
                    return Response::json(array('status' => 'false'));
                }
            }
        }
    }

    private function verifyOptions($moms)
    {
        foreach($moms as $k => $mom) {
            if($mom->id == $this->user->id) {
                $moms[$k]->options = true;
            } else {
                $moms[$k]->options = false;
            }
        }
        return $moms;
    }

    public function listaStatusPublic($publicId)
    {
        $number = Input::get('number');
        $offset = Input::get('offset');

        $profile = $this->profile->find($publicId);
        $momPublic = $this->mural->showFriendsMoms(array($profile->id), $number, $offset);
        if(count($momPublic) > 0) {
            $momPublic = $this->createPathImagesProfile($momPublic);
        }

        $page = View::make('components.mural.mamae', compact('momPublic'))->render();
        return Response::json(array('page' => $page));
    }
}