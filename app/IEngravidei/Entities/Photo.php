<?php  namespace IEngravidei\Entities;

class Photo extends \Eloquent
{

    protected $guarded = array('id');
    public function validate($input)
    {
        $rules = array(
            'filename' => 'required',
        );

        $messages = array(
            'filename.required' => 'Nome do arquivo deve ser espeficiado'
        );

        return \Validator::make($input, $rules, $messages);
    }
}