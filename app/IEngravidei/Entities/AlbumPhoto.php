<?php namespace IEngravidei\Entities;


class AlbumPhoto extends \Eloquent
{
    protected $table = 'album_photo';
    protected $guarded = array();

    public $timestamps = false;
}