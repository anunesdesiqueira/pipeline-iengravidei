<?php
class ProfilesTableSeeder extends Seeder {

    public function run()
    {


        $profiles = array(

            array(
                'email'               => 'usuario@bg7.com.br',
                'name'                => 'Usuario BG7',
                'baby_name'           => 'Maria Joaquina',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'usuario@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'usuario@bg7.com.br'),
                'password'            => Hash::make('bg7@2013'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'usuario@bg7.com.br'),
                'public_id'           => 'maria.joaquina',

            ),

            array(
                'email'               => 'eduardo.alencar@bg7.com.br',
                'name'                => 'Eduardo Alencar de Oliveira',
                'baby_name'           => 'Eduardo Alencar de Oliveira Jr',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'eduardo.alencar@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'eduardo.alencar@bg7.com.br'),
                'password'            => Hash::make('eduardo10'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'eduardo.alencar@bg7.com.br'),
                'public_id'           => 'eduardo.alencar',
            ),

            array(
                'email'               => 'tiago.salomao@bg7.com.br',
                'name'                => 'Thiago Salomão',
                'baby_name'           => 'Cirillo',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'tiago.salomao@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'tiago.salomao@bg7.com.br'),
                'password'            => Hash::make('mudar123'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'tiago.salomao@bg7.com.br'),
                'public_id'           => 'tiago.salomao',

            ),

            array(
                'email'               => 'maria@bg7.com.br',
                'name'                => 'Maria Joaquina',
                'baby_name'           => 'Chaves',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'maria@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'maria@bg7.com.br'),
                'password'            => Hash::make('maria123'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'maria@bg7.com.br'),
                'public_id'           => 'maria2',

            ),

            array(
                'email'               => 'kika@bg7.com.br',
                'name'                => 'Kika Nunes',
                'baby_name'           => 'Capitolina',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'kika@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'kika@bg7.com.br'),
                'password'            => Hash::make('kika123'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'kika@bg7.com.br'),
                'public_id'           => 'kika.nunes',
            ),

            array(
                'email'               => 'giselle.pinho@bg7.com.br',
                'name'                => 'Giselle Pinho',
                'baby_name'           => 'Fernanda',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'giselle.pinho@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'giselle.pinho@bg7.com.br'),
                'password'            => Hash::make('giselle123'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'giselle.pinho@bg7.com.br'),
                'public_id'           => 'giselle.pinho',
            ),

            array(
                'email'               => 'bianca.lopes@bg7.com.br',
                'name'                => 'Bianca Lopes',
                'baby_name'           => 'Julia',
                'activated'           => 1,
                'activation_code'     => sha1(microtime(). uniqid(rand(), true) . 'bianca.lopes@bg7.com.br'),
                'reset_password_code' => sha1(microtime(). uniqid(rand(), true) . 'bianca.lopes@bg7.com.br'),
                'password'            => Hash::make('bianca123'),
                'plan_id'             => 1,
                'hash'                => sha1(microtime(). uniqid(rand(), true) . time(). 'bianca.lopes@bg7.com.br'),
                'public_id'           => 'bianca.lopes',
            ),

        );
        //DB::table('profiles')->truncate();
        DB::table('profiles')->insert($profiles);
    }

}

