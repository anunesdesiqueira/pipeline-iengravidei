<?php namespace Bg7\Eventme\Model;

class Event extends \Eloquent {

    protected $table = 'events';

    /**
     * @param $input
     * @return mixed
     */
    public function validate($input)
    {
        $rules = array(
          'name' => 'required|min:3',
           'profile_id' => 'required|integer'
        );

        $messages = array(
          'name.required' => 'Digite o nome do email',
          'name.min' => 'O nome do evento deve possuir no minimo 3 caracteres',
          'profile_id.required' => 'O evento deve pertencer a um usuário',
          'profile_id.integer' => 'Id do usuário inválido'
        );

        return \Validator::make($input, $rules, $messages);
    }

}