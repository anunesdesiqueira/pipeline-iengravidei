<?php

namespace IEngravidei\Repositories;

interface VisitantesRepositoryInterface
{
    public function add($data);
    public function count($id_profile);
    public function read($id_profile);
    public function solicitacoes($id_profile, $paginate = 5);
    public function deletar($id_profile, $hash);
    public function update($id_profile, $hash);
}