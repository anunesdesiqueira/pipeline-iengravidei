<!-- Só aparece na HOME -->
<section class="home">
    <div class="holder">
        <h1><span>iengravidei</span></h1>

        {{ Form::open(array('route' => 'register.store', 'id' => 'registering-profile')) }}
        <div class="box-cadastro">
            <ul>
                <li class="facebook">Crie um novo login ou <a href="{{ URL::to('facebook') }}">Facebook</a></li>
                <li>
                    <label>Seu Nome*:
                        {{ Form::text('name',Input::old('name'), array('style' => 'width:370px')) }}
                    </label>
                </li>
                <li>
                    <label>E-mail*:
                        {{ Form::text('email',Input::old('email'), array('style' => 'width:399px')) }}
                    </label>
                </li>
                <li>
                    <label>Senha*:
                        <input type="password" name="password" id="password" style="width:107px" />
                    </label>
                    <label style="padding-left:8px;">Confirmar Senha*:
                        <input type="password" name="password_confirmation" style="width:107px" />
                     </label>
                </li>
                <li><p>* CAMPO OBRIGATÓRIO</p></li>
                <li><input type="submit" value="CADASTRAR" /></li>
            </ul>
        </div>
        {{ Form::close() }}
        <a href="#" class="veja-mais">Veja mais</a>
    </div>
</section>
<!-- End: Só aparece na HOME -->