<?php namespace Bg7\Notifiqueme;

use Illuminate\Support\ServiceProvider;

class NotifiquemeServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('bg7/notifiqueme');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
        $this->app['notifiqueme'] = $this->app->share(function($app)
        {
           return new Notifiqueme;
        });

        $this->app->booting(function()
        {

            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Notifiqueme', 'Bg7\Notifiqueme\Facades\Notifiqueme');

        });

        $this->app->bind(
            'Bg7\Notifiqueme\Repositories\NotificationRepository',
            'Bg7\Notifiqueme\Repositories\EloquentNotificationRepository'

        );
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('notifiqueme');
	}

}