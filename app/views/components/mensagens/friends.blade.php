@if(count($friends) > 0)
    <ul>
        @foreach($friends as $friend)
            <li>
                <a href="{{ $friend['slug'] }}">
                    <div class="thumb"><img src="{{ $friend['profile_picture']['small'] }}" /></div>
                    <div class="description">
                        <h3>{{ $friend['name'] }}</h3>
                        <p>{{ $friend['msg'] }}</p>
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
@endif
