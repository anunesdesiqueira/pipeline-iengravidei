// Global vars
var number = 10; // mural limit
var offset = 0; // mural offset
var busy = false;

$(window).load(function () {
    loadindGetData();
});

$(window).scroll(function() {
    if(($(document).height() - $(window).height() - $(document).scrollTop()) < 440 && !busy) {
        busy = true;
        loadindGetData();
    }
});

function loadindGetData() {
    busy = true;
    CallMethod(baseUri + '/'+ publicId +'/mural','GET',{number : number, offset : offset},'','',successMural);
    CallMethod(baseUri + '/'+ publicId +'/recados','GET',{number : number, offset : offset},'','',successRecados);
    setOffset();
}

function successMural(data) {
    var mamae = $('.mural-nao-logado ul');
    mamae.append(data.page);
    readMore();
}

function successRecados(data) {
    var recados = $('.recados-especiais ul.listaRecadosEspeciais');
    recados.append(data.page);
}

function setOffset() {
    busy = false;
    offset = offset+number;
}

$(document.body).on('click', '.cabecalho a.enviarMsg', function(e) {
    e.preventDefault();
    var container = $('.mensagem-amiga');
    container.css('display', 'block');

});

$(document.body).on('click', '.mensagem-amiga .close', function(e) {
    e.preventDefault();
    var container = $('.mensagem-amiga');
    container.css('display', 'none');
});

$(document.body).on('submit', 'form#form-mensagem-amiga', function(e) {
    e.preventDefault();

    var params = $(this).serialize();
    CallMethod(baseUri + '/meu-perfil/mensagens/enviar','POST',params,loadingFormShowMensagens,loadindFormHideMensagens,successMensagens);
});

function loadingFormShowMensagens() {
    var submit = document.getElementsByClassName('bt_enviar_mensagem');
    $(submit).val('Enviando...');
    $(submit).css({color : '#ded5ab'}).prop({ disabled : true });
}

function loadindFormHideMensagens() {
    var submit = document.getElementsByClassName('bt_enviar_mensagem');
    $(submit).val('Enviar');
    $(submit).css({color : '#867a42'}).prop({ disabled : false });
}

function successMensagens(data) {
    if('true' === data.status) {
        $( 'form#form-mensagem-amiga' ).each(function(){
            this.reset();
        });

        var container = $('.mensagem-amiga');
        container.css('display', 'none');

        $.magnificPopup.open({
            items: [{
                src:'<div class="popup-presentes-confirm">' +
                    '<h6>Mensagem enviada com sucesso.</h6>' +
                    '<div class="container" style="width: 73px;">' +
                    '<a class="bt-popup-mensagem" href="javascript:void(0)">Fechar</a>' +
                    '</div>' +
                    '</div>',
                type: 'inline'
            }],
            closeOnBgClick : false,
            showCloseBtn : false
        });
    }
}

$(document.body).on('click', '.bt-popup-mensagem', function(e) {
    e.preventDefault();
    $.magnificPopup.close();
});