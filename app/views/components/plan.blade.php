<div class="holder">
    <div class="escolha-plano">
        <div class="plano-info">
            <h2 name="escolha-um-plano">Escolha seu Plano!</h2>
            <p>Durante os 6 primeiros meses quem fizer<br>o cadastro Grátis automaticamente entra com o <br>Perfil Premium e se beneficia de todas<br>as funcionalidades oferecidas.</p>
        </div>
        <div class="faixa-promo"></div>
        <ul>
            <li>
                <h3 class="free">100% Grátis</h3>
                <ul>
                    <li><span class="tickFree">&nbsp;</span>Perfil Individual</li>
                    <li><span class="tickFree">&nbsp;</span>Baby Line</li>
                    <li><span class="tickFree">&nbsp;</span>Mural</li>
                    <li><span class="tickFree">&nbsp;</span>Lista de Presentes</li>
                    <li><span class="tickFree">&nbsp;</span>Criador de Convites e Eventos</li>
                    <li><span class="tickFree">&nbsp;</span>Álbum de Fotos (3 álbuns)</li>
                </ul>
            </li>
            <li>
                <h3 class="premium"><span>Apenas</span>R$14<span class="cents">,90</span><span class="mes">/mês</span></h3>
                <ul>
                    <li><span class="tickPremium">&nbsp;</span>Perfil Individual</li>
                    <li><span class="tickPremium">&nbsp;</span>Baby Line</li>
                    <li><span class="tickPremium">&nbsp;</span>Mural</li>
                    <li><span class="tickPremium">&nbsp;</span>Lista de Presentes</li>
                    <li><span class="tickPremium">&nbsp;</span>Criador de Convites e Eventos</li>
                    <li><span class="tickPremium">&nbsp;</span>Álbum de Fotos (12 álbuns)</li>
                    <li><span class="tickPremium">&nbsp;</span>Enquete</li>
                    <li><span class="tickPremium">&nbsp;</span>Dicas de Profissionais</li>
                    <li><span class="tickPremium">&nbsp;</span>Personalização da Página</li>
                </ul>
            </li>
        </ul>
        <div class="bt-assine-ja">
            <a href="{{ route('plan.register', array('plan' => 'Premium-Free')) }}">Assine Já!</a>
        </div>
    </div>
</div>