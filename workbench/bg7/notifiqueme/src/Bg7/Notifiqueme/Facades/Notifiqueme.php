<?php namespace Bg7\Notifiqueme\Facades;

/**
 * Class Notifiqueme
 * @package Bg7\Notifiqueme\Facades
 */
use Illuminate\Support\Facades\Facade;

class Notifiqueme extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'notifiqueme';
    }
}