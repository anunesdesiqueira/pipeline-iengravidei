<div class="dicas-profissionais">
    <h2 title="Dicas Profissionais"><span>Dicas Profissionais</span></h2>
    <p>Dicas sobre a gestação, cuidados e melhores práticas</p>

    <div class="busca">
        {{ Form::open( array('id' => 'form-dicas') ) }}
            <input name="search" type="text" />
            <input type="submit" />
        {{ Form::close() }}
    </div>

    <ul>
        @foreach ($dicas as $k => $dica)
            <li {{{ 1 === $dica->type ? 'class=produto' : '' }}} >
                @if(!empty($dica->picture))
                    <img src="{{ asset($dica->picture) }}" alt="{{ $dica->title }}" />
                @endif
                <h3>{{ $dica->title }}</h3>
                {{ $dica->body }}
                @if(!empty($dica->youtube))
                    {{ $dica->youtube }}
                @endif
                <span>postado por: {{ $dica->author }}</span>
            </li>
        @endforeach

        @if(!Auth::client()->get()->plan_roles['professional_tips'])
            <li class="ativacao">
                <p>Quer ler todas as dicas dos profissionais?</p>
                <a href="#">Ative Agora</a>
            </li>
        @endif
    </ul>
</div>