@extends('layouts.master')

@section('content')

<div class="holder">
    @include('components.cabecalho.header_active')

    {{ Form::open() }}
	<div class="customizacao">
		<h2 name="personalizar">Customização de Tela</h2>
		<p>Aqui você escolhe o fundo de tela que tem mais a ver com você e seu futuro bebê. Cores para meninos, meninas ou até a cor do time de coração do papai. Todos os seus amigos verão a página com o seu jeito.</p>
		<h3>Escolha um dos templates abaixo:</h3>
		<ul>
            <li>
                <label>
                    <div class="thumb0"></div>
                    {{ Form::radio('tpl', '0', $layout['layout'] == 0 ? true : false) }}
                </label>
            </li>
            <li>
				<label>
					<div class="thumb1"></div>
					{{ Form::radio('tpl', '1', $layout['layout'] == 1 ? true : false) }}
				</label>
			</li>
			<li>
				<label>
					<div class="thumb2"></div>
					{{ Form::radio('tpl', '2', $layout['layout'] == 2 ? true : false) }}
				</label>
			</li>
			<li>
				<label>
					<div class="thumb3"></div>
					{{ Form::radio('tpl', '3', $layout['layout'] == 3 ? true : false) }}
				</label>
			</li>
            {{--
			<li class="custom">
				<label>
					<div class="thumb3"></div>
					{{ Form::radio('layout', 'custom', ($layout == 'custom')) }}			
				</label>
				<div class="foto-user">
					<h4>ESCOLHA SUA FOTO</h4>							
					<div class="uploadHolder">
						<div class="fileName"></div>
						<div class="mascara"></div>
						{{ Form::file('picture') }}
					</div>
				</div>
			</li>
            --}}
		</ul>
		
		<div class="botoes">
			<button>Visualizar</button>
			<input type="submit" value="Habilitar" />
		</div>
		
	</div>
    {{ Form::close(); }}
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/personalize.js') }}
    {{ HTML::script('assets/js/enquete-logged.js') }}
@stop