<?php namespace IEngravidei\Repositories;

interface DenunciationRepositoryInterface
{
    public function find($id);
    public function all();
    public function create($input);
    public function destroy($id);

}