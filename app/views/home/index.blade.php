@extends('layouts.master')

@section('cadastro')
    @include('components.register')
@stop

@section('content')
<div class="holder home">
    <h2>Bem-vinda à 1 Rede Social para Gestantes do mundo!</h2>
    <p class="intro">Somos uma comunidade apaixonada pela maternidade. Pelo indescritível e sublime estado em que ficamos quando<br/>estamos esperando nosso filho ou nossa filha! Compartilhe, emocione-se, converse, aprenda, ensine e viva<br/>intensamente esse momento! Estamos juntos com você para a contagem regressiva.</p>
    <div class="divisor top"></div>
    <div class="col1">
        <h3>Em destaque<span>O que oferecemos</span></h3>
        <div class="box">
            <a href="{{ route('services') }}" title="Lista de Presentes"><div class="thumb"></div></a>
            <div class="description">
                <h4><a href="{{ route('services') }}" title="Lista de Presentes">Lista de Presentes</a></h4>
                <p>Crie sua lista de presentes e receba tudo sem sair do site! Facilite a sua vida e de seus amigos!</p>
            </div>
        </div>
        <div class="box presentes">
            <a href="{{ route('services') }}" title="Baby Line"><div class="thumb"></div></a>
            <div class="description">
                <h4><a href="{{ route('services') }}" title="Baby Line">Baby Line</a></h4>
                <p>Uma linha para postar uma foto por semana e ver o desenvolvimento da barriga. Muito legal!</p>
            </div>
        </div>
        <h3 class="lista">Outros serviços<span>Lista completa</span></h3>
        <ul class="lista-completa">
            <li class="ico1"><a class="tooltip" href="{{ route('services') }}" title="Meu Mural">Meu Mural</a></li>
            <li class="ico2"><a class="tooltip" href="{{ route('services') }}" title="Lista de Presentes">Lista de Presentes</a></li>
            <li class="ico3"><a class="tooltip" href="{{ route('services') }}" title="Clube de Descontos">Clube de Descontos</a></li>
            <li class="ico4"><a class="tooltip" href="{{ route('services') }}" title="Baby Line">Baby Line</a></li>
            <li class="ico5"><a class="tooltip" href="{{ route('services') }}" title="Dicas Profissionais">Dicas Profissionais</a></li>
            <li class="ico6"><a class="tooltip" href="{{ route('services') }}" title="Enquete">Enquete</a></li>
            <li class="ico7"><a class="tooltip" href="{{ route('services') }}" title="Recados Especiais">Recados Especiais</a></li>
            <li class="ico8"><a class="tooltip" href="{{ route('services') }}" title="Personalização de Convites">Personalização de Convites</a></li>
            <li class="ico9"><a class="tooltip" href="{{ route('services') }}" title="Álbum de Fotos">Álbum de Fotos</a></li>
            <li class="ico10"><a class="tooltip" href="{{ route('services') }}" title="Criador de Eventos">Criador de Eventos</a></li>
        </ul>
    </div>
    <div class="col2">
        <a class="image-popup-pagina" href="{{ asset('assets/img/zoom-big.jpg') }}">
            <img src="{{ asset('assets/img/zoom-small.jpg') }}" />
            <p>Veja como ficará sua página</p>
        </a>
    </div>
    <div class="divisor botton"></div>
    @include('components.cadastro')
</div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/home.js') }}
    @if(Session::has('flashMessage'))
        HTML::entities('
            <script>
                $(function () {
                    $.magnificPopup.open({
                        items: [{
                            src:'<div class="popup-presentes-confirm" style="max-width: 450px;">' +
                                '<h6>{{ Session::get('flashMessage') }}</h6>' +
                                '</div>',
                            type: 'inline'
                        }]
                    });
                });
            </script>
        ');
    @endif

    @if($errors->any())
        HTML::entities('
            <script>
                $(function () {
                    $.magnificPopup.open({
                        items: [{
                            src:'<div class="popup-presentes-confirm">' +
                                '<ul class="modal-errors">' +
                                '{{ implode("", $errors->all("<li>*:message</li>")) }}' +
                                '</ul>' +
                                '</div>',
                            type: 'inline'
                        }]
                    });
                });
            </script>
        ');
    @endif
@stop