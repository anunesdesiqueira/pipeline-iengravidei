<?php

namespace IEngravidei\Repositories;

interface MuralRepositoryInterface
{
    public function createStatus($data);
    public function momDeleteStatus($id_profile, $hash);
    public function momShowStatus($id_profile, $hash);
    public function showFriendsMoms($friends, $number, $offset, $filter = null);
    public function showAllMoms($friends, $number, $offset, $filter = null);
}