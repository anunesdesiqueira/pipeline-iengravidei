{{ Form::open(array('route' => 'search', 'method' => 'GET', 'id' => 'form-search')) }}
    <ul class="faca-busca">
        <li>
            <label>Faça sua busca</label>
            {{ Form::text('q', $terms['q'], array('placeholder' =>'Nome da Mamãe')) }}
            {{ Form::submit('Buscar') }}
        </li>
    </ul>
    <ul class="filtro">
        <li>
            <label>Filtre sua busca por:</label>
            {{ Form::text('father_name', $terms['father_name'], array('placeholder' =>'Nome do Papai')) }}
            <div class="comboHolder">
                <div class="mascara">
                    <div class="optName"></div>
                </div>
                {{ Form::select('state_id', array('' => 'Selecione o estado')+ $states, $terms['state_id'], array('id' => 'state')) }}
            </div>
            {{ Form::submit('Buscar') }}
        </li>
    </ul>
{{ Form::close() }}