<?php

use IEngravidei\Repositories\ProfileRepositoryInterface as Profile;
use IEngravidei\Repositories\SpecialMessageRepositoryInterface as SpecialMessage;

class SpecialMessagesController extends BaseController
{
	public function __construct(Profile $profile, SpecialMessage $specialMessage)
	{
        parent::__construct();

        $this->profile = $profile;
        $this->specialMessage = $specialMessage;
	}

    public function show()
    {
        $this->specialMessage->read($this->user->id);

        return View::make('special_messages.show');
    }

    public function mNotApprovad()
    {
        $mNotApproved = $this->specialMessage->listNotApproved($this->user->id);

        $page = View::make('components.recados_especiais.em_aprovacao', compact('mNotApproved'))->render();
        return Response::json(array('page' => $page));
    }

    public function mApproved()
    {
        $mApproved = $this->specialMessage->listApproved($this->user->id);

        $page = View::make('components.recados_especiais.aprovados', compact('mApproved'))->render();
        return Response::json(array('page' => $page));
    }

    public function send($publicId)
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'specialname'   => 'required',
                'specialemail'  => 'required|email|max:155',
                'specialmsg'    => 'required',
            ), array(
                'specialname.required' => 'O nome não pode estar vazio.',
                'specialemail.required' => 'O email não pode estar vazio.',
                'specialmsg.required' => 'Menssagem não pode estar vazio.'
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $public = $this->profile->find($publicId);
                $data['id'] = $public->id;

                $sendMessage = $this->specialMessage->send($data);

                if($sendMessage)
                    return Response::json(array('status' => 'true'));
            }
        }
    }

    public function deleted()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'hash'   => 'required',
            ), array(
                'hash.required' => 'Recado não encontrado.',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $deleted = $this->specialMessage->deleted($data['hash'], $this->user->id);

                if($deleted)
                    return Response::json(array('status' => 'true'));
            }
        }
    }

    public function approved()
    {
        $data = Input::all();

        if($data) {
            $validation = \Validator::make($data, array(
                'hash'   => 'required',
            ), array(
                'hash.required' => 'Recado não encontrado.',
            ));

            if($validation->fails()) {
                return $validation->messages()->toJson();
            } else {
                $approved = $this->specialMessage->approved($data['hash'], $this->user->id);

                if($approved)
                    return Response::json(array('status' => 'true'));
            }
        }
    }

    public function recadosPublic($publicId)
    {
        $number = Input::get('number');
        $offset = Input::get('offset');

        $profile = $this->profile->find($publicId);
        $recados_especiais = $this->specialMessage->listPublic($profile->id, $number, $offset);

        $page = View::make('components.recados_especiais.recados', compact('recados_especiais'))->render();
        return Response::json(array('page' => $page));
    }
}
