@if(!empty($poll->question))
    <div class="enqueteBox">
        <h3>Enquete</h3>
        <p>{{ $poll->question->question }}</p>
        {{ isset($permissions['voted'])? '' : Form::open(array('id' => 'enquete-form')) }}
            {{ Form::hidden('poll', $poll->question->slug) }}
            <ul>
                @foreach ($poll->answers as $answer)
                <li>
                    <label>{{ $answer->answer }}</label>
                    {{ isset($permissions['voted'])? '' : Form::radio('enqt', $answer->id) }}
                    <div class="barra">
                        <div class="mascara"></div>
                        <div class="porcentagem" style="width:{{ 0 === $poll->total ? 0 : round($answer->total*100/$poll->total) }}%;"></div>
                    </div>
                    <span>{{ 0 === $poll->total ? 0 : round($answer->total*100/$poll->total) }}%</span>
                </li>
                @endforeach
            </ul>
            {{ isset($permissions['voted'])? '' : Form::submit('Vote', array('id' => 'voted-enquete')) }}
        {{ isset($permissions['voted'])? '' : Form::close() }}
    </div>
@else
    <div class="publicidade">
        <h3>Publicidade</h3>
        <div class="banner">
            <img src="{{ asset('assets/img/banner.gif') }}">
        </div>
    </div>
@endif