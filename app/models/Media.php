<?php

class Media extends Eloquent {

    protected $table = 'medias';
    protected $guarded = array('id', 'id');

    public static function registersPictures($foto_name, $album_name)
    {
        $media = Media::create(array('name' => $album_name."/".$foto_name));

        return $media->id;
    }

    public static function excludePictures($id_picture)
    {
        $return = Media::whereRaw('id = ?', array($id_picture))->update(array('is_active' => 0));

        return $return;
    }
}
