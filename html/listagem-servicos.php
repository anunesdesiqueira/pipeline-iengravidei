<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<!-- Só aparece na HOME -->
<section class="home">
	<div class="holder">		
		<h1><span>iengravidei</span></h1>
		<div class="box-cadastro">
			<ul>
				<li class="facebook">Crie um novo login ou <a href="#">Facebook</a></li>
				<li><label>Seu Nome: <input type="text" style="width:370px;" /></label></li>
				<li><label>Nome do Bebê<span>*</span>: <input type="text" style="width:330px;" /></label></li>
				<li><label>E-mail: <input type="text" style="width:399px;" /></label></li>
				<li>
					<label>Senha: <input type="text" style="width:114px;" /></label>
					<label style="padding-left:8px;">Confirmar Senha: <input type="text" style="width:114px;" /></label>
				</li>
				<li><p>* CAMPO OBRIGATÓRIO</p></li>
				<li><input type="submit" value="CADASTRAR" /></li>
			</ul>
		</div>
		<a href="#" class="veja-mais">Veja mais</a>
	</div>
</section>
<!-- End: Só aparece na HOME -->

<section class="content">
	<div class="holder">
	
		<div class="servicos-exclusivos">
			
			<h2>Serviços Exclusivos</h2>
			
			<p>Crie seu perfil e veja tudo que a 1ª Rede Social para Gestantes do Mundo tem para você!</p>
			
			<ul>
				<li>
					<div class="thumb-servicos ico-mural"></div>
					<div class="description">
						<h3 class="t-mural">Meu Mural</h3>
						<p>Aqui é o lugar para postar e guardar para sempre seus momentos especiais. Vídeo do ultrassom, sentimentos do dia-a-dia e ainda pode compartilhar tudo em suas redes sociais. </p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-presentes"></div>
					<div class="description">
						<h3 class="t-presentes">Lista de Presentes</h3>
						<p>Crie sua lista de presentes e receba tudo sem sair do site! Facilite a sua vida e de seus amigos!</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-descontos"></div>
					<div class="description">
						<h3 class="t-descontos">Clube de Descontos</h3>
						<p>Dependendo do seu plano, você tem descontos em lojas parceiras que podem ser muito maiores que o valor da assinatura. Afinal, quem não gosta de economizar?</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-baby"></div>
					<div class="description">
						<h3 class="t-baby">Baby Line</h3>
						<p>Uma linha para postar uma foto por semana e ver o desenvolvimento da barriga. Muito legal!</p>
					</div>
				</li>				
				<li>
					<div class="thumb-servicos ico-profissionais"></div>
					<div class="description">
						<h3 class="t-profissionais">Dicas Profissionais</h3>
						<p>Médicos, terapeutas, decoradoras e muitos outros profissionais irão dar as melhroes dicas para você.</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-enquete"></div>
					<div class="description">
						<h3 class="t-enquete">Enquete</h3>
						<p>Crie enquetes para pedir a opinião dos amigos: Que nome colocar? Que escola indicam? Tudo muito fácil!</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-recados"></div>
					<div class="description">
						<h3 class="t-recados">Recados Especiais</h3>
						<p>Suas amigas, cadastradas ou não, poderão deixar mensagens num só lugar, desejando tudo de bom para você e seu bebê!</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-convites"></div>
					<div class="description">
						<h3 class="t-convites">Personalização de Convites</h3>
						<p>Escolha o fundo de tela  com detalhes para meninos ou meninas ou até escolha uma foto sua! Todos os seus amigos verão a página com o seu jeito.</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-fotos"></div>
					<div class="description">
						<h3 class="t-fotos">Álbum de Fotos</h3>
						<p>Crie álbuns exclusivos para guardar os momentos desses meses inesquecíveis!</p>
					</div>
				</li>
				<li>
					<div class="thumb-servicos ico-eventos"></div>
					<div class="description">
						<h3 class="t-eventos">Criador de Eventos</h3>
						<p>Crie listas de presentes e convites lindos e super fofos para chamar os amigos para o Chá de Bebê, Chá de Fralda, Nascimento ou o que você quiser!</p>
					</div>
				</li>
			</ul>
			
			<div class="cadastre-tambem">
				<h5>Cadastre-se você também.</h5>
				<p>Na 1ª Rede Social para Gestantes do mundo!</p>
				<a href="#">CLIQUE AQUI</a>
			</div>
			
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>