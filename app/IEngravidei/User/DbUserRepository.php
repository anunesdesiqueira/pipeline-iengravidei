<?php namespace IEngravidei\User;

use IEngravidei\Repositories\UserRepositoryInterface;

use IEngravidei\Entities\User;
use IEngravidei\Entities\ProfileActivated;
use IEngravidei\Entities\ProfileFacebook;

class DbUserRepository implements UserRepositoryInterface {

    public function __construct()
    {
        $this->user = new User;
    }

	public $messages = array(
        'password.required' => 'Digite sua senha',
        'password.between' => 'Senha deve possuir no minimo 5 e no máximo 16 caracteres',
        'password.confirmed'    => 'Senhas não conferem',		
		'picture.required' => 'A imagem de perfil é obrigatória',
		'picture.required_if' => 'Você precisa selecionar uma imagem',
		'picture.image' => 'O arquivo enviado não é uma imagem',
		'picture.max' => 'O arquivo deve ser menor que :max',
		'picture.mimes' => 'A imagem de perfil deve ser do tipo JPG',
		'public_id.required' => 'O nome de usuário é obrigatório',
		'public_id.unique' => 'O nome de usuário escolhido já está sendo utilizado',
		'public_id.alpha_num' => 'O nome de usuário deve conter apenas letras e números',
		'public_id.max' => 'O nome do usuário deve conter no máximo :max caracteres',
		'pregnancy_weeks.required' => 'Semanas de gravidez é obrigatório',
		'pregnancy_weeks.numeric' => 'Digite apenas números para informar as semanas de gravidez',
		'country_id.required' => 'O país é obrigatório',
		'country_id.integer' => 'O campo país é inválido',
		'birth.required' => 'A data de nascimento é obrigatória',
		'birth.date_format' => 'O formato da data de nascimento é inválida',
		'father_name.max' => 'O nome do pai deve conter no máximo :max caracteres',
		'layout.required' => 'Escolha um dos layouts para continuar',
		'layout.in' => 'Opção de layout inválida',
	);

    public function find($id)
    {
        return 'Find';
    }

    public function create($input)
    {
        $validator = $this->user->validate($input);

        if($validator->passes())
        {
            /* Create profile */
            $profile = User::create(array(
                            'email' => $input['email'],
                            'name' => $input['name'],
                            'password' => \Hash::make($input['password']),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ));



            print_r($profile); die;

            /*
            $this->user->name = $input['name'];
            $this->user->email = $input['email'];
            $this->user->password = \Hash::make($input['password']);
            $this->user->created_at = date('Y-m-d H:i:s');
            $this->user->updated_at = date('Y-m-d H:i:s');
            $this->user->save();
            */

            /* Create Activated */
            $activated = new ProfileActivated;
            $activated->id_profile =
            $activated->activation_code = $this->user->activation_code = sha1(microtime(). uniqid(rand(), true) . $input['email']);
            $activated->save();

            return true;
        }
        return $validator;
    }

	public function createFromFacebook($uid, $me, $access_token)
	{
		$this->user = $this->user->whereRaw('email = ?', array($me['email']))->first();
		
		if ($this->user)
		{
			$this->user->facebook_uid = $uid;
			$this->user->save();			
		}
		else
		{
			$this->user = new User;
            $this->user->name = $me['name'];
            $this->user->email = $me['email'];
            $this->user->password = \Hash::make(str_random(6));
            $this->user->activation_code = sha1(microtime(). uniqid(rand(), true) . $me['email']);
            $this->user->reset_password_code = sha1(microtime(). uniqid(rand(), true) . $me['email']);
			$this->user->facebook_uid = $uid;
			$this->user->activated = 1;
			$this->user->save();
		}
		
		$facebook = New FacebookProfile;
		$facebook->id = $uid;		
		$facebook->username = $me['username'];
		$facebook->email = $me['email'];	
		$facebook->access_token = $access_token;
		$facebook->save();	
		
		return $this->user;			
	}
	
	public function setActive($activationCode)
	{
		$user = $this->user
			->where('activation_code', '=', $activationCode)
			->where('activated', '=', 0)
			->first();

		if ($user)
		{
			$user->activated = 1;
			$user->save();			
		}

		return $user;
	}
	
    public function completeProfile($user)
    {
		unset($user['_token']);
        $obj = $this->user->find($user['id']);
		$obj->public_id = $user['public_id'];
		$obj->pregnancy_weeks = $user['pregnancy_weeks'];
		$obj->city_id = $user['city_id'];
		$obj->state_id = $user['state_id'];
		$obj->country_id = $user['country_id'];
		$obj->birth = $user['birth'];
		$obj->father_name = $user['father_name'];
        $obj->baby_name = $user['baby_name'];
        $obj->profile_picture = $user['profile_picture'];
		return $obj->save($user);
    }

    public function editProfile($user)
    {
		unset($user['_token']);	
        $obj = $this->user->find($user['id']);
		$obj->pregnancy_weeks = $user['pregnancy_weeks'];		
		$obj->city_id = isset($user['city_id']) ? $user['city_id'] : null;				
		$obj->state_id = isset($user['state_id']) ? $user['state_id'] : null;								
		$obj->country_id = $user['country_id'];								
		$obj->birth = implode("-",array_reverse(explode("/",$user['birth'])));
		$obj->father_name = $user['father_name'];	
		$obj->baby_name = $user['baby_name'];	
		if (isset($user['profile_picture']) && !empty($user['profile_picture']))		
			$obj->profile_picture = $user['profile_picture'];			
		if (isset($user['password']) && !empty($user['password']))
			$obj->password = \Hash::make($user['password']);										
		return $obj->save($user);
    }	

	public function validateProfileEditing($input){
		return \Validator::make($input, array(
			'picture' => 'image|max:200|mimes:jpg,jpeg',
			'pregnancy_weeks' => 'required|numeric',
			'country_id' => 'required|integer',
            'password'  => 'between:5,16|confirmed',			
			'birth' => 'required|date_format:d/m/Y',
			'father_name' => 'max:100'
		), $this->messages);
	}

	public function validateProfileLayoutPersonalizing($input){
		return \Validator::make($input, array(
			'layout' => 'required|in:layout1,layout2,layout3,custom',
			'picture' => 'required_if:layout,custom|image|max:200|mimes:jpg,jpeg'		
		), $this->messages);
	}	

    public function friendships($id)
    {
        $friends = $this->user->find($id)->friends()->get();

		foreach($friends as &$friend)
			$friend = $this->getProfilePictures($friend);
		
		return $friends;
    }

	public function getProfilePictures($obj)
	{
		$ext = '.' . pathinfo($obj->profile_picture, PATHINFO_EXTENSION);
		$name = strstr($obj->profile_picture, $ext, true);
		$path = \Config::get('iengravidei.pictures.profile.public_path');
		$pics['original'] = "{$name}{$ext}";
		foreach(\Config::get('iengravidei.pictures.profile.thumbnails') as $key => $dimensions){
			$pics[$key] = "{$path}{$name}-{$dimensions[0]}x{$dimensions[1]}{$ext}";
		}
		$obj->picture = $pics;		
		return $obj;
	}
	
	public function removeFriendship($id, $friendId)
    {
		\DB::delete('DELETE FROM profile_profile WHERE who_asked = ? AND who_accepted = ?', array($id, $friendId));
		\DB::delete('DELETE FROM profile_profile WHERE who_accepted = ? AND who_asked = ?', array($id, $friendId));
    }	
    
	public function getById($id)
    {
		$user = $this->user->whereRaw('id = ?', array($id))->first();
		$user->birth = implode("/",array_reverse(explode("-",$user->birth)));
		return $user;
    }

	public function getByPublicId($publicId)
    {
       return $this->user->whereRaw('public_id = ?', array($publicId))->first();
    }

	public function getByEmail($email)
    {
        return $this->user->whereRaw('email = ?', array($email))->first();
    }

    public function getByFacebookId($facebookId)
    {
       return $this->user->whereRaw('facebook_uid = ?', array($facebookId))->first();
    }

	public function saveLayout($profileId, $data)
    {
		return \DB::table('profiles')->where('id', $profileId)->update(array(
			'layout' => $data['layout'],
			'layout_image' => $data['layout_image']
		));
    }    

}