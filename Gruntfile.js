module.exports = function( grunt ) {

    grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

        concat: {
            js: {
                src: [
                    'public/assets/js/jquery-1.11.1.js',
                    'public/assets/js/jquery.maskedinput.js',
                    'public/assets/js/jquery.elevatezoom.js',
                    'public/assets/js/jquery.magnific-popup.js',
                    'public/assets/js/jquery.placeholders.js',
                    'public/assets/js/jquery.expander.js',
                    'public/assets/js/jquery.tinyscrollbar.js',
                    'public/assets/js/jquery.tinycarousel.js',
                    'public/assets/js/jquery.youtube.player.js',
                    'public/assets/js/jquery.multi-select.js',
                    'public/assets/js/jquery.confirmon.js',
                    'public/assets/js/jquery.validate.js',
                    'public/assets/js/jquery.validate.additional-methods.js',
                    'public/assets/js/plupload.full.min.js',
                    'public/assets/js/remove.space.strings.js',
                    'public/assets/js/jquery.tooltipster.js',
                    'public/assets/js/all.js'
                ],
                dest: 'public/assets/js/main.js'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            my_target: {
                files: {
                    'public/assets/js/main.js': ['public/assets/js/main.js']
                }
            }
        },
        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        expand: true,
                        cwd: 'public/assets/img/',
                        src: ['**/*.png'],
                        dest: 'public/assets/img/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        expand: true,
                        cwd: 'public/assets/img/',
                        src: ['**/*.jpg'],
                        dest: 'public/assets/img/',
                        ext: '.jpg'
                    }
                ]
            },
            gif: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        expand: true,
                        cwd: 'public/assets/img/',
                        src: ['**/*.gif'],
                        dest: 'public/assets/img/',
                        ext: '.gif'
                    }
                ]
            }
        },
        watch: {
            configFiles: {
                files: [ 'Gruntfile.js'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: [
                    'public/assets/js/all.js'
                ],
                tasks: ['concat','uglify'],
                options: {
                    livereload: true,
                    atBegin: true
                }
            }
        }
	});

	//Load plugins
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    // Register default tasks
    grunt.registerTask('default', ['concat', 'uglify', 'imagemin', 'watch']);
};
