<?php

namespace IEngravidei\Entities;

class ProfilePublished extends \Eloquent
{
    protected $table = 'profiles_published';
    protected $guarded = array();

    public $timestamps = false;
}