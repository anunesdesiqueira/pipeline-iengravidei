<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    /*
     * Retorna todos os amigos
     * */
    public static function getMyFriends($user_id)
    {
        $return = DB::table('profiles_relationships as a')
            ->join('users as b', 'b.id', '=', 'a.users_id')
            ->join('users as c', 'a.users_friend_id', '=', 'c.id')
            ->where('a.users_id', '=', $user_id)
            ->where('a.is_active', '=', 1)
            ->get(array('a.id as id_friendship', 'c.*'));

        return $return;
    }

    /*
     * Retorna os dados de um usuário, passando o hash_user
     * */
    public static function myFriend($hahs_user)
    {
        $return = User::whereRaw('
            hash_user = ?
            and
            activated = 1',
            array($hahs_user)
        )->get();

        return $return;
    }

    /*
     * Verifica se o usuário é amigo
     * */
    public static function areFriends($user_id, $user_friend_id)
    {
        $return = DB::table('profiles_relationships as a')
            ->join('users as b', 'b.id', '=', 'a.users_id')
            ->where('a.users_id', '=', $user_id)
            ->where('a.users_friend_id', '=', $user_friend_id)
            ->where('a.is_active', '=', 1)
            ->get(array('a.is_active as are'));

        return $return;
    }

}