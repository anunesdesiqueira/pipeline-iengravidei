<div class="mensagem-amiga">
    <h3>Nova Mensagem</h3>
    <button title="Fechar (Esc)" type="button" class="close mfp-close">×</button>
    {{ Form::open(array('id' => 'form-mensagem-amiga')) }}
        {{ Form::hidden('public', $publicId) }}
        <label>Escreva sua mensagem:</label>
        {{ Form::textarea('body') }}
        {{ Form::submit('Enviar', array('class' => 'bt_enviar_mensagem')) }}
    {{ Form::close() }}
</div>
