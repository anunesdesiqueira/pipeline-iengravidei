<?php namespace Bg7\Notifiqueme\Repositories;


/**
 * Class NotifiquemeRepository
 * @package Bg7\Notifiqueme\Repositories
 */
interface NotificationRepository
{

    public function push(array $notificationData);
    public function pull($profileId);
    public function numberOfNotifications($profileId);

}