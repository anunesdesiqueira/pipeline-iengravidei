@extends('layouts.master')

@section('content')
    <div class="holder">
        @include('components.cabecalho.header_active')
        <div class="enquete">
            @include('components.poll.add')
        </div>
    </div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/enquete-logged.js') }}
    {{ HTML::script('assets/js/enquetes.js') }}
@stop