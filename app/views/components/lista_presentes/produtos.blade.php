<ul class="itens-main">
    @foreach($gifts as $gift)
    <li>
        <div class="thumb">
            <img src="{{ $gift->picture['main'] }}" />
        </div>
        <p>{{ $gift->description }}</p>
        <span>R$ {{ str_replace('.', ',', $gift->price) }}</span>
        {{ Form::button('adicionar',  array('rel' => $gift->id, 'class' => 'bt-itens-medium')) }}
    </li>
    @endforeach
</ul>
{{ $gifts->appends($gifts->faixa)->links() }}