@if(count($friends))
    <ul class="listar">
    @foreach($friends as $friend)
        <li rel="{{ $friend['public_id'] }}">
            <div class="foto">
                <div class="thumb">
                    <a href="{{ route('user.public', array('publicId' => $friend['public_id'])) }}"><img src="{{ $friend['profile_picture']['small'] }}" /></a>
                </div>
                <a href="#" class="excluir">Excluir</a>
            </div>
            <div class="description">
                <h3><a href="{{ route('user.public', array('publicId' => $friend['public_id'])) }}">{{ $friend['name'] }}</a></h3>
                <a href="{{ $friend['slug'] }}" class="msg">Enviar mensagem</a>
            </div>
        </li>
    @endforeach
    </ul>
    {{ $friends->links() }}
@else
    <p>Você ainda não possui nenhuma amiga</p>
@endif