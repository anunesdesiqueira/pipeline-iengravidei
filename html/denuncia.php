<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if IE 9]><html class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>IEngravidei</title>
	<meta name="description" content="">	
	<link rel="stylesheet" href="css/main.css">	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<div class="background"></div>

<?php include 'inc/login.php'; ?>

<header>
	<div class="search">
		<input type="text" placeholder="Procure pela Mamãe:" />
		<input type="submit" />
	</div>
	<nav>
		<ul>
			<li class="home active">
				<div class="marcacao"></div>
				<a href="#">Home</a>
			</li>
			<li class="sobre">
				<div class="marcacao"></div>
				<a href="#">Sobre Nós</a>
			</li>
			<li class="planos">
				<div class="marcacao"></div>
				<a href="#">Planos</a>
			</li>
			<li class="servicos">
				<div class="marcacao"></div>
				<a href="#">Serviços</a>
			</li>
			<li class="contato">
				<div class="marcacao"></div>
				<a href="#">Contato</a>
			</li>
		</ul>
	</nav>	
</header>

<!-- Só aparece na HOME -->
<section class="home">
	<div class="holder">		
		<h1><span>iengravidei</span></h1>
		<div class="box-cadastro">
			<ul>
				<li class="facebook">Crie um novo login ou <a href="#">Facebook</a></li>
				<li><label>Seu Nome: <input type="text" style="width:370px;" /></label></li>
				<li><label>Nome do Bebê<span>*</span>: <input type="text" style="width:330px;" /></label></li>
				<li><label>E-mail: <input type="text" style="width:399px;" /></label></li>
				<li>
					<label>Senha: <input type="text" style="width:114px;" /></label>
					<label style="padding-left:8px;">Confirmar Senha: <input type="text" style="width:114px;" /></label>
				</li>
				<li><p>* CAMPO OBRIGATÓRIO</p></li>
				<li><input type="submit" value="CADASTRAR" /></li>
			</ul>
		</div>
		<a href="#" class="veja-mais">Veja mais</a>
	</div>
</section>
<!-- End: Só aparece na HOME -->

<section class="content">
	<div class="holder denuncia">
		<h2>Faça sua Denúncia</h2>
		
		<div class="box">
			<h3>Gostaria de se identificar ao fazer essa denúncia?</h3>
			<ul>
				<li><label><input type="radio" /> Não. Quero fazer essa denúncia anonimamente.</label></li>
				<li><label><input type="radio" /> Sim. Por favor, pegue meu login já cadastrado.</label></li>
			</ul>
			
			<h3>Que tipo de denúncia você quer fazer?</h3>
			<ul>
				<li><label><input type="radio" />Pedofilia e pornografia infantil</label></li>
				<li><label><input type="radio" />Exploração sexual</label></li>
				<li><label><input type="radio" />Apologia e incitação ao crime</label></li>
				<li><label><input type="radio" />Neonazismo</label></li>
				<li><label><input type="radio" />Apologia e incitação a práticas cruéis contra animais</label></li>
				<li><label><input type="radio" />Calúnia, difamação, injúria e crimes contra a honra</label></li>
				<li><label><input type="radio" />Direitos autorais</label></li>
				<li><label><input type="radio" />Falsa identidade</label></li>
				<li><label><input type="radio" />Pornografia</label></li>
				<li><label><input type="radio" />Vírus e Spam</label></li>
				<li><label><input type="radio" />Racismo, xenofobia e intolerância sexual ou religiosa</label></li>
				<li><label><input type="radio" />Outros</label></li>
			</ul>
		</div>
		
		<div class="box">
			<h3>E-mail para contato:</h3>
			<input type="text" />		
			<h3>Que tipo de conteúdo foi postado?</h3>
			<ul>
				<li><label><input type="radio" />Áudio</label></li>
				<li><label><input type="radio" />Imagens</label></li>
				<li><label><input type="radio" />Url/Sites Externos</label></li>
				<li><label><input type="radio" />Comentários/Textos/Conversa</label></li>
				<li><label><input type="radio" />Vídeos</label></li>
				<li><label><input type="radio" />Perfil</label></li>
				<li><label><input type="radio" />Outros</label></li>
			</ul>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="box">
			
			<h3>Envie uma imagem do conteúdo abusivo (campo de upload limitado a 300Kb)</h3>
			<div class="upload">
				<input type="file" />
				<div class="btn-fake">Upload de foto</div>
			</div>
			<div class="fileName"></div>
			
			<div class="clearfix"></div>
			
			<h3 class="mgTop40px">Caso necessário deixe aqui outros comentários</h3>
			<textarea></textarea>
			
			<div class="clearfix"></div>
			
			<input type="submit" value="Enviar Denúncia" />
		</div>
		
	</div>
</section>

<?php include 'inc/footer.php'; ?>

<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>