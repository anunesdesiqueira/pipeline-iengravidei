<?php

use Illuminate\Database\Migrations\Migration;

class CreteEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//

        Schema::create('events', function($table)
        {
            $table->bigIncrements('id');
            $table->integer('giftlist_id')->default(0);
            $table->integer('template_id')->default(0);
            $table->string('hash', 60);
            $table->string('name');
            $table->timestamp('when')->nullable();
            $table->string('where');

        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('events');
	}

}